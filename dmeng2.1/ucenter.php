<?php
get_header();
get_header( 'masthead' ); ?>
<div id="main" class="container" role="main">
  <div class="row">
    <article id="content">

    <div id="ucenter_tips"></div>

    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-3" id="ucenter_nav"></div>
      <div class="col-lg-10 col-md-10 col-sm-9" id="ucenter_page">
        <div id="ucenter_page_content"></div>
        <div class="text-left" id="ucenter_loading" style="display:none">
          <div class="loading-indicator"><?php _e( '正在加载，请稍后…', 'dmeng' );?></div>
        </div>
      </div>
    </div>
<?php

  $dmeng_Security = dmeng_Security();

  if ($dmeng_Security->maybe_auto_RSA()) 
    echo $dmeng_Security->RSA_public_key(true);

?>
<script>

!function ($) {
  'use strict';

  var $document = $(document),
    $content = $('#content'),
    $tips = $('#ucenter_tips'),
    $nav = $('#ucenter_nav'),
    $page = $('#ucenter_page'),
    $page_content = $('#ucenter_page_content'),
    $loading = $('#ucenter_loading'),
    current_id = dmeng.ucenter.current ? dmeng.ucenter.current : 'home',
    ucenterLoading = false,
    paged = 1;

  $(document).ready(function () {
    dmeng.readyAjax.done(function (data) {
        
        if (data.userdata) {

          if (data.userdata.ID) {
            dmeng.user.ID = data.userdata.ID;
            if (data.userdata.user_info) {
              var user_info = '';
              $.each(data.userdata.user_info, function (key, value) {
                user_info += '<li><a href="'+dmeng.ucenter.url_format.replace( '%s', key )+'" data-page="'+key+'" class="'+key+( key==current_id ? ' active' : '')+'">'+dmeng.ucenter.text[key]+'('+value + ')</a></li>';
              });
            }

            $nav.html( '<img class="avatar ucenter_avatar" width="80" height="80" src="'+data.userdata.avatar+'" /><ul id="ucenter_menu"><li><a href="'+dmeng.ucenter.url_format.replace( '%s', 'home' )+'" data-page="home" class="h3 home'+( 'home'==current_id ? ' active' : '')+'">'+data.userdata.display_name+'</a></li>'+user_info+'</ul><a class="logout_url" href="'+dmeng.logouturl()+'">&laquo; '+dmeng.ucenter.text.logout+'</a>' );
            
            $page_content.html( $loading.clone().removeAttr('id').fadeIn(400, function () {
              dmeng.getUcenterPage();
            }) );

          } else {
            
            $tips.html( '<?php _e( '您还没登录，请先登录。', 'dmeng' );?><a href="'+dmeng.loginurl()+'"><?php _e( '点击登录 »', 'dmeng' );?></a>' );

          }
        }
        
    });
  });

  dmeng.getUcenterPage = function (page_id) {
    ucenterLoading = true;
    page_id = page_id ? page_id : current_id;
    $.ajax({
      type: 'POST', 
      url: dmeng.ajaxurl, 
      data: {
        action: 'dmeng_ucenter_page',
        page: page_id,
        paged: paged,
        _wpnonce: dmeng._wpnonce
      }, 
      complete: function (response) {
        $page_content.html( response.responseText );
        history.pushState(null, '', dmeng.ucenter.url_format.replace( '%s', page_id ));
        ucenterLoading = false;
      }
    });
  }
  
  $document.on('click', '#ucenter_menu li a',function () {
    if (ucenterLoading) {
      return false;
    }
    $page_content.html( $loading.clone().removeAttr('id').fadeIn() );
    paged = 1;
    current_id = $(this).data('page');
    $(this).parents('ul').find('a').removeClass('active');
    $(this).addClass('active');
    dmeng.getUcenterPage();
    return false;
  });
  
  $document.on('change', '#paginate', function () {
    if (ucenterLoading) {
      return false;
    }
    $page_content.html( $loading.clone().removeAttr('id').fadeIn() );
    paged = $(this).find(":selected").val();
    dmeng.getUcenterPage();
    return false;
  });

  $document.on('click', '#get_next, #get_prev, #get_news, #get_message', function () {
    if (ucenterLoading) {
      return false;
    }
    $page_content.html( $loading.clone().removeAttr('id').fadeIn() );
    paged = parseInt(paged);
    switch ($(this).attr('id')) {
      case 'get_news' : 
        paged = 'news';
        break;
      case 'get_next' : 
        paged++;
        break;
      case 'get_prev' : 
        paged--;
        break;
      default : 
        paged = 1;
    }
    dmeng.getUcenterPage();
    return false;
  });
  
  $document.on('click', '#more_form-toggle', function () {
    $('#more_form').fadeToggle();
    return false;
  });

  <?php
    if ($dmeng_Security->maybe_auto_RSA()) {
  ?>
  $.getScript('<?php echo dmeng_jsencrypt_uri();?>', function(){
    var public_key = $('#RSA_PUBLIC_KEY').val(),
        crypt = new JSEncrypt();
    crypt.setKey(public_key);
    window.RSAencrypt = function(text) {
      var pwd = crypt.encrypt('<?php echo $dmeng_Security->RSA_text_prefix();?>'+text);
      if (pwd) {
        text = '<?php echo $dmeng_Security->RSA_text_prefix();?>'+pwd;
      }
      return text;
    };
  });
  <?php
    }
  ?>


  $document.on('submit', '#profile_form, #email_form, #pass_form', function () {
    
    var s = $(this).find('[type=submit]');
    s.button('loading');
    
    if (typeof RSAencrypt==='function') {
      var fields = $(this).serializeArray();
      $.each(fields, function(i, field) {
        if (field.value && ('user_email'==field.name || 'pass1'==field.name || 'pass2'==field.name)) {
          field.value = RSAencrypt(field.value);
          fields[i] = field;
        }
      });
      fields = $.param(fields);
    } else {
      var fields = $(this).serialize();
    }

    $.ajax({
      type: 'POST', 
      url: dmeng.ajaxurl, 
      data: {
        action: 'dmeng_'+$(this).attr('id'),
        formdata: fields,
        _wpnonce: dmeng._wpnonce
      },
      complete: function (e) {
        s.prev('p').html('<p class="text-danger">'+e.responseText+'</p>');
        s.button('reset');
      }
    });
    
    return false;
  });
  
  $document.on('click', '#destroy_other_sessions', function () {
    if ($(this).hasClass('disabled')) {
      return;
    }
    var s = $(this).button('loading');
    $.ajax({ 
      type: 'POST', 
      url: dmeng.ajaxurl, 
      data: {
        action: 'dmeng_destroy_other_sessions'
      },
      complete: function (e) {
        s.html(e.responseText);
        dmeng.getUcenterPage();
      }
    });
    return false;
  });
  
}(jQuery);
</script>

    </article>
  </div>
</div><!-- #main -->
<?php get_footer(); ?>
