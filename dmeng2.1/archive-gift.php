<?php
get_header();
get_header( 'masthead' ); ?>
<div id="main" class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
  <div class="row">
    <article id="content" class="col-lg-12 col-md-12 archive archive-gift" role="article" itemscope="" itemtype="http://schema.org/Article">
      <header class="entry-header">
        <h1 class="entry-title hidden"><?php _e( '积分换礼', 'dmeng' );  ?></h1>
        <div class="entry-filter">
<?php
$t = ( get_query_var( 't' ) ) ? intval(get_query_var( 't' )) : null;
$max = ( get_query_var( 'max' ) ) ? intval(get_query_var( 'max' )) : null;
$min = ( get_query_var( 'min' ) ) ? intval(get_query_var( 'min' )) : null;

$gift_archive_link = add_query_arg( array( 't'=>$t, 'max'=>$max, 'min'=>$min ), get_post_type_archive_link( 'gift' ) );

$filter_output = '';						
$terms = get_terms('gift_tag');
if ( !empty( $terms ) && !is_wp_error( $terms ) ) {

	$filter_output .= '<ul class="clearfix">';
		$filter_output .= '<li class="title">'.__('礼品分类', 'dmeng').'</li>';
		$filter_tags[] = array(
			'title' => __('全部', 'dmeng'),
			'url' => remove_query_arg(array('t'), $gift_archive_link),
			'id' => 0
		);
		foreach ( $terms as $term ) {
			$filter_tags[] = array(
				'title' => $term->name,
				'url' => add_query_arg('t', $term->term_id, $gift_archive_link),
				'id' => $term->term_id
			);
			$terms_id[] = $term->term_id;
		}
		foreach( $filter_tags as $filter_tag ){
			$filter_id = in_array($filter_tag['id'], $terms_id) ? $filter_tag['id'] : 0;
			$filter_output .= sprintf('<li><a href="%s"%s>%s</a></li>', $filter_tag['url'], ($filter_id==$t ? ' class="active"' : ''), $filter_tag['title'] );
		}
		$filter_output .= '</ul>';

}

echo $filter_output;
?>
        </div>
        
        <div class="entry-meta"><?php
            global  $wp_query, $paged;
            printf( ' [ 共找到 %1$s 件礼品，当前显示第 %2$s 页 ] ', dmeng_count_num($wp_query->found_posts), '<i class="num">'.max( $paged, 1 ).'</i>' );
            if ($t)
              echo strip_tags(term_description( $t, 'gift_tag' ), '<a>');
          ?></div>
      </header>
      <div class="entry-content clearfix" itemprop="articleBody">

<?php 
          while ( have_posts() ) : the_post();
?>
<article <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/Article">
  <div class="single-gift">
  <?php echo dmeng_thumbnail(get_template_directory_uri().'/images/blank-220x146.png'); ?>
  <header class="entry-header">
    <h3 class="entry-title" itemprop="name"><?php 
    $title = get_the_title();
    echo apply_filters( 'dmeng_the_title', '<a href="'.get_permalink().'" rel="bookmark" itemprop="url"><span itemprop="name">'.( empty($title) ? get_post_field( 'post_name', get_the_ID() ) : esc_html(get_the_title()) ).'</span></a>' ); ?></h3>
    <div class="entry-meta">
      <p><?php
						$credit = intval(get_post_meta( get_the_ID(), 'dmeng_gift_credit', true ));
						printf('<span class="credit">%s</span>', ( $credit ? $credit.' '.__('积分', 'dmeng') : '<em>'.__('免费领取', 'dmeng').'</em>') . ( $post->post_status=='future' ? sprintf('<span class="future"> %s </span>', __('[兑换还没开始]', 'dmeng') ) : '') );
        ?></p>
        <?php
            $buyers = (array)json_decode(get_post_meta( get_the_ID(), 'dmeng_gift_buyers', true ));
            echo '<span class="exchange">' . sprintf( __( '已兑换%s次', 'dmeng' ), count($buyers) ) . '</span>';
?>
    </div>
  </header>
  </div>
</article>
<?php
          endwhile;
        ?> 

      </div>
      <footer class="entry-footer">
        <?php dmeng_paginate();?>
      </footer>
    </article>
  </div>
 </div><!-- #main -->
<?php get_footer('colophon'); ?>
<?php get_footer(); ?>
