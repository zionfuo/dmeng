<header id="masthead" itemscope itemtype="http://schema.org/WPHeader">
  <div class="container header-content">
    <div class="row">
      <div class="col-md-6 col-xs-6">
        <div class="custom-header clearfix">
          <?php
            if ( get_header_image() ) {
              $custom_header = get_custom_header();

              $logo_data = array();
              $logo_data['url'] = $custom_header->url ? $custom_header->url : get_theme_support( 'custom-header', 'default-image');
              $logo_data['width'] = $custom_header->width ? $custom_header->width : get_theme_support( 'custom-header', 'width');
              $logo_data['height'] = $custom_header->height ? $custom_header->height : get_theme_support( 'custom-header', 'height');

              printf(
                  '<div class="header-logo"><a href="%4$s" rel="home"><img src="%1$s" width="%2$s" height="%3$s" alt="%5$s" /></a></div>',
                  $logo_data['url'],
                  $logo_data['width'],
                  $logo_data['height'],
                  esc_url(home_url('/')),
                  get_bloginfo('name')
                );
            }
          ?>
          <div class="header-text">
            <div class="name">
              <a href="<?php echo esc_url(home_url('/'));?>" rel="home" id="name"><?php bloginfo('name');?></a>
            </div>
            <div class="description" id="desc"><?php bloginfo('description');?></div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-xs-6">
        <div class="clearfix" style="letter-spacing:1px;margin:25px 15px 10px;">
          <div style="float:left">
            <img src="http://s.dmeng.net/imagesCoding-48.png" width="44" height="44" style="margin:0 15px 0 0" alt="" />
          </div>
          <div style="line-height:20px">2.1 coming soon…</div>
          <div class="small"><a href="http://www.dmeng.net/dmeng-theme-2-1.html">提建议</a> / <a href="http://www.dmeng.net/for-dmeng-2-1.html">参加众筹</a></div>
        </div>
      </div>
    </div>
  </div>
  <div class="navbar navbar-default navbar-static-top" role="banner">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".header-navbar-collapse"><span class="sr-only"><?php _e('切换菜单','dmeng');?></span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
      </div>
      <nav id="navbar" class="collapse navbar-collapse header-navbar-collapse" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <?php

        if ( has_nav_menu( 'header_menu' ) ) {
          wp_nav_menu( array(
            'menu'              => 'header_menu',
            'theme_location'    => 'header_menu',
            'depth'             => 0,
            'container'         => '',
            'container_class'   => '',
            'menu_class'        => 'nav navbar-nav',
            'items_wrap'     => '<ul class="%2$s">%3$s</ul>',
            'walker'            => new dmeng_Bootstrap_Menu()
          )  );
        }

        // 右侧菜单
        if ( has_nav_menu( 'header_right_menu' ) ) {
          wp_nav_menu( array(
            'menu'              => 'header_right_menu',
            'theme_location'    => 'header_right_menu',
            'depth'             => 0,
            'container'         => '',
            'container_class'   => '',
            'menu_class'        => 'nav navbar-nav navbar-right',
            'items_wrap'     => '<ul class="%2$s">%3$s</ul>',
            'walker'            => new dmeng_Bootstrap_Menu()
          )  );
        }
      ?>
      </nav><!-- #navbar -->
    </div>
  </div>
</header><!-- #masthead -->