<footer id="colophon" class="container" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
  <div class="<?php echo apply_filters('dmeng_footer_widget_panel_class', 'panel panel-default text-muted');?>">
    <div class="panel-body"><?php
    
/**
 * 底部边栏
 */
if( is_active_sidebar( 'sidebar-2' ) ){
    dynamic_sidebar( 'sidebar-2' );
}

      if ( has_nav_menu( 'link_menu' ) ) {
        wp_nav_menu( array(
          'menu'              => 'link_menu',
          'theme_location'    => 'link_menu',
          'depth'             => -1,
          'container'         => '',
          'container_class'   => '',
          'menu_id'        => 'link_menu',
          'menu_class'        => 'breadcrumb',
          'items_wrap'     => '<ul id="%1$s" class="%2$s"><li class="active"><span class="glyphicon glyphicon-list-alt"></span> '.__('链接','dmeng').'</li> %3$s</ul>',
          'walker'            => new dmeng_Bootstrap_Menu()
        )  );
      }
?>
    </div>
    <div class="panel-footer clearfix">
      <?php
      $output = sprintf(
        '&copy; %s <a href="%s">%s</a> ',
        date( 'Y', current_time( 'timestamp', 0 ) ),
        home_url('/'),
        get_bloginfo('name')
       );
       $icp = get_option('zh_cn_l10n_icp_num', '');
       $output .=__('版权所有','dmeng').' '.( $icp ? '<a href="http://www.miitbeian.gov.cn/" rel="nofollow" target="_blank" class="text-muted">'.$icp.'</a>' : '' );
       $output .= '<span class="pull-right copyright">'.
        apply_filters('dmeng_footer_copyright', 
          sprintf(
            __('<a target="_blank" href="%1$s">WordPress主题</a> 源自 <a target="_blank" rel="friend" href="%2$s">多梦</a> 创作','dmeng'),
            'http://www.dmeng.net/#dmeng2.1CR', 
            'http://duomeng.me/#dmeng2.1CR'
          )
        ).'</span>';
       echo $output;
      ?>
    </div>
  </div>
</footer>
<?php
if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
  ?><p id="debug-notice" class="num text-center small" style="color:#999">当前处于调试模式，本页执行结果：共 <?php echo get_num_queries(); ?> 次查询，花费了 <?php timer_stop(3); ?> 秒 </p><?php
}
?>
  <div id="float-nav">
    <ul id="float-nav-menu" style="display:none">
      <li id="pageShare" title="<?php _e('分享', 'dmeng');?>">
        <span class="glyphicon glyphicon-share"></span>
        <div id="shareBox" class="floatBoxWrapper">
          <div class="floatBox">
            <div id="shareNotice" data-friend-text="<?php _e('已改为您的推广链接', 'dmeng');?>"><?php _e('本页二维码', 'dmeng');?></div>
            <img id="shareQrcode" class="ldbg" width="130" height="130" alt="<?php _e('本页二维码', 'dmeng');?>" data-api="//open.dmeng.net/qrcode.png?text=" />
            <a href="javascript:;" id="sharePrompt" data-notice="<?php _e('快捷键：同时按下 Ctrl 和 C 然后按下 Enter 即可', 'dmeng');?>"><?php _e('点击复制网址', 'dmeng');?></a>
          </div>
        </div>
      </li>
      <?php
        if ( is_singular() && comments_open() ) {
          ?>
      <li id="goComments" title="<?php _e( '评论', 'dmeng' );?>"><span class="glyphicon glyphicon-comment"></span></li>
      <?php
      }
      ?>
      <li id="goTop" style="display:none" title="<?php _e( '返回顶部', 'dmeng' );?>">
        <span class="glyphicon glyphicon-chevron-up"></span>
<?php

    if (is_singular()) {

      $content = get_the_content();
      $matches = array();  
      $index_li = $ol = $depth_num = '';
      if(preg_match_all("/<h([2-6]).*?\>(.*?)<\/h[2-6]>/is", $content, $matches)) {

        //~ $matches[0] 是原标题，包括标签，如<h2>标题</h2>
        //~ $matches[1] 是标题层级，如<h2>就是“2”
        //~ $matches[2] 是标题内容，如<h2>标题</h2>就是“标题”
        
        foreach ($matches[1] as $key=>$level) {

          if( $ol && intval($ol)<$level){
            $index_li .= '<ul>';
            $depth_num = intval($depth_num)+1;
          }

          if( $ol && intval($ol)>$level ){
            $index_li .= '</li>'.str_repeat('</ul></li>', intval($depth_num));
            $depth_num = 0;
          }

          $hash_key = 'title-' . ($key+1);

          if ( $ol && intval($ol)==$level)
            $index_li .= '</li>';
          $index_li .= '<li><a href="#'.$hash_key.'">'.$matches[2][$key].'</a>';

          if (($key+1)==count($matches[1]))
            $index_li .= '</li>'.str_repeat('</ul></li>', intval($depth_num));

          $ol = $level;
        }
        if ($index_li)
          echo '<div class="floatBoxWrapper"><div class="floatBox"><div id="nav_index" data-toggle="hash"><h5>'.__('文章目录','dmeng').'</h5><ul>' . $index_li . '</ul></div></div></div>';
      }
    }
?>
      </li>
    </ul>
  </div>