<?php if ( ( have_comments() || comments_open() ) && post_type_supports( get_post_type(), 'comments' ) ) { ?>
  <div class="text-center" id="comments-loading">
    <div class="loading-indicator"><?php _e( '评论加载中', 'dmeng' );?></div>
  </div>
<?php } ?>
