<?php
/*
Template Name: 只显示内容
*/
get_header();
get_header( 'masthead' ); ?>
<div id="main" class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
  <div class="row">
    <?php 
      while ( have_posts() ) : the_post();
    ?>
      <article id="content" <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/Article">
        <div class="entry-content"  itemprop="articleBody" >
          <?php 
          the_content(); 
          ?>
        </div>
      </article>
    <?php
      endwhile;
    ?>
    <?php get_sidebar();?>
  </div>
 </div><!-- #main -->
<?php get_footer('colophon'); ?>
<?php get_footer(); ?>
