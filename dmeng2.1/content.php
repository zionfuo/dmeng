<article id="content" <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/Article">
  <header class="entry-header">
    <div class="entry-location"><?php echo dmeng_breadcrumb_html();?></div>
    <h1 class="entry-title" itemprop="name"><?php echo apply_filters( 'dmeng_the_title', esc_html(get_the_title()) );?><?php if( is_preview() || current_user_can('edit_post', get_the_ID()) ) echo ' <small><a href="'.get_edit_post_link().'">'.__('Edit This').'</a></small>'; ?></h1>
    <?php dmeng_post_meta(); ?>
  </header>
  <div class="entry-content"  itemprop="articleBody" >
    <?php the_content();?>
    <?php dmeng_post_page_nav(); ?>
  </div>
  <footer class="entry-footer">
    <?php dmeng_post_copyright(get_the_ID());?>
    <?php dmeng_vote_html( get_the_ID(), 'post');?>
<?php

// 百度分享HTML
$bdshare = '<div class="text-center"><div class="bdsharebuttonbox" style="display:inline-block">';
$bdshare_list = array(
  'more' => '',
  'qzone' => '分享到QQ空间',
  'tsina' => '分享到新浪微博',
  'tqq' => '分享到腾讯微博',
  'douban' => '分享到豆瓣网',
  'renren' => '分享到人人网',
  'tieba' => '分享到百度贴吧',
  'weixin' => '分享到微信'
);
foreach ($bdshare_list as $share_key=>$share_title) {
  $bdshare .= '<a href="#" class="bds_'.$share_key.'" data-cmd="'.$share_key.'" title="'.$share_title.'"></a>';
}
$bdshare .= '</div></div>';

// 百度分享参数
global $post;
$post_excerpt = $post->post_excerpt ? $post->post_excerpt : $post->post_content;
$post_excerpt = str_replace(array("\t", "\r\n", "\r", "\n"), "", strip_tags($post_excerpt)); 
$bdshare .= '<script>';
$bdshare .= "window._bd_share_main = false; window._bd_share_config = { common : { bdText : '".addslashes(sprintf(__('分享文章《%s》', 'dmeng'), esc_html(get_the_title())) )."', bdDesc : '".addslashes(wp_trim_words($post_excerpt, 120, '').'...' )."', bdUrl : '".add_query_arg('fid', get_current_user_id(), get_permalink())."', bdPic : '".dmeng_get_the_thumbnail()."', bdSnsKey : {'tsina':'".get_option('dmeng_open_weibo_key', '')."', 'tqq':'".get_option('dmeng_open_qq_id', '')."','qzone':'".get_option('dmeng_open_qq_id', '')."'} }, share : [{ 'bdStyle' : 1, 'bdSize' : 32 }] };with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];";
$bdshare .= '</script>';
      
echo $bdshare;

?>
  </footer>
  <?php
    echo dmeng_adsense('single','comment');
  ?>
  <div id="comments"><?php comments_template(); ?></div>
</article>
