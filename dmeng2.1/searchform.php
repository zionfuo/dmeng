<form class="input-group" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
  <span class="input-group-addon"><?php _e( '搜索','dmeng' );?></span>
  <input type="text" class="form-control" placeholder="<?php _e( '请输入检索关键词…', 'dmeng' );?>" name="s" id="s" required>
  <span class="input-group-btn"><button type="submit" class="btn btn-default" id="searchsubmit"><span class="glyphicon glyphicon-search"></span></button></span>
</form>