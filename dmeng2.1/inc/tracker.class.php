<?php

/**
 * 流量统计
 */

class dmeng_Tracker {
  
  public function __construct() {
    
    add_action( 'load-themes.php', array( $this, 'install' ) );
    
    add_action( 'admin_init', array( $this, 'delete_traffic_init' ) );

    add_filter( 'dmeng_ready_ajax', array( $this, 'ajax_action' ) );
    
    add_filter( 'dmeng_scripts_args', array( $this, 'vars_output' ) );

  }
  
  /**
   * 数据库表格名称
   */
  public $table_name = 'dmeng_tracker';   

  /**
   * 启用主题时创建数据库表格
   * https://codex.wordpress.org/Plugin_API/Action_Reference/load-themes.php
   *
   * ALTER TABLE `wp_dmeng_tracker` ADD `count` BIGINT(20) NOT NULL DEFAULT '0' AFTER `traffic`;
   */
  public function install() {
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    dmeng_create_db_table(
      $this->table_name,
      " CREATE TABLE `$this->table_name` (
        `ID` int NOT NULL AUTO_INCREMENT, 
        `type` varchar(20),
        `pid` tinytext,
        `traffic` int,
        PRIMARY KEY(type),
        KEY pid (pid(191))
      ) $charset_collate;"
    );
  }
  
  /**
   * 获取数据
   */
  public function get( $type='' ) {

    $type = sanitize_text_field($type);

    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;   
    
    if ( $type && in_array($type , array('single', 'attachment', 'cat', 'tag', 'search', 'author') ) )
      $sql = "SELECT sum(traffic) FROM $table_name WHERE type='$type'";
    else
      $sql = "SELECT sum(traffic) FROM $table_name";
    
    $check = $wpdb->get_var( $sql );

    return isset($check) ? absint($check) : 0;

  }
   
  public function get_var( $type, $pid ) {

    $type = sanitize_text_field($type);
    $pid = sanitize_text_field($pid);
    $cache_key = $type . '-' . $pid;

    $cache = wp_cache_get( $cache_key, $this->table_name );
    if ( false !== $cache )
      return $cache;

    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;   
    
    $check = $wpdb->get_var( "SELECT traffic FROM $table_name WHERE type='$type' AND pid='$pid'" );

    $result = isset($check) ? absint($check) : 0;
    
    wp_cache_set( $cache_key, $result, $this->table_name );

    return $result;
  }
  
  /**
   * 更新数据
   */
  public function update( $type, $pid ) {

    $type = sanitize_text_field($type);
    $pid = sanitize_text_field($pid);
    $cache_key = $type . '-' . $pid;
    
    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name; 
    
    $check = $wpdb->get_var( "SELECT traffic FROM $table_name WHERE type='$type' AND pid='$pid'" );

    if ( isset($check) ) {
      /**
       * 更新字段并更新缓存
       */
      $traffic = (int)$check+1;
      if (
        $wpdb->update(
          $table_name, 
          array( 
            'traffic' => $traffic
          ), 
          array(
            'type' => $type,
            'pid' => $pid
          )
        )
      ) {
        wp_cache_replace( $cache_key, $traffic, $this->table_name );
      }
    } else {
      /**
       * 插入字段并添加缓存
       */
      $traffic = 1;
      if (
        $wpdb->insert(
          $table_name, 
          array( 
            'type' => $type,
            'pid' => $pid,
            'traffic' => $traffic
          )
        )
      ) {
        wp_cache_set( $cache_key, $traffic, $this->table_name );
      }
    }

    return $traffic;
  }
  
  /**
   * 删除数据
   */
   
  public function delete( $type , $pid ) {

    $type = sanitize_text_field($type);
    $pid = sanitize_text_field($pid);
    $cache_key = $type . '-' . $pid;
    
    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name; 

    if ( $wpdb->get_var( "SELECT traffic FROM $table_name WHERE type='$type' AND pid='$pid'" ) ) {
      if (
        $wpdb->delete( $table_name, array( 'type' => $type, 'pid' => $pid ) )
      ) {
        wp_cache_delete( $cache_key, $this->table_name );
        return true;
      }
    }
    
    return false;
  }

  public function delete_traffic_post( $pid ) {
    $this->delete( 'single', $pid );
  }

  public function delete_traffic_category( $pid ) {
    $this->delete( 'cat', $pid );
  }

  public function delete_traffic_tag( $pid ) {
    $this->delete( 'tag', $pid );
  }

  public function delete_traffic_user( $pid ) {
    $this->delete( 'author', $pid);
  }

  public function delete_traffic_attachment( $pid ) {
    $this->delete( 'attachment', $pid );
  }

  public function delete_traffic_term( $term, $tt_id, $taxonomy ) {
    $this->delete( 'tax_'.$taxonomy, $term );
  }

  public function delete_traffic_init() {
    add_action( 'delete_post', array( $this, 'delete_traffic_post' ), 10 );
    add_action( 'delete_category', array( $this, 'delete_traffic_category' ), 10);
    add_action( 'delete_post_tag', array( $this, 'delete_traffic_tag' ), 10);
    add_action( 'delete_user', array( $this, 'delete_traffic_user' ), 10);
    add_action( 'delete_attachment', array( $this, 'delete_traffic_attachment' ), 10);
    add_action( 'delete_term', array( $this, 'delete_traffic_term' ), 10, 3);
  }
  
  /**
   * 统计代码参数
   * @param $tracker['type'] 页面类型，如 home / search，默认是unknown
   * @param $tracker['pid'] 页面唯一身份，默认为1，是search时为搜索关键词，是post时为post id，以此类推
   */

  public function vars( $hash=false ) {
    
    global $wp_query;
    
    $object = $wp_query->get_queried_object();
    
    $tracker = array();
    
    switch(TRUE){
        
        case $wp_query->is_home() : $tracker['type'] = 'home';
        break;
        
        case $wp_query->is_front_page() : $tracker = array( 'type' => 'single' , 'pid' => $object->ID );
        break;

        case $wp_query->is_singular() : $tracker = array( 'type' => $wp_query->is_attachment() ? 'attachment' : 'single' , 'pid' => $object->ID );
        break;
        
        case $wp_query->is_category() : $tracker = array( 'type' => 'cat' , 'pid' => $object->term_id );
        break;
        
        case $wp_query->is_tag() : $tracker = array( 'type' => 'tag' , 'pid' => $object->term_id );
        break;
        
        case $wp_query->is_search() : $tracker = array( 'type' => 'search' , 'pid' => $wp_query->get('s') );
        break;
        
        case $wp_query->is_author() : $tracker = array( 'type' => 'author' , 'pid' => $object->ID );
        break;

        case $wp_query->is_date() : $tracker['type'] = 'date';

          switch(TRUE){
                
            case $wp_query->is_year() :
              $tracker['type'] .= '_year';
              $tracker['pid'] = $wp_query->get('m') ? $wp_query->get('m') : $wp_query->get('year');
            break;
                
            case $wp_query->is_month() :
              $tracker['type'] .= '_month';
              $tracker['pid'] = $wp_query->get('m') ? $wp_query->get('m') : $wp_query->get('year') . sprintf ( "%02d", $wp_query->get('monthnum'));
            break;
                
            case $wp_query->is_day() :
              $tracker['type'] .= '_day';
              $tracker['pid'] = $wp_query->get('m') ? $wp_query->get('m') : $wp_query->get('year') . sprintf ( "%02d", $wp_query->get('monthnum')) . sprintf ( "%02d", $wp_query->get('day'));
            break;
    
          }
              
        break;  //~ date end

        case $wp_query->is_post_type_archive() : $tracker['type'] = 'post_type_'.$wp_query->get('post_type');
        break;
            
        case $wp_query->is_tax() : $tracker = array( 'type' => 'tax_'.$object->taxonomy , 'pid' => $object->term_id );
        break;
        
        case $wp_query->is_archive() : $tracker['type'] = 'archive';
        break;

        case $wp_query->is_404() : $tracker['type'] = '404';
        break;
      
    }
    
    $default = array( 'type' => 'unknown' , 'pid' => 1 );
    if ($hash)
      $default['hash'] = $wp_query->query_vars_hash;
      
    return apply_filters( 'dmeng_tracker_vars', wp_parse_args( $tracker, $default ) );

  }
  
  /**
   * 输出参数
   */
  public function vars_output( $scripts_array ) {
    $scripts_array['tracker'] = $this->vars(true);
    return $scripts_array;
  }
  
  /**
   * 流量排行
   */
  public function rank( $type='single', $limit=10, $exclude=array() ) {
    
    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name; 
    
    $results = array();
    
    if ( $type=='search' ) {
      
      $results = $wpdb->get_results( "SELECT pid,traffic FROM $table_name WHERE type = 'search' ORDER BY -traffic ASC LIMIT $limit ");
      
    } else {
      
      $table_posts = $wpdb->posts;
      
      if ($type==='view')
        $type = 'single';
      
      $post_where = '';
      if (false===in_array($type, array('single', 'any'))) {
        $post_types = (array)$type;
        foreach ($post_types as $key=>$post_type) {
          $post_types[$key] = '\''.$post_type.'\'';
        }
        $post_where .= 'AND '.$table_posts.'.post_type IN ('.join(',', $post_types).')';
      }
      
      $exclude = (array)$exclude;
      if ($exclude) {
        foreach ($exclude as $key=>$pid) {
          $exclude[$key] = '\''.$pid.'\'';
        }
        $post_where .= 'AND '.$table_name.'.pid NOT IN ('.join(',', $exclude).')';
      }

      $results = $wpdb->get_results("
        SELECT 
          $table_name.pid AS pid,$table_name.traffic AS traffic 
        FROM 
          $table_name 
        JOIN 
          $table_posts 
        ON 
          $table_posts.ID = $table_name.pid 
        WHERE 
          $table_posts.post_status = 'publish' $post_where AND $table_name.type = 'single' 
        GROUP BY 
          $table_posts.ID 
        ORDER BY 
          -traffic 
        ASC 
        LIMIT 
          $limit 
      ");
    
    }
    
    return $results;
  }
  
  /**
   * 流量 AJAX
   */
  public function ajax_action( $return_array ) {
  
    if ( 
      !isset($_POST['tracker']) 
      || !isset($_POST['tracker']['type']) 
      || !isset($_POST['tracker']['pid']) 
      || !isset($_POST['tracker']['hash']) 
    ) {
      return $return_array;
    }
    
    $type = sanitize_text_field($_POST['tracker']['type']);
    $pid = sanitize_text_field($_POST['tracker']['pid']);
    
    $return_array['tracker'] = number_format_i18n($this->update($type, $pid));
    $return_array['_wpnonce'] = wp_create_nonce(sanitize_text_field($_POST['tracker']['hash']));
    
    return $return_array;
  
  }
  
  /**
   * 相关搜索
   */
  public function related_search($keyword, $number=10) {
    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;
    $keyword = $wpdb->esc_like($keyword);
    $results = $wpdb->get_results(
      "
      SELECT 
        pid
      FROM 
        $table_name 
      WHERE 
        `type` = 'search' AND `pid` NOT LIKE '$keyword' AND `pid` LIKE '%$keyword%'
      ORDER BY 
        -traffic 
      ASC 
      LIMIT 
        $number
      "
    );
    return $results;
  }
}

$dmeng_Tracker = new dmeng_Tracker;
