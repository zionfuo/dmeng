<?php

/**
 * 小工具
 */

class dmeng_Widget {
  
  public function __construct() {
    add_action( 'in_widget_form', array( $this, 'display_int_form' ), 10, 3);
    add_filter( 'widget_update_callback', array( $this, 'update_callback' ), 10, 2);
    add_filter( 'widget_display_callback', array( $this, 'display_callback' ) );
    add_action( 'admin_print_footer_scripts', array( $this, 'form_scripts' ) );
  }

  public function display_int_prefix() {
    //~ 字段使用前缀防止重复，dso 是 dmeng show on 首字母
    return  'dso_';
  }
  
  public function display_int_array() {
    //~ 因为使用前缀，所以字段名称就只取首字母，防止字段名过长，造成不必要的浪费
    return array(
      'a' => __('文章', 'dmeng'),
      'p' => __('页面', 'dmeng'),
      'c' => __('分类', 'dmeng'),
      't' => __('标签', 'dmeng'),
      'u' => __('作者', 'dmeng'),
      's' => __('搜索', 'dmeng'),
      'h' => __('首页', 'dmeng'),
      'o' => __('其他页面', 'dmeng'),
      'pc' => __('PC', 'dmeng'),
      'mb' => __('Moblie', 'dmeng')
    );
  }

  //~ 添加设置到在小工具设置表单中
  public function display_int_form($widget, $return, $instance ) {

    $prefix = $this->display_int_prefix();
    $template = $this->display_int_array();

    ?>  
  <p>
    <button type="button" class="button widget_show_option_toggle"><?php _e('设置显示位置/终端', 'dmeng');?></button> 
    <button type="button" class="button widget_show_option_checked" style="display:none"><?php _e('全选', 'dmeng');?></button> 
    <button type="button" class="button widget_show_option_clear" style="display:none"><?php _e('全不选', 'dmeng');?></button> 
  </p>
  <div style="display:none">
    <p><?php _e('显示位置', 'dmeng');?></p>
    <hr />
    <p><?php _e('勾上代表在该项对应页面显示，后面输入框输入排除的ID，以逗号隔开，如果只输入ID而不勾选该项则代表只在这些ID页面显示。', 'dmeng');?></p>
    <p>
    <?php
    
      foreach( $template as $abbr=>$title ){

        //~ 终端单独显示在下面，所以这里排除
        if( in_array($abbr, array( 'pc', 'mb' )) ) continue;
        
        $key = $prefix . $abbr;

    ?>
      <input type="checkbox" class="checkbox" <?php checked( isset($instance[$key]) ) ?> id="<?php echo $widget->get_field_id($key); ?>" name="<?php echo $widget->get_field_name($key); ?>">
      <label for="<?php echo $widget->get_field_id($key); ?>"><?php echo $title;?></label> &nbsp; 
        <?php 
        //~ 搜索页和首页不显示ID
        if( in_array($abbr, array( 's', 'h', 'o')) === false ) { 
          $id_key = $key.'_id';
          ?>
          <input id="<?php echo $widget->get_field_id($id_key); ?>" name="<?php echo $widget->get_field_name($id_key); ?>" type="text" value="<?php if(isset($instance[$id_key])) echo esc_attr($instance[$id_key]); ?>">
          <br /><br />
        <?php } ?>
    <?php
      }
      ?>
    </p>
    <hr />
    <?php
    
    $pc_key = $prefix . 'pc';
    $mb_key = $prefix . 'mb';

    ?>
    <p> <?php _e('显示终端', 'dmeng');?> &nbsp; 
      <input type="checkbox" class="checkbox" <?php checked( isset($instance[$pc_key]) ) ?> id="<?php echo $widget->get_field_id( $pc_key ); ?>" name="<?php echo $widget->get_field_name( $pc_key ); ?>">
      <label for="<?php echo $widget->get_field_id( $pc_key ); ?>"><?php _e('PC', 'dmeng');?></label> &nbsp; 
      <input type="checkbox" class="checkbox" <?php checked( isset($instance[$mb_key]) ) ?> id="<?php echo $widget->get_field_id( $mb_key ); ?>" name="<?php echo $widget->get_field_name( $mb_key ); ?>">
      <label for="<?php echo $widget->get_field_id( $mb_key ); ?>"><?php _e('Moblie', 'dmeng');?></label> &nbsp; 
    </p>
    <br />
    
  </div>
    <?php
  }
  
  public function update_callback($instance, $new_instance) {
    
    $prefix = $this->display_int_prefix();
    $template = $this->display_int_array();
    
    foreach( $template as $abbr=>$title ){
      
      $key = $prefix . $abbr;
      
      $instance[$key] = $new_instance[$key];
      
      if( in_array($abbr, array( 's', 'h', 'o', 'pc', 'mb')) === false ) { 
        
        $id_key = $key . '_id';
        
        //~ 处理排除ID
        $key_array = explode( ',', trim(str_replace('，', ',', $new_instance[$id_key] ), ',') );
        $new_pid = array();
        foreach( $key_array as $pid ){
          $pid = intval(trim($pid));
          if($pid>0) $new_pid[] = $pid;
        }
        
        $instance[$id_key] = ( empty($new_pid) ? '' : join( ',', $new_pid ) );
      }
      
    }
    
    return $instance;
    
  }

  public function display_check($instance, $name) {
    if( empty($instance[ $name . '_id' ]) ){
      if( isset($instance[ $name ])===false )
        return false;
    }else{
      if( isset($instance[ $name ])===in_array(get_queried_object_id(), explode(',', $instance[ $name . '_id' ]) ) )
        return false;
    }
    return true;
  }

  public function display_callback($instance) {
    
    $prefix = $this->display_int_prefix();
    
    //~ 显示终端
    if ( dmeng_is_mobile() ) {
      //~ 移动端
      if( isset($instance[ $prefix . 'mb' ])===false )
        return false;
    } else {
      //~ PC
      if( isset($instance[ $prefix . 'pc' ])===false )
        return false;
    }
    
    //~ 首页
    if( ( is_home() || is_front_page() ) && isset($instance[ $prefix . 'h' ])===false )
      return false; 
    
    //~ 文章页
    if( is_single() && $this->display_check( $instance, $prefix.'a' )===false )
      return false; 
      
    //~ 页面
    if( is_page() && $this->display_check( $instance, $prefix.'p' )===false )
      return false; 
      
    //~ 分类
    if( is_category() && $this->display_check( $instance, $prefix.'c' )===false )
      return false; 
    
    //~ 标签
    if( is_tag() && $this->display_check( $instance, $prefix.'t' )===false )
      return false; 
    
    //~ 作者
    if( is_author() && $this->display_check( $instance, $prefix.'u' )===false )
      return false; 
    
    //~ 搜索
    if( is_search() && isset($instance[ $prefix . 's' ])===false )
      return false; 
    
    //~ 其他页面，代码如诗
    if( is_home()===false
       && is_front_page()===false
       && is_single()===false
       && is_page()===false
       && is_category()===false
       && is_tag()===false
       && is_author()===false
       && is_search()===false
       && isset($instance[ $prefix . 'o' ])===false )
      return false; 
      
    return $instance;
  }

  public function form_scripts() {
    global $pagenow;
    if ( $pagenow!='widgets.php' )
      return;
  ?>
  <script type="text/javascript">
  (function($){
    $(document).on("click",".widget_show_option_toggle",function(){
      $(this).parent().next().toggle('fast');
      $(this).siblings('.widget_show_option_checked').toggle('fast');
      $(this).siblings('.widget_show_option_clear').toggle('fast');
    });
    $(document).on("click",".widget_show_option_checked",function(){
      var o = $(this).parent().next();
      o.find("[type='checkbox']").prop("checked", true);
      o.find("[type='text']").val('');
    });
    $(document).on("click",".widget_show_option_clear",function(){
      var o = $(this).parent().next();
      o.find("[type='checkbox']").prop("checked", false);
      o.find("[type='text']").val('');
    });
  })(jQuery);
  </script>
  <?php
  }

}

$dmeng_Widget = new dmeng_Widget;

/*
 * 自定义小工具 @author 多梦 at 2014.06.21 
 * 
 */
 
function dmeng_widget_cache_key($widget) {
  return 'dm-wd'.$widget->number.'-'.md5( $widget->get_field_id('cache') . ( $widget->id_base=='recent_user' ? get_option('dmeng_cached_recent_user') : '' ) );
}

class DmengAnalyticsWidget extends WP_Widget {

  function __construct() {
    parent::__construct( 'analytics', __( ' 站点统计' , 'dmeng' ) , array('classname' => 'widget_analytics', 'description' => __( '站点统计信息' , 'dmeng' ) ) );
  }

  function widget( $args, $instance ) {

    $cache_key = dmeng_widget_cache_key($this);
    $cache_data = (array)wp_unslash(json_decode(get_transient($cache_key), true));
    if ( !empty($cache_data) ) {
      $list = $cache_data;
    } else {
    
      $data = array();
      global $dmeng_Count;
      foreach($instance as $type=>$value) {
        if ( $value!='on' )
          continue;
        $count = $dmeng_Count->get($type);
        if ( !empty($count) )
          $data[$type] = $count;
      }

      if ( empty($data) )
        return;

      $list = array(
        'data' => $data,
        'timestamp' => current_time( 'timestamp', 0 )
      );
      set_transient( $cache_key, json_encode($list), 3600 );
    }

    $label = array(
      'post' => __( '文章', 'dmeng' ),
      'cat' => __( '分类', 'dmeng' ),
      'tag' => __( '标签', 'dmeng' ),
      'user' => __( '用户', 'dmeng' ),
      'comment' => __( '评论', 'dmeng' ),
      'view' => __( '浏览总数', 'dmeng' ),
      'search' => __( '搜索次数', 'dmeng' ),
    );

    extract($args);
  
    $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

    $output = $before_widget;
    if ( $title )
      $output .= $before_title . $title . $after_title;

    $output .= '<ul>';
    
    $output .= '<li class="update">'.__('统计时间', 'dmeng') . ' : <time title="'.sprintf(__('统计时间：%s', 'dmeng') , date( 'Y-m-d H:i:s', $list['timestamp'] ) ).' ">'.date( 'm-d H:i', $list['timestamp'] ).'</time></li>';
    
    foreach( $list['data'] as $key=>$count ) {
      $output .= '<li title="'.number_format_i18n($count).'">'.$label[$key].' : '. dmeng_number_format($count).'</li>';
    }

    $output .= '</ul>';

    $output .= $after_widget;
     
    echo $output;
    
  }

  function form( $instance ) {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'post' => 'on', 'cat' => 'on', 'tag' => 'on', 'user' => 'on', 'comment' => 'on', 'view' => 'on', 'search' => 'on') );
    $title = $instance['title'];
?>
  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('标题：','dmeng');?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
  </p>
  <p><?php _e('选择要显示的项目。','dmeng');?></p>
<p>
  
  <?php
  
  $list = array(
    'post' => __('文章总数','dmeng'),
    'cat' => __('文章分类','dmeng'),
    'tag' => __('文章标签','dmeng'),
    'user' => __('用户总数','dmeng'),
    'comment' => __('评论总数','dmeng'),
    'view' => __('浏览总数','dmeng'),
    'search' => __('搜索次数','dmeng')
  );
  
  foreach( $list as $key=>$value ){
  ?>
  <input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id($key); ?>" name="<?php echo $this->get_field_name($key); ?>" <?php if(isset($instance[$key])&&trim($instance[$key])=='on') echo 'checked="checked"';?>> <label for="<?php echo $this->get_field_id($key); ?>"><?php echo $value?></label><br>
  <?php
  }
  ?>

</p>
<?php
  }

  function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $new_instance = wp_parse_args((array) $new_instance, array());
    $list = array('title', 'post', 'cat', 'tag', 'user', 'comment', 'view', 'search');
    foreach( $list as $key ){
      $instance[$key] = strip_tags($new_instance[$key]);
    }
    set_transient( dmeng_widget_cache_key($this), null, 3600 );
    return $instance;
  }

}

class DmengCreditRankWidget extends WP_Widget {

  function __construct() {
    parent::__construct( 'creditRank', __( ' 积分排行榜' , 'dmeng' ) , array('classname' => 'widget_credit_rank', 'description' => __( '用户可用积分排行榜' , 'dmeng' ) ) );
  }

  function widget( $args, $instance ) {

    extract($args);

    $cache_key = dmeng_widget_cache_key($this);
    $cache_data = (array)wp_unslash(json_decode(get_transient($cache_key), true));
    if ( !empty($cache_data) ) {
    
      $data = $cache_data;
      
    } else {

      $where = empty($instance['exclude']) ? '' : 'AND user_id NOT IN ('.$instance['exclude'].')';
      global $wpdb;
      $rank = $wpdb->get_results( "SELECT user_id,meta_value FROM $wpdb->usermeta WHERE meta_key = 'dmeng_credit' $where ORDER BY -meta_value ASC LIMIT 6 ");
      
      if ( empty($rank) )
        return;
      
      $data = array();
      foreach( $rank as $term ) {
        $user = get_user_by( 'id',  $term->user_id );
        $data[$user->ID] = array(
          'name' => ( filter_var($user->user_url, FILTER_VALIDATE_URL) ? '<a href="'.$user->user_url.'" target="_blank" rel="external nofollow">'.$user->display_name.'</a>' : $user->display_name ),
          'posts_url' => get_author_posts_url( $term->user_id ),
          'avatar' => get_avatar( $term->user_id , 20 ),
          'credit' => $term->meta_value
        );
      }
      
      set_transient( $cache_key, json_encode($data), 3600 );
    }

    $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

    $output = $before_widget;
    if ( $title )
      $output .= $before_title . $title . $after_title;

    $output .= '<ul>';
    foreach ( $data as $user ) {
      $output .= '<li class="clearfix"><i class="num">'.dmeng_number_format($user['credit']).'</i><a href="'.$user['posts_url'].'" class="posts_url">'.$user['avatar'].'</a>'.$user['name'].'</li>';
    }
    $output .= '</ul>';

    $output .= $after_widget;
    
    echo $output;
  }

  function form( $instance ) {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'exclude' => '1') );
    $title = $instance['title'];
?>
  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('标题：','dmeng');?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('exclude'); ?>"><?php _e('除了：','dmeng');?></label>
    <input type="text" value="<?php echo esc_attr($instance['exclude']); ?>" name="<?php echo $this->get_field_name('exclude'); ?>" id="<?php echo $this->get_field_id('exclude'); ?>" class="widefat">
    <br>
    <small><?php _e('用户ID，多个ID请用英文逗号（,）隔开','dmeng');?></small>
  </p>
<?php
  }

  function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $new_instance = wp_parse_args((array) $new_instance, array( 'title' => '', 'exclude' => '1') );
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['exclude'] = join(',', array_map('intval', explode(',', $new_instance['exclude'])));
    set_transient( dmeng_widget_cache_key($this), null, 3600 );
    return $instance;
  }
  
}

class DmengRankWidget extends WP_Widget {

  function __construct() {
    parent::__construct( 'dmengRank', __( ' 排行榜' , 'dmeng' ) , array('classname' => 'widget_dmeng_rank', 'description' => __( '热门文章/热议文章/搜索排行榜' , 'dmeng' ) ) );
  }

  function widget( $args, $instance ) {

    extract($args);
    $instance = wp_parse_args( (array) $instance, array( 'search' => 'on', 'comment' => 'on', 'vote' => 'on', 'view' => 'on', 'number' => 10 ) );
    $number = absint($instance['number']);

    $type = array_filter(array(
      ( $instance['search']=='on' ? 'search' : '' ),
      ( $instance['comment']=='on' ? 'comment' : '' ),
      ( $instance['vote']=='on' ? 'vote' : '' ),
      ( $instance['view']=='on' ? 'view' : '' )
    ));
    
    if ( $number<1 || empty($type) )
      return;

    $cache_key = dmeng_widget_cache_key($this);
    $cache_data = (array)wp_unslash(json_decode(get_transient($cache_key), true));
    if ( !empty($cache_data) ) {
      $items = $cache_data;
    } else {
    
      $items = array();
          
      global $dmeng_Rank;

      if ( in_array( 'search', $type ) ) {
        $items[] = array(
          'style' => 'success',
          'id' => 'search',
          'icon' => 'glyphicon-search',
          'data' => $dmeng_Rank->rank( 'search', $number ),
          'title' => __( '搜索次数最多的%s个关键词','dmeng')
        );
      }

      if ( in_array( 'comment', $type ) ) {
        $items[] = array(
          'style' => 'info',
          'id' => 'comment',
          'icon' => 'glyphicon-volume-up',
          'data' => $dmeng_Rank->rank( 'comment', $number ),
          'title' => __( '最多人评论的%s篇内容','dmeng')
        );
      }

      if ( in_array( 'vote', $type ) ) {
        $items[] = array(
          'style' => 'warning',
          'id' => 'vote',
          'icon' => 'glyphicon-stats',
          'data' => $dmeng_Rank->rank( 'vote', $number ),
          'title' => __( '按投票率排行的%s篇内容','dmeng')
        );
      }

      if ( in_array( 'view', $type ) ) {
        $items[] = array(
          'style' => 'danger',
          'id' => 'view',
          'icon' => 'glyphicon-fire',
          'data' => $dmeng_Rank->rank( 'view', $number ),
          'title' => __( '按浏览次数排行的%s篇内容','dmeng')
        );
      }

      if ( empty($items) )
        return;
        
      set_transient( $cache_key, json_encode($items), 3600 );
    }
    
    $output = '<aside id="accordion-'.$args['widget_id'].'" class="panel-group">';

    $i = 1;
    foreach( $items as $item ){
      $id = $item['id'].'-'.$args['widget_id'];

      $output .= '<div class="panel panel-'.$item['style'].'">';
      $output .= '<div class="panel-heading"><h3 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-'.$args['widget_id'].'" href="#'.$id.'"><span class="glyphicon '.$item['icon'].'"></span> '.sprintf( $item['title'] , $number ).'</a></h3></div>';
      $output .= ' <div id="'.$id.'" class="panel-collapse collapse '.($i==count($items) ? 'in' : '').'">';
      foreach( $item['data']['data'] as $data ){
        $output .= '<li class="list-group-item"><span class="badge" title="'.esc_attr(sprintf( $item['data']['title_format'], number_format_i18n($data['rank']) )).'">'.dmeng_number_format($data['rank']).'</span> <a href="'.$data['url'].'" title="'.esc_attr($data['title']).'">'.$data['title'].'</a></li>';
      }
      $output .= '</div></div>';

      $i++;
    }
    
    $output .= '</aside>';

    echo $output;

  }
  
  function update( $new_instance, $old_instance ) {

    $instance = $old_instance;
    $new_instance = (array)$new_instance;

    foreach( array('title', 'search', 'comment', 'vote', 'view') as $key ){
      $instance[$key] = strip_tags($new_instance[$key]);
    }
    $instance['number'] = absint($new_instance['number']);
    
    set_transient( dmeng_widget_cache_key($this), null, 3600 );
    return  $instance;
  }

  function form( $instance ) {
    $instance = wp_parse_args( (array) $instance, array( 'search' => 'on', 'comment' => 'on', 'vote' => 'on', 'view' => 'on', 'number' => 10 ) );
    $number = absint($instance['number']);
?>
<p><?php _e('选择要显示的项目。','dmeng');?></p>
<p>
  <?php
  
  $list = array(
    'search' => __('搜索次数最多的关键词','dmeng'),
    'comment' => __('评论最多的内容','dmeng'),
    'vote' => __('最多人投票的内容','dmeng'),
    'view' => __('浏览次数最多的内容','dmeng'),
  );
  
  foreach( $list as $key=>$value ){
  ?>
  <input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id($key); ?>" name="<?php echo $this->get_field_name($key); ?>" <?php if(isset($instance[$key])&&trim($instance[$key])=='on') echo 'checked="checked"';?>> <label for="<?php echo $this->get_field_id($key); ?>"><?php echo $value?></label><br>
  <?php
  }
  ?>
</p>
  <p>
    <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('显示数量：','dmeng');?></label>
    <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" size="3">
  </p>
<?php
  }
  
}

class DmengRecentUserWidget extends WP_Widget {

  function __construct() {
    parent::__construct( 'recent_user', __( ' 最近登录用户' , 'dmeng' ) , array('classname' => 'widget_recent_user', 'description' => __( '显示最近登录用户头像' , 'dmeng' ) ) );

    add_action( 'wp_login', array( $this, 'update_cache_timestamp') );
  }

  function update_cache_timestamp() {
    update_option( 'dmeng_cached_recent_user', current_time( 'timestamp', 0 ) );
  }

  function widget( $args, $instance ) {

    extract($args);

    $number = absint($instance['number']);
    if ($number<1)
      return;
    
    $cache_key = dmeng_widget_cache_key($this);
    $cache_data = (array)wp_unslash(json_decode(get_transient($cache_key), true));
    if ( !empty($cache_data) ) {
      $users = $cache_data;
    } else {
    
      $user_query = new WP_User_Query( array( 
        'orderby' => 'meta_value', 
        'order' => 'DESC', 
        'meta_key' => 'dmeng_latest_login', 
        'number' => $number, 
        'exclude'=> array_map('intval', explode(',', str_replace('，', ',', trim($instance['exclude'], ',，')))),
        'fields'=>'ID' 
      ) );

      if (empty($user_query->results))
        return;

      $recent_user = $user_query->results;
      $users = array();
      foreach( $recent_user as $user_id ) {
        $users[] = array(
          'name' => get_the_author_meta( 'display_name', $user_id ),
          'posts_url' => get_author_posts_url($user_id),
          'avatar' => get_avatar($user_id, '50')
        );
      }
      
      set_transient( $cache_key, json_encode($users), 3600 );
    }

    $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

    $output = $before_widget;
    if ( $title )
      $output .= $before_title . $title . $after_title;
      
    $output .= '<ul>';
    foreach( $users as $user ){
        $output .='<li><a href="'.$user['posts_url'] .'" target="_blank" title="'.$user['name'].'">'.$user['avatar'].'</a></li>';
    }
    $output .= '</ul>';

    $output .= $after_widget;
    echo $output;

  }

  function form( $instance ) {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '最近登录用户', 'number' => 10, 'exclude' => 1) );
    $title = $instance['title'];
    $number = (int)$instance['number'];
?>
  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('标题：','dmeng');?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('显示数量：','dmeng');?></label>
    <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" size="3">
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('exclude'); ?>"><?php _e('除了：','dmeng');?></label>
    <input type="text" value="<?php echo esc_attr($instance['exclude']); ?>" name="<?php echo $this->get_field_name('exclude'); ?>" id="<?php echo $this->get_field_id('exclude'); ?>" class="widefat">
    <br>
    <small><?php _e('用户ID，多个ID请用英文逗号（,）隔开','dmeng');?></small>
  </p>
<?php
  }

  function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $new_instance = wp_parse_args((array) $new_instance, array( 'title' => '最近登录用户', 'number' => 10));
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['number'] = (int)$new_instance['number'];
    $instance['exclude'] = join(',', array_map('intval', explode(',', $new_instance['exclude'])));
    
    set_transient( dmeng_widget_cache_key($this), null, 3600 );
    return  $instance;
  }

}

class DmengRecentCommentWidget extends WP_Widget {

	public function __construct() {
		$widget_ops = array('classname' => 'widget_recent_comments', 'description' => __( '多梦主题定制的用以替换默认的最近评论小工具', 'dmeng' ) );
		parent::__construct('recent-comments', __('Recent Comments'), $widget_ops);
		$this->alt_option_name = 'widget_recent_comments';

		add_action( 'comment_post', array($this, 'flush_widget_cache') );
		add_action( 'edit_comment', array($this, 'flush_widget_cache') );
		add_action( 'transition_comment_status', array($this, 'flush_widget_cache') );
	}

	public function flush_widget_cache() {
		set_transient( dmeng_widget_cache_key($this), null, 3600 );
	}

	public function widget( $args, $instance ) {
    extract($args);

    $number = absint($instance['number']);
    if ($number<1)
      return;   

		global $comments, $comment;

    $cache_data = array();
    if ( ! $this->is_preview() ) {
      $cache_key = dmeng_widget_cache_key($this);
      $cache_data = (array)wp_unslash(json_decode(get_transient($cache_key), true));
    }
    
    if ( !empty($cache_data) ) {
      $data = $cache_data;
    } else {

      $exclude = isset($instance['exclude']) ? explode(',', $instance['exclude']) : array(1);
      $comments = get_comments( apply_filters( 'widget_comments_args', array(
        'number'      => $number,
        'author__not_in' => $exclude,
        'status'      => 'approve',
        'post_status' => 'publish',
        'type' => 'comment'
      ) ) );
      
      if (empty($comments))
        return;
      
      remove_filter('get_comment_text', 'dmeng_look_replace');
      
      $data = array();

      foreach ($comments as $comment) {
        $data[] = array(
          'author' => get_comment_author($comment),
          'title' => dmeng_the_title($comment->comment_post_ID),
          'comment' => wp_trim_words(get_comment_text(), 38),
          'url' => esc_url( get_comment_link( $comment->comment_ID ) )
        );
      }

      add_filter('get_comment_text', 'dmeng_look_replace');
      
      if ( ! $this->is_preview() ) {
        set_transient( $cache_key, json_encode($data), 3600 );
      }
    }

    $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

    $output = $before_widget;
    if ( $title )
      $output .= $before_title . $title . $after_title;

    $output .= '<div class="comments-list">';

    foreach ($data as $item) {
      $output .= '<a href="'.$item['url'].'">';
      $output .= '<div class="clearfix">';
      $output .= '<div class="comment-info">'.sprintf( _x( '%1$s on %2$s', 'widgets' ), $item['author'], $item['title']).'</div>';
      $output .= '</div>';
      $output .= '<div class="comment-content">'.$item['comment'].'</div>';
      $output .= '</a>';
    }

		$output .= '</div>';
		$output .= $after_widget;

		echo $output;
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = absint( $new_instance['number'] );
		$instance['exclude'] = join(',', array_map('intval', explode(',', $new_instance['exclude'])));
		$this->flush_widget_cache();

		return $instance;
	}

	public function form( $instance ) {
		$title  = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$exclude = isset( $instance['exclude'] ) ? esc_attr( $instance['exclude'] ) : '1';
?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of comments to show:' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
    
  <p>
    <label for="<?php echo $this->get_field_id('exclude'); ?>"><?php _e('除了：','dmeng');?></label>
    <input type="text" value="<?php echo $exclude; ?>" name="<?php echo $this->get_field_name('exclude'); ?>" id="<?php echo $this->get_field_id('exclude'); ?>" class="widefat">
    <br>
    <small><?php _e('用户ID，多个ID请用英文逗号（,）隔开','dmeng');?></small>
  </p>
<?php
	}
}

function dmeng_register_widgets() {
  register_widget( 'DmengAnalyticsWidget' );
  register_widget( 'DmengCreditRankWidget' );
  register_widget( 'DmengRankWidget' );
  register_widget( 'DmengRecentUserWidget' );
  register_widget( 'DmengRecentUserWidget' );
  unregister_widget( 'WP_Widget_Recent_Comments' );
  register_widget( 'DmengRecentCommentWidget' );
}
add_action( 'widgets_init', 'dmeng_register_widgets' );