<?php

function get_dmeng_user_vote( $uid, $count=true, $type='', $vote='', $limit=0, $offset=0 ){

  $uid = intval($uid);
  
  if( !$uid )
    return;

  $type = in_array($type, array('post', 'comment')) ? $type : '';
  $vote = in_array($vote, array('up', 'down')) ? $vote : '';
  
  global $wpdb;
  $table_name = $wpdb->prefix . 'dmeng_meta';

  $where = "WHERE user_id='$uid' ";
  
  if($type) {
    $vote_type = 'vote_'.$type.'_%';
    $where .= "AND meta_key LIKE '$vote_type' ";
  }
  
  if($vote) $where .= "AND meta_value LIKE '$vote' ";

  if($count){
    $check = $wpdb->get_var( "SELECT COUNT(*) FROM $table_name $where" );
  }else{
    $field = $vote ? 'meta_key' : 'meta_key,meta_value';
    $check = $wpdb->get_results( "SELECT $field FROM $table_name $where ORDER BY meta_id DESC LIMIT $offset,$limit" );
  }
  
  $count = $check ? $check : 0;
  
  return $count;
}

function dmeng_get_user_like($user_id, $number=10, $paged=1) {
  
    $up_posts_count = intval(get_dmeng_user_vote($user_id, true, 'post', 'up'));
    $up_comments_count = intval(get_dmeng_user_vote($user_id, true, 'comment', 'up'));
    $up_count = $up_posts_count+$up_comments_count;

    $number = 10;
    $offset = ($paged-1)*$number;
  
    $up_result = get_dmeng_user_vote($user_id, false, '', 'up', $number, $offset);
    
    $up_data = array();

    if ( $up_result ) {
      foreach( $up_result as $up_meta ){
        
        $up_key = explode('_', $up_meta->meta_key);
        
        if ( empty($up_key[1]) || empty($up_key[2]) )
          continue;
        
        if ($up_key[1]=='post') {
            
          query_posts( 'p='.$up_key[2] );
          
          if( have_posts() ) {
          
            while ( have_posts() ) : the_post();

              $up_data[] = array(
                'title' => get_the_title(),
                'url' => get_permalink(),
                'excerpt' => apply_filters( 'the_excerpt', get_the_excerpt() ),
                'type' => 'post',
                'type_label' => __('文章', 'dmeng'),
                'id' => get_the_ID()
              );
              
            endwhile;
          }
          
          wp_reset_query();
            
        }
        
        if ($up_key[1]=='comment') {
          
          $comment = get_comment( $up_key[2] ); 
          
              $up_data[] = array(
                'title' => sprintf( __('%s的评论', 'dmeng'), dmeng_trim_username($comment->comment_author) ),
                'url' => get_comment_link($comment->comment_ID),
                'excerpt' => get_comment_text($comment->comment_ID),
                'type' => 'comment',
                'type_label' => __('评论', 'dmeng'),
                'id' => $comment->comment_ID
              );

        }

      }
    }
    return array( 'posts_count'=>$up_posts_count, 'comments_count'=>$up_comments_count, 'data'=>$up_data );
}

function dmeng_vote_count( $uid=0, $type='post', $vote='up' ) {
  
  $type = $type=='post' ? 'post' : 'comment';
  $vote = $vote=='up' ? 'up' : 'down';
  $uid = intval($uid);
  
  global $dmeng_Meta;
  return $dmeng_Meta->count( 'vote_'.$type.'_%', $vote, $uid );
}

function dmeng_vote_html( $id=0, $type='post' ) {

  if ( !$id || !in_array( $type, array( 'post', 'comment' ) ) )
    return;

  $vote_class = 'vote-group';
  $up_class = 'up';
  $down_class = 'down';

  $key = 'vote_'.$type.'_'.$id;
  
  $uid = get_current_user_id();
  
  if ($uid>0) {
  
    $vote = get_dmeng_meta($key,$uid);

    if ($vote) {
      
      $vote_class .= ' disabled';
      
      if ($vote=='up') {
        $up_class .= ' active';
        $down_class .= ' disabled';
      } elseif ($vote=='down') {
        $down_class .= ' active';
        $up_class .= ' disabled';
      }
    }
  
  }

  $votes_up = (int)get_metadata( $type, $id, 'dmeng_votes_up', true );
  $votes_down = (int)get_metadata( $type, $id, 'dmeng_votes_down', true );

  if($type=='post'){
    
    $count = $votes_up+$votes_down;

    $rating = ($votes_up+$votes_down)>0 ? round($votes_up/($votes_up+$votes_down)*5, 1) : 0;
    $rating = max( $rating, 1 );
    
    echo '<div class="hidden" itemprop="rating" itemscope itemtype="http://data-vocabulary.org/Rating"><span itemprop="average">'.$rating.'</span><span itemprop="votes">'.$count.'</span><span itemprop="count">'.get_comments_number().'</span></div>';
    
    echo '
  <div class="'.$vote_class.'" id="post-vote" role="group" data-vote-type="post" data-vote-id="'.$id.'">
      <a href="javascript:;" class="'.$up_class.' btn btn-default btn-lg"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span> <i class="num">'.$votes_up.'</i></a>
      <a href="javascript:;" class="'.$down_class.' btn btn-default btn-lg"><span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span> <i class="num">'.$votes_down.'</i></a>
    </div>
    ';

  }else{

  echo '<span class="'.$vote_class.'" data-vote-id="'.$id.'"><a href="javascript:;" class="'.$up_class.'"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span> <i class="num">'.$votes_up.'</i></a><a href="javascript:;" class="'.$down_class.'"><span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span> <i class="num">'.$votes_down.'</i></a></span>';
  
  }
}

function dmeng_vote_ajax_callback() {

  do_action( 'dmeng_before_ajax' );
  
  if ( !isset($_POST['vote']['type']) || 
    !isset($_POST['vote']['id']) || 
    !isset($_POST['vote']['vote'])
   ) {
    die( 'dataEmpty' );
  }

  $type = sanitize_text_field($_POST['vote']['type'])=='post' ? 'post' : 'comment';
  $id = absint($_POST['vote']['id']);
  
  $key = 'vote_'.$type.'_'.$id;
  $uid = get_current_user_id();
  
  $vote = sanitize_text_field($_POST['vote']['vote']);
  if ( !in_array($vote, array('up', 'down')) ) {
    if ( ( $uid && !in_array($vote, array('cancel_up', 'cancel_down')) ) || !$uid )
      $vote = 'up';
  }

  $status = $type=='post' ? get_post_status( $id ) : wp_get_comment_status( $id );
  
  if ( !in_array( $status, array('publish', 'inherit', 'approved') ) )
    die('statusError');

  if ( $uid )
    update_dmeng_meta($key, str_replace(  array('cancel_up', 'cancel_down'), '', $vote ), $uid);
  
  if ( in_array($vote, array('up', 'down')) && $uid<=0 )
    add_dmeng_meta($key, $vote, $uid);

  $vote = str_replace( 'cancel_', '', $vote );
  $count = absint(get_dmeng_meta_count($key, $vote));
  update_metadata( $type, $id, 'dmeng_votes_' . $vote, $count );
  
  echo $count;
  die();
}
add_action( 'wp_ajax_dmeng_vote_ajax', 'dmeng_vote_ajax_callback' );
add_action( 'wp_ajax_nopriv_dmeng_vote_ajax', 'dmeng_vote_ajax_callback' );

function dmeng_post_vote_ajax_action( $return_array ){
  
  if ( 
    !isset($_POST['tracker']) 
    || !isset($_POST['tracker']['type']) 
    || !isset($_POST['tracker']['pid']) 
    || $_POST['tracker']['type']!='single' 
  ) {
    return $return_array;
  }
  
  $post_id = intval($_POST['tracker']['pid']);

  $post_vote = array();
  $post_vote['up'] = (int)get_post_meta( $post_id, 'dmeng_votes_up', true );
  $post_vote['down'] = (int)get_post_meta( $post_id, 'dmeng_votes_down', true );
    
  $uid = get_current_user_id();
  if ($uid>0)
    $vote = get_dmeng_meta( 'vote_post_' . $post_id, $uid );
  
  if ( !empty($vote) && in_array( $vote, array('up', 'down') ) ) {
    $post_vote['active'] = $vote;
  }
  $return_array['post_vote'] = $post_vote;

  return $return_array;
}
add_filter( 'dmeng_ready_ajax', 'dmeng_post_vote_ajax_action' );
