<?php

/**
 * 排行榜
 */

class dmeng_Rank {

  public $table_name = 'dmeng_rank';

  public function rank( $sort='view', $number=10, $post_type='any', $expire=false ) {
    
    $expire = (int)$expire;
    if ($expire) {
      $cache_key = md5(serialize(func_get_args()));
      if ( false!==($cache_data = wp_cache_get($cache_key, $this->table_name)) )
        return $cache_data;
    }

    $data = array();
    
    if ( $sort==='search' ) {
    
      $rank = get_dmeng_traffic_rank('search', $number);
      if ($rank) {
        $data['title_format'] = __('%s次搜索','dmeng');
        foreach( $rank as $search ) {
          $data['data'][] = array(
            'url' => add_query_arg('s', urlencode($search->pid), home_url('/')),
            'title' => $search->pid,
            'rank' => $search->traffic
          );
        }
      }
    
    } else if ( $sort==='comment' ) {

      $args = array( 'posts_per_page' => $number, 'orderby' => 'comment_count', 'ignore_sticky_posts' => true, 'post_type' => $post_type, 'no_found_rows' => true, 'suppress_filters' => true );
    
      if ('any'===$post_type || in_array('page', (array)$post_type))
        $args['post__not_in'] = (array)get_option( 'page_on_front' );
      
      $query = new WP_Query($args);

      if ( $query->have_posts() ) {
        $data['title_format'] = __('%s条评论','dmeng');
        while ( $query->have_posts() ) {
          $query->the_post();
          $data['data'][] = array(
            'ID' => get_the_ID(),
            'url' => get_permalink(),
            'title' => get_the_title(),
            'rank' => get_comments_number()
          );
        }
      }
      wp_reset_postdata();
      
    } else if ( $sort==='date' ) {
      
      $args = array( 'posts_per_page' => $number, 'orderby' => 'date', 'ignore_sticky_posts' => true, 'post_type' => $post_type, 'no_found_rows' => true, 'suppress_filters' => true );

      if ('any'===$post_type || in_array('page', (array)$post_type))
        $args['post__not_in'] = (array)get_option( 'page_on_front' );
      
      $query = new WP_Query($args);

      if ( $query->have_posts() ) {
        $data['title_format'] = __('%s发布','dmeng');
        while ( $query->have_posts() ) {
          $query->the_post();
          $data['data'][] = array(
            'ID' => get_the_ID(),
            'url' => get_permalink(),
            'title' => get_the_title(),
            'rank' => get_the_time('U')
          );
        }
      }
      wp_reset_postdata();  
      
    } else if ( $sort==='vote_up' ) {
      
      $meta_key = 'dmeng_votes_up';
      $args = array( 'posts_per_page' => $number, 'meta_key' => $meta_key, 'orderby' => 'meta_value_num', 'ignore_sticky_posts' => true, 'post_type' => $post_type, 'no_found_rows' => true, 'suppress_filters' => true );

      if ('any'===$post_type || in_array('page', (array)$post_type))
        $args['post__not_in'] = (array)get_option( 'page_on_front' );
      
      $query = new WP_Query($args);

      if ( $query->have_posts() ) {
        $data['title_format'] = __('%s人点赞','dmeng');
        while ( $query->have_posts() ) {
          $query->the_post();
          $data['data'][] = array(
            'ID' => get_the_ID(),
            'url' => get_permalink(),
            'title' => get_the_title(),
            'rank' => (int)get_post_meta(get_the_ID(), $meta_key, true)
          );
        }
      }
      wp_reset_postdata();
            
    } else if ( $sort==='pingback' ) {
      
      global $wpdb;
      $table_posts = $wpdb->posts;
      $table_comments = $wpdb->comments;

      $post_type_where = '';
      if ('any'!==$post_type) {
        $post_types = (array)$post_type;
        foreach ($post_types as $key=>$type) {
          $post_types[$key] = '\''.$type.'\'';
        }
        $post_type_where = 'AND '.$table_posts.'.post_type IN ('.join(',', $post_types).')';
      }
      
      $rank = $wpdb->get_results("
        SELECT 
          $table_comments.comment_post_ID AS post_id, count($table_comments.comment_ID) AS count
        FROM 
          $table_comments 
        JOIN 
          $table_posts 
        ON 
          $table_posts.ID = $table_comments.comment_post_ID
        WHERE 
          $table_posts.post_status = 'publish' $post_type_where AND 
          $table_comments.comment_type LIKE 'pingback' AND 
          $table_posts.ID NOT LIKE (SELECT option_value FROM $wpdb->options WHERE option_name LIKE 'page_on_front' LIMIT 1)
        GROUP BY 
          $table_comments.comment_post_ID 
        ORDER BY 
          count 
        DESC 
        LIMIT 
          $number
      ");

      if ($rank) {
        $data['title_format'] = __('%s次引用','dmeng');
        foreach( $rank as $vote ) {
          $data['data'][] = array(
            'ID' => $vote->post_id,
            'url' => get_permalink($vote->post_id),
            'title' => get_the_title($vote->post_id),
            'rank' => $vote->count
          );
        }
      }
      
    } else if ( $sort==='vote' ) {

      global $wpdb;
      $table_posts = $wpdb->posts;
      $table_postmeta = $wpdb->postmeta;
      
      $post_type_where = '';
      if ('any'!==$post_type) {
        $post_types = (array)$post_type;
        foreach ($post_types as $key=>$type) {
          $post_types[$key] = '\''.$type.'\'';
        }
        $post_type_where = 'AND '.$table_posts.'.post_type IN ('.join(',', $post_types).')';
      }
      
      $rank = $wpdb->get_results("
        SELECT 
          $table_postmeta.post_id,sum($table_postmeta.meta_value+0) AS count 
        FROM 
          $table_postmeta 
        JOIN 
          $table_posts 
        ON 
          $table_posts.ID = $table_postmeta.post_id
        WHERE 
          $table_posts.post_status = 'publish' $post_type_where AND 
          ( $table_postmeta.meta_key='dmeng_votes_up' OR $table_postmeta.meta_key='dmeng_votes_down' ) AND 
          $table_posts.ID NOT LIKE (SELECT option_value FROM $wpdb->options WHERE option_name LIKE 'page_on_front' LIMIT 1)
        GROUP BY 
          $table_postmeta.post_id 
        ORDER BY 
          count 
        DESC 
        LIMIT 
          $number
      ");
      
      // 代码如诗
      
      if ($rank) {
        $data['title_format'] = __('%s人投票','dmeng');
        foreach( $rank as $vote ) {
          $data['data'][] = array(
            'ID' => $vote->post_id,
            'url' => get_permalink($vote->post_id),
            'title' => get_the_title($vote->post_id),
            'rank' => $vote->count
          );
        }
      }

    } else {
    
      $rank = get_dmeng_traffic_rank($post_type, $number, get_option( 'page_on_front' ));
      if ($rank) {
        $data['title_format'] = __('%s次浏览','dmeng');
        foreach( $rank as $view ) {
          $data['data'][] = array(
            'ID' => $view->pid,
            'url' => get_permalink($view->pid),
            'title' => get_the_title($view->pid),
            'rank' => $view->traffic
          );
        }
      }

    }

    if ($expire)
      wp_cache_set($cache_key, $data, $this->table_name, $expire);
    
    return $data;
  }

}

$dmeng_Rank = new dmeng_Rank;
