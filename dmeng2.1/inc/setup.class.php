<?php

/**
 * 主题初始化 
 */

class dmeng_Setup {

  /**
   * 统一前缀
   */
  public function prefix() {
    return 'dmeng_';
  }

  public function __construct() {

    /**
     * 挂载处理动作和钩子
     * https://codex.wordpress.org/Plugin_API
     * 
     * 具体方法的文档在对应的函数注释中
     */

     /**
      * 禁止自动转换符号
      * http://www.dmeng.net/wordpress-disable-texturize.html
      */
    add_filter( 'run_wptexturize', '__return_false' );

     /**
      * 统一登录用户不显示工具栏 
      */
    add_filter( 'show_admin_bar', '__return_false' );

    /**
     * 移除网页元信息里的 WordPress 版本信息
     */
    add_filter( 'the_generator', '__return_empty_string' );
    remove_action( 'wp_head', 'wp_generator' ); 

    /**
     * 移除“功能”小工具中的 WordPress 链接
     * https://codex.wordpress.org/Plugin_API/Filter_Reference/widget_meta_poweredby
     */
    add_filter( 'widget_meta_poweredby', '__return_empty_string' );

    /**
     * 移除 emoji 表情处理代码
     */
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );

    add_action( 'after_switch_theme', 'flush_rewrite_rules' );

    add_action( 'after_setup_theme', array( $this, 'dmeng_setup' ) );
    
    add_action( 'widgets_init',  array( $this, 'register_sidebars' ) );
    
    add_filter( 'excerpt_length',  array( $this, 'custom_excerpt_length' ) );
    add_filter( 'excerpt_more',  array( $this, 'custom_excerpt_more' ) );
    add_filter( 'get_the_excerpt',  array( $this, 'custom_excerpt' ) );
    
    remove_filter( 'the_excerpt', 'wpautop' );
    
    add_filter( 'post_class',  array( $this, 'sanitize_post_class' ) );
    
    add_filter( 'body_class',  array( $this, 'sanitize_body_class' ) );
    
    add_filter( 'the_password_form',  array( $this, 'custom_password_form' ) );
    
    add_filter( 'get_avatar', array( $this, 'replace_get_avatar' ) );

    add_action( 'wp_enqueue_scripts', array( $this, 'dmeng_scripts' ) );
    
    add_action( 'init', array( $this, 'dmeng_init' ) );

    if (false===dmeng_is_mobile())
      add_filter( 'the_content', array( $this, 'article_index' ) );

    add_filter( 'comment_reply_link', array( $this, 'comment_reply_link' ), 10, 3 );
    
    add_filter( 'default_option_dmeng_copyright_content_default', array( $this, 'default_copyright_content' ) );

    add_action( 'login_enqueue_scripts', array( $this, 'login_logo' ) );
    
    add_action( 'wp_head', array( $this, 'dmeng_setting_head' ) );
    add_action( 'wp_footer', array( $this, 'dmeng_setting_footer' ) );

    if ( dmeng_is_mobile() && apply_filters( 'dmeng_redirect_mobile', true )) {
      add_action( 'template_redirect', array( $this, 'redirect_mobile' ) );
    }
  }

  /**
   * 主题初始化
   * https://codex.wordpress.org/Plugin_API/Action_Reference/after_setup_theme
   */
  public function dmeng_setup() {
    
    /**
     * 本地化翻译语言文件目录
     * https://codex.wordpress.org/Function_Reference/load_theme_textdomain
     */
    load_theme_textdomain( 'dmeng', get_template_directory() . '/languages' );
    
    /**
     * 添加主题支持
     * https://codex.wordpress.org/Function_Reference/add_theme_support
     */
    add_theme_support( 'html5', array(
      'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );

    add_theme_support( 'custom-header', array(
      'default-image'          => get_template_directory_uri().'/images/screenshot_64.png',
      'random-default'         => false,
      'width'                  => 64,
      'height'                 => 64,
      'flex-height'            => true,
      'flex-width'             => true,
      'default-text-color'     => '444444',
      'header-text'            => true,
      'uploads'                => true,
      'admin-preview-callback' => 'dmeng_custom_header_admin_preview'
    ) );
    add_theme_support( 'custom-background' );
    add_theme_support( 'post-thumbnails', array( 'post', 'gift' ) );
    
    /**
     * 设置缩略图尺寸
     * https://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
     */
    set_post_thumbnail_size( 220, 146, true );
    
    /**
     * 注册菜单
     * https://codex.wordpress.org/Function_Reference/register_nav_menus
     */
    register_nav_menus( array(
      'header_menu' => __( '头部菜单', 'dmeng' ),
      'header_right_menu' => __( '头部右侧菜单', 'dmeng' ),
      'link_menu' => __( '链接菜单', 'dmeng' ),
      'mobile_menu' => __( '移动端菜单', 'dmeng' )
    ) );
  
  }

  /**
   * 注册侧边栏
   * https://codex.wordpress.org/Function_Reference/register_sidebar
   */
  public function register_sidebars() {
    register_sidebar( array(
      'name' => __( '主侧边栏', 'dmeng' ),
      'id' => 'sidebar-1',
      'description' => __( '主要的侧边栏', 'dmeng' ),
      'before_widget' => '<aside id="%1$s" class="'.apply_filters('dmeng_widget_panel_class', 'widget clearfix %2$s').'">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => __( '底部边栏', 'dmeng' ),
      'id' => 'sidebar-2',
      'description' => __( '显示在底部', 'dmeng' ),
      'before_widget' => '<aside id="%1$s" class="widget clearfix footer-widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
  }
  
  /**
   * 自定义摘要字数
   * https://codex.wordpress.org/Plugin_API/Filter_Reference/excerpt_length
   */
  public function custom_excerpt_length() {
    return (dmeng_get_the_thumbnail() ? 90 : 120);
  }
  
  /**
   * 自定义摘要省略符号
   * https://codex.wordpress.org/Plugin_API/Filter_Reference/excerpt_more
   */
  public function custom_excerpt_more() {
    return '&hellip;';
  }

  public function custom_excerpt( $excerpt ) {
    if (is_search()) {
      
      global $post;
      $excerpt = wp_trim_excerpt(strip_tags($post->post_content));
      $excerpt_length = apply_filters( 'excerpt_length', $this->custom_excerpt_length() );
      $excerpt_more = apply_filters( 'excerpt_more', $this->custom_excerpt_more() );

      $key = trim(get_search_query());
      $keypos = strpos($excerpt, $key);
      if ($keypos && $keypos>$excerpt_length) {
        
        /**
         * 标点符号匹配
         * /[^a-zA-Z0-9\x{4e00}-\x{9fa5}\s]/u
         * /[^a-zA-Z0-9\x{4e00}-\x{9fa5}_-]/u
         */
        $pattern = '/[^a-zA-Z0-9\x{4e00}-\x{9fa5}\s]/u';
        $pmatch = preg_match_all($pattern, substr($excerpt, 0, $keypos), $matches, PREG_OFFSET_CAPTURE);
        if ($pmatch) {
          $excerpt_subpos = end($matches[0])[1];
        } else {
          $excerpt_subpos = $keypos-12;
        }

        $excerpt = substr($excerpt, $excerpt_subpos);

        if ($pmatch)
          $excerpt = preg_replace($pattern, '', $excerpt, 1);
      }

      $excerpt = wp_trim_words( $excerpt, $excerpt_length, $excerpt_more );
    }

    return $excerpt;
  }

  /**
   * 自定义文章查看密码表单
   * https://codex.wordpress.org/Using_Password_Protection
   */
  public function custom_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( add_query_arg('action','postpass',wp_login_url()) ) . '" method="post" class="form-inline"><ul class="list-inline"><li><input name="post_password" id="' . $label . '" type="password" class="form-control" placeholder="'.__('请输入密码…','dmeng').'"></li><li><button type="submit" class="btn btn-default" id="searchsubmit">'.__('提交','dmeng').'</button></li></ul><div class="help-block">' . __( '这是一篇受密码保护的文章，您需要提供访问密码。','dmeng' ) . '</div></form>';
    return $o;
  }

  /**
   * 替换 Gravatar 头像服务网址为国内网址
   * https://codex.wordpress.org/Plugin_API/Filter_Reference/get_avatar
   */
  public function replace_get_avatar( $avatar ) {
    $avatar = str_replace( array( 'www.gravatar.com', '0.gravatar.com', '1.gravatar.com', '2.gravatar.com' ), 'cn.gravatar.com', $avatar);
    return $avatar;
  }

  /**
   * 多梦主题脚本
   * https://codex.wordpress.org/Plugin_API/Action_Reference/wp_enqueue_scripts
   */
  public function new_jquery_uri() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', dmeng_jquery_uri(), array(), '2.1.4' );
  }
  
  public function dmeng_scripts() {

    $this->new_jquery_uri();

    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.4' );
    wp_enqueue_style( 'dmeng-base', get_template_directory_uri() . '/css/dmeng-base-'.DMENG_CSS_VER.'.css', array( 'bootstrap' ), DMENG_VER );
    wp_enqueue_style( 'dmeng' , get_template_directory_uri() . '/css/dmeng-'.DMENG_CSS_VER.'.css', array( 'dmeng-base' ), DMENG_VER );
    
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '3.3.4' );
    if (DMENG_SCRIPT_DEV) {
      wp_enqueue_script( 'lazyload', get_template_directory_uri() . '/js/jquery.lazyload.min.js', array( 'jquery' ), '1.9.5' );
      wp_enqueue_script( 'dmeng-base', get_template_directory_uri() . '/js/dev/dmeng-base-'.DMENG_SCRIPT_VER.'.js', array( 'bootstrap', 'lazyload' ), DMENG_VER );
      wp_enqueue_script( 'dmeng', get_template_directory_uri() . '/js/dev/dmeng-'.DMENG_SCRIPT_VER.'.js', array( 'dmeng-base' ), DMENG_VER );
      wp_enqueue_script( 'iscroll', get_template_directory_uri() . '/js/iscroll.min.js', array( 'jquery' ), '5.1.3' );
    } else {
      wp_enqueue_script( 'dmeng-min', get_template_directory_uri() . '/js/dmeng-'.DMENG_SCRIPT_VER.'.min.js', array( 'bootstrap' ), DMENG_VER );
    }

  }
  
  /**
   * 过滤文章内容CSS类
   * http://codex.wordpress.org/Function_Reference/post_class
   */
  public function sanitize_post_class( $classes ) {
    $classes = array();
    
    if ( is_home() || is_archive() || is_search() ) {
      $classes[] = 'single-post clearfix';
    } else {
      $classes[] = 'single';
      if (false===dmeng_is_mobile())
        $classes[] = 'col-lg-8 col-md-8';
    }

    return $classes;
  }
  
  /**
   * 过滤body CSS类
   * http://codex.wordpress.org/Function_Reference/body_class
   */
  public function sanitize_body_class( $classes ) {
    if ( $ucenter_page = is_dmeng_ucenter() )
      $classes = array( 'ucenter_page', 'ucenter-'.$ucenter_page );
    return $classes;
  }
  
  public function dmeng_init() {

    $this->keep_spiders_out();
    $this->remove_open_sans();
    
    global $wp_rewrite;
    $archive_slug = $post_type = 'post';
    $feeds = '(' . trim( implode( '|', $wp_rewrite->feeds ) ) . ')';
    add_rewrite_rule( "{$archive_slug}/?$", "index.php?post_type=$post_type", 'top' );
    add_rewrite_rule( "{$archive_slug}/feed/$feeds/?$", "index.php?post_type=$post_type" . '&feed=$matches[1]', 'top' );
    add_rewrite_rule( "{$archive_slug}/$feeds/?$", "index.php?post_type=$post_type" . '&feed=$matches[1]', 'top' );
    add_rewrite_rule( "{$archive_slug}/{$wp_rewrite->pagination_base}/([0-9]{1,})/?$", "index.php?post_type=$post_type" . '&paged=$matches[1]', 'top' );
  }

  /**
   * 拒绝部分爬虫索引网页，如七牛镜像爬虫
   * https://codex.wordpress.org/Plugin_API/Action_Reference/init
   */
  public function keep_spiders_out() {
    if ( isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], 'qiniu-imgstg-spider') !== false) {
      header('HTTP/1.1 503 Service Temporarily Unavailable');
      echo 'poor guy';
      exit;
    }
  }
  
  public function remove_open_sans() {
    wp_deregister_style( 'open-sans' );
    wp_register_style( 'open-sans', false );
    wp_enqueue_style('open-sans','');
  }
  
  public function article_index($content) {
    $post_index = (int)get_option('dmeng_post_index_all',1);
    if ( in_array( $post_index, array(1, 2, 3) ) && ( is_single() || is_page() ) ) {
      if ( $post_index===2 && is_page() )
        return $content;
      if ( $post_index===3 && is_single() )
        return $content;
      $matches = array();  
      $index_li = $ol = $depth_num = '';
      if(preg_match_all("/<h([2-6]).*?\>(.*?)<\/h[2-6]>/is", $content, $matches)) {

        //~ $matches[0] 是原标题，包括标签，如<h2>标题</h2>
        //~ $matches[1] 是标题层级，如<h2>就是“2”
        //~ $matches[2] 是标题内容，如<h2>标题</h2>就是“标题”
        
        foreach ($matches[1] as $key=>$level) {

          if( $ol && intval($ol)<$level){
            $index_li .= '<ul>';
            $depth_num = intval($depth_num)+1;
          }

          if( $ol && intval($ol)>$level ){
            $index_li .= '</li>'.str_repeat('</ul></li>', intval($depth_num));
            $depth_num = 0;
          }
          
          $hash_key = 'title-' . ($key+1);
          $content = str_replace($matches[0][$key], '<h'.$level.' id="'.$hash_key.'">'.$matches[2][$key].'</h'.$level.'>', $content);
          if ( $ol && intval($ol)==$level)
            $index_li .= '</li>';
          $index_li .= '<li><a href="#'.$hash_key.'">'.$matches[2][$key].'</a>';

          if (($key+1)==count($matches[1]))
            $index_li .= '</li>'.str_repeat('</ul></li>', intval($depth_num));

          $ol = $level;
        }
        $content = '<div class="article_index" data-toggle="hash"><h5>'.__('文章目录','dmeng').'<span class="caret"></span></h5><ul>' . $index_li . '</ul></div>' . $content;
      }
    }
    return $content;
  }

  public function comment_reply_link( $link, $args, $comment ) {
    /**
     * ajax 加载评论模板时因为默认函数的 add_query_arg 没有设置文章网址，所以会错误把当前网址也就是 wp-admin/admin-ajax.php 作为根网址
     * 这里替换为正确的地址
     */
    $reply_url = esc_url( add_query_arg( 'replytocom', $comment->comment_ID, get_permalink($comment->comment_post_ID) ) ) . "#" . $args['respond_id'];
    $link = preg_replace( "/(?<=href=(\"|'))[^\"']+(?=(\"|'))/", ( is_user_logged_in() ? $reply_url : wp_login_url($reply_url) ), $link );
    return $link;
  }
  
  public function default_copyright_content($default) {
    if (empty($default)) {
      $default = sprintf(__('原文链接：%s，转发请注明来源！','dmeng'),'<a href="{link}" rel="author">{title}</a>');
    }
    return $default;
  }
  
  public function dmeng_setting_head() {
    $head_css = dmeng_setting('head_css');
    echo dmeng_setting('head_code').($head_css ? '<style type="text/css">'.$head_css.'</style>' : '');
  }
  
  public function dmeng_setting_footer() {
    echo dmeng_setting('footer_code');
  }
  
  public function login_logo() {
    
    $this->new_jquery_uri();
    
    if ( get_header_image() ) {
      $custom_header = get_custom_header();
      $logo_data = array();
      $logo_data['url'] = $custom_header->url ? $custom_header->url : get_theme_support( 'custom-header', 'default-image');
      $logo_data['width'] = $custom_header->width ? $custom_header->width : get_theme_support( 'custom-header', 'width');
      $logo_data['height'] = $custom_header->height ? $custom_header->height : get_theme_support( 'custom-header', 'height');
      
      $css = sprintf('background-image:url(%1$s);-webkit-background-size:%2$spx %3$spx;background-size:%2$spx %3$spx;width:%2$spx;height:%3$spx;', $logo_data['url'], $logo_data['width'], $logo_data['height']);
    }else{
      $css = 'display:none;';
    }
    ?>
    <style type="text/css">
      body.login div#login h1 a{
        <?php echo $css;?>
      }
    </style>
  <?php 
  }
  
  public function redirect_mobile() {
    dmeng_die(
      get_bloginfo('url').'<br>'.__('暂不支持移动端访问', 'dmeng').'<br>error: mobile theme not found',
      __('请使用电脑访问', 'dmeng'),
      false
    );
  }
}
