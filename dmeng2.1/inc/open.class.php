<?php

/**
 * 
 */

class dmeng_Open {

  public function is_open( $platform ) {
    return apply_filters( 'dmeng_open_'.$platform, true );
  }

  public function __construct() {
    add_filter( 'get_avatar' , array( $this, 'replace_open_avatar' ), 10, 2 );
    add_filter( 'default_avatar_select' , array( $this, 'replace_default_avatar_select' ) );
    add_action( 'login_form', array( $this, 'open_login_form' ) );
    add_action( 'register_form', array( $this, 'open_login_form' ) );
  }

  public function replace_open_avatar( $avatar, $id_or_email ) {
     
     if ( is_numeric($id_or_email) ) {

      $user_id = $id_or_email;
    
    } elseif ( is_object($id_or_email) ) {

      $user_id = empty( $id_or_email->user_id ) ? $id_or_email->comment_author_email : $id_or_email->user_id;
        
    }else{
      
      $user_id = email_exists($id_or_email);
      
    }

    if ( intval($user_id) > 0 ) {
      $dmeng_avatar = get_user_meta( $user_id, 'dmeng_avatar', true );
      $open_avatar = apply_filters( 'dmeng_open_avatar', false, $user_id, $dmeng_avatar );
      if ( !empty($open_avatar) ) {
        $avatar = preg_replace( "/src='(.+?)'/", "src='".$open_avatar."'", $avatar );
        $avatar = preg_replace( "/srcset='(.+?) 2x'/", "srcset='".$open_avatar." 2x'", $avatar );
      }
    }
    
    return $avatar;
  
  }

  public function replace_default_avatar_select( $avatar_list ) {
    global $avatar_defaults;
    $avatar = explode('<br />', $avatar_list );
    $content = '';
    $i = 0;
    foreach( $avatar_defaults as $default_key=>$default_value ) {
      preg_match( "/src='(.+?)'/", get_avatar( 'email@example.com', 32, $default_key ), $example_avatar );
      $avatar[$i] = preg_replace( "/src='(.+?)'/", "src='".$example_avatar[1]."&amp;forcedefault=1'", $avatar[$i] );
      $avatar[$i] = preg_replace( "/srcset='(.+?) 2x'/", "srcset='".$example_avatar[1]."&amp;forcedefault=1 2x'", $avatar[$i] );
      $content .= $avatar[$i] . '<br />'; 
      $i++;
    }
    return $content;
  }

  public function open_login_form() {
    
    $open = apply_filters( 'dmeng_open_loginform', array() );
    
    global $pagenow;
    $login_page = $pagenow=='wp-login.php';

    $output = '';
    foreach( $open as $key=>$op ) {
      if ( $this->is_open($op['slug']) ) {
        $avatar = apply_filters( 'dmeng_open_loginform_'.$op['slug'].'_avatar', '', get_current_user_id(), $op['slug'] );
        $title = $op['title'] .  ( $login_page||empty($avatar) ? __( '登录', 'dmeng' ) : '' );
        $output .= '<a '. ( !empty($op['style'])&&($login_page||empty($avatar)) ? 'style="'.$op['style'].'" ': '' ).' title="'.$title.'" href="'.dmeng_open_url( $op['slug'] ).'">'. ( $login_page||empty($avatar) ? '' : '<img src="'.$avatar.'" width="20" height="20" /> ' ) . $title .'</a>';
      }
    }
    
    if ( ''===$output )
      return;
    
    echo '<style type="text/css">.dmengopen{margin:0 0 0 -10px}.dmengopen a{color:#666;background:#f9f9f9;text-decoration:none;padding:6px 8px;margin:7px 0 8px 10px;display:inline-block;border:1px solid rgba(0,0,0,0.15);border-radius:4px;box-sizing:border-box;text-align:center}.dmengopen a:hover{opacity:.8}</style><p style="margin:0"><label style="font-size:12px">'.__('使用第三方帐号可直接登录', 'dmeng').'</label><br></p><div class="dmengopen">' . $output . '</div>';

    return;
  }

}
$dmeng_Open = new dmeng_Open;


class dmeng_Open_connect {
  
  public $connect;
  public $action;
  
  public $prefix = 'dmeng_open_';
  
  public $client_id = 1;
  public $client_secret = 1;
  
  public $client_id_key;
  public $client_secret_key;
  
  public $openid;
  public $access_token;
  public $open_user_name;

  public $avatar_format;
  
  public function __construct() {
    
    if ( $this->client_id_key )
      $this->client_id = get_option( $this->prefix . $this->client_id_key );
    
    if ( $this->client_secret_key )
      $this->client_secret = get_option( $this->prefix . $this->client_secret_key );

    add_filter( 'dmeng_open_'.$this->connect, array( $this, 'is_open' ) );
    
    if ( $this->is_open() ) {
      add_filter( 'dmeng_open_avatar', array( $this, 'open_avatar' ), 10, 3 );
      add_filter( 'dmeng_avatar_radio', array( $this, 'avatar_radio' ), 10, 3 );
      add_filter( 'dmeng_open_loginform', array( $this, 'open_loginform' ), 10, 3 );
      add_filter( 'dmeng_open_loginform_'.$this->connect.'_avatar', array( $this, 'open_avatar' ), 10, 3 );
      add_action( 'dmeng_email_verify_by_code', array( $this, 'email_verify_register' ) );
    }

    if ( $this->connect == $this->current_connect() )
      add_action( 'init', array( $this, 'connect_template' ) );

  }
  
  public function connect() {
    return $this->connect;
  }

  public function name() {
    return __( '开放平台' ,'dmeng' );
  }
  
  public function user_activation_key( $return_value=false ) {

    $key = $this->prefix . 'user_activation_key';
    if ( $return_value==false )
      return $key;
    
    if ( empty($_COOKIE[$key]) )
      return '';
    
    $activation = (array)json_decode( wp_unslash($_COOKIE[$key]), true);

    if ( 
      empty($activation) 
      || empty($activation['connect']) 
      || $activation['connect']!=$this->connect 
      || empty($activation['key']) 
      || empty($activation['nonce']) 
    ) {
      return '';
    }
    
    if ( wp_verify_nonce( $activation['nonce'], $activation['key'] ) )
      return $activation['key'];

    delete_transient( $activation['nonce'] );
    setcookie( $key, ' ', time() - YEAR_IN_SECONDS );
    return '';

  }

  public function openid_field_name( $connect='' ) {
    return 'dmeng_' . ( empty($connect) ? $this->connect : $connect ) . '_openid';
  }
  
  public function token_field_name( $connect='' ) {
    return 'dmeng_' . ( empty($connect) ? $this->connect : $connect ) . '_access_token';
  }

  public function open_loginform( $connect ) {
    if ( $this->connect && $this->name() )
      $connect[] = array(
        'slug' => $this->connect,
        'title' => $this->name(),
        'style' => $this->loginform_style()
      );
    return $connect;
  }

  public function loginform_style() {
    return '';
  }

  public function open_avatar( $avatar, $user_id, $connect ) {
    
    if ( $this->connect == $connect && $this->is_open() ) {
      $U = $this->get_user_info($user_id);
      if ( $U['ID'] && $U['TOKEN'] && $this->avatar_format ) {
        $avatar = str_replace( array( '##CID##', '##UID##' ), array( $this->client_id, $U['ID'] ), $this->avatar_format );
      }
    }

    return $avatar;
  }

  public function avatar_radio( $avatars ) {
    $avatar_url = $this->open_avatar( '', get_current_user_id(), $this->connect );
    if ( $avatar_url ) {
      
      $avatars[$this->connect] = array(
        'name' => $this->name(),
        'img' => $avatar_url
      );

      if ( get_user_meta( get_current_user_id(), 'dmeng_avatar', true )==$this->connect )
        add_filter( 'dmeng_avatar_checked', array( $this, 'connect' ) );
    }
    return $avatars;
  }

  public function current_connect() {
    global $pagenow;
    if ( 
      $pagenow!='wp-login.php' 
      || empty($_GET['connect']) 
      || empty($_GET['action']) 
      || !in_array($_GET['action'], array('login', 'logout')) 
      || ( empty($_GET['nonce']) && empty($_GET['code']) ) 
      || intval(get_option( 'dmeng_open_'.sanitize_key($_GET['connect']) ,1)) !== 1
    ) {
      return false;
    }
    return sanitize_key($_GET['connect']);
  }
  
  public function get_user_info( $user_id=0, $connect='' ) {
    
    $user_id = intval( empty($user_id) ? get_current_user_id() : $user_id );
    $connect = sanitize_key( empty($connect) ? $this->connect : $connect );

    return array(
      'ID' => get_user_meta( $user_id, $this->openid_field_name( $connect ), true ),
      'TOKEN' => get_user_meta( $user_id, $this->token_field_name( $connect ), true )
    );

  }

  public function is_open() {

    if ( intval(get_option( $this->prefix . $this->connect,1)) === 1 && $this->client_id && $this->client_secret )
      return true;

    return false;
  }

  public function connect_redirect_uri() {
    return add_query_arg(
      array(
        'connect' => $this->connect,
        'action' => $this->action,
        'really'=>( !empty($_GET['really']) ? $_GET['really'] : null ),
        'redirect_to'=>( !empty($_GET['redirect_to']) ? urlencode(esc_url($_GET['redirect_to'])) : null ),
      ), wp_login_url()
    );
  }

  public function black_link( $redirect='' ) {
    return  '<a href="'.( empty($redirect) ? dmeng_get_redirect_url(get_edit_profile_url()) : esc_url($redirect) ).'">&laquo; '.__('返回','dmeng').'</a>';
  }

  public function email_verify_register( $data ) {
    
    if ( 
      empty($data) 
      || empty($data['email']) 
      || ! is_email($data['email']) 
      || empty($data['connect']) 
      || empty($data['openid'])
    ) {
      dmeng_die(
        __('链接已失效，请重新获取。', 'dmeng').$this->black_link() );
    }
    
    $code = sanitize_key($_GET['code']);
    $transient = (array)wp_unslash(json_decode(get_transient($code), true));

    global $dmeng_Email;
    delete_transient( $dmeng_Email->verify_field_name($code) );
    
    if (
      empty($transient) 
      || empty($transient['connect']) 
      || $transient['connect'] != $data['connect'] 
      || empty($transient['openid']) 
      || $transient['openid'] != $data['openid'] 
    ) {
      dmeng_die(
        __('链接已失效，请重新获取。', 'dmeng').$this->black_link() );
    }

    $this->connect = $transient['connect'];

    $this->register( array(
      'openid' => $transient['openid'],
      'token' => $transient['token'],
      'name' => $transient['name'],
      'email' => $data['email']
    ) );
    
    die();
  }

  public function connect_template() {

    if ( $this->is_open()===false )
      dmeng_die( __( '此登录方式无效，请选择其他登录方式。', 'dmeng' ), __( '无效登录', 'dmeng' ) );
    
    $connect = sanitize_key($_GET['connect']);
    $action = $_GET['action']=='login' ? $_GET['action'] : 'logout';

    $current_user_id = get_current_user_id();
    
    /**
     * 未登录访问提示
     */
    if ( !$current_user_id && $action=='logout' )
      dmeng_die(
        __( '本功能仅登录用户可用。', 'dmeng' ) . ' <a href="'.wp_login_url().'">'.__('登录','dmeng').' &raquo;</a>'
      );

    if ( !$current_user_id && !empty($_COOKIE[$this->user_activation_key()]) ) {
      if ( !empty($_GET['re']) && $_GET['re']=='key' ) {
        delete_transient( $this->user_activation_key(true) );
        setcookie( $this->user_activation_key(), ' ', time() - YEAR_IN_SECONDS );
      } else {
        $transient_key = $this->user_activation_key(true);
        if ( $transient_key ) {
          $transient = (array)wp_unslash(json_decode(get_transient($transient_key), true));
          if ( !empty($transient) && !empty($transient['openid']) && !empty($transient['token']) && !empty($transient['name']) ) {
            $this->openid = $transient['openid'];
            $this->access_token = $transient['token'];
            $this->open_user_name = $transient['name'];
            $this->email_register_form();
          }
        }
      }
    }

    /**
     * nonce检查提示
     */
    if ( 
      empty($_GET['code'])
      && $current_user_id
      && !wp_verify_nonce( trim($_GET['nonce']), $connect.$action )
    ) {
      dmeng_die(
        '<p>' . __( '当前链接无效，请点击“确认本次操作”以跳转至有效链接。', 'dmeng' ) . '</p><a href="'.dmeng_open_url().'">'.__( '确认本次操作', 'dmeng' ).' &raquo;</a>',
        __( '无效链接', 'dmeng' )
      );
    }
    
    $this->connect = $connect;
    $this->action = $action;

    $user_info = $this->get_user_info( $current_user_id );
    
    $headline = sprintf( __( '绑定新的“%1$s”帐号', 'dmeng' ), $this->name() );
    
    if ( $current_user_id ) {

      $this->openid = $user_info['ID'];
      $this->access_token = $user_info['TOKEN'];
      
      /**
       * 重复绑定
       */
      if ( $this->action=='login' && !empty($this->openid) && !empty($this->access_token) ) {
        dmeng_die(
           '<p>' . sprintf( __('你好，%s'), get_the_author_meta( 'display_name', $current_user_id ) ) .'</p>
           <p><img width="80" src="'.$this->open_avatar( '', $current_user_id, $this->connect ).'" /></p>
           <p>' .sprintf( __( '你已绑定了"%1$s"账户，如要更换，请先%2$s', 'dmeng' ), $this->name(), '<a href="'.dmeng_open_url( $this->connect, 'logout' ).'">'.__('解除绑定','dmeng').'  &raquo; </a>' ) . '</p>' . 
           $this->black_link()
        , $headline );
      }

      if ( $this->action=='logout' )
        $this->logout();

    }

    /**
     * 获取 openid 和 token
     */
    $this->get_auth();

    $id_field = $this->openid_field_name();
    $token_field = $this->token_field_name();

    global $wpdb;
    $user_exists = $wpdb->get_var( "SELECT user_id FROM $wpdb->usermeta WHERE meta_key='$id_field' AND meta_value='$this->openid' " );
    $really_exists = ( isset($user_exists) && intval($user_exists)>0 );

    /**
     * 绑定操作
     */
    if ( $current_user_id ) {

      if ( $really_exists && $user_exists!=$current_user_id ) {
        
        $nonce_key = $user_exists.'to'.$current_user_id;
        
        if ( !wp_verify_nonce( trim(isset($_GET['really']) ? $_GET['really'] : ''), $nonce_key ) ) {
          
          dmeng_die(
            '<p>' . sprintf( __( '本站另一个帐号（%2$s）已经绑定了这个%1$s', 'dmeng' ), $this->name(), '<a href="'.get_author_posts_url( $user_exists ).'" target="_blank">'.get_the_author_meta( 'display_name', $user_exists ).'</a>' ) . '</p>' .
            '<p>'. sprintf( __( '如果你选择继续，将取消原有帐号绑定，并绑定当前帐号（%s）', 'dmeng' ), '<a href="'.get_author_posts_url( $current_user_id ).'" target="_blank">'.get_the_author_meta( 'display_name', $current_user_id ).'</a>' ) .'</p>' . 
            '<p><a href="'.add_query_arg( array( 'really' => wp_create_nonce( $nonce_key ), 'nonce' => wp_create_nonce( $connect.$action ), 'state'=>null, 'code'=>null ) ).'">'.__('确认操作','dmeng').'  &raquo; </a></p>' . 
            $this->black_link()
          , __( '重复绑定', 'dmeng' ) );
          
        } else {
          
          /**
           * 删除原有绑定信息
           */
          delete_user_meta( $user_exists, $id_field );
          delete_user_meta( $user_exists, $token_field );

        }

      }

      update_user_meta( $current_user_id, $id_field, $this->openid );
      update_user_meta( $current_user_id, $token_field, $this->access_token );
      dmeng_die(
        '<p><img width="80" src="'.$this->open_avatar( '', $current_user_id, $this->connect ).'" /></p>
        <p>' .sprintf( __( '你已成功绑定%1$s帐号"%2$s"，此后你可以直接使用此%1$s帐号登录本站。', 'dmeng' ), $this->name(), $this->show_user_name() ) . '</p>' . 
        $this->black_link()
        , __('绑定成功','dmeng') );

    }
    
    /**
     * 老用户登录
     */
    if ( $really_exists )
      $this->login( $user_exists );

    /**
     * 新用户
     */
  
    $user_info = array(
      'connect' => $this->connect,
      'openid' => $this->openid,
      'token' => $this->access_token,
      'name' => $this->show_user_name()
    );

    $transient_key = wp_hash(serialize($user_info));

    set_transient( $transient_key, json_encode($user_info), 3600  );

    if ( setcookie( $this->user_activation_key(), json_encode(array( 'connect'=>$this->connect, 'key'=>$transient_key, 'nonce'=>wp_create_nonce($transient_key) )), current_time('timestamp')+3600 ) ) {
      dmeng_die(
        '<script>location.reload();</script>',
        __( '跳转中…', 'dmeng' )
      );
    }
    
    dmeng_die(
      __( '操作失败，请重试。', 'dmeng' ) . $this->black_link()
    );

  }

  public function get_auth() {

    $state_cookie_key = $this->connect.'_state';
    
    /**
     * 获取识别码
     */
    if ( empty($_GET['code']) ) {
      
      $state = isset($_COOKIE[$state_cookie_key]) ? sanitize_key($_COOKIE[$state_cookie_key]) : '';
      
      if ( empty($state) ) {
        $state = wp_create_nonce(rand());
        setcookie( $state_cookie_key, $state, current_time('timestamp')+600 );
      }

      $this->get_code( $state );
    }

    if ( 
      empty($_GET['state']) 
      || empty($_COOKIE[$state_cookie_key]) 
      || sanitize_key($_GET['state']) != sanitize_key($_COOKIE[$state_cookie_key]) 
    ) {
      dmeng_die(
        '<p>' . __( '登录链接无效，请返回重试。', 'dmeng' ) . '</p>' . $this->black_link( is_user_logged_in() ? '' : remove_query_arg('code') ),
        __( '无效链接', 'dmeng' )
      );
    }
    
    setcookie( $state_cookie_key, ' ', time() - YEAR_IN_SECONDS );

    /**
     * 获取令牌
     */
    $this->get_token();

  }
  
  public function get_code( $state ) {
    dmeng_die(
      "<script> top.location.href='" . add_query_arg( array( 'code'=>wp_create_nonce(rand()), 'state'=>$state ) ) . "'</script>",
      __( '跳转中…', 'dmeng' )
    );
  }
  
  public function get_token() {}
  
  public function show_user() {}
  
  public function show_user_name() {}
  
  public function email_register_form() {

    dmeng_die(
      '<p>'. sprintf( __( '欢迎%1$s用户”%2$s“登录本站，请验证一个邮箱以完成注册。', 'dmeng' ), $this->name(), $this->open_user_name ).'</p>'.
      '<p><img width="80" src="'.str_replace( array( '##CID##', '##UID##' ), array( $this->client_id, $this->openid ), $this->avatar_format ).'" /><br><span class="small"><a href="'.add_query_arg( array( 're'=>'key' ), dmeng_open_url( $this->connect ) ).'">'. sprintf( __('更新%s授权信息','dmeng'), $this->name() ) .' &raquo;</a></span></p>'.
      '<p class="danger">' . ( isset($_POST['email']) ? dmeng_verify_mail( $_POST['email'], $this->user_activation_key(true), array( 'connect'=>$this->connect, 'openid'=>$this->openid ) ) : __( '请输入你的密保邮箱：', 'dmeng' ) ) . '</p>' .
      '<form method="post"><p><input type="text" class="button" name="email" value="'.( isset($_POST['email']) ? esc_attr($_POST['email']) : '' ).'" > <input type="submit" class="button" value="'.__( '发送验证邮件', 'dmeng' ).'" /></p></form>'. 
      '<br><p class="small">'.__( '提示：绑定已有帐号请先登录。', 'dmeng' ).'<a href="'.wp_login_url().'">'.__('登录本站','dmeng').' &raquo;</a></p>'
      ,  __( '新用户注册', 'dmeng' )
    );
    
  }

  public function register( $data ) {
    
    if ( empty($data['openid']) || empty($data['token']) || empty($data['name']) )
      dmeng_die(
        __( '用户信息不足，无法注册！', 'dmeng' )
      );

    $this->openid = $data['openid'];
    $this->access_token = $data['token'];

    $insert_user_id = wp_insert_user( array(
      'user_login'  => wp_hash( $data['openid'] . $data['token'] ),
      'user_email'  => $data['email'],
      'nickname'  => $data['name'],
      'display_name'  => $data['name'],
      'user_pass' => wp_generate_password(),
      'role' => get_option( 'dmeng_open_role', 'contributor' )
    ) );
    
    if ( is_wp_error($insert_user_id) )
      dmeng_die(
        '<p>' . __( '新用户注册失败，请重试或报告管理员。', 'dmeng' ) . '</p><p>' . $insert_user_id->get_error_message() . '</p>' . $this->black_link()
      );
      
    $new_user_info = array(
      'ID' => $insert_user_id
    );
    
    $new_user_login = 'u'.$insert_user_id;
    if ( !username_exists( $new_user_login ) ) {
      global $wpdb;
      $wpdb->update( $wpdb->users, array( 'user_login' => $new_user_login ), array( 'ID' => $insert_user_id ) );
      $new_user_info['user_nicename'] = $new_user_login;
    }

    wp_update_user( $new_user_info );
    update_user_meta( $insert_user_id , 'dmeng_avatar', $this->connect );

    $this->login( $insert_user_id );
  }

  public function login( $user_id=0 ) {
    
    $user_id = intval($user_id);
    $user = get_user_by( 'id', $user_id ); 

    if ( is_wp_error( $user ) )
      dmeng_die(
        '<p>' . __( '登录失败，请重试或通知管理员。', 'dmeng' ) . '</p>' . 
        '<p>' . $user->get_error_message() . '</p>' . 
        $this->black_link(),
        __( '无法获取用户信息', 'dmeng' )
      );

    setcookie( $this->user_activation_key(), ' ', time() - YEAR_IN_SECONDS );
      
    if ( $this->openid )
      update_user_meta( $user->ID, $this->openid_field_name(), $this->openid );
    if ( $this->access_token )
      update_user_meta( $user->ID, $this->token_field_name(), $this->access_token );

    wp_set_current_user( $user->ID, $user->user_login );
    wp_set_auth_cookie( $user->ID, absint(get_option('dmeng_open_remember_the_user',1)) );
    do_action( 'wp_login', $user->user_login, $user );

    header( 'Location:' . dmeng_get_redirect_url(get_edit_profile_url()) );
    exit;
  }

  public function logout() {
    
    $current_user_id = get_current_user_id();
    $user_name = get_the_author_meta( 'display_name', $current_user_id );

    $headline = sprintf( __( '解除“%1$s”帐号绑定', 'dmeng' ), $this->name() );
    
    /**
     * 没有绑定
     */
    if ( empty($this->openid) || empty($this->access_token) ) {
      dmeng_die(
        '<p>' . sprintf( __( '咦？%s，你怎么会在这里呢？', 'dmeng' ), $user_name ) . '</p>' .
        '<p>' . sprintf( __( '你没有绑定“%s”帐号！如果你想绑定，请点这里', 'dmeng' ), $this->name() ) . ' &raquo; <a href="'.dmeng_open_url().'">'.__( '绑定', 'dmeng' ).'</a></p>' .
        '<p>' . __( '迷路了就点击返回', 'dmeng' ) . '</p>' .
        $this->black_link()
      , $headline );
    }

    $hello = '<p>' . sprintf( __('你好，%s'), $user_name ) .'</p>';
    $open_avatar = '<p><img width="80" src="'.$this->open_avatar( '', $current_user_id, $this->connect ).'" /></p>';
    
    /**
     * 解除绑定
     */
    if ( empty($_GET['really']) || !wp_verify_nonce( trim($_GET['really']), 'of course' ) ) {
      dmeng_die(
        $hello . $open_avatar . '<p>' . sprintf( __('你确定解除“%s”绑定吗？','dmeng'), $this->name() ) . '<a href="'.add_query_arg( 'really', wp_create_nonce( 'of course' ) ).'">'.__('确认操作','dmeng').'  &raquo; </a>' . '</p>' . $this->black_link(),
        __('解除账号绑定','dmeng')
      , $headline );
    }
  
    delete_user_meta( $current_user_id, $this->openid_field_name() );
    delete_user_meta( $current_user_id, $this->token_field_name() );
    
    $this->after_logout();

    dmeng_die(
      $hello . '<p>' . sprintf( __('你已成功解除“%s”绑定。','dmeng'), $this->name() ) . '</p>' . $this->black_link(),
      __('解除账号绑定','dmeng')
    , $headline );
  }

  public function after_logout() {
  }

  public function remote_retrieve_body( $response ) {
    
    if ( is_wp_error($response) || empty($response['body']) )
      dmeng_die(
        __( '无法连接开放平台，请重试或联系管理员！', 'dmeng' ) . ( WP_DEBUG ? '<br>'. $response->get_error_message() : '' ),
        __( '获取信息失败', 'dmeng' )
      );
    
    return $response['body'];
  }

}

class dmeng_Open_connect_qq extends dmeng_Open_connect {
  
  public $connect = 'qq';

  public $client_id_key = 'qq_id';
  public $client_secret_key = 'qq_key';
  
  public $avatar_format = 'http://q.qlogo.cn/qqapp/##CID##/##UID##/100';
  
  public function __construct() {
    
    if ((int)get_option('dmeng_open_qq')===0)
      return;

    parent::__construct();
  }
  
  public function name() {
    return __( 'QQ' ,'dmeng' );
  }
  
  public function loginform_style() {
    return 'color:#fff;background:#5ca3df;';
  }
  
  public function get_code( $state ) {
    dmeng_die(
      "<script> top.location.href='https://graph.qq.com/oauth2.0/authorize?".http_build_query(array(
          'response_type' => 'code',
          'client_id' => $this->client_id,
          'state' => $state,
          'scope' => 'get_user_info,get_info,add_t,del_t,add_pic_t,get_repost_list,get_other_info,get_fanslist,get_idollist,add_idol,del_idol',
          'redirect_uri' => $this->connect_redirect_uri()
      )). "'</script>",
      __( '跳转中…', 'dmeng' )
    );
  }
  
  public function get_token() {
    
    $response = $this->remote_retrieve_body(wp_remote_get( 'https://graph.qq.com/oauth2.0/token?' . http_build_query(array(
        'grant_type' => 'authorization_code',
        'client_id' => $this->client_id,
        'client_secret' => $this->client_secret,
        'code' => $_GET['code'],
        'redirect_uri' => $this->connect_redirect_uri()
      ))
    ));

     if ( strpos($response, 'callback' ) !== false ) {
      $lpos = strpos( $response, '(' );
      $rpos = strrpos( $response, ')' );
      $response  = substr( $response, $lpos + 1, $rpos - $lpos -1 );
      $msg = json_decode( $response );
      if ( isset($msg->error) ) {
        dmeng_die(
          '<p>'.__( 'QQ Token 获取失败，请重试或报告管理员', 'dmeng' ).'</p><h3>error : </h3>' . $msg->error . ' <h3>msg  : </h3>' . $msg->error_description,
          __( '获取信息有误', 'dmeng' )
        );
      }
     }

    $params = array();
    parse_str( $response, $params );

    $str = $this->remote_retrieve_body(wp_remote_get( 'https://graph.qq.com/oauth2.0/me?access_token=' . $params['access_token'] ));

    if ( strpos($str, 'callback') !== false ) {
      $lpos = strpos($str, '(');
      $rpos = strrpos($str, ')');
      $str  = substr($str, $lpos + 1, $rpos - $lpos -1);
    }

    $user = json_decode($str);
     
    if ( isset($user->error) )
      dmeng_die(
        '<p>'.__( 'QQ openid 获取失败，请重试或报告管理员', 'dmeng' ).'</p><h3>error : </h3>' . $user->error . ' <h3>msg  : </h3>' . $user->error_description,
        __( '获取信息有误', 'dmeng' )
      );

    $this->openid = $user->openid;
    $this->access_token = $params['access_token'];

  }
  
  public function show_user() {
    
    $user_info = $this->remote_retrieve_body(wp_remote_get( 'https://graph.qq.com/user/get_user_info?access_token='.$this->access_token.'&oauth_consumer_key='.$this->client_id.'&openid='.$this->openid ));
    
    $user_info = (array)json_decode($user_info, true);
  
    if ( isset($user_info->ret) )
      dmeng_die(
        '<p>' . sprintf( __( '获取%s用户信息出错，请重试或报告管理员。', 'dmeng' ), $this->name() ) . '</p><h3>error : </h3>' . $user_info->ret . ' <h3>msg  : </h3>' . $user_info->msg
      );
    
    return $user_info;

  }
  
  public function show_user_name() {
    $user_info = $this->show_user();
    return $user_info['nickname'];
  }
  
}
$dmeng_Open_connect_qq = new dmeng_Open_connect_qq;

class dmeng_Open_connect_weibo extends dmeng_Open_connect {
  
  public $connect = 'weibo';
  
  public $client_id_key = 'weibo_key';
  public $client_secret_key = 'weibo_secret';
  
  public $avatar_format = 'http://tp1.sinaimg.cn/##UID##/180/1.jpg';

  public function __construct() {

    if ((int)get_option('dmeng_open_weibo')===0)
      return;

    parent::__construct();
  }
  
  public function name() {
    return __( '微博' ,'dmeng' );
  }

  public function loginform_style() {
    return 'color:#fff;background:#de604f;';
  }
  
  public function get_code( $state ) {
    dmeng_die(
      "<script> top.location.href='https://api.weibo.com/oauth2/authorize?".http_build_query(array(
          'response_type' => 'code',
          'client_id' => $this->client_id,
          'state' => $state,
          'redirect_uri' => $this->connect_redirect_uri()
      )). "'</script>",
      __( '跳转中…', 'dmeng' )
    );
  }
  
  public function get_token() {
    
    $response = $this->remote_retrieve_body(wp_remote_post( 'https://api.weibo.com/oauth2/access_token?', array( 'body' => array(
      'grant_type' => 'authorization_code',
      'client_id' => $this->client_id,
      'client_secret' => $this->client_secret,
      'code' => $_GET['code'],
      'redirect_uri' => $this->connect_redirect_uri()
    )) ));
    
    $response = (array)json_decode($response,true);
      
    if ( isset($response['error']) )
      dmeng_die(
        '<p>'.__( '微博授权信息获取失败，请重试或报告管理员', 'dmeng' ).'</p><h3>error : </h3>' . $response['error'] . ' <h3>msg  : </h3>' . $response['error_description'],
        __( '获取信息有误', 'dmeng' )
      );

    $this->openid = $response['uid'];
    $this->access_token = $response['access_token'];
    
  }
  
  public function show_user() {
    
    $user_info = $this->remote_retrieve_body(wp_remote_get( 'https://api.weibo.com/2/users/show.json?access_token='.$this->access_token.'&uid='.$this->openid));
    
    $user_info = (array)json_decode($user_info, true);
  
    if ( isset($user_info['error']) )
      dmeng_die(
        '<p>' . sprintf( __( '获取%s用户信息出错，请重试或报告管理员。', 'dmeng' ), $this->name() ) . '</p><h3>error : </h3>' . $user_info['error_code'] . ' <h3>msg  : </h3>' . $user_info['error']
      );
    
    return $user_info;

  }
  
  public function show_user_name() {
    $user_info = $this->show_user();
    return $user_info['name'];
  }
  
  public function after_logout() {
    /**
     * 取消绑定后主动取消用户的授权
     */
    return wp_remote_retrieve_body(wp_remote_get('https://api.weibo.com/oauth2/revokeoauth2?access_token='.$this->access_token));
  }

}
$dmeng_Open_connect_weibo = new dmeng_Open_connect_weibo;
