<?php

class dmeng_Author_Template {
  
  public function __construct() {
    add_filter( 'dmeng_author_main_notice', array( $this, 'notice_selection' ) );
    add_action( 'dmeng_author_main_content', array( $this, 'main_selection' ) );
    add_action( 'dmeng_author_main_footer', array( $this, 'main_footer' ) );
    
    add_action( 'wp_ajax_dmeng_get_comment', array( $this, 'get_comment_ajax_action' ) );
    add_action( 'wp_ajax_nopriv_dmeng_get_comment', array( $this, 'get_comment_ajax_action' ) );    
    
    add_action( 'wp_ajax_dmeng_get_like', array( $this, 'get_like_ajax_action' ) );
    add_action( 'wp_ajax_nopriv_dmeng_get_like', array( $this, 'get_like_ajax_action' ) );
  }

  public function tabs() {
    return apply_filters('dmeng_author_template_tabs', array(
        'post' => __('文章', 'dmeng'),
        'comment' => __('评论', 'dmeng'),
        'like' => __('赞', 'dmeng')
    ));
  }
  
  public function tab_keys() {
    return array_keys($this->tabs());
  }
  
  public function get_tab() {
    return isset($_GET['tab'])&&in_array($_GET['tab'], $this->tab_keys()) ? $_GET['tab'] : 'post';
  }  

  public function notice_selection( $notice ) {
    global $tab;
    if ( 'post'===$tab ) {
      return $this->notice_post();
    }
    return $notice;
  }
  
  public function notice_post() {
    global $wp_query;
    if ($wp_query->found_posts===0)
      return __('还没有发表过文章', 'dmeng');
    return sprintf( __('发表了 %s 篇文章', 'dmeng'), dmeng_count_num($wp_query->found_posts) );
  }
  
  public function notice_comment() {
    $count = $GLOBALS['dmeng_Count']->comment(0, $GLOBALS['curauth']->ID);
    if ($count===0)
      return __('还没有发表过评论', 'dmeng');
    return sprintf( __('发表了 %s 条评论', 'dmeng'), dmeng_count_num($count) );
  }
  
  public function notice_like() {
    $vote = $GLOBALS['dmeng_Count']->user_vote($GLOBALS['curauth']->ID, '', 'up');
    $vote = array_filter(array_map('array_filter', $vote));
    if ( empty($vote) )
      return __('还没有赞过', 'dmeng');
    
    if ( empty($vote['post']['up']) || empty($vote['comment']['up']) )
      return sprintf(__('共有 %s 次赞','dmeng'), $vote['post']['up']+$vote['comment']['up']);

    return sprintf(__('共有 %1$s 次赞，其中包括 %2$s 篇文章和 %3$s 条评论。','dmeng'), $vote['post']['up']+$vote['comment']['up'], $vote['post']['up'], $vote['comment']['up']);
  }

  public function main_selection() {
    global $tab;
    if ('post'===$tab) {
      while ( have_posts() ) : the_post();
        get_template_part( 'content', 'archive' );
      endwhile;
      return;
    }

    ?>
<script>
var dmengUcenter;
!function($){
  'use strict';
  dmengUcenter = {
      $tips : $('#tips'), 
      user_id : <?php echo $GLOBALS['curauth']->ID;?>, 
      paged : 1, 
      loading : false,
      load : function(){},
      autoLoad : function() {
        var loading = dmengUcenter.loading;
        if (loading) {
          return;
        }
        loading = true;
        dmengUcenter.loaded = $.ajax({
          type: 'POST',
          url: dmeng.ajaxurl,
          data: {
            action: 'dmeng_get_<?php echo $tab;?>',
            user_id: dmengUcenter.user_id,
            paged: dmengUcenter.paged,
            _wpnonce: dmeng._wpnonce
          },
          complete: function (response) {
            loading = false;
          }
        });
      },
      next : function () {
        dmengUcenter.paged++;
        dmengUcenter.load();
      },
      prev : function () {
        dmengUcenter.paged--;
        dmengUcenter.load();
      },
      selectPaged : function () {
        if (dmengUcenter.loading) {
          return false;
        }
        dmengUcenter.paged = $(this).find(":selected").val();
        dmengUcenter.load();
        return false;
      },
      errorTips : function(text, default_text) {
        default_text = default_text ? default_text : dmengUcenter.$tips.html();
        dmengUcenter.$tips.addClass('text-danger').html(text).fadeIn('normal', function(){
          setTimeout(function(){
            dmengUcenter.$tips.removeClass('text-danger').html(default_text);
          }, 3000);
        });
        return false;
      }
  };
}(jQuery);
</script>
    <?php
    if (in_array($tab, array('comment', 'like'))===false)
      return;
    ?>

<ul id="uclist" class="noborder" style="display:none"></ul>

<script>
!function($){
  'use strict';
  var $tips = dmengUcenter.$tips,
      firstLoad = true;
      dmengUcenter.load = function () {
        if (firstLoad===false) {
          dmeng.goHash('#uclist');
          $('#uclist').html('<li><?php _e('加载中…','dmeng'); ?></li>');
        }
        dmengUcenter.autoLoad();
        dmengUcenter.loaded.success(function (response) {
          var $uclist =  $('#uclist');
          if (typeof response==='object') {
            $tips.html(response.tips);
            if (response.data) {
              $uclist.html(response.data).fadeIn();
            }
          } else {
            $uclist.html(response).fadeIn();
          }
          firstLoad = false;
        });

      };
  $(document).ready(function () {
    dmeng.readyAjax.complete(function () {
      if (dmeng.user.ID===dmengUcenter.user_id) {
        $tips.html('<?php _e('查看自己的记录请到用户中心。', 'dmeng');?><a href="'+dmeng.ucenter.url_format.replace( '%s', '<?php echo $tab;?>' )+'"><?php _e('点击查看','dmeng');?></a>').fadeIn();
        return;
      }
      dmengUcenter.load();
    });
  }).on('click', '#get_prev', dmengUcenter.prev
  ).on('click', '#get_next', dmengUcenter.next
  ).on('change', '#paginate', dmengUcenter.selectPaged
  );
}(jQuery);
</script>
    <?php
  }

  public function get_comment_ajax_action() {
    do_action( 'dmeng_before_ajax' );
    
    if ( empty($_POST['user_id']) )
      die( '<li>'.__('用户ID不能为空', 'dmeng').'</li>' );

    $user_id = absint($_POST['user_id']);

    global $dmeng_Count;
    $count = $dmeng_Count->comment(0, $user_id);

    $result = array();
    $result['tips'] = sprintf( __('共发表了 %s 条评论','dmeng'), $count );

    $paged = empty($_POST['paged']) ? 1 : absint($_POST['paged']);
    $number = 10;
    $offset = ($paged-1)*$number;
    
    $comments = get_comments(array(
      'status' => '',
      'order' => 'DESC',
      'number' => $number,
      'offset' => $offset,
      'user_id' => $user_id
    ));
    
    if ($comments) {
      $result['data'] = '';
      foreach ($comments as $comment) {
        $reply = '';
        if ( $comment->comment_parent ) {
          $parent = get_comment($comment->comment_parent);
          if ( $parent->user_id!=$comment->user_id )
            $reply = '<span class="info">' . sprintf( __( '回复%s：', 'dmeng' ), dmeng_auto_comment_author_name($parent) ) . '</span>';
        }
        $result['data'] .= '<li><div class="text">' . $reply . get_comment_text($comment->comment_ID) . '</div>'.
                ( $comment->comment_approved!=1 ? '<span class="status pending">'.__( '待审', 'dmeng' ).'</span>' : '' ) . 
                '<a class="info" href="'.htmlspecialchars( get_comment_link( $comment->comment_ID) ).'" target="_blank">'.sprintf(__('%1$s 发表在 %2$s','dmeng'),$comment->comment_date,get_the_title($comment->comment_post_ID)).'</a></li>';
      }
      ob_start();
      global $dmeng_UCenter;
      $dmeng_UCenter->setting_paginate( ceil($count/$number), $paged );
      $result['data'] .= ob_get_contents();
      ob_end_clean();
    }
    
    wp_send_json($result);
  }
  
  public function get_like_ajax_action() {
    do_action( 'dmeng_before_ajax' );
    
    if ( empty($_POST['user_id']) )
      die( '<li>'.__('用户ID不能为空', 'dmeng').'</li>' );

    $user_id = absint($_POST['user_id']);

    $paged = empty($_POST['paged']) ? 1 : absint($_POST['paged']);
    $number = 10;
    $up = dmeng_get_user_like( $user_id, $number, $paged );
    $count = $up['posts_count']+$up['comments_count'];
    $up_data = $up['data'];

    $result = array();
    $result['tips'] = sprintf(__('赞了 %1$s 次，其中包括 %2$s 篇文章， %3$s 条评论。','dmeng'), $count, $up['posts_count'], $up['comments_count']);

    if ($up_data) {
      
      $result['data'] = '';
      
      foreach( $up_data as $up_term ){
        $result['data'] .= '<li><div class="text">'.$up_term['excerpt'].'</div><div class="info">['.$up_term['type_label'].']<a class="up_title" href="'.$up_term['url'].'" target="_blank">'.$up_term['title'].'</a> <span class="glyphicon glyphicon-thumbs-up"></span>'.intval(get_metadata($up_term['type'], $up_term['id'], 'dmeng_votes_up', true)).'<span class="glyphicon glyphicon-thumbs-down"></span>'.intval(get_metadata($up_term['type'], $up_term['id'], 'dmeng_votes_down', true)).'</div></li>';
      }
      
      ob_start();
      global $dmeng_UCenter;
      $dmeng_UCenter->setting_paginate( ceil($count/$number), $paged );
      $result['data'] .= ob_get_contents();
      ob_end_clean();
    }

    wp_send_json($result);
  }

  public function main_footer() {
    global $tab;
    if ($tab!=='post')
      return;
    dmeng_paginate();
  }

}

$dmeng_Author_Template = new dmeng_Author_Template;