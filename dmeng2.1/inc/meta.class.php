<?php

/**
 * 
 */

class dmeng_Meta {
  
  public function __construct() {
    add_action( 'load-themes.php', array( $this, 'install' ) );
    add_action( 'delete_user', array( $this, 'delete_user_action' ) );
  }
  
  /**
   * 数据库表格名称
   */
  public $table_name = 'dmeng_meta';   

  /**
   * 启用主题时创建数据库表格
   * https://codex.wordpress.org/Plugin_API/Action_Reference/load-themes.php
   */
  public function install() {
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    dmeng_create_db_table(
      $this->table_name,
      " CREATE TABLE `$this->table_name` (
        `meta_id` int NOT NULL AUTO_INCREMENT, 
        `user_id` int,
        `meta_key` tinytext,
        `meta_value` tinytext,
        PRIMARY KEY(user_id),
        KEY meta_key (meta_key(191))
      ) $charset_collate;"
    );
  }

  /**
   * 计数
   */
  public function count( $key, $value=0, $uid='all' ) {

    $key = sanitize_text_field($key);
    $value = sanitize_text_field($value);
    if ( $uid!='all' )
      $uid = absint($uid);
    
    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;

    $sql = "SELECT count(meta_id) FROM $table_name WHERE meta_key LIKE '$key'";
    if ( $value )
      $sql .= " AND meta_value LIKE '$value'";
    if ( is_int($uid) )
      $sql .= " AND user_id = '$uid'";

    $check = $wpdb->get_var($sql);

    if ( isset($check) )
      return absint($check);
      
    return 0;
  }

  public function get_var( $key , $uid=0 ) {

    $key = sanitize_text_field($key);
    $uid = absint($uid);
    $cache_key = $key.'-'.$uid;
    
    $cache = wp_cache_get( $cache_key, $this->table_name );
    if ( false !== $cache )
      return $cache;
      
    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;
    
    $check = $wpdb->get_var( "SELECT meta_value FROM $table_name WHERE meta_key='$key' AND user_id='$uid'" );
    
    $result = isset($check) ? $check : 0;
    
    wp_cache_set( $cache_key, $result, $this->table_name );

    return $result;
  }

  public function get( $key , $uid=0, $limit=0, $offset=0 ) {

    $key = sanitize_text_field($key);
    $uid = absint($uid);
    
    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;
    
    $sql = "SELECT user_id, meta_value FROM $table_name WHERE meta_key='$key'";
    if ( $uid )
      $sql .= " AND user_id='$uid'";
    
    $sql .= " ORDER BY meta_id DESC";
    
    if($limit) $sql .= " LIMIT $offset,$limit";
    
    $check = $wpdb->get_results( $sql );

    return isset($check) ? $check : 0;
  }

  public function exists($uid, $key, $value) {
    
    $key = sanitize_text_field($key);
    $value = sanitize_text_field($value);
    $uid = absint($uid);
    
    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;
    
    $check = $wpdb->get_var( "SELECT meta_id FROM $table_name WHERE user_id='$uid' AND meta_key='$key' AND meta_value='$value'" );

    return isset($check) ? $check : 0;

  }

  /**
   * 添加meta
   */
  public function add( $key, $value, $uid=0 ) {

    $key = sanitize_text_field($key);
    $value = sanitize_text_field($value);
    $uid = absint($uid);
    
    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;

    if ( $wpdb->query( "INSERT INTO $table_name (user_id,meta_key,meta_value) VALUES ('$uid', '$key', '$value')" ) )
      return 1;
    
    return 0;
  }

  /**
   * 更新meta
   */
  public function update( $key, $value='', $uid=0 ) {

    $key = sanitize_text_field($key);
    $value = sanitize_text_field($value);
    $uid = absint($uid);
    $cache_key = $key . '-' . $uid;

    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;
    
    $check = $wpdb->get_var( "SELECT meta_id FROM $table_name WHERE user_id='$uid' AND meta_key='$key'" );
    if ( isset($check) ) {
      /**
       * 更新字段并更新缓存
       */
      if ( $wpdb->query( "UPDATE $table_name SET meta_value='$value' WHERE meta_id='$check'" ) ) {
        wp_cache_replace( $cache_key, $value, $this->table_name );
        return $check;
      }
    }else{
      /**
       * 插入字段并添加缓存
       */
      if ( $wpdb->query( "INSERT INTO $table_name (user_id,meta_key,meta_value) VALUES ('$uid', '$key', '$value')" ) ) {
        wp_cache_set( $cache_key, $value, $this->table_name );
        return 'inserted';
      }
    }

    return 0;
  }

  /**
   * 删除meta
   */
  public function delete( $key, $value=0, $uid='all' ) {

    $key = sanitize_text_field($key);
    $value = sanitize_text_field($value);
    if ( $uid!='all' )
      $uid = absint($uid);
    
    $cache_key = $key . '-' . $uid;
    
    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;

    $where = " WHERE meta_key='$key'";
    if ($value)
      $where .= " AND meta_value='$value'";
    if ( is_int($uid) )
      $where .= " AND user_id='$uid'";
    
    if ( $wpdb->get_var( "SELECT meta_id FROM $table_name".$where ) ) {
      if ( $wpdb->query( "DELETE FROM $table_name".$where ) )
        return wp_cache_delete( $cache_key, $this->table_name );
    }
    
    return false;
  }

  public function delete_user_action( $user_id ) {

    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;
    
    $key = 'vote_%';
    /**
     * 投票数据改为游客投票
     */
    $wpdb->query( " UPDATE $table_name SET user_id = 0 WHERE meta_key LIKE '$key' AND user_id='$user_id' " );
    
    /**
     * 删除非投票数据，如礼品兑换信息
     */
    $wpdb->query( " DELETE FROM $table_name WHERE meta_key NOT LIKE '$key' AND user_id='$user_id' " );
  }

}

$dmeng_Meta = new dmeng_Meta;
