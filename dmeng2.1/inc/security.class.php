<?php

/**
 * Security
 */

class dmeng_Security {
  
  public function __construct() {
    
    add_filter('prepend_attachment', array($this, 'private_attachment') );
    add_filter('attachment_fields_to_edit', array($this, 'add_attachment_field'), 10, 2);
    add_filter('attachment_fields_to_save', array($this, 'save_attachment_field'), 10, 2);
    
    add_filter('sanitize_option_dmeng_black_list', array($this, 'sanitize_black_list'), 10, 2);

    add_action('wp_authenticate', array($this, 'check_authentication') );
    add_action('register_post', array($this, 'check_authentication') );

    global $pagenow;;
    if ('wp-login.php'==$pagenow && $this->maybe_auto_RSA()) {
      add_action( 'login_enqueue_scripts', array( $this, 'need_scripts' ) );
      add_action( 'login_init', array( $this, 'login_pwd_maybe_RSA' ) );
    }
  }
  
  public function private_attachment( $p ) {
    if( post_password_required() )
      $p = '';
    return $p;
  }

  public function add_attachment_field( $form_fields, $post ) {
    $field_value = $post->post_password;
    $form_fields['post_password'] = array(
      'value' => $field_value ? $field_value : '',
      'label' => __( '密码', 'dmeng' ),
      'helps' => __( '设置密码访问，无需加密留空即可' )
    );
    return $form_fields;
  }

  public function save_attachment_field( $post, $attachment ) {
    if ( isset( $attachment['post_password'] ) ) {
      $post['post_password'] = $attachment['post_password'];
    }
    return $post;
  }

  /**
   * 黑名单更新时过滤好格式
   */
  public function sanitize_black_list($text) {
    $sep = '|';
    if (strpos($text, $sep)) {
      $new = array();
      foreach (explode($sep, $text) as $key) {
        $key = trim($key);
        if (!empty($key))
          $new[] = $key;
      }
      $text = join($sep, $new);
    }
    return $text;
  }

  /**
   * 用户名黑名单检查
   */
  public function check_blacklist( $user_login ) {
    $black_list = array_filter( explode( '|', trim(get_option('dmeng_black_list'), '|' ) ) );
    if ( !empty($black_list) && in_array($user_login, $black_list) )
      return true;

    return false;
  }
  
  public function check_authentication( $user_login ) {
    if ( $this->check_blacklist($user_login) ) {
      dmeng_die( '<p>' . sprintf( __( '该%s已被系统保留，不能用于注册或登录！', 'dmeng' ), ( is_email($user_login) ? __( '邮箱地址', 'dmeng' ) : __( '用户名', 'dmeng' ) ) ) . "</p><p><a href='javascript:history.back()'>".__('&laquo; Back')."</a></p>" );
    }
  }

  public function maybe_auto_RSA() {
    return false===is_ssl() && function_exists('openssl_private_decrypt');
  }
  
  /**
   * RSA公钥
   */
  public function RSA_public_key($html=false) {
    $key = get_option('dmeng_rsa_public_key', 
'
-----BEGIN PUBLIC KEY-----
MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgGUFM3v4Fi7sSs8FHWkYjctMZu6e
B6gjiMgCl4AqPi0/+qYqEBIuVDB2DXFao3XQCNR7exfn1swYoto7z5gm+OoussUi
PKiINfMNH00WJETPb1z7PY3FyBHPOl4Aylsrvr8ipiEsCPBhoyvG/uRupPrls1lZ
va+4b/pwpZRQUsTbAgMBAAE=
-----END PUBLIC KEY-----
'
    );
    return $html ? '<textarea style="display:none" id="RSA_PUBLIC_KEY">'.$key.'</textarea>' : $key;
  }

  /**
   * RSA私钥
   */
  public function RSA_private_key($html=false) {
    $key = get_option('dmeng_rsa_private_key', 
'
-----BEGIN RSA PRIVATE KEY-----
MIICWgIBAAKBgGUFM3v4Fi7sSs8FHWkYjctMZu6eB6gjiMgCl4AqPi0/+qYqEBIu
VDB2DXFao3XQCNR7exfn1swYoto7z5gm+OoussUiPKiINfMNH00WJETPb1z7PY3F
yBHPOl4Aylsrvr8ipiEsCPBhoyvG/uRupPrls1lZva+4b/pwpZRQUsTbAgMBAAEC
gYAJVVSvZHlwNmam/9r6xcj7+oAcFb+xWi8eA5dJwR1jguC+j25Sr7MZkx6k1XH6
xZC7SPlTeRkQXEpXjWUdsqQWqSJl7+SPrFMWb4Bop1feavEFTmSNbO1w4LhFUfdZ
MHb6jaHWS9H5C5+mpTxQxMRluHTKR0mpf94VwA2TTMphgQJBAKdwm8fcLAQo29qf
cEsb8+RFU5/TUxBnsixQdQW39FmdHF3FjhcZMjhjQ8LkHWbQJ/ni89KXRcz0ppMS
K32RVkECQQCac18T1ZNZ84Wa+kvUGbPxvb/Q9PRrmuVmkzx40u8i9kG2cY15IGlo
mLks8qeHdwc6AkE/yRIKneLDWVhnS6wbAkAm9vKZkOgBU8zN1HqaT/fysBtkvxW+
ZJM28sY4vXU0gXY/cCC/yVJBgwAa02DwMxKKeUEKSb+pkWlOIlVSlEbBAkAnzyzm
c9ozCS2asIDy/lz/JiVWuox1X8tZEQI5MkUpAcgWOlZ4rf39U8AG8dR0eh2GuiYP
Rsg7rE5Sg6jhlwOZAkAyOL5djk5OGCqZUPurwkah7IRAzBxDW8AMIUNPXKT1uHpR
uunDPfQwD9Bnb67BItGuCCU+goypt/pU0TFMpSCB
-----END RSA PRIVATE KEY-----
'
    );
    return $html ? '<textarea style="display:none" id="RSA_PRIVATE_KEY">'.$key.'</textarea>' : $key;
  }

  /**
   * 在RSA加密信息前加的一个验证码，根据这个判断是否需要RSA解密
   */
  public function RSA_text_prefix() {
    static $nonce;
    if (false===isset($nonce)) {
      $nonce = wp_create_nonce('RSA').'/';
    }
    return $nonce;
  }

  public function need_scripts() {
    wp_enqueue_script( 'jquery' );
?>
<!-- 
  非SSL访问在发送POST数据之前使用非对称加密算法加密密码明文
  同样质量的网站，Google会优先考虑HTTPS站点，有条件的话使用HTTPS也是不错的选择
  多梦 2015/8/30
-->
<style type="text/css">
#loginform{position:relative}
#loginform:before,#rsa_notice{display:block;content:'Loading RSA';padding:30px;text-align:center;font-size:20px;line-height:42px;font-family:Microsoft YaHei;font-weight:100;position:absolute;background:#fff;border-top:1px solid #dd3d36;color:#dd3d36;top:0;left:0;right:0;bottom:0}
#loginform.ok:before{display:none}
#rsa_notice{position:absolute;top:0;left:0;right:0;bottom:0;content:''}
#rsa_notice.ok{border:0;border-bottom:1px solid #009a61;color:#009a61;background:rgba(255,255,255,.5);font-size:15px;position:fixed;top:0;bottom:auto;padding:0}
</style>
<?php echo $this->RSA_public_key(true);?>
<script>
window.addEventListener('load', function(){
  (function($) {
    var form_id = '#loginform', user_login = '#user_login', notice_id = '#rsa_notice';
    $(form_id).append($('<div />').attr('id', notice_id.replace('#','') ).html('Loading RSA')).addClass('ok');
    $(notice_id).append('<br><?php _e('正在强制启用加密模式…', 'dmeng');?>')
    $.getScript('<?php echo dmeng_jsencrypt_uri();?>', function(){
      var public_key = $('#RSA_PUBLIC_KEY').val(),
          crypt = new JSEncrypt();
      if (false===crypt.encrypt('test')) {
        $(notice_id).html('<br><?php _e('加密程序启用异常', 'dmeng');?><br><?php _e('请刷新重试', 'dmeng');?>');
        return;
      }
      $(notice_id).append('<br><?php _e('程序已准备就绪', 'dmeng');?>');
      setTimeout(function(){
        $(notice_id).hide(0, function(){
          $(this).addClass('ok').html('<?php _e('正在使用RSA公钥加密算法防止密码明文被窃取', 'dmeng');?>').fadeIn();
        });
      }, 500);
      crypt.setKey(public_key);
      $(document).on('submit', form_id, function () {
        var $pass = $(this).find('#user_pass');
            pwd = crypt.encrypt('<?php echo $this->RSA_text_prefix();?>'+$pass.val());
        if (pwd) {
          $pass.val('<?php echo $this->RSA_text_prefix();?>'+pwd);
        }
      });
    }).fail(function(){
      $(notice_id).append('<br><?php _e('加载加密程序失败', 'dmeng');?><br><?php _e('请刷新重试', 'dmeng');?>');
    });
  })(jQuery); 
}, false);
</script>
    <?php
  }

  public function maybe_RSA($text) {

    $prefix = $this->RSA_text_prefix();
    $length = strlen($prefix);
    
    if (0===strpos($text, $prefix) && function_exists('openssl_private_decrypt')) {

      openssl_private_decrypt(
        base64_decode(substr($text, $length)), 
        $decode, 
        $this->RSA_private_key()
      );
      
      if ($decode)
        $text = substr($decode, $length);

    }

    return $text;
  }
  
  public function login_pwd_maybe_RSA() {
    if (!empty($_POST['pwd']))
      $_POST['pwd'] = $this->maybe_RSA($_POST['pwd']);
  }

}

$dmeng_Security = new dmeng_Security;

function dmeng_Security() {
  global $dmeng_Security;
  return $dmeng_Security;
}
