<?php

class dmeng_Shortcode {

  public function __construct() {
  
    /**
     * 在文本小工具不自动添加P标签
     */
    add_filter( 'widget_text', 'shortcode_unautop' );

    /**
     * 在文本小工具也执行短代码
     */
    add_filter( 'widget_text', 'do_shortcode' );

    /**
     * 在评论内容也执行pem
     */
    add_filter( 'get_comment_text', array( $this, 'do_pem_in_comment' ) );

    add_action( 'pre_comment_on_post', array( $this, 'pre_comment_is_empty' ) );

    /**
     * 登录可见短代码
     */
    add_shortcode( 'dmengl2v', array( $this, 'dmengl2v_callback' ) );

    /**
     * 登录并评论可见短代码
     */
    add_shortcode( 'dmengr2v', array( $this, 'dmengr2v_callback' ) );

    /**
     * 评论的隐私标签
     */
    add_shortcode( 'pem', array( $this, 'pem_callback' ) );
    
    /**
     * 幻灯片
     */
    add_shortcode( 'dmengslide', array( $this, 'dmengslide_callback' ) );
    add_filter( 'dmeng_scripts_args', array( $this, 'dmengslide_script_uri' ) );

    /**
     * 文章列表
     */
    add_shortcode( 'dmenglist', array( $this, 'dmenglist_callback' ) );
    
    /**
     * 分类列表
     */
    add_shortcode( 'dmengterms', array( $this, 'dmengterms_callback' ) );  
    
    /**
     * 排行榜
     */
    add_shortcode( 'dmengrank', array( $this, 'dmengrank_callback' ) );    
    
    /**
     * 附件
     */
    add_shortcode( 'dmengfile', array( $this, 'dmengfile_callback' ) );

    /**
     * dmeng action
     * 用户功能短代码
     */
    add_shortcode( 'dmengaction', array( $this, 'dmengaction_callback' ) );

    add_action( 'wp_insert_comment', array( $this, 'do_dmengaction_after_comment' ), 1, 2 );
  }

  public function do_pem_in_comment( $content ) {

    global $shortcode_tags;
    if ( 
      false===strpos($content, '[pem')
      || empty($shortcode_tags) 
      || !is_array($shortcode_tags)
      || empty($shortcode_tags['pem'])
    ) {
      return $content;
    }

    /**
     * 备份短代码标签到 $shortcode_tags_backup
     * 只保留 pem 短代码
     */
    $shortcode_tags_backup = $shortcode_tags;
    $shortcode_tags = array(
      'pem' => $shortcode_tags['pem']
    );

    /**
     * 执行短代码，也就是执行 pem 短代码
     */
    $content = do_shortcode($content);

    /**
     * 还原备份到 $shortcode_tags
     * 删除备份变量 $shortcode_tags_backup
     */
    $shortcode_tags = $shortcode_tags_backup;
    unset($shortcode_tags_backup);
    
    return $content;
  }

  /**
   * 不允许只有短代码没有评论内容
   */
  public function pre_comment_is_empty() {
    $comment = ( isset($_POST['comment']) ) ? strip_shortcodes(trim($_POST['comment'])) : null;
    if (empty($comment))
      wp_die( __( '<strong>ERROR</strong>: please type a comment.' ), 200 );
  }

  public function do_dmengaction_after_comment( $comment_id, $comment_object ) {
    if ( !$comment_id || $comment_object->comment_approved != 1 || !empty($comment_object->comment_type) )
      return;

    do_shortcode(stripcslashes(htmlspecialchars_decode(get_post_meta( $comment_object->comment_post_ID, 'dmeng_comment_action', true ))));
  }
  
  public function dmengaction_callback( $atts ) {

    /**
     * 不在文章循环里执行
     */
    if ( in_the_loop() || empty($atts['do']) )
      return;  

    global $pagenow, $dmeng_Count;
    $in_comment = $pagenow=='wp-comments-post.php';
    $post = $in_comment ? $GLOBALS['post'] : get_post(intval($_POST['post_id']));
    if ( empty($post) )
      return __( '无法查询文章信息', 'dmeng' );

    $atts['do'] = sanitize_key($atts['do']);
    $atts['time'] = isset($atts['time']) ? absint($atts['time']) : 0;
    
    $current_user_id = get_current_user_id();
    $time = $in_comment ? $dmeng_Count->comment( $post->ID, $current_user_id ) : $dmeng_Count->exchange( $post->ID, $current_user_id );

    if ( $atts['time'] && $atts['time']!=$time )
      return;

    do_action( 'dmengaction_callback', $atts, $time, $post, $in_comment );

  }

  public function dmengl2v_callback( $atts, $content ) {
    if ( !empty($content) && !is_user_logged_in() ) {
      $content = '<a class="dmengl2v" href="'.wp_login_url(in_the_loop() ?get_permalink() : '').'">' . __('[此处内容登录可见]','dmeng') . '</a>';
    }
    return $content;
  }

  public function dmengr2v_callback( $atts, $content ) {

    if ( 
      empty($content) 
      || false===in_the_loop()
    ) {
      return $content;
    }
    
    global $post, $dmeng_Count;

    $current_user_id = get_current_user_ID();

    if ( ( !$current_user_id || $current_user_id != $post->post_author || !user_can( $current_user_id, 'edit_others_posts' ) ) && 
          $dmeng_Count->comment( $post->ID, $current_user_id )<1
    )  {
      $content = '<a class="dmengr2v" href="'.( $current_user_id ? '#comments' : wp_login_url(get_comments_link($post->ID)) ).'">' . __('[此处内容登录并发表评论可见]','dmeng') . '</a>';
    }

    return $content;
  }
  
  public static function pem_tips($before='', $after='') {
    return $before.'<abbr title="'.__('只有评论/文章作者或更高权限的用户才可见','dmeng').'">'.__('此处包含隐私内容','dmeng').'</abbr>'.$after;
  }
  
  public static function pem_tips_for_email( $atts ) {
    return self::pem_tips('（', '）');
  }

  public static function pem_callback( $atts, $content ) {
    global $comment;
    if ( !empty($content) && !empty($comment) ) {
      if ( 
          !current_user_can( 'moderate_comments' ) && 
          !in_array( get_current_user_id(), array( get_post_field( 'post_author', $comment->comment_post_ID ), $comment->user_id ) ) 
      ) {
        $content = self::pem_tips();
      } else {
        $content = '<span class="pem" title="'.__('此处红色字体内容为隐私内容','dmeng').'">'.$content.'</span>';
      }
    }
    return $content;
  }
  
  public static function dmengslide_callback( $atts ) {
    $atts = shortcode_atts( array( 'id' => 0 ), $atts );
    if ($atts['id'])
      return self::dmengslide_to_html( $atts['id']=='home' ? get_option('dmeng_slide_home',0) : $atts['id'] );
  }
  
  public static function dmengslide_to_html( $id ) {

    $data = wp_parse_args(
      (array)json_decode(get_option('dmeng_slide_'.intval($id)), true), 
      array('img'=>array(), 'url'=>array(), 'title'=>array(), 'desc'=>array(), 'parallax'=>array())
    );
    
    if (empty($data['img']))
      return;

    $items = array();
    
    foreach ($data['img'] as $key=>$img) {
      
      if (empty($img))
        continue;

      $item = $caption = '';

      if (!empty($data['title'][$key]))
        $caption .= '<h3>'.$data['title'][$key].'</h3>';
      
      if (!empty($data['desc'][$key]))
        $caption .= '<div>'.$data['desc'][$key].'</div>';
      
      $item .= $caption==='' ? '' : '<div class="caption">'.$caption.'</div>';
      
      $item .= '<img src="'.$img.'" alt="'.esc_attr($data['title'][$key]).'">';
      
      if (!empty($data['url'][$key]))
        $item = '<a href="'.esc_url($data['url'][$key]).'">'.$item.'</a>';
      
      $items[] = '<div class="dmengslide">'.$item.'</div>';
    }
    
    if (empty($items))
      return;

    $output = '<div class="dmengslide-wrapper">';
    $output .= '<div class="dmengslide-scroller clearfix">'.join('', $items).'</div>';
    
    $output .= '<div class="dmengslide-indicator"></div>';
    
    if (count($items)>1)
      $output .= '<div class="dmengslide-prev"><span class="glyphicon glyphicon-menu-left"></span></div><div class="dmengslide-next"><span class="glyphicon glyphicon-menu-right"></span></div>';
    
    $output .= '</div>';

    return $output;
  }

  public function dmengslide_script_uri($args) {
    if (empty($args['isluri']))
      $args['isluri'] = apply_filters('dmeng_iscroll_uri', get_template_directory_uri() . (DMENG_SCRIPT_DEV ? '/js/dev/dmeng-slide.js' : '/js/dmeng-slide.min.js'));
    
    return $args;
  }
  
  public static function dmenglist_callback( $atts ) {
    $atts = shortcode_atts( array( 
      'style' => 'archive', 
      'sticky_posts' => 0, 
      'taxonomy' => '', 
      'terms' => '', 
      'post_type' => '', 
      'number' => 10 
    ), $atts );
    
    if ($atts['sticky_posts']) {
      $sticky_posts = get_option('sticky_posts');
      if (empty($sticky_posts))
        return;
      $args = array( 'post__in'=>$sticky_posts );
    } else {
      $atts['terms'] = array_filter(array_map('intval', explode(',', $atts['terms'])));
      
      if (!empty($atts['terms']) && empty($atts['taxonomy']))
        $atts['taxonomy'] = 'category';

      $args = array(
        'post_type' => $atts['post_type'],
        'ignore_sticky_posts' => true,
        'posts_per_page' => intval($atts['number']),
      );
      
      if ($atts['terms']) {
        $args['tax_query'] = array(
          array(
            'taxonomy' => $atts['taxonomy'],
            'terms'    => $atts['terms'],
          ),
        );
        $args['post_type'] = get_taxonomy($atts['taxonomy'])->object_type;
      }
    }
    
    return self::dmenglist_query($args, $atts);
  }

  public static function dmenglist_query( $args, $atts ) {
    ob_start();
    
    query_posts( $args );

      while ( have_posts() ) : the_post();

      if ('list'===$atts['style']) {

        $comments_number = get_comments_number();
        $comments_number = $comments_number ? '<span>'.sprintf(__( '%s条评论', 'dmeng' ), '<i class="num">'.dmeng_number_format($comments_number).'</i>' ).'</span>' : '';

        $timestamp = get_the_time('U');
        echo '<li><time title="'.date(get_option('date_format'), $timestamp).'">'.date('m-d', $timestamp).'</time><a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a>'.$comments_number.'<span>'.sprintf(__( '%s次浏览', 'dmeng' ), '<i class="num">'.dmeng_number_format(get_dmeng_traffic('single', get_the_ID())).'</i>' ).'</span></li>';
        
      } else if ('thumbnail'===$atts['style']) {

        $thumbnail = dmeng_thumbnail(get_template_directory_uri().'/images/blank-220x146.png', false, false);
        echo '<li><a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'">' . $thumbnail .'<div class="entry-title">'.get_the_title().'</div></a></li>';

      } else {

        get_template_part('content', 'archive');

      }

      endwhile;
    wp_reset_query();
    
    $output = ob_get_contents();
    ob_end_clean();

    if (empty($output))
      return;
    
        $terms_title = array();
        if ($atts['terms']) {
          $terms = get_terms($atts['taxonomy'], array( 'include'=>$atts['terms'], 'fields'=>'id=>name' ));
          foreach ($terms as $term_id=>$name) {
            $terms_title[] = '<a href="'.get_term_link($term_id, $atts['taxonomy']).'" target="_blank">'.$name.'</a>';
          }
          $terms_title = '<h3><span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span>'.join(' / ', $terms_title).'</h3>';
        }
        
        $output = '<ul class="posts clearfix">'.$output.'</ul>';
        
        if ($terms_title)
           $output = $terms_title.$output;
      
      $output = '<div class="'.join(' ', array_filter(array($atts['style'].'_posts', 'posts-list'))).'">'.$output.'</div>';

    return $output;
  }
  
  public static function dmengterms_callback( $atts ) {
    $atts = shortcode_atts( array( 
      'style' => 'cloud', 
      'taxonomy' => 'category', 
      'include' => '', 
      'exclude' => '', 
      'number' => 15
    ), $atts );

    $terms = get_terms($atts['taxonomy'], array(
      'orderby' => 'count',
      'order' => 'DESC',
      'include' => array_map('intval', explode(',', $atts['include'])),
      'exclude' => array_map('intval', explode(',', $atts['exclude'])),
      'number' => intval($atts['number'])
    ));
    
    if (is_wp_error($terms))
      return;

    $output = '';
    
    foreach ($terms as $term) {

      $output .= '<a href="' . get_term_link( $term ) . '" title="'.esc_attr($term->name).'" class="vertical-center">' . $term->name . '</a>';
      
    }
    
    if ($output)
      return '<div class="terms-cloud clearfix">'.$output.'</div>';
  }
  
  public static function dmengrank_callback( $atts ) {
    $atts = shortcode_atts( array( 
      'number' => 6,
      'type' => ''
    ), $atts );
    
    $number = intval($atts['number']);
    $dmengrank_id = empty($GLOBALS['dmengrank_id']) ? 1 : (int)$GLOBALS['dmengrank_id']+1;
    
    if ('search'==$atts['type']) {
    
      global $dmeng_Rank;
      $rank = $dmeng_Rank->rank('search', $number, '', 3600);
      if (empty($rank))
        return;
      
      $output = '<div class="hot-search"><h3>大家在搜索这些…</h3>';
      foreach ($rank['data'] as $data) {
        $output .= '<a href="'.esc_url($data['url']).'" target="_blank">'.$data['title'].'</a>';
      }
      $output .= '</div>';
      
      return $output;
    }

    global $dmeng_Rank;

    $list = array();

    $list['date'] = array(
      'title' => __('最新', 'dmeng'),
      'attr_title' => __('按发布日期排行', 'dmeng'),
      'data' => ''
    );
    $rank = $dmeng_Rank->rank( 'date', $number, 'post', 3600 );
    foreach ($rank['data'] as $data) {
      $list['date']['data'] .= '<li><a href="'.esc_url($data['url']).'" title="'.esc_attr($data['title']).'" target="_blank">'.$data['title'].'</a><span>'.date( get_option('date_format'), $data['rank'] ).'</span></li>';
    }

    $list['vote_up'] = array(
      'title' => __('点赞', 'dmeng'),
      'attr_title' => __('按点赞人数排行', 'dmeng'),
      'data' => ''
    );
    $rank = $dmeng_Rank->rank( 'vote_up', $number, 'post', 3600 );
    foreach ($rank['data'] as $data) {
      $list['vote_up']['data'] .= '<li><a href="'.esc_url($data['url']).'" title="'.esc_attr($data['title']).'" target="_blank">'.$data['title'].'</a><span>'.sprintf($rank['title_format'], '<i class="num">'.dmeng_number_format($data['rank']).'</i>' ).'</span></li>';
    }

    $list['view'] = array(
      'title' => __('浏览', 'dmeng'),
      'attr_title' => __('按浏览量排行', 'dmeng'),
      'data' => ''
    );
    $rank = $dmeng_Rank->rank( 'view', $number, 'post', 3600 );
    foreach ($rank['data'] as $data) {
      $list['view']['data'] .= '<li><a href="'.esc_url($data['url']).'" title="'.esc_attr($data['title']).'" target="_blank">'.$data['title'].'</a><span>'.sprintf($rank['title_format'], '<i class="num">'.dmeng_number_format($data['rank']).'</i>' ).'</span></li>';
    }
    
    $list['comment'] = array(
      'title' => __('评论', 'dmeng'),
      'attr_title' => __('按评论数量排行', 'dmeng'),
      'data' => ''
    );
    $rank = $dmeng_Rank->rank( 'comment', $number, 'post', 3600 );
    foreach ($rank['data'] as $data) {
      $list['comment']['data'] .= '<li><a href="'.esc_url($data['url']).'" title="'.esc_attr($data['title']).'" target="_blank">'.$data['title'].'</a><span>'.sprintf($rank['title_format'], '<i class="num">'.dmeng_number_format($data['rank']).'</i>' ).'</span></li>';
    }
    
    $list['pingback'] = array(
      'title' => __('引用', 'dmeng'),
      'attr_title' => __('按Pingbacks排行', 'dmeng'),
      'data' => ''
    );
    $rank = $dmeng_Rank->rank( 'pingback', $number, 'post', 3600 );
    foreach ($rank['data'] as $data) {
      $list['pingback']['data'] .= '<li><a href="'.esc_url($data['url']).'" title="'.esc_attr($data['title']).'" target="_blank">'.$data['title'].'</a><span>'.sprintf($rank['title_format'], '<i class="num">'.dmeng_number_format($data['rank']).'</i>' ).'</span></li>';
    }

    
    $nav = $content = '';
    $i = 0;
    foreach ($list as $term) {
      $id = 'rank'.$dmengrank_id.$i;
      
      $nav .= '<li role="presentation"'.( $i===0 ? 'class="active"' : '').'><a href="#'.$id.'" aria-controls="'.$id.'" role="tab" data-toggle="tab" title="'.$term['attr_title'].'">'.$term['title'].'</a></li>';
      
      $content .= '<div role="tabpanel" class="tab-pane fade '.( $i===0 ? ' in active' : '').'" id="'.$id.'"><ul class="posts">'.$term['data'].'</ul></div>';
      
      $i++;
    }

    return '<div class="posts_rank tab_posts"><ul class="nav nav-tabs" role="tablist">'.$nav.'</ul><div class="tab-content list_posts">'.$content.'</div></div>';

  }
  
  public static function dmengfile_callback($atts) {
    $atts = shortcode_atts( array( 
      'id' => 0
    ), $atts );
    
    return dmeng_get_attachment_metadata($atts['id']);
  }

}
$dmeng_Shortcode = new dmeng_Shortcode;
