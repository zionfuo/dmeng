<?php

/**
 * 缓存
 * 
 * wp_using_ext_object_cache()
 */

class dmeng_Cache {

  public $nav_menu_cache_key_prefix;
  public $nav_menu_cache_expiration;
  public $no_hierarchical;

  public function __construct() {
    
    $this->nav_menu_cache_key_prefix = 'dm_nav_';
    $this->no_hierarchical = array('link_menu', 'mobile_menu');
    $this->nav_menu_cache_expiration = wp_using_ext_object_cache() ? 600 : 3600;

    add_action( 'wp_update_nav_menu', array( $this, 'clear_nav_menu_cache' ) );
    add_filter( 'wp_nav_menu', array( $this, 'set_nav_menu_cache' ), 10, 2 );
    add_filter( 'pre_wp_nav_menu', array( $this, 'get_nav_menu_cache' ), 10, 2 );

  }

  public function nav_menu_cache_key( $args, $hash=true ) {

    global $wp_query;
    if ($wp_query->is_preview())
      return '';

    /**
     * 生成钥匙的条件，默认包含移动端判断，菜单位置以及更新时间戳
     */
    $term = array(
      dmeng_is_mobile(), 
      $args->theme_location,
      $args->menu,
      $args->menu_class,
      $args->menu_id,
      $args->container,
      $args->container_class,
      $args->container_id,
      $args->depth,
      $args->before,
      $args->after,
      $args->link_before,
      $args->link_after,
      $args->echo,
      $args->items_wrap,
      get_option('dmeng_cached_nav_menu')
    );

    /**
     * 加入当前页面查询hash
     */
    if ($hash)
      $term[] = $wp_query->query_vars_hash;

    return $this->nav_menu_cache_key_prefix . md5(serialize($term));
  }
  
  public function get_nav_menu_cache( $nav_menu, $args ) {

    $cache_key = $this->nav_menu_cache_key($args, (false===in_array($args->theme_location, $this->no_hierarchical)));
    if ($cache_key) {
      $menu = get_transient( $cache_key );

      if ($menu && 0===strpos($menu, $this->nav_menu_cache_key_prefix))
        $menu = get_transient($menu);

      if (false !== $menu)
        $nav_menu = $menu;
    }

    return $nav_menu;
  }

  public function set_nav_menu_cache( $nav_menu, $args ) {

    if (in_array($args->theme_location, $this->no_hierarchical)) {

      set_transient(
        $this->nav_menu_cache_key($args, false), 
        $nav_menu, 
        $this->nav_menu_cache_expiration
      );

      return $nav_menu;
    }

    $active_comment = '<!--active_item-->';

    if (strpos($nav_menu, $active_comment)) {

      $nav_menu = str_replace($active_comment, '', $nav_menu);
      $cache_data = $nav_menu.sprintf(__('<!-- cached %s -->', 'dmeng'), current_time('mysql'));

    } else {
      
      /**
       * 菜单没有高亮元素时，当前页面只缓存一个通用菜单缓存的钥匙
       */
      $cache_data = $this->nav_menu_cache_key($args, false);

      /**
       * 更新通用菜单缓存
       */ 
      set_transient(
        $cache_data, 
        $nav_menu.sprintf(__('<!-- cached %s -->', 'dmeng'), current_time('mysql')), 
        $this->nav_menu_cache_expiration
      );
      
      
      $cache_data = $cache_data;
    }

    set_transient(
      $this->nav_menu_cache_key($args), 
      $cache_data, 
      $this->nav_menu_cache_expiration
    );

    return $nav_menu;
  }

  public function clear_nav_menu_cache() {
    update_option('dmeng_cached_nav_menu', current_time('timestamp'));
  }

}

$dmeng_Cache = new dmeng_Cache;
