<?php

/**
 * 主题设置面板
 */
 
function dmeng_options_table_build($rows) {
  
  $output = '<table class="form-table"><tbody>';

  foreach ($rows as $row) {
    
    $row = array_merge(
      array(
        'required' => false,
        'type' => '',
        'input_type' => 'text',
        'title' => '',
        'description' => '',
        'name' => '',
        'id' => '',
        'value' => '',
        'classes' => '',
        'size' => 5
      ), $row
    );
    
    $description = empty($row['description']) ? '' : '<p class="description">'.$row['description'].'</p>';

    switch($row['type']) {
      case 'input' : 

        $output .=
        '<tr>
          <th scope="row"><label for="'.$row['name'].'">'.$row['title'].'</label></th>
          <td>
            <input name="'.$row['name'].'" type="'.$row['input_type'].'" id="'.$row['id'].'" value="'.esc_attr($row['value']).'" class="regular-text ltr '.$row['classes'].'" '.($row['required'] ? 'required="required" ' : '').'> '.$description.'
          </td>
        </tr>';

        break;
      case 'textarea' : 

        $output .=
        '<tr>
          <th scope="row"><label for="'.$row['name'].'">'.$row['title'].'</label></th>
          <td>
            <fieldset>
              <legend class="screen-reader-text"><span>'.$row['title'].'</span></legend>
              <p><textarea name="'.$row['name'].'" rows="'.$row['size'].'" cols="50" id="'.$row['id'].'" class="large-text '.$row['classes'].'" '.($row['required'] ? 'required="required" ' : '').'>'.$row['value'].'</textarea></p>
              '.$description.'
            </fieldset>
          </td>
        </tr>';
        break;
      case 'select' : 

        $options = '';
        foreach( $row['options'] as $key=>$value ){
          $options .= sprintf('<option value="%1$s"%2$s>%3$s</option>', $key, ($key==$row['value'] ? ' selected="selected"' : ''), $value);
        }

        $output .=
        '<tr>
          <th scope="row"><label for="'.$row['name'].'">'.$row['title'].'</label></th>
          <td>
            <select name="'.$row['name'].'" id="'.$row['id'].'">'.$options.'</select>
            '.$description.'
          </td>
        </tr>';
        
        break;
      case 'checkbox' : 
      
        $options = '';
        foreach( $row['options'] as $key=>$value ){
          $options .= sprintf('<label><input name="%1$s[]" type="checkbox" value="%2$s"%3$s> %4$s </label> ', $row['name'], $key, (in_array($key, (array)$row['value']) ? ' checked' : ''), $value);
        }
        
        $output .=
        '<tr>
          <th scope="row"><label for="'.$row['name'].'">'.$row['title'].'</label></th>
          <td>
            <fieldset>
              <legend class="screen-reader-text"><span>'.$row['title'].'</span></legend>
              '.$options.'
            </fieldset>
            '.$description.'
          </td>
        </tr>';
        break;
      default :
      
        $output .=
        '<tr>
          <th scope="row"><label for="'.$row['name'].'">'.$row['title'].'</label></th>
          <td>
            '.$row['value'].$description.'
          </td>
        </tr>';

    }
  }

  $output .= '</tbody></table>';
  
  return $output;
}

function dmeng_options_form_output($attr=array(), $fields='', $submit_button=true) {

  $submit_button = $submit_button ? '<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="'.__('保存更改', 'dmeng').'"></p>' : '';

  $output = '<form ';

  foreach ($attr as $name=>$value) {
    $output .= $name.'="'.esc_attr($value).'" ';
  }

  $output .= '>'.$fields.$submit_button.'</form>';

  return $output;
}

function dmeng_options_notice($notice='', $success=true) {
  $notice = !empty($notice) ? $notice : ( $success ? __('更新成功') : __('更新出错') );
  return '<div id="dmeng-options-notice" '.($success ? '' : 'class="error"').'>'.$notice.'</div>';
}

function dmeng_options_new_nonce($action) {
  return '<script>var dop_nonce = "'.wp_create_nonce($action).'";</script>';
}

function dmeng_options_check_nonce($action) {
  if (empty($_POST['_wpnonce']) || false===wp_verify_nonce($_POST['_wpnonce'], $action)) {
    echo dmeng_options_notice(
      __('你确定这样做？', 'dmeng').
      dmeng_options_new_nonce($action).
      '<br /><p><a class="button" href="javascript:getOptionsPage();">'.__('重试', 'dmeng').'</a></p>', 
    false);
    die();
  }
}

class dmeng_Options {

  public function __construct() {
    add_action( 'admin_menu', array($this, 'admin_menu_page') );
    add_action( 'wp_ajax_dmeng_options_page', array($this, 'options_ajax') );
  }
  
  public function admin_menu_page() {

    $title = __('多梦主题设置','dmeng');

    add_menu_page( $title, $title, 'manage_options', 'dmeng_options', array($this, 'dmeng_options_page'), 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKTWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVN3WJP3Fj7f92UPVkLY8LGXbIEAIiOsCMgQWaIQkgBhhBASQMWFiApWFBURnEhVxILVCkidiOKgKLhnQYqIWotVXDjuH9yntX167+3t+9f7vOec5/zOec8PgBESJpHmomoAOVKFPDrYH49PSMTJvYACFUjgBCAQ5svCZwXFAADwA3l4fnSwP/wBr28AAgBw1S4kEsfh/4O6UCZXACCRAOAiEucLAZBSAMguVMgUAMgYALBTs2QKAJQAAGx5fEIiAKoNAOz0ST4FANipk9wXANiiHKkIAI0BAJkoRyQCQLsAYFWBUiwCwMIAoKxAIi4EwK4BgFm2MkcCgL0FAHaOWJAPQGAAgJlCLMwAIDgCAEMeE80DIEwDoDDSv+CpX3CFuEgBAMDLlc2XS9IzFLiV0Bp38vDg4iHiwmyxQmEXKRBmCeQinJebIxNI5wNMzgwAABr50cH+OD+Q5+bk4eZm52zv9MWi/mvwbyI+IfHf/ryMAgQAEE7P79pf5eXWA3DHAbB1v2upWwDaVgBo3/ldM9sJoFoK0Hr5i3k4/EAenqFQyDwdHAoLC+0lYqG9MOOLPv8z4W/gi372/EAe/tt68ABxmkCZrcCjg/1xYW52rlKO58sEQjFu9+cj/seFf/2OKdHiNLFcLBWK8ViJuFAiTcd5uVKRRCHJleIS6X8y8R+W/QmTdw0ArIZPwE62B7XLbMB+7gECiw5Y0nYAQH7zLYwaC5EAEGc0Mnn3AACTv/mPQCsBAM2XpOMAALzoGFyolBdMxggAAESggSqwQQcMwRSswA6cwR28wBcCYQZEQAwkwDwQQgbkgBwKoRiWQRlUwDrYBLWwAxqgEZrhELTBMTgN5+ASXIHrcBcGYBiewhi8hgkEQcgIE2EhOogRYo7YIs4IF5mOBCJhSDSSgKQg6YgUUSLFyHKkAqlCapFdSCPyLXIUOY1cQPqQ28ggMor8irxHMZSBslED1AJ1QLmoHxqKxqBz0XQ0D12AlqJr0Rq0Hj2AtqKn0UvodXQAfYqOY4DRMQ5mjNlhXIyHRWCJWBomxxZj5Vg1Vo81Yx1YN3YVG8CeYe8IJAKLgBPsCF6EEMJsgpCQR1hMWEOoJewjtBK6CFcJg4Qxwicik6hPtCV6EvnEeGI6sZBYRqwm7iEeIZ4lXicOE1+TSCQOyZLkTgohJZAySQtJa0jbSC2kU6Q+0hBpnEwm65Btyd7kCLKArCCXkbeQD5BPkvvJw+S3FDrFiOJMCaIkUqSUEko1ZT/lBKWfMkKZoKpRzame1AiqiDqfWkltoHZQL1OHqRM0dZolzZsWQ8ukLaPV0JppZ2n3aC/pdLoJ3YMeRZfQl9Jr6Afp5+mD9HcMDYYNg8dIYigZaxl7GacYtxkvmUymBdOXmchUMNcyG5lnmA+Yb1VYKvYqfBWRyhKVOpVWlX6V56pUVXNVP9V5qgtUq1UPq15WfaZGVbNQ46kJ1Bar1akdVbupNq7OUndSj1DPUV+jvl/9gvpjDbKGhUaghkijVGO3xhmNIRbGMmXxWELWclYD6yxrmE1iW7L57Ex2Bfsbdi97TFNDc6pmrGaRZp3mcc0BDsax4PA52ZxKziHODc57LQMtPy2x1mqtZq1+rTfaetq+2mLtcu0W7eva73VwnUCdLJ31Om0693UJuja6UbqFutt1z+o+02PreekJ9cr1Dund0Uf1bfSj9Rfq79bv0R83MDQINpAZbDE4Y/DMkGPoa5hpuNHwhOGoEctoupHEaKPRSaMnuCbuh2fjNXgXPmasbxxirDTeZdxrPGFiaTLbpMSkxeS+Kc2Ua5pmutG003TMzMgs3KzYrMnsjjnVnGueYb7ZvNv8jYWlRZzFSos2i8eW2pZ8ywWWTZb3rJhWPlZ5VvVW16xJ1lzrLOtt1ldsUBtXmwybOpvLtqitm63Edptt3xTiFI8p0in1U27aMez87ArsmuwG7Tn2YfYl9m32zx3MHBId1jt0O3xydHXMdmxwvOuk4TTDqcSpw+lXZxtnoXOd8zUXpkuQyxKXdpcXU22niqdun3rLleUa7rrStdP1o5u7m9yt2W3U3cw9xX2r+00umxvJXcM970H08PdY4nHM452nm6fC85DnL152Xlle+70eT7OcJp7WMG3I28Rb4L3Le2A6Pj1l+s7pAz7GPgKfep+Hvqa+It89viN+1n6Zfgf8nvs7+sv9j/i/4XnyFvFOBWABwQHlAb2BGoGzA2sDHwSZBKUHNQWNBbsGLww+FUIMCQ1ZH3KTb8AX8hv5YzPcZyya0RXKCJ0VWhv6MMwmTB7WEY6GzwjfEH5vpvlM6cy2CIjgR2yIuB9pGZkX+X0UKSoyqi7qUbRTdHF09yzWrORZ+2e9jvGPqYy5O9tqtnJ2Z6xqbFJsY+ybuIC4qriBeIf4RfGXEnQTJAntieTE2MQ9ieNzAudsmjOc5JpUlnRjruXcorkX5unOy553PFk1WZB8OIWYEpeyP+WDIEJQLxhP5aduTR0T8oSbhU9FvqKNolGxt7hKPJLmnVaV9jjdO31D+miGT0Z1xjMJT1IreZEZkrkj801WRNberM/ZcdktOZSclJyjUg1plrQr1zC3KLdPZisrkw3keeZtyhuTh8r35CP5c/PbFWyFTNGjtFKuUA4WTC+oK3hbGFt4uEi9SFrUM99m/ur5IwuCFny9kLBQuLCz2Lh4WfHgIr9FuxYji1MXdy4xXVK6ZHhp8NJ9y2jLspb9UOJYUlXyannc8o5Sg9KlpUMrglc0lamUycturvRauWMVYZVkVe9ql9VbVn8qF5VfrHCsqK74sEa45uJXTl/VfPV5bdra3kq3yu3rSOuk626s91m/r0q9akHV0IbwDa0b8Y3lG19tSt50oXpq9Y7NtM3KzQM1YTXtW8y2rNvyoTaj9nqdf13LVv2tq7e+2Sba1r/dd3vzDoMdFTve75TsvLUreFdrvUV99W7S7oLdjxpiG7q/5n7duEd3T8Wej3ulewf2Re/ranRvbNyvv7+yCW1SNo0eSDpw5ZuAb9qb7Zp3tXBaKg7CQeXBJ9+mfHvjUOihzsPcw83fmX+39QjrSHkr0jq/dawto22gPaG97+iMo50dXh1Hvrf/fu8x42N1xzWPV56gnSg98fnkgpPjp2Snnp1OPz3Umdx590z8mWtdUV29Z0PPnj8XdO5Mt1/3yfPe549d8Lxw9CL3Ytslt0utPa49R35w/eFIr1tv62X3y+1XPK509E3rO9Hv03/6asDVc9f41y5dn3m978bsG7duJt0cuCW69fh29u0XdwruTNxdeo94r/y+2v3qB/oP6n+0/rFlwG3g+GDAYM/DWQ/vDgmHnv6U/9OH4dJHzEfVI0YjjY+dHx8bDRq98mTOk+GnsqcTz8p+Vv9563Or59/94vtLz1j82PAL+YvPv655qfNy76uprzrHI8cfvM55PfGm/K3O233vuO+638e9H5ko/ED+UPPR+mPHp9BP9z7nfP78L/eE8/sl0p8zAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAIpSURBVHjapFPbT5JxGH456MBNys9hkcFiX0PmDSdDl/8CiBZMK4Rh/Qetrlqzrkvb17zJC+Wq3FKXF26AY+vEhqlh3eSwrlzyxYAkvPgO4+nCVWKYtZ7L9/c+7/Z7DgqXw0YNajUplUoqFovU0NjYbXfYB53OrvNMC3OKiKhYKn5eW1tJZd5mZiRRTDMMQ9VqlSRZJnI5bNTb46YO1tw04PNOxuMxCKKAgxBEAfF4DAM+76MO1tzU2+Mml8NG1OW0k4U164YC/sVcLoejwPM8hgL+RQtr1nU57UTsGRP5+jwTPM/jb8HzPHx9ngnWbCLqtFq6E4nYkaRCoYCbN67j5YvnAIClpTg6rRY3hYaDDyVR+iN5t7KLSHgYRISrI2EAgCzLCAWDHHHc+Or+5Y2NDzXkSuUbIuEQ1EpCKHgZm5vZn28cN75C0ejU1o/B2P17aDe0YebJYwDA11IJ1yJhKIgQvHIJO+WdmuPR6NSWmvZBq9VQuVymu3dGSaPVUOrVa5qejpL/Yj+NjT0gXbOOfsPBLywsPIPZZITVchbthjYE/BeQz+frasNx4yt1RZydfQrmeDP6vR7k81/qkiVR2hPxMBvn5+aQzWYPdSaRiO3Z+N9B+tco57a3a6KsMpw8Qcd0OuHTx8355eV0G9Pa6jIaT5NKVWMQiaJAyWSSRm/fmnz/bn1Er9dXZFkmRd06222DTte52jqvvkllMuszkiSmmZZfdf4+ALA7pZDqm7WiAAAAAElFTkSuQmCC' ); 

    add_submenu_page( 'dmeng_options', __('多梦主题短代码','dmeng'), __('短代码工具','dmeng'), 'manage_options', 'dmeng_options_shortcode', array($this, 'dmeng_options_shortcode_page') ); 
  }

  public function dmeng_options_page() {
    $this->options_page_output(__('多梦主题设置','dmeng'), __FUNCTION__);
  }
  
  public function dmeng_options_shortcode_page() {
    $this->options_page_output(__('多梦主题短代码','dmeng'), __FUNCTION__);
  }
  
  public function options_page_output($title, $slug) {

  ?>
    <style>
      #dmeng-options{font-family:"Microsoft Yahei","冬青黑体简体中文 w3","宋体";line-height:1.5;font-size:15px}
      #dmeng-options h1,#dmeng-options h2,#dmeng-options h3{font-weight:100}
      #dmeng-options .hide{display:none}
      #dmeng-options-tab,.options-box{margin:10px 0;padding:8px 0;border:1px solid #e5e5e5;box-shadow:0 1px 1px rgba(0,0,0,.04);background:#fff}
      .options-box{padding:10px 15px}
      .options-box>h3:first-child{margin-top:.5em}
      #dmeng-options-tab a{text-decoration:none;padding:0 12px}
      #dmeng-options-tab a:focus{outline:0!important;box-shadow:none!important}
      #dmeng-options-tab a.active{color:inherit}
      #dmeng-options-tab a:not(:first-child){border-left:1px solid #ddd}
      #dmeng-options-page>h3{margin-bottom:.5em;color:#626262}
      #dmeng-options-page>:not(.options-box){padding-left:15px;padding-right:15px}
      #dmeng-options-notice{background:#fff;border-left:4px solid #7ad03a;-webkit-box-shadow:0 1px 1px 0 rgba(0,0,0,.1);box-shadow:0 1px 1px 0 rgba(0,0,0,.1);margin:10px 0;padding:8px 15px}
      #dmeng-options-notice.error{border-color:#d54e21}
    </style>
    <div class="wrap" id="dmeng-options">
      <h2><?php echo $title; ?></h2>
      <div id="dmeng-options-tab">
        <?php
          foreach ($this->tabs_filter($slug) as $key=>$title) {
            echo '<a href="#'.$key.'" data-page="'.$key.'" class="'.$key.'">'.$title.'</a>';
          }
        ?>
      </div>
      <div id="dmeng-options-page"></div>
    </div>

    <?php wp_enqueue_media();?>
    <script>

    var dop_nonce;

    !function($){
      'use strict';
      var page_eid = '#dmeng-options-page',
          tab_eid = '#dmeng-options-tab',
          $page = $(page_eid),
          $tab = $(tab_eid),
          hash = location.hash,
          current_id = hash ? hash.replace('#', '') : '',
          current_id = current_id && $tab.children('[data-page='+current_id+']').length>0 ? current_id : $tab.children().first().data('page'),
          loading = false,
          postData = {};

      window.getOptionsPage = function(notice) {
        if (loading) {
          return;
        }
        $tab.children('a').removeClass('active');
        $tab.children('[data-page='+current_id+']').addClass('active');
        location.hash = current_id;
        notice = notice ? notice : '<?php _e('加载中…', 'dmeng');?>';
        $page.html('<div class="options-box">'+notice+'</div>');
        loading = true;
        $.ajax({
          type: 'POST', 
          url: ajaxurl, 
          data: {
            action: 'dmeng_options_page',
            slug: '<?php echo $slug;?>',
            page: current_id,
            data: postData,
            _wpnonce: dop_nonce
          }, 
          complete: function (response) {
            $page.html(response.responseText);
            loading = false;
          }
        });
      };

      $(document).ready(function() {
        getOptionsPage();
      }).on('click', tab_eid+' a', function(e){
        e.preventDefault();
        postData = {};
        current_id = $(this).data('page');
        getOptionsPage();
      }).on('submit', page_eid+' form', function(e){
        e.preventDefault();
        postData = $(this).serialize();
        getOptionsPage('<?php _e('提交中…', 'dmeng');?>');
      }).on('click', '.confirm_action', function() {
        var r = confirm($(this).data('confirm-title'));
        if (false===r) {
          return false;
        }
      });

    }(jQuery);
    </script>
  <?php
  }

  public function tabs_filter($slug) {
    return apply_filters($slug.'-tabs', array());
  }

  public function options_ajax() {

    if ( false===current_user_can('manage_options') ) {
      die('你无权操作');
    }

    if (!empty($_POST['slug']) && !empty($_POST['page'])) {
      do_action( $_POST['slug'].'_'.$_POST['page'] );
    }

    die();
  }

}

$dmeng_Options = new dmeng_Options;

class dmeng_Options_Default {
  
  public $tabs;

  public function __construct() {
    
    $this->tabs = array(
      'main' => __('概况', 'dmeng'),
      'general' => __('常规设置', 'dmeng'),
      'home' => __('首页', 'dmeng'),
      'writing' =>__('撰写','dmeng'),
      'reading' =>__('阅读','dmeng'),
      'adsense' =>__('广告','dmeng'),
      'discussion' =>__('讨论','dmeng'),
      'open' =>__('开放平台','dmeng'),
      'credit' =>__('积分','dmeng'),
      'gift' =>__('积分换礼','dmeng'),
      'smtp' =>__('SMTP','dmeng'),
      'seo' =>__('SEO','dmeng'),
      'mobile' =>__('移动端','dmeng')
    );

    add_filter('dmeng_options_page-tabs', array($this, 'add_tab'));

    foreach (array_keys($this->tabs) as $key) {
      add_action('dmeng_options_page_'.$key, array($this, $key.'_options'));
    }

  }

  public function add_tab($tabs) {
    $tabs = array_merge($this->tabs, $tabs);
    return $tabs;
  }
  
  public function main_options() {
  ?>
  <div class="options-box">
    <span style="color:#a94442;background:#f2dede;padding:0 5px"><?php _e('请注意：此版本为测试版，部分功能不可用，不带有主题升级检查功能，请保持关注并及时获取最新版本！', 'dmeng');?></span>

    <h3><?php _e('统计', 'dmeng');?></h3>

    <?php
      $label = array(
        'post' => __( '文章', 'dmeng' ),
        'cat' => __( '分类', 'dmeng' ),
        'tag' => __( '标签', 'dmeng' ),
        'user' => __( '用户', 'dmeng' ),
        'comment' => __( '评论', 'dmeng' ),
        'view' => __( '浏览总数', 'dmeng' ),
        'search' => __( '搜索次数', 'dmeng' ),
      );
      global $dmeng_Count;
      foreach ($label as $type=>$title) {
        $count = $dmeng_Count->get($type);
        if ( !empty($count) )
          echo ''.$title.' <code>'.$count.'</code> ';
      }
    ?>

    <h3><?php _e('链接', 'dmeng');?></h3>
    <ul>
      <li>主题官网：<a href="http://www.dmeng.net/" target="_blank">http://www.dmeng.net/</a></li>
      <li>作者主页：<a href="http://duomeng.me/" target="_blank">http://duomeng.me/</a></li>
    </ul>

    <h3><?php _e('致谢', 'dmeng');?></h3>
    <p>感谢以下小伙伴提供的支持</p>
    <ol>
      <li>Bootstrap <a href="http://getbootstrap.com/" target="_blank">http://getbootstrap.com/</a></li>
      <li>jQuery <a href="http://jquery.com/" target="_blank">http://jquery.com/</a></li>
      <li>Lazy Load Plugin for jQuery <a href="http://www.appelsiini.net/projects/lazyload" target="_blank">http://www.appelsiini.net/</a></li>
      <li>iScroll <a href="http://cubiq.org/iscroll-5" target="_blank">http://cubiq.org/iscroll-5</a></li>
      <li>七牛云存储 <a href="https://portal.qiniu.com/signup?code=3ldifmmzc22qa" target="_blank">https://www.qiniu.com/</a></li>
      <li>阿里云ECS <a href="http://www.aliyun.com/product/ecs/?ali_trackid=2:mm_46071101_7414056_29420481:1421314877_2k2_2067429985" target="_blank">http://www.aliyun.com/</a></li>
    </ol>

  </div>
    
  <?php
  }

  public function general_options() {

    if (!empty($_POST['data'])) {

      dmeng_options_check_nonce(__FUNCTION__);

      parse_str( $_POST['data'], $data );

      update_option('zh_cn_l10n_icp_num', sanitize_text_field($data['zh_cn_l10n_icp_num']));

      update_option('dmeng_general_setting', json_encode(array(
        'head_code' => $data['head_code'],
        'footer_code' => $data['footer_code'],
        'head_css' => $data['head_css']
      )));
      
      update_option('dmeng_black_list', $data['black_list']);

      echo dmeng_options_notice();
    }

    $general = (array)wp_unslash(json_decode(get_option('dmeng_general_setting'), true));

    $head_code = empty($general['head_code']) ? '' : $general['head_code'];
    $footer_code = empty($general['footer_code']) ? '' : $general['footer_code'];
    $head_css = empty($general['head_css']) ? '' : $general['head_css'];

    $rows = array();
    
    $rows[] = array(
      'type' => 'input',
      'name' => 'zh_cn_l10n_icp_num',
      'value' => get_option('zh_cn_l10n_icp_num'),
      'title' => __('ICP备案号', 'dmeng'),
      'description' => __('会被（工信部网站）链接包含并显示在网站底部。', 'dmeng')
    );   
    
    $rows[] = array(
      'type' => 'textarea',
      'classes' => 'code',
      'name' => 'head_code',
      'value' => $head_code,
      'title' => __('头部HEAD代码', 'dmeng'),
      'description' => __('如添加meta信息验证网站所有权。', 'dmeng')
    );   
    
    $rows[] = array(
      'type' => 'textarea',
      'classes' => 'code',
      'name' => 'footer_code',
      'value' => $footer_code,
      'title' => __('脚部统计代码', 'dmeng'),
      'description' => __('放置统计代码或安全网站认证小图标等。', 'dmeng')
    );   
    
    $rows[] = array(
      'type' => 'textarea',
      'classes' => 'code',
      'name' => 'head_css',
      'value' => $head_css,
      'title' => __('自定义CSS', 'dmeng'),
      'description' => __('以下内容会被放置在&lt;style&gt;标签之内，无需输入&lt;style type="text/css"&gt;和&lt;/style&gt;。', 'dmeng')
    );    
    
    $rows[] = array(
      'type' => 'textarea',
      'classes' => 'code',
      'name' => 'black_list',
      'value' => get_option('dmeng_black_list'),
      'title' => __('登录安全', 'dmeng'),
      'description' => __('请使用 | 分隔开，而且 | 前后都不要留空格。请谨慎操作！黑名单里的用户名都不能用来注册，也不能登录。', 'dmeng')
    );

    echo dmeng_options_form_output(
      array(
        'method' => 'post',
        'class' => 'options-box'
      ),
      dmeng_options_new_nonce(__FUNCTION__) . dmeng_options_table_build($rows)
    );

  }

  public function home_options() {

    if (!empty($_POST['data'])) {

      dmeng_options_check_nonce(__FUNCTION__);

      parse_str( $_POST['data'], $data );

      update_option('dmeng_home_seo', json_encode(array(
        'keywords' => $data['keywords'],
        'description' => $data['description']
      )));

      echo dmeng_options_notice();
    }

    $general = (array)wp_unslash(json_decode(get_option('dmeng_home_seo'), true));
    
    $keywords = empty($general['keywords']) ? '' : $general['keywords'];
    $description = empty($general['description']) ? '' : $general['description'];
    
    $rows = array();

    $rows[] = array(
      'type' => 'textarea',
      'name' => 'keywords',
      'value' => $keywords,
      'title' => __('首页关键词', 'dmeng'),
      'description' => __('网站首页的网页关键词。', 'dmeng')
    );

    $rows[] = array(
      'type' => 'textarea',
      'name' => 'description',
      'value' => $description,
      'title' => __('首页描述', 'dmeng'),
      'description' => __('网站首页的网页描述，推荐200字以内。', 'dmeng')
    );

    echo dmeng_options_form_output(
      array(
        'method' => 'post',
        'class' => 'options-box'
      ),
      dmeng_options_new_nonce(__FUNCTION__) . dmeng_options_table_build($rows)
    );

  }

  public function writing_options() {

    if (!empty($_POST['data'])) {

      dmeng_options_check_nonce(__FUNCTION__);

      parse_str( $_POST['data'], $data );
      
      if (isset($data['can_post_cat']))
        update_option('dmeng_can_post_cat', json_encode(array_map('intval', $data['can_post_cat'])));

      if (isset($data['copyright_content_default']))
        update_option('dmeng_copyright_content_default', sanitize_text_field($data['copyright_content_default']));        

      foreach (array('copyright_status_default', 'post_index', 'post_min_strlen', 'post_max_strlen') as $key) {
        if (isset($data[$key]))
          update_option('dmeng_'.$key, (int)$data[$key]);
      }

      echo dmeng_options_notice();
    }
    
    $copyright_status_default = (int)get_option('dmeng_copyright_status_default', 1);
    $copyright_content_default = wp_unslash(get_option('dmeng_copyright_content_default'));
    $post_index = (int)get_option('dmeng_post_index', 1);
  
    $can_post_cat = array_map('intval', (array)wp_unslash(json_decode(get_option('dmeng_can_post_cat'), true)));
    
    $categories_array = array();
    $categories = get_categories( array('hide_empty' => 0) );
    foreach ( $categories as $category ) {
      $categories_array[$category->term_id] = $category->name;
    }

    $rows = array();

    $rows[] = array(
      'type' => 'select',
      'name' => 'copyright_status_default',
      'value' => $copyright_status_default,
      'options' => array(
        1 => __( '显示', 'dmeng' ),
        0 => __( '不显示', 'dmeng' )
      ),
      'title' => __('默认版权声明开关', 'dmeng'),
      'description' => __('在文章/页面内容下的版权声明', 'dmeng')
    );
    
    $rows[] = array(
      'type' => 'textarea',
      'name' => 'copyright_content_default',
      'value' => $copyright_content_default,
      'title' => __('默认版权声明内容', 'dmeng'),
      'description' => __('版权声明内容，文章链接用{link}表示，文章标题用{title}表示，站点地址用{url}表示，站点名称用{name}表示', 'dmeng')
    );
    
    $rows[] = array(
      'type' => 'select',
      'name' => 'post_index',
      'value' => $post_index,
      'options' => array(
        1 => __( '显示', 'dmeng' ),
        0 => __( '不显示', 'dmeng' )
      ),
      'title' => __('默认锚点导航开关', 'dmeng'),
      'description' => __('选择是时将把文章页和页面内容中的H标题生成锚点导航目录。', 'dmeng')
    );

    
    $post_rows = array();

    $post_rows[] = array(
      'type' => 'checkbox',
      'name' => 'can_post_cat',
      'value' => $can_post_cat,
      'options' => $categories_array,
      'title' => __('允许投稿的分类', 'dmeng'),
      'description' => __('不选择任何分类则不开放投稿', 'dmeng')
    );
    
    $post_rows[] = array(
      'type' => 'input',
      'name' => 'post_min_strlen',
      'value' => get_option('dmeng_post_min_strlen', 140),
      'title' => __('投稿的最少字数', 'dmeng'),
      'description' => __('限制最少字数，一篇文章少于这个字数不允许投稿。一条微博是140，所以默认是140字。', 'dmeng')
    );
    
    $post_rows[] = array(
      'type' => 'input',
      'name' => 'post_max_strlen',
      'value' => get_option('dmeng_post_max_strlen', 12000),
      'title' => __('投稿的最多字数', 'dmeng'),
      'description' => __('限制最多字数，一篇文章超过这个字数不允许投稿。刊物文章一般是2200-12000，所以默认12000字。', 'dmeng')
    );


    echo dmeng_options_form_output(
      array(
        'method' => 'post',
        'class' => 'options-box'
      ),
      dmeng_options_new_nonce(__FUNCTION__).
      dmeng_options_table_build($rows).
      '<h3>'.__('投稿','dmeng').'</h3>'.
      dmeng_options_table_build($post_rows)
    );
    
  }
  
  public function reading_options() {

    if (!empty($_POST['data'])) {

      dmeng_options_check_nonce(__FUNCTION__);

      parse_str( $_POST['data'], $data );

      foreach (array('copyright_status_all', 'post_index_all', 'google_code_prettify') as $key) {
        if (isset($data[$key]))
          update_option('dmeng_'.$key, (int)$data[$key]);
      }
      
      if (isset($data['post_thumbnail']))
        update_option('dmeng_post_thumbnail', json_encode($data['post_thumbnail']));
      
      echo dmeng_options_notice();
    }

    $copyright_status_all = (int)get_option('dmeng_copyright_status_all', 1);
    $post_index_all = (int)get_option('dmeng_post_index_all', 1);
    $post_thumbnail = (array)wp_unslash(json_decode(get_option('dmeng_post_thumbnail','{"on":"1","suffix":"?imageView2/1/w/220/h/146/q/100"}'), true));

    $google_code_prettify = (int)get_option('dmeng_google_code_prettify', 0);

    $rows = array();

    $rows[] = array(
      'type' => 'select',
      'name' => 'copyright_status_all',
      'value' => $copyright_status_all,
      'options' => array(
        1 => __('显示', 'dmeng'),
        0 => __('不显示', 'dmeng')
      ),
      'title' => __('版权声明开关', 'dmeng'),
      'description' => __('开关网站的版权声明（选择关闭将全部不显示，无论文章页怎么设置）。', 'dmeng')
    );

    $rows[] = array(
      'type' => 'select',
      'name' => 'post_index_all',
      'value' => $post_index_all,
      'options' => array(
        1 => __('全部都显示', 'dmeng'),
        2 => __('只在文章页显示', 'dmeng'),
        3 => __('只在页面显示', 'dmeng'),
        0 => __('不显示', 'dmeng')
      ),
      'title' => __('锚点导航开关', 'dmeng'),
      'description' => __('开关文章的锚点导航（选择关闭将全部不显示，无论文章页怎么设置）。', 'dmeng')
    );

    $rows[] = array(
      'type' => 'select',
      'name' => 'post_thumbnail[on]',
      'value' => (int)$post_thumbnail['on'],
      'options' => array(
        1 => __('只显示特色图像', 'dmeng'),
        2 => __('没有特色图像时显示文章的第一张图片', 'dmeng'),
        0 => __('不显示', 'dmeng')
      ),
      'title' => __('文章缩略图', 'dmeng'),
      'description' => __('在列表页显示文章缩略图（推荐设置220x146特色图像）。', 'dmeng')
    );

    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'post_thumbnail[suffix]',
      'value' => $post_thumbnail['suffix'],
      'title' => __('缩略图地址后缀', 'dmeng'),
      'description' => __('常用于缩略图处理。使用七牛云存储的童鞋直接使用默认值 ?imageView2/1/w/220/h/146/q/100 即可。', 'dmeng')
    );

    $rows[] = array(
      'type' => 'select',
      'name' => 'google_code_prettify',
      'value' => $google_code_prettify,
      'options' => array(
        1 => __('启用', 'dmeng'),
        0 => __('禁用', 'dmeng')
      ),
      'title' => __('高亮代码', 'dmeng'),
      'description' => __('在页面载入 Google Code Prettify 高亮代码。启用后将自动高亮&lt;code&gt;标签中的内容，除此之外你也可以手动给元素添加 prettyprint CSS类实现代码高亮，支持 &lt;pre&gt; &lt;code&gt; &lt;xmp&gt; 三种标签，如&lt;pre class="prettyprint"&gt;。', 'dmeng')
    );
    
    echo dmeng_options_form_output(
      array(
        'method' => 'post',
        'class' => 'options-box'
      ),
      dmeng_options_new_nonce(__FUNCTION__) . dmeng_options_table_build($rows)
    );

  }
  
  public function adsense_options() {

    if (!empty($_POST['data'])) {

      dmeng_options_check_nonce(__FUNCTION__);

      parse_str( $_POST['data'], $data );

      foreach (array('adsense_archive', 'adsense_author', 'adsense_single') as $key) {
        if (isset($data[$key]))
          update_option('dmeng_'.$key, json_encode($data[$key]));
      }

      echo dmeng_options_notice();
    }

    $adsense_archive = (array)wp_unslash(json_decode(get_option('dmeng_adsense_archive','{"top":"","bottom":""}'), true));
    $adsense_author = (array)wp_unslash(json_decode(get_option('dmeng_adsense_author','{"top":"","bottom":""}'), true));
    $adsense_single = (array)wp_unslash(json_decode(get_option('dmeng_adsense_single', '{"top":"","comment":"","bottom":""}'), true));

    $rows = array();

    $rows[] = array(
      'type' => 'textarea',
      'name' => 'adsense_archive[top]',
      'value' => $adsense_archive['top'],
      'title' => __('归档页广告', 'dmeng'),
      'description' => __('分类/标签/搜索/日期归档页顶部', 'dmeng')
    );
    
    $rows[] = array(
      'type' => 'textarea',
      'name' => 'adsense_archive[bottom]',
      'value' => $adsense_archive['bottom'],
      'title' => '',
      'description' => __('分类/标签/搜索/日期归档页底部', 'dmeng')
    );

    $rows[] = array(
      'type' => 'textarea',
      'name' => 'adsense_author[top]',
      'value' => $adsense_author['top'],
      'title' => __('作者页广告', 'dmeng'),
      'description' => __('作者页顶部', 'dmeng')
    );
    
    $rows[] = array(
      'type' => 'textarea',
      'name' => 'adsense_author[bottom]',
      'value' => $adsense_author['bottom'],
      'title' => '',
      'description' => __('作者页底部', 'dmeng')
    );

    $rows[] = array(
      'type' => 'textarea',
      'name' => 'adsense_single[top]',
      'value' => $adsense_single['top'],
      'title' => __('内容页广告', 'dmeng'),
      'description' => __('文章/页面/附件页顶部', 'dmeng')
    );
    
    $rows[] = array(
      'type' => 'textarea',
      'name' => 'adsense_single[comment]',
      'value' => $adsense_single['comment'],
      'title' => '',
      'description' => __('文章/页面/附件页评论框上方', 'dmeng')
    );

    $rows[] = array(
      'type' => 'textarea',
      'name' => 'adsense_single[bottom]',
      'value' => $adsense_single['bottom'],
      'title' => '',
      'description' => __('文章/页面/附件页底部', 'dmeng')
    );
    
    echo dmeng_options_form_output(
      array(
        'method' => 'post',
        'class' => 'options-box'
      ),
      dmeng_options_new_nonce(__FUNCTION__) . dmeng_options_table_build($rows)
    );

  }

  public function discussion_options() {

    if (!empty($_POST['data'])) {

      dmeng_options_check_nonce(__FUNCTION__);

      parse_str( $_POST['data'], $data );

      foreach (array('sticky_comment_title', 'sticky_comment_button_txt') as $key) {
        if (isset($data[$key]))
          update_option('dmeng_'.$key, sanitize_text_field($data[$key]));
      }
      
      update_option('dmeng_hide_comment_email', (int)$data['hide_comment_email']);
      
      echo dmeng_options_notice();
    }

    $sticky_comment_title = get_option('dmeng_sticky_comment_title', __('置顶评论','dmeng'));
    $sticky_comment_button_txt = get_option('dmeng_sticky_comment_button_txt',__('置顶','dmeng'));
    $hide_comment_email = (int)get_option('dmeng_hide_comment_email', 1);

    $rows = array();

    $rows[] = array(
      'type' => 'input',
      'name' => 'sticky_comment_title',
      'value' => $sticky_comment_title,
      'title' => __('置顶评论标题文本', 'dmeng'),
      'description' => ''
    );
    
    $rows[] = array(
      'type' => 'input',
      'name' => 'sticky_comment_button_txt',
      'value' => $sticky_comment_button_txt,
      'title' => __('置顶按钮文本', 'dmeng'),
      'description' => ''
    );
    
    $rows[] = array(
      'type' => 'select',
      'name' => 'hide_comment_email',
      'value' => $hide_comment_email,
      'options' => array(
        1 => __('隐藏', 'dmeng'),
        0 => __('不隐藏', 'dmeng')
      ),
      'title' => __('隐藏邮箱地址', 'dmeng'),
      'description' => __('自动隐藏评论中的邮箱地址，只有评论作者、文章作者或更高权限的用户可见。', 'dmeng')
    );

    echo dmeng_options_form_output(
      array(
        'method' => 'post',
        'class' => 'options-box'
      ),
      dmeng_options_new_nonce(__FUNCTION__) . dmeng_options_table_build($rows)
    );

  }
  
  public function open_options() {

    if (!empty($_POST['data'])) {

      dmeng_options_check_nonce(__FUNCTION__);

      parse_str( $_POST['data'], $data );

      foreach (array('open_qq', 'open_weibo', 'open_sms', 'open_remember_the_user') as $key) {
        if (isset($data[$key]))
          update_option('dmeng_'.$key, (int)$data[$key]);
      }
      
      foreach (array('open_qq_id', 'open_qq_key', 'open_weibo_key', 'open_weibo_secret', 'open_sms_key', 'open_role') as $key) {
        if (isset($data[$key]))
          update_option('dmeng_'.$key, sanitize_text_field($data[$key]));
      }
      
      echo dmeng_options_notice();
    }

    $roles = array();
    $editable_roles = array_reverse( get_editable_roles() );
    foreach ( $editable_roles as $role => $details ) {
      $name = translate_user_role($details['name'] );
      $role = esc_attr($role);
      $roles[$role] = $name;
    }

    $rows = array();

    $rows[] = array(
      'type' => 'select',
      'name' => 'open_qq',
      'value' => (int)get_option('dmeng_open_qq', 1),
      'options' => array(
        1 => __('启用', 'dmeng'),
        0 => __('关闭', 'dmeng')
      ),
      'title' => __('启用QQ登录','dmeng').' [ <a href="http://www.dmeng.net/connect-qq.html" title="'.__('网站接入QQ登录申请','dmeng').'" target="_blank">?</a> ]',
      'description' => __('启用前提是设置了相应的 QQ ID 和 QQ KEY 。', 'dmeng')
    );
    
    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'open_qq_id',
      'value' => get_option('dmeng_open_qq_id'),
      'title' => __('QQ ID','dmeng'),
      'description' => ''
    );

    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'open_qq_key',
      'value' => get_option('dmeng_open_qq_key'),
      'title' => __('QQ KEY','dmeng'),
      'description' => ''
    );

    $rows[] = array(
      'type' => 'select',
      'name' => 'open_weibo',
      'value' => (int)get_option('dmeng_open_weibo', 1),
      'options' => array(
        1 => __('启用', 'dmeng'),
        0 => __('关闭', 'dmeng')
      ),
      'title' => __('启用微博登录','dmeng').' [ <a href="http://www.dmeng.net/connect-weibo.html" title="'.__('网站接入微博登录申请','dmeng').'" target="_blank">?</a> ]',
      'description' => __('启用前提是设置了相应的 WEIBO KEY 和 WEIBO SECRET 。', 'dmeng')
    );

    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'open_weibo_key',
      'value' => get_option('dmeng_open_weibo_key'),
      'title' => __('WEIBO KEY','dmeng'),
      'description' => ''
    );

    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'open_weibo_secret',
      'value' => get_option('dmeng_open_weibo_secret'),
      'title' => __('WEIBO SECRET','dmeng'),
      'description' => ''
    );
    
    $rows[] = array(
      'type' => 'select',
      'name' => 'open_sms',
      'value' => (int)get_option('dmeng_open_sms', 1),
      'options' => array(
        1 => __('启用', 'dmeng'),
        0 => __('关闭', 'dmeng')
      ),
      'title' => __('启用短信登录','dmeng'),
      'description' => __('默认使用聚合短信API发送短信。', 'dmeng')
    );

    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'open_sms_key',
      'value' => get_option('dmeng_open_sms_key'),
      'title' => __('短信 API KEY','dmeng'),
      'description' => ''
    );

    $rows[] = array(
      'type' => 'select',
      'name' => 'open_role',
      'value' => get_option('dmeng_open_role', 'contributor'),
      'options' => $roles,
      'title' => __('默认角色','dmeng'),
      'description' => __('新登录用户的角色，默认是投稿者。', 'dmeng')
    );

    $rows[] = array(
      'type' => 'select',
      'name' => 'open_remember_the_user',
      'value' => (int)get_option('dmeng_open_remember_the_user', 1),
      'options' => array(
        1 => __('启用', 'dmeng'),
        0 => __('关闭', 'dmeng')
      ),
      'title' => __('记住登录状态','dmeng'),
      'description' => __('使用开放平台登录是否记住登录状态，不记住的话当关闭浏览器窗口后需要重新登录。', 'dmeng')
    );
    
    echo dmeng_options_form_output(
      array(
        'method' => 'post',
        'class' => 'options-box'
      ),
      dmeng_options_new_nonce(__FUNCTION__) . dmeng_options_table_build($rows)
    );

  }  

  public function credit_options() {

    if (!empty($_POST['data'])) {

      dmeng_options_check_nonce(__FUNCTION__);

      parse_str( $_POST['data'], $data );

      foreach (array('reg_credit', 'rec_post_credit', 'rec_post_num', 'rec_comment_credit', 'rec_comment_num', 'rec_reg_credit', 'rec_reg_num', 'rec_view_credit', 'rec_view_num') as $key) {
        if (isset($data[$key]))
          update_option('dmeng_'.$key, (int)$data[$key]);
      }
      
      echo dmeng_options_notice();
    }

    $rows = array();

    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'reg_credit',
      'value' => get_option('dmeng_reg_credit', '50'),
      'title' => __('新用户注册奖励','dmeng'),
      'description' => __('新用户注册自动奖励的积分','dmeng')
    );

    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'rec_post_credit',
      'value' => get_option('dmeng_rec_post_credit', '50'),
      'title' => __('投稿一次得分','dmeng'),
      'description' => ''
    );
    
    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'rec_post_num',
      'value' => get_option('dmeng_rec_post_num', '5'),
      'title' => __('每天投稿次数','dmeng'),
      'description' => __('每天可以获得积分奖励的投稿次数','dmeng')
    );  
    
    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'rec_comment_credit',
      'value' => get_option('dmeng_rec_comment_credit', '5'),
      'title' => __('评论一次得分','dmeng'),
      'description' => ''
    );   
    
    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'rec_comment_num',
      'value' => get_option('dmeng_rec_comment_num', '50'),
      'title' => __('每天评论次数','dmeng'),
      'description' => __('每天可以获得积分奖励的评论次数','dmeng')
    );
    
    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'rec_reg_credit',
      'value' => get_option('dmeng_rec_reg_credit', '50'),
      'title' => __('注册推广一次得分','dmeng'),
      'description' => ''
    );

    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'rec_reg_num',
      'value' => get_option('dmeng_rec_reg_num','5'),
      'title' => __('每天注册推广次数','dmeng'),
      'description' => __('每天可以获得积分奖励的注册推广次数','dmeng')
    );    
    
    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'rec_view_credit',
      'value' => get_option('dmeng_rec_view_credit', '5'),
      'title' => __('访问推广一次得分','dmeng'),
      'description' => ''
    );

    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'rec_view_num',
      'value' => get_option('dmeng_rec_view_num','50'),
      'title' => __('每天访问推广次数','dmeng'),
      'description' => __('每天可以获得积分奖励的访问推广次数','dmeng')
    );

    echo dmeng_options_form_output(
      array(
        'method' => 'post',
        'class' => 'options-box'
      ),
      dmeng_options_new_nonce(__FUNCTION__) . dmeng_options_table_build($rows)
    );

  }
  
  public function gift_options() {

    if (!empty($_POST['data'])) {

      dmeng_options_check_nonce(__FUNCTION__);

      parse_str( $_POST['data'], $data );

      foreach (array('is_gift_open', 'is_gift_future', 'gift_num') as $key) {
        if (isset($data[$key]))
          update_option('dmeng_'.$key, (int)$data[$key]);
      }
      
      foreach (array('gift_filter', 'gift_tips', 'gift_notice') as $key) {
        if (isset($data[$key]))
          update_option('dmeng_'.$key, sanitize_text_field($data[$key]));
      }
      
      echo dmeng_options_notice();
    }

    $dmeng_is_gift_open = (int)get_option('dmeng_is_gift_open', 0);
    $dmeng_gift_filter = get_option('dmeng_gift_filter', '0-100,100-1000,1000-10000,10000-0');
    $dmeng_is_gift_future = (int)get_option('dmeng_is_gift_future', 1);
    $dmeng_gift_num = (int)get_option('dmeng_gift_num', 12);
    $dmeng_gift_tips = get_option('dmeng_gift_tips');
    $dmeng_gift_notice = get_option('dmeng_gift_notice');

    $rows = array();

    $rows[] = array(
      'type' => 'select',
      'name' => 'is_gift_open',
      'value' => $dmeng_is_gift_open,
      'options' => array(
        1 => __('启用', 'dmeng'),
        0 => __('关闭', 'dmeng')
      ),
      'title' => __('开启积分换礼','dmeng'),
      'description' => __('启用积分换礼功能。', 'dmeng')
    );
    
    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'gift_filter',
      'value' => $dmeng_gift_filter,
      'title' => __('积分范围','dmeng'),
      'description' => __('上方筛选条件中的积分筛选，以英文 , 号分割，0-100代表100以下，100-1000代表100至1000，10000-0代表10000以上','dmeng')
    );

    $rows[] = array(
      'type' => 'select',
      'name' => 'is_gift_future',
      'value' => $dmeng_is_gift_future,
      'options' => array(
        1 => __('启用', 'dmeng'),
        0 => __('关闭', 'dmeng')
      ),
      'title' => __('定时发布','dmeng'),
      'description' => __('显示定时发布的礼品', 'dmeng')
    );

    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'gift_num',
      'value' => $dmeng_gift_num,
      'title' => __('显示礼品数量','dmeng'),
      'description' => __('列表页一页显示的礼品数量。因为一排4列，所以推荐设置可以被4整除的数字，默认是12。','dmeng')
    );

    $rows[] = array(
      'type' => 'textarea',
      'name' => 'gift_tips',
      'value' => $dmeng_gift_tips,
      'title' => __('温馨提示','dmeng'),
      'description' => __('礼品详细信息下的提示语。','dmeng')
    );

    $rows[] = array(
      'type' => 'textarea',
      'name' => 'gift_notice',
      'value' => $dmeng_gift_notice,
      'title' => __('兑换须知','dmeng'),
      'description' => __('礼品兑换须知','dmeng')
    );

    echo dmeng_options_form_output(
      array(
        'method' => 'post',
        'class' => 'options-box'
      ),
      dmeng_options_new_nonce(__FUNCTION__) . dmeng_options_table_build($rows)
    );

  }
  
  public function smtp_options() {

    if (!empty($_POST['data'])) {

      dmeng_options_check_nonce(__FUNCTION__);

      parse_str( $_POST['data'], $data );

      update_option('dmeng_smtp', json_encode(array_map('sanitize_text_field', $data['dmeng_smtp'])));

      echo dmeng_options_notice();
    }

    $smtp = wp_parse_args(
      (array)wp_unslash(json_decode(get_option('dmeng_smtp'), true)),
      array(
        'option' => 0,
        'host' => '',
        'ssl' => 0,
        'port' => 25,
        'user' => '',
        'pass' => '',
        'name' => ''
      )
    );

    $rows = array();

    $rows[] = array(
      'type' => 'select',
      'name' => 'dmeng_smtp[option]',
      'value' => (int)$smtp['option'],
      'options' => array(
        1 => __('启用', 'dmeng'),
        0 => __('关闭', 'dmeng')
      ),
      'title' => __('启用SMTP','dmeng'),
      'description' => ''
    );
    
    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'dmeng_smtp[host]',
      'value' => $smtp['host'],
      'title' => __('发信服务器','dmeng'),
      'description' => ''
    );  
    
    $rows[] = array(
      'type' => 'select',
      'name' => 'dmeng_smtp[ssl]',
      'value' => (int)$smtp['ssl'],
      'options' => array(
        1 => __('启用', 'dmeng'),
        0 => __('关闭', 'dmeng')
      ),
      'title' => __('启用SSL','dmeng'),
      'description' => ''
    );
    
    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'dmeng_smtp[port]',
      'value' => $smtp['port'],
      'title' => __('端口号','dmeng'),
      'description' => ''
    );

    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'dmeng_smtp[user]',
      'value' => $smtp['user'],
      'title' => __('发信账号','dmeng'),
      'description' => ''
    );
    
    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => 'dmeng_smtp[pass]',
      'value' => $smtp['pass'],
      'title' => __('账号密码','dmeng'),
      'description' => ''
    );   
    
    $rows[] = array(
      'type' => 'input',
      'name' => 'dmeng_smtp[name]',
      'value' => $smtp['name'],
      'title' => __('显示名称','dmeng'),
      'description' => ''
    );

    echo dmeng_options_form_output(
      array(
        'method' => 'post',
        'class' => 'options-box'
      ),
      dmeng_options_new_nonce(__FUNCTION__) . dmeng_options_table_build($rows)
    );

  }
  
  public function seo_options() {

    global $dmeng_Sitemap;

    if (!empty($_POST['data'])) {

      dmeng_options_check_nonce(__FUNCTION__);

      parse_str( $_POST['data'], $data );

      if (isset($data[$dmeng_Sitemap->baidu_api_field_name]))
        update_option($dmeng_Sitemap->baidu_api_field_name, esc_url($data[$dmeng_Sitemap->baidu_api_field_name]));

      update_option($dmeng_Sitemap->setting_field_name, 
        (isset($data[$dmeng_Sitemap->setting_field_name]) ? 1 : 0)
      );    

      if (isset($data['seo_meta']))
        update_option('dmeng_seo_meta', (int)$data['seo_meta']);

      echo dmeng_options_notice();
    }

    $rows = array();

    $rows[] = array(
      'value' => ( (int)get_option('blog_public') ? __('已打开','dmeng') : __('请到“设置 > 阅读 > 对搜索引擎的可见性”打开','dmeng') ),
      'title' => __('对搜索引擎的可见性','dmeng'),
      'description' => ''
    );
    
    $rows[] = array(
      'value' => '<a href="'.$dmeng_Sitemap->get_sitemap_index_url().'" target="_blank">'.$dmeng_Sitemap->get_sitemap_index_url().'</a>',
      'title' => __('地图索引地址','dmeng'),
      'description' => ''
    );

    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => $dmeng_Sitemap->baidu_api_field_name,
      'value' => get_option($dmeng_Sitemap->baidu_api_field_name),
      'title' => __('百度推送接口','dmeng'),
      'description' => '<label for="'.$dmeng_Sitemap->setting_field_name.'">
        <input name="'.$dmeng_Sitemap->setting_field_name.'" type="checkbox" id="'.$dmeng_Sitemap->setting_field_name.'" '.((int)get_option($dmeng_Sitemap->setting_field_name) ? 'checked="checked"' : '').'> '.__('发布文章时自动通知百度', 'dmeng').' [ <a href="http://zhanzhang.baidu.com/college/courseinfo?id=267&page=2#h2_article_title9" target="_blank">?</a> ]
      </label>'
    );  
    
    $rows[] = array(
      'value' => '<button type="button" class="button button-primary ping-baidu">'.__('推送今天更新的文章', 'dmeng').'</button> <button type="button" class="button button-primary ping-baidu" data-index="sitemap">'.__('推送地图索引', 'dmeng').'</button>',
      'title' => __('推送到百度','dmeng'),
      'description' => ''
    );

    $rows[] = array(
      'type' => 'select',
      'name' => 'seo_meta',
      'value' => (int)get_option('dmeng_seo_meta', 1),
      'options' => array(
        1 => __('启用', 'dmeng'),
        0 => __('关闭', 'dmeng')
      ),
      'title' => __('使用关键词描述','dmeng'),
      'description' => __('如果使用了插件管理网页关键词和描述，可将主题自带的关闭','dmeng')
    );

    ob_start();
    ?>
        <script>
        var pinging = false;
        !function($){
          $('.ping-baidu').on('click', function(){
            if (pinging) {
              return;
            }
            pinging = true;
            var $btn = $(this);
            $btn.html('<?php _e('推送中…请稍后…', 'dmeng');?>').addClass('disabled').prop('disabled', true);
            $.ajax({
              type: 'POST',
              url: ajaxurl,
              data: { 'action':'<?php echo $dmeng_Sitemap->setting_field_name;?>', 'nonce':'<?php echo wp_create_nonce($dmeng_Sitemap->setting_field_name);?>', 'index':$(this).data('index') },
              complete: function (e) {
                $btn.html(e.responseText ? e.responseText : '<?php _e('出错了，请刷新重试', 'dmeng');?>');
                pinging = false;
              }
            });
            return false;
          });
        }(jQuery);
        </script>
     <?php
    $script = ob_get_clean();

    echo dmeng_options_form_output(
      array(
        'method' => 'post',
        'class' => 'options-box'
      ),
      dmeng_options_new_nonce(__FUNCTION__) . dmeng_options_table_build($rows) . $script
    );

  }
  
  public function mobile_options() {

    $theme_field_name = 'dmeng_mobile_theme';
    $home_field_name = 'dmeng_mobile_home';
    $siteurl_field_name = 'dmeng_mobile_siteurl';
  
    if (!empty($_POST['data'])) {

      dmeng_options_check_nonce(__FUNCTION__);

      parse_str( $_POST['data'], $data );

      if (isset($data[$theme_field_name])) {
        $new_mobile_theme = $data[$theme_field_name];
        if (isset($themes[$new_mobile_theme]))
          update_option( $theme_field_name, $new_mobile_theme );
      }

      if (isset($data[$home_field_name])) {
        update_option($home_field_name, esc_url($data[$home_field_name]));
      }

      if (isset($data[$siteurl_field_name])) {
        update_option($siteurl_field_name, esc_url($data[$siteurl_field_name]));
      }
      
      echo dmeng_options_notice();
    }

    $themes = wp_get_themes();
    $options = array();
    foreach ($themes as $name=>$theme) {
      if ('dmeng2.1'!=$theme->get('Template'))
        continue;
      $options[$name] = $theme->get('Name').'('.$name.')';
    }
    $have_themes = !empty($options);
    
    $mobile_theme = get_option($theme_field_name, 'dmeng2.1-mobile');
    $mobile_home = get_option($home_field_name);
    $mobile_siteurl = get_option($siteurl_field_name);
    
    $rows = array();

    $rows[] = array(
      'value' => (true===apply_filters('using_dmeng_theme_switch', '__return_false') ? __('已启用','dmeng') : __('请安装并启用！','dmeng')),
      'title' => __('切换插件','dmeng'),
      'description' => __('根据WordPress的工作机制，要完整地完成主题切换，需要使用插件实现。','dmeng')
    );

    $rows[] = array(
      'type' => ($have_themes ? 'select' : ''),
      'name' => $theme_field_name,
      'value' => ($have_themes ? $mobile_theme : __('请上传多梦主题2.1的子主题', 'dmeng')),
      'options' => $options,
      'title' => __('选择主题','dmeng'),
      'description' => ''
    );
    
    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => $siteurl_field_name,
      'value' => $mobile_siteurl,
      'title' => __('WordPress地址（URL）','dmeng'),
      'description' => __('移动端的WordPress地址（URL）','dmeng')
    ); 
    
    $rows[] = array(
      'type' => 'input',
      'classes' => 'code',
      'name' => $home_field_name,
      'value' => $mobile_home,
      'title' => __('站点地址（URL）','dmeng'),
      'description' => __('移动端的站点地址（URL）','dmeng')
    );  

    echo dmeng_options_form_output(
      array(
        'method' => 'post',
        'class' => 'options-box'
      ),
      dmeng_options_new_nonce(__FUNCTION__) . dmeng_options_table_build($rows)
    );
  }

}

new dmeng_Options_Default;

class dmeng_Options_Shortcode {
  
  public $tabs;

  public function __construct() {
    
    $this->tabs = array(
      'dmengslide' => __('幻灯片', 'dmeng'),
      'dmengl2v' => __('登录可见', 'dmeng'),
      'dmengr2v' => __('登录评论可见', 'dmeng'),
      'dmenglist' => __('文章列表', 'dmeng'),
      'dmengterms' => __('分类列表', 'dmeng'),
      'dmengrank' => __('排行榜', 'dmeng'),
      'dmengfile' => __('附件', 'dmeng'),
      'dmengaction' => __('同步动作', 'dmeng')
    );

    add_filter('dmeng_options_shortcode_page-tabs', array($this, 'add_tab'));

    foreach (array_keys($this->tabs) as $key) {
      add_action('dmeng_options_shortcode_page_'.$key, array($this, $key.'_options'));
    }

  }

  public function add_tab($tabs) {
    $tabs = array_merge($this->tabs, $tabs);
    return $tabs;
  }
  
  public function dmengslide_options() {

    $action = 'list';

    $msg = '';
    $success = true;
    
    if (!empty($_POST['data'])) {

      dmeng_options_check_nonce(__FUNCTION__);

      parse_str( $_POST['data'], $data );

      if (
        !empty($data['new'])
        && !empty($data['_wpnonce'])
        && is_numeric($data['new'])
        && wp_verify_nonce($data['_wpnonce'], 'new')
      ) {
        
        $action = 'new';
        // 新建
        
      } else if (
        !empty($data['edit'])
        && !empty($data['_wpnonce'])
        && is_numeric($data['edit'])
        && wp_verify_nonce($data['_wpnonce'], 'edit_'.$data['edit'])
      ) {
        
        $action = 'edit';
        // 编辑
        
      } else if (
        !empty($data['update'])
        && !empty($data['_wpnonce'])
        && is_numeric($data['update'])
        && wp_verify_nonce($data['_wpnonce'], 'update_'.$data['update'])
      ) {
        
        // 编辑数据
        $action = 'update';
        
        if (
          !empty($data['img'])
          && !empty($data['url'])
          && !empty($data['title'])
          && !empty($data['desc'])
        ) {

          $slide_id = $data['update'];

          $image = $url = $title = $desc = array();

          foreach ($data['img'] as $key=>$img) {
            if (empty($img))
              continue;
            $image[] = $img;
            $url[] = $data['url'][$key];
            $title[] = $data['title'][$key];
            $desc[] = $data['desc'][$key];
          }
          
          update_option('dmeng_slide_'.$slide_id, json_encode(array(
            'name' => sanitize_text_field($data['name']),
            'img' => $image,
            'url' => $url,
            'title' => $title,
            'desc' => $desc
          )));
          
          $slides = (array)wp_unslash(json_decode(get_option('dmeng_slides'), true));
          if (false===in_array($slide_id, $slides)) {
            $slides[] = $slide_id;
            update_option('dmeng_slides', json_encode($slides));
          }

          $msg = __('幻灯片更新成功', 'dmeng');

        }

      } else if (
        !empty($data['delete'])
        && !empty($data['_wpnonce'])
        && is_numeric($data['delete'])
        && wp_verify_nonce($data['_wpnonce'], 'delete_'.$data['delete'])
      ) {
        
        // 删除
        $action = 'delete';
        delete_option( 'dmeng_slide_'.$data['delete'] );
        $msg = sprintf(__('操作成功，ID %s 幻灯片已被删除', 'dmeng'), '<b>'.$data['delete'].'</b>' );
      }
      
      if ($msg)
        echo dmeng_options_notice($msg, $success);

    }

    // 提示
    echo '<p class="options-box">'.sprintf(__( '小提示：为了保证最佳的用户体验，请以 750px 作为图片宽度（主内容区域宽度），同一组的图片采用一致高度，图片编辑完成后使用压缩工具压缩以提升加载速度，如 %1$s 可减少图片 70%% 体积。如果你不能保证图片的尺寸和质量，那么我们不建议你使用幻灯片，糟糕的幻灯片会严重影响网站的美观性！', 'dmeng' ), '<a href="https://tinypng.com/" target="_blank">TinyPNG</a>').'</p>';
    
    // 页面 nonce
    echo dmeng_options_new_nonce(__FUNCTION__);

    if (
      'new'==$action
      || 'edit'==$action
      || 'update'==$action
    ) {

      if ('new'==$action) {
        $slide_id = current_time('timestamp');
        $slide = array();
      } else {
        $slide_id = (int)$data[$action];
        $slide = (array)wp_unslash(json_decode(get_option('dmeng_slide_'.$slide_id), true));
      }

      $slide = array_merge(
        array(
          'name' => '',
          'img' => array(),
          'url' => array(),
          'title' => array(),
          'desc' => array()
        ), $slide
      );

      echo 
        dmeng_options_form_output(
          array( 'method' => 'post', 'style' => 'display:inline' ),
          '<input class="button" type="submit" value="'.__('返回列表', 'dmeng').'" />',
          false
        ) . 
        dmeng_options_form_output(
          array( 'method' => 'post', 'style' => 'display:inline' ),
          '<input type="hidden" name="edit" value="'.$slide_id.'" />
          <input type="hidden" name="_wpnonce" value="'.wp_create_nonce('edit_'.$slide_id).'" />
          <input class="button" type="submit" value="'.__('刷新', 'dmeng').'" />',
          false
        ) . 
        dmeng_options_form_output(
          array( 'method' => 'post', 'style' => 'display:inline' ),
          '<input type="hidden" name="delete" value="'.$slide_id.'" />
          <input type="hidden" name="_wpnonce" value="'.wp_create_nonce('delete_'.$slide_id).'" />
          <input class="button confirm_action" type="submit" value="'.__('删除', 'dmeng').'" data-confirm-title="'.__('删除后不可恢复，你确定删除吗？', 'dmeng').'" />',
          false
        );

        echo '<div class="options-box hide" id="example-slide">'.dmeng_options_table_build(array(
          array(
            'value' => '<input name="img[]" type="text" value="" class="regular-text ltr" required="required"> 
            <a href="javascript:;" class="button slide_upload_button">'.__('选择或上传', 'dmeng').'</a> 
            <a href="javascript:;" class="button slide_preview_button">'.__('预览', 'dmeng').'</a>',
            'title' => __('图片（必须）', 'dmeng')
          ), 
          array(
            'type' => 'input',
            'name' => 'url[]',
            'value' => '',
            'title' => __('链接', 'dmeng')
          ),
          array(
            'type' => 'input',
            'name' => 'title[]',
            'value' => '',
            'title' => __('标题', 'dmeng')
          ),
          array(
            'type' => 'input',
            'name' => 'desc[]',
            'value' => '',
            'title' => __('描述', 'dmeng')
          ),
          array(
            'value' => '
              <a href="javascript:;" class="button slide_up">'.__('往上移', 'dmeng').'</a>
              <a href="javascript:;" class="button slide_down">'.__('往下移', 'dmeng').'</a>
              <a href="javascript:;" class="button slide_delete">'.__('删除', 'dmeng').'</a>
              <a href="javascript:;" class="button slide_add">'.__('添加一项', 'dmeng').'</a>
					  ',
            'title' => __('操作', 'dmeng')
          ),
        )).'</div>'.
        dmeng_options_form_output(
          array( 'method' => 'post', 'id' => 'slideform', 'style' => 'margin:0;padding:0' ),
          '<h3>'.__('正在编辑幻灯片组', 'dmeng').'</h3>'.
          '<div class="options-box">'.
          dmeng_options_table_build(array(
            array(
              'type' => 'input',
              'name' => 'name',
              'value' => $slide['name'],
              'title' => __('名称', 'dmeng'),
              'description' => __('仅作为列表页显示区分', 'dmeng')
            ),
            array(
              'value' => '<input value="'.esc_attr('[dmengslide id="'.$slide_id.'"]').'" type="text" class="regular-text ltr" disabled="disabled" />',
              'title' => __('短代码', 'dmeng'),
              'description' => __('在文章内容等地方调用使用的短代码', 'dmeng')
            )
          )).
          '</div><div id="afterslide"></div>
          <input type="hidden" name="update" value="'.$slide_id.'" />
          <input type="hidden" name="_wpnonce" value="'.wp_create_nonce('update_'.$slide_id).'" />
          <input class="button button-primary" type="submit" value="'.__('保存更改', 'dmeng').'" /> 
          <a href="javascript:;" class="button slide_new">'.__('添加一项', 'dmeng').'</a>
          ',
          false
        );
          
        if ($slide['img']) {
          foreach ($slide['img'] as $key=>$img) {
            if (empty($img))
              continue;
            
            echo '
            <div class="slide-item hide">
              <b class="img">'.$img.'</b>
              <b class="url">'.$slide['url'][$key].'</b>
              <b class="title">'.$slide['title'][$key].'</b>
              <b class="desc">'.$slide['desc'][$key].'</b>
            </div>';
          }
        }
          
?>
<script type="text/javascript">
!function($){
$(document).ready(function($){

  window.newSlide = function(img, url, title, desc) {
    var $slide = $('#example-slide').clone(true),
        data = {'img':img, 'url':url, 'title':title, 'desc':desc};
    $slide.attr('id', null).removeClass('hide').addClass('slide-box');
    for (key in data) {
      if (data[key]) {
        $slide.find('[name*='+key+']').val(data[key]);
      }
    }
    return $slide;
  };

  $.each($('.slide-item'), function(){
    $('#afterslide').before(newSlide(
      $(this).find('.img').html(),
      $(this).find('.url').html(),
      $(this).find('.title').html(),
      $(this).find('.desc').html()
    ));
    $(this).remove();
  });

	var upload_frame;   
  $('.slide_upload_button').on('click', function(event) {
    event.preventDefault();
    var $input = $(this).siblings('input');
    upload_frame = wp.media({
      multiple: false,
      library : {
        type : 'image'
      }
    });   
    upload_frame.on('select',function(){
      attachment = upload_frame.state().get('selection').first().toJSON();
      $input.val(attachment.url);
    });   
    upload_frame.open();
  });
  $('.slide_preview_button').on('click', function() {
		var $input = $(this).siblings('input');
    if ($input.val()) {
      window.open($input.val());
    } else {
      $input.focus();
    }
  });
  $('.slide_up').on('click', function() {
		var $table = $(this).parents('.slide-box');
		$table.prev('.slide-box').before($table);
  });
  $('.slide_down').on('click', function() {
		var $table = $(this).parents('.slide-box');
		$table.next('.slide-box').after($table);
  });
  $('.slide_delete').on('click', function() {
    if ($('.slide-box').length>1) {
      $(this).parents('.slide-box').remove();;
    } else {
      alert('<?php _e('一组幻灯片至少要有一个幻灯片', 'dmeng');?>');
    }
  });
  $('.slide_add').on('click', function() {
    $(this).parents('.slide-box').after(newSlide());
  });
  $('.slide_new').on('click', function() {
    $('#afterslide').before(newSlide());
  });
});
}(jQuery);
</script>
<?php

    } else {

      echo dmeng_options_form_output(
            array('method' => 'post'),
            '<input type="hidden" name="new" value="1" />
            <input type="hidden" name="_wpnonce" value="'.wp_create_nonce('new').'" />
            <input class="button button-primary" type="submit" value="'.__('新建一组幻灯片', 'dmeng').'" />',
            false
           );
    
      $slides = (array)wp_unslash(json_decode(get_option('dmeng_slides'), true));
      if ($slides) {
        $filter_slides = array_flip($slides);
        foreach ($slides as $slide_id) {
          $slide = (array)wp_unslash(json_decode(get_option('dmeng_slide_'.$slide_id), true));
          if (
            empty($slide)
            || empty($slide['img'])
            || false===is_array($slide['img'])
          ) {
            unset($filter_slides[$slide_id]);
            continue;
          }

          $row = array();

          $edit_form = dmeng_options_form_output(
            array( 'method' => 'post', 'style' => 'display:inline' ),
            '<input type="hidden" name="edit" value="'.$slide_id.'" />
            <input type="hidden" name="_wpnonce" value="'.wp_create_nonce('edit_'.$slide_id).'" />
            <input class="button" type="submit" value="'.__('编辑', 'dmeng').'" />',
            false
          );
          
          $delete_form = dmeng_options_form_output(
            array( 'method' => 'post', 'style' => 'display:inline' ),
            '<input type="hidden" name="delete" value="'.$slide_id.'" />
            <input type="hidden" name="_wpnonce" value="'.wp_create_nonce('delete_'.$slide_id).'" />
            <input class="button confirm_action" type="submit" value="'.__('删除', 'dmeng').'" data-confirm-title="'.__('删除后不可恢复，你确定删除吗？', 'dmeng').'" />',
            false
          );
          
          $row[] = array(
            'value' => '<input value="'.esc_attr('[dmengslide id="'.$slide_id.'"]').'" type="text" class="regular-text ltr" disabled="disabled" />'.$edit_form.$delete_form,
            'title' => $slide['name']
          );

          $image = '';
          foreach ($slide['img'] as $img) {
            $image .= '<img src="'.$img.'" width="200" style="margin-right:10px;border:0" />';
          }
          $row[] = array(
            'value' => $image,
            'title' => __('预览', 'dmeng')
          );

          echo '<div class="options-box">'.dmeng_options_table_build($row).'</div>';
          
        }
        
        $filter_slides = array_flip($filter_slides);
        if ($filter_slides!=$slides) {
          update_option('dmeng_slides', json_encode($filter_slides));
        }

      }
    
    }
  }
  
  public function dmengl2v_options() {
    echo '<div class="options-box">'.dmeng_options_table_build(array(
      array(
        'title' => __('说明', 'dmeng'),
        'value' => sprintf(__('用户登录后可见，未登录用户会提示 %s 并链接到登录页面', 'dmeng'), '<span style="color:#a94442;background:#f2dede;padding:0 .25em;margin:0 .25em">' . __('[此处内容登录可见]','dmeng') . '</span>')
      ),
      array(
        'title' => __('用法', 'dmeng'),
        'value' => __('使用<b>[dmengl2v]</b>和<b>[/dmengl2v]</b>包含需要隐藏的内容，且前后无空格', 'dmeng')
      ),
      array(
        'title' => __('示例', 'dmeng'),
        'value' => __('[dmengl2v]这里的内容需要登录后才可见。[/dmengl2v]', 'dmeng')
      )
    )).'</div>';
  }
  
  public function dmengr2v_options() {
    echo '<div class="options-box">'.dmeng_options_table_build(array(
      array(
        'title' => __('说明', 'dmeng'),
        'value' => sprintf(__('用户登录后可见，未登录用户会提示 %s 并链接到登录页面', 'dmeng'), '<span style="color:#a94442;background:#f2dede;padding:0 .25em;margin:0 .25em">' . __('[此处内容登录并发表评论可见]','dmeng') . '</span>')
      ),
      array(
        'title' => __('注意', 'dmeng'),
        'value' => __('仅在文章/页面内容中有效，其他地方使用正常返回内容。', 'dmeng')
      ),
      array(
        'title' => __('用法', 'dmeng'),
        'value' => __('使用<b>[dmengr2v]</b>和<b>[/dmengr2v]</b>包含需要隐藏的内容，且前后无空格', 'dmeng')
      ),
      array(
        'title' => __('示例', 'dmeng'),
        'value' => __('[dmengr2v]这里的内容需要登录并发布评论后才可见。[/dmengr2v]', 'dmeng')
      )
    )).'</div>';
  }
  
  public function dmenglist_options() {
    echo '<div class="options-box">'.dmeng_options_table_build(array(
      array(
        'title' => __('说明', 'dmeng'),
        'value' => __('通过短代码调用文章列表', 'dmeng')
      ),
      array(
        'title' => __('参数', 'dmeng'),
        'value' => 
          '<b>'.__('style', 'dmeng').'</b> => '.__('列表样式，可选 list / thumbnail / archive', 'dmeng').'<br />'.
          '<b>'.__('sticky_posts', 'dmeng').'</b> => '.__('只显示置顶文章，可选 0 / 1', 'dmeng').'<br />'.
          '<b>'.__('taxonomy', 'dmeng').'</b> => '.__('分类法别名', 'dmeng').'<br />'.
          '<b>'.__('terms', 'dmeng').'</b> => '.__('分类ID，多个以 , 隔开', 'dmeng').'<br />'.
          '<b>'.__('post_type', 'dmeng').'</b> => '.__('文章类型', 'dmeng').'<br />'.
          '<b>'.__('number', 'dmeng').'</b> => '.__('显示数量', 'dmeng').'<br />'
 
      ),
      array(
        'title' => __('示例', 'dmeng'),
        'value' => __('[dmengterms number="15"]', 'dmeng')
      ),
      array(
        'title' => __('备注', 'dmeng'),
        'value' => __('测试版暂不提供可视化生成工具，且正式版参数可能会有所调整', 'dmeng')
      )
    )).'</div>';
  }
  
  public function dmengterms_options() {
    echo '<div class="options-box">'.dmeng_options_table_build(array(
      array(
        'title' => __('说明', 'dmeng'),
        'value' => __('通过短代码调用分类列表', 'dmeng')
      ),
      array(
        'title' => __('参数', 'dmeng'),
        'value' => 
          '<b>'.__('style', 'dmeng').'</b> => '.__('列表样式，可选 cloud', 'dmeng').'<br />'.
          '<b>'.__('taxonomy', 'dmeng').'</b> => '.__('分类法别名', 'dmeng').'<br />'.
          '<b>'.__('include', 'dmeng').'</b> => '.__('必须包含分类ID，多个以 , 隔开', 'dmeng').'<br />'.
          '<b>'.__('exclude', 'dmeng').'</b> => '.__('排除分类ID，多个以 , 隔开', 'dmeng').'<br />'.
          '<b>'.__('number', 'dmeng').'</b> => '.__('显示数量', 'dmeng').'<br />'
 
      ),
      array(
        'title' => __('示例', 'dmeng'),
        'value' => __('[dmenglist style="list" terms="84"]', 'dmeng')
      ),
      array(
        'title' => __('备注', 'dmeng'),
        'value' => __('测试版暂不提供可视化生成工具，且正式版参数可能会有所调整', 'dmeng')
      )
    )).'</div>';
  }
  
  public function dmengrank_options() {
    echo '<div class="options-box">'.dmeng_options_table_build(array(
      array(
        'title' => __('说明', 'dmeng'),
        'value' => __('通过短代码调用排行榜', 'dmeng')
      ),
      array(
        'title' => __('参数', 'dmeng'),
        'value' => 
          '<b>'.__('type', 'dmeng').'</b> => '.__('类型，可选 search / posts ', 'dmeng').'<br />'.
          '<b>'.__('number', 'dmeng').'</b> => '.__('显示数量', 'dmeng').'<br />'
 
      ),
      array(
        'title' => __('示例', 'dmeng'),
        'value' => __('[dmengrank number="6"]', 'dmeng')
      ),
      array(
        'title' => __('备注', 'dmeng'),
        'value' => __('测试版暂不提供可视化生成工具，且正式版参数可能会有所调整', 'dmeng')
      )
    )).'</div>';
  }
  
  public function dmengfile_options() {
    echo '<div class="options-box">'.dmeng_options_table_build(array(
      array(
        'title' => __('说明', 'dmeng'),
        'value' => __('通过短代码调用附件信息', 'dmeng')
      ),
      array(
        'title' => __('参数', 'dmeng'),
        'value' => 
          '<b>'.__('id', 'dmeng').'</b> => '.__('附件ID', 'dmeng')
      ),
      array(
        'title' => __('示例', 'dmeng'),
        'value' => __('[dmengfile id="16"]', 'dmeng')
      ),
      array(
        'title' => __('备注', 'dmeng'),
        'value' => __('测试版暂不提供可视化生成工具，且正式版参数可能会有所调整', 'dmeng')
      )
    )).'</div>';
  }
  
  public function dmengaction_options() {
    echo '<div class="options-box">'.dmeng_options_table_build(array(
      array(
        'title' => __('说明', 'dmeng'),
        'value' => __('在用户发表评论或兑换礼品等操作的同时做其他动作，测试版仅支持积分操作', 'dmeng')
      ),
      array(
        'title' => __('参数', 'dmeng'),
        'value' => 
          '<b>'.__('time', 'dmeng').'</b> => '.__('用户第几次操作时运行，留空或设置为0时代表每一次', 'dmeng').'<br />'.
          '<b>'.__('do', 'dmeng').'</b> => '.__('动作名称，可选 credit ', 'dmeng').'<br />'.
          '<b>'.__('plus', 'dmeng').'</b> => '.__('添加的积分', 'dmeng').'<br />'.
          '<b>'.__('minus', 'dmeng').'</b> => '.__('消费的积分', 'dmeng').'<br />'.
          '<b>'.__('title', 'dmeng').'</b> => '.__('备注', 'dmeng').'<br />'
      ),
      array(
        'title' => __('示例', 'dmeng'),
        'value' => __('由于功能较为复杂，正式版将提供可视化工具生成，测试版建议暂不使用', 'dmeng')
      ),
      array(
        'title' => __('备注', 'dmeng'),
        'value' => __('测试版暂不提供可视化生成工具，且正式版参数可能会有所调整', 'dmeng')
      )
    )).'</div>';
  }

}

new dmeng_Options_Shortcode;