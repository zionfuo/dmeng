<?php

class dmeng_Postmeta {

  public function __construct() {
    add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
    add_action( 'save_post', array( $this, 'save_meta_boxes_data' ) );
  }
  
  public function add_meta_boxes() {
    
    $meta_boxes = array(
      'copyright' => array(
        'screens' => array( 'post', 'page' ),
        'title' => __( '版权声明', 'dmeng' )
      ),
      'comment_action' => array(
        'screens' => array( 'post', 'page', 'gift' ),
        'title' => __( '评论动作', 'dmeng' )
      )
    );

    foreach ( $meta_boxes as $key=>$data ) {
      foreach ( $data['screens'] as $screen ) {
        add_meta_box(
          'dmeng_'.$key,
          $data['title'],
          array( $this, $key.'_callback' ),
          $screen
        );
      }
    }
    
  }
  
  public function save_meta_boxes_data( $post_id ) {

    if (
      ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      || ! isset( $_POST['dmeng_meta_box_nonce'] ) 
      || ! wp_verify_nonce( $_POST['dmeng_meta_box_nonce'], 'dmeng_meta_box' ) 
      || ! current_user_can( 'edit_'.(  isset($_POST['post_type']) ? sanitize_key($_POST['post_type']) : 'post' ), $post_id )
    ) {
      return;
    }
    
    if ( isset( $_POST['dmeng_copyright_status'] ) ) {
      update_post_meta( $post_id, 'dmeng_copyright_status', absint($_POST['dmeng_copyright_status']) );
    }
    
    if ( isset( $_POST['dmeng_copyright_content'] ) ) {
      update_post_meta( $post_id, 'dmeng_copyright_content', htmlspecialchars($_POST['dmeng_copyright_content']) );
    }
    
    if ( isset( $_POST['dmeng_comment_action'] ) ) {
      update_post_meta( $post_id, 'dmeng_comment_action', htmlspecialchars($_POST['dmeng_comment_action']) );
    }
    
  }
  
  public function copyright_callback( $post ) {

    wp_nonce_field( 'dmeng_meta_box', 'dmeng_meta_box_nonce' );

    $cs = get_post_meta( $post->ID, 'dmeng_copyright_status', true );
    $cc = get_post_meta( $post->ID, 'dmeng_copyright_content', true );

    $copyright_status = is_numeric($cs) ? absint($cs) : absint(get_option('dmeng_copyright_status_default',1));
    $copyright_content = $cc ? $cc : get_option('dmeng_copyright_content_default');

    ?>
    <p>
      <select name="dmeng_copyright_status">
        <option value="1" <?php if( $copyright_status===1) echo 'selected="selected"';?>><?php _e( '显示', 'dmeng' );?></option>
        <option value="9" <?php if( $copyright_status!==1) echo 'selected="selected"';?>><?php _e( '不显示', 'dmeng' );?></option>
      </select>
    </p>
    <p><?php _e( '版权声明内容，文章链接用{link}表示，文章标题用{title}表示，站点地址用{url}表示，站点名称用{name}表示。', 'dmeng' );?></p>
    <textarea name="dmeng_copyright_content" rows="1" cols="50" class="large-text code"><?php echo stripcslashes(htmlspecialchars_decode($copyright_content));?></textarea>
    <?php
  }
  
  public function comment_action_callback( $post ) {
    $comment_action = get_post_meta( $post->ID, 'dmeng_comment_action', true );
    ?>
    <p><?php _e( '用户发表评论时执行的短代码，可以同时使用多个短代码，其他内容将被忽略。', 'dmeng' );?></p>
    <textarea name="dmeng_comment_action" rows="1" cols="50" class="large-text code"><?php echo stripcslashes(htmlspecialchars_decode($comment_action));?></textarea>
    <?php
  }

}
$dmeng_Postmeta = new dmeng_Postmeta;
