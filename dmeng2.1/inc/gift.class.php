<?php
/**
 * 
 */

class dmeng_Gift {

  public $post_type = 'gift';
  public $post_type_args = array();
  public $taxonomy = 'gift_tag';
  public $count_key = 'dmeng_gifts';

  public function __construct() {
  
    if ( dmeng_open_ctrl($this->post_type)===false )
      return;
  
    $this->post_type_args = array(
        'public' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'register_meta_box_cb' => array( $this, 'register_meta_box' ),
        'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
        'labels'  => array(
          'name'               => __('积分换礼', 'dmeng'),
          'singular_name'      => __('礼品', 'dmeng'),
          'menu_name'          => __('积分换礼', 'dmeng'),
          'name_admin_bar'     => __('积分换礼', 'dmeng'),
          'add_new'            => __('添加礼品', 'dmeng'),
          'add_new_item'       => __('添加礼品', 'dmeng'),
          'new_item'           => __('新礼品', 'dmeng'),
          'edit_item'          => __('编辑礼品', 'dmeng'),
          'view_item'          => __('查看礼品', 'dmeng'),
          'all_items'          => __('全部礼品', 'dmeng'),
          'search_items'       => __('搜索礼品', 'dmeng'),
          'parent_item_colon'  => __('礼品上级：', 'dmeng'),
          'not_found'          => __('没有找到礼品', 'dmeng'),
          'not_found_in_trash' => __('回收站里没有礼品', 'dmeng'),
        )
    );
  
    add_action( 'init', array( $this, 'gift_init' ) );
    
    add_filter( 'wp_title', array( $this, 'page_title' ), 10, 2 );
    
    add_action( 'save_post', array( $this, 'save_meta_box_data' ) );

    add_filter( 'post_type_link', array( $this, 'permalink' ), 10, 2);
    add_filter( 'term_link', array( $this, 'gift_tag_link' ), 10, 3 );
    
    add_filter( 'dmeng_breadcrumb_path', array( $this, 'breadcrumb_path' ) );

    add_filter( 'nav_menu_css_class', array( $this, 'nav_class' ), 10, 2);
    add_filter( 'post_class', array( $this, 'post_class' ), 99 );
    
    add_filter( 'option_dmeng_gift_tips', array( $this, 'default_text' ) );
    add_filter( 'option_dmeng_gift_notice', array( $this, 'default_text' ) );
    
    add_filter( 'dmeng_ready_ajax', array( $this, 'ready_ajax_action' ) );
    
    add_filter( 'query_vars', array( $this, 'query_vars' ) );
    add_filter( 'posts_where', array( $this, 'archive_posts_where' ) );
    add_filter( 'posts_join', array( $this, 'archive_posts_join' ) );
    add_filter( 'posts_groupby', array( $this, 'archive_posts_groupby' ) );
    add_action( 'pre_get_posts', array( $this, 'pre_get_gift' ) );
    
    add_action( 'wp', array( $this, 'query_404_error' ) );
    
    add_action( 'wp_ajax_dmeng_exchange', array( $this, 'exchange_action' ) );

    add_action( 'dmeng_author_main_content', array( $this, 'main_selection' ) );
    add_action( 'wp_ajax_dmeng_get_gift', array( $this, 'get_gift_ajax_action' ) );
    
    add_filter( 'dmeng_author_template_tabs', array( $this, 'text_i18n' ) );
    
    add_filter( 'dmeng_ucenter_text_i18n', array( $this, 'text_i18n' ) );
    add_filter( 'dmeng_ready_ajax_userdata', array( $this, 'ready_ajax_userdata' ), 10, 2 );
    add_action( 'dmeng_ucenter_setting_page_gift', array( $this, 'setting_page' ) );
  }
  
  /**
   * 网页标题
   */
  public function page_title( $title, $sep ) {
    if (is_singular($this->post_type)) {
      
        $title = join(" $sep ",
          array_filter(array(
            esc_html(get_the_title()),
            $this->post_type_args['labels']['name'],
            get_bloginfo('name')
          ))
        );
        
    } else if (is_post_type_archive($this->post_type)) {
      
      global $wp_query;
      if (!empty($wp_query->tax_query->queried_terms[$this->taxonomy]['terms'][0])) {
        $term = get_term_by('id', $wp_query->tax_query->queried_terms[$this->taxonomy]['terms'][0], $this->taxonomy);
        $title = join(" $sep ",
          array_filter(array(
            (empty($term->name) ? '' : esc_html($term->name)),
            $this->post_type_args['labels']['name'],
            get_bloginfo('name')
          ))
        );
      }
      
    }
    return $title;
  }

  public function register_meta_box() {
    foreach( array( 'info'=>__('礼品信息', 'dmeng'), 'content'=>__('礼品内容', 'dmeng'), 'action'=>__('兑换动作', 'dmeng') ) as $key=>$title ){
      add_meta_box(
        'dmeng_gift_meta_'.$key,
        $title,
        array( $this, $key.'_meta_box_callback' ),
        $this->post_type
      );
    }
  }

  public function info_meta_box_callback( $post ) {
		
		wp_nonce_field( 'dmeng_gift_meta_box', 'dmeng_gift_meta_box_nonce' );

		$gift = json_decode(get_post_meta( $post->ID, 'dmeng_gift_info', true ));

		if( !$gift ){
			$gift = json_decode('{"price":"99","stock":"99","max":"1","express":"1","content":"","attachment":[]}');
		}

		$credit = intval(get_post_meta( $post->ID, 'dmeng_gift_credit', true ));

		$info_items = array(
			array(
				'name' => '价格',
				'slug' => 'price',
				'value' => $gift->price,
				'excerpt' => '礼品市场价，单位是人民币“元”'
			),
			array(
				'name' => '积分',
				'slug' => 'credit',
				'value' => $credit,
				'excerpt' => '兑换需要积分，0 代表免费'
			),
			array(
				'name' => '库存',
				'slug' => 'stock',
				'value' => $gift->stock,
				'excerpt' => '礼品数量，0 代表无限量'
			),
			array(
				'name' => '最多',
				'slug' => 'max',
				'value' => $gift->max,
				'excerpt' => '每人最多兑换的数量，0 代表无限量'
			)
		);

		$output = '<style>
		#dmeng_gift_info label{margin-right:8px;}
		#dmeng_gift_content{margin: 0;height: 4em;width: 98%;}
		#dmeng_gift_attachment li{line-height:36px;border:1px solid #eee;}
		#dmeng_gift_attachment li label{background: #f5f5f5;padding: 5px 8px;margin: 0 8px;}
		#dmeng_gift_attachment li .delete{margin: 0 8px;border: 1px solid;padding: 3px 6px;}
		</style>
		<div id="dmeng_gift_info">';
		
		$output .= sprintf( '<p>%s</p>', __('填写信息只能是数字，文字会被转为 0', 'dmeng') );
		foreach( $info_items as $info_item ){
			$output .= sprintf( '<p><label for="%2$s">%1$s</label><input name="%2$s" id="%2$s" type="text" value="%3$s"> %4$s</p>' , $info_item['name'] , 'dmeng_gift_'.$info_item['slug'] , $info_item['value'] , $info_item['excerpt'] );
		}

		$express = dmeng_get_express_array();

		$output .= '<p><label for="dmeng_gift_express">'.__('物流', 'dmeng').'</label><select name="dmeng_gift_express" id="dmeng_gift_express">';
		
		foreach( $express as $express_key=>$express_title){
			$output .= sprintf( '<option value="%s" %s>%s</option>', $express_key, ( $gift->express==$express_key ? 'selected="selected"' : '' ), $express_title );
		}

		$output .= '</select></p>';
		
		$output .= '</div>';
		
		echo $output;

  }
  
  public function content_meta_box_callback( $post ) {
		
		$gift = json_decode(get_post_meta( $post->ID, 'dmeng_gift_info', true ));

		if( !$gift ){
			$gift = json_decode('{"price":"99","credit":"0","stock":"99","max":"1","express":"1","content":"","attachment":[]}');
		}
		
		echo '<p>'.__('以下内容在用户使用积分兑换后可见，礼品介绍请放在文章内容中。注意：编辑后需要保存文章才可保存', 'dmeng').'</p>';
		wp_editor( stripslashes(htmlspecialchars_decode($gift->content)), 'dmeng_gift_content', array( 'media_buttons' => false ) );
		
		$output = '<p><a class="button select_gift_attachment" href="javascript:;" data-uploader_title="'.__('选择附件', 'dmeng').'" data-uploader_button_text="'.__('添加附件', 'dmeng').'">'.__('添加附件', 'dmeng').'</a></p>';
		$output .= '<p>'.__('附件地址会被加密，请不要泄露真实地址！名称是附件的标题，所有人都可见。注意：本功能只适合小文件传输。', 'dmeng').'</p>';
		$output .= '<ul id="dmeng_gift_attachment">';
		if($gift->attachment && is_array($gift->attachment)){
			foreach($gift->attachment as $gift_attachment){
				$output .= '<li><a href="javascript:;" class="delete">'.__('删除', 'dmeng').'</a><input type="hidden" name="dmeng_gift_attachment[]" value="'.$gift_attachment.'"><label>ID</label>'.$gift_attachment.'<label>'.__('名称', 'dmeng').'</label><a href="'.get_edit_post_link($gift_attachment).'" target="_blank">'.get_the_title($gift_attachment).'</a></li>';
			}
		}
		$output .= '</ul>';
		echo $output;

?>
<script>
jQuery('#dmeng_gift_attachment .delete').live('click', function( event ){
	 jQuery( this ).parent('li').remove();
});

var file_frame;
jQuery('.select_gift_attachment').live('click', function( event ){
 
	event.preventDefault();

    if ( file_frame ) {
      file_frame.open();
      return;
    }

    file_frame = wp.media.frames.file_frame = wp.media({
      title: jQuery( this ).data( 'uploader_title' ),
      button: {
        text: jQuery( this ).data( 'uploader_button_text' ),
      },
      multiple: false
    });
 
    file_frame.on( 'select', function() {
		attachment = file_frame.state().get('selection').first().toJSON();
		jQuery('#dmeng_gift_attachment').append('<li><a href="javascript:;" class="delete"><?php _e('删除', 'dmeng');?></a><input type="hidden" name="dmeng_gift_attachment[]" value="'+attachment.id+'"><label>ID</label>'+attachment.id+'<label><?php _e('名称', 'dmeng');?></label><a href="'+attachment.editLink+'" target="_blank">'+attachment.title+'</a></li>');
    });
 
    file_frame.open();
});
</script>
<?php
  }
  
  public function action_meta_box_callback( $post ) {
    $action = get_post_meta( $post->ID, 'dmeng_exchange_action', true );
    ?>
    <p><?php _e( '用户兑换礼品时执行的短代码，可以同时使用多个，短代码以外的内容将被忽略。', 'dmeng' );?></p>
    <textarea name="dmeng_exchange_action" rows="1" cols="50" class="large-text code"><?php echo stripcslashes(htmlspecialchars_decode($action));?></textarea>
    <?php
  }
  
  public function save_meta_box_data( $post_id ) {
    if ( 
              ! isset( $_POST['dmeng_gift_meta_box_nonce'] ) || 
              ! wp_verify_nonce( $_POST['dmeng_gift_meta_box_nonce'], 'dmeng_gift_meta_box' ) || 
              ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || 
              ! current_user_can( 'edit_post', $post_id )
    ) {
      return;
    }
      
      $gift_data = array();

      foreach( array('price', 'stock', 'max', 'express') as $info_item ){
        if ( isset( $_POST['dmeng_gift_'.$info_item] ) ) $gift_data[$info_item] =  intval($_POST['dmeng_gift_'.$info_item]);
      }

      if ( isset( $_POST['dmeng_gift_content'] ) ) $gift_data['content'] =  htmlspecialchars($_POST['dmeng_gift_content']);
      if ( isset( $_POST['dmeng_gift_attachment'] ) ) $gift_data['attachment'] =  (array)$_POST['dmeng_gift_attachment'];
      
      $credit = isset( $_POST['dmeng_gift_credit'] ) ? intval($_POST['dmeng_gift_credit']) : 0;
      
      update_post_meta( $post_id, 'dmeng_gift_credit', $credit);
      
      update_post_meta( $post_id, 'dmeng_gift_info', wp_slash(json_encode(wp_parse_args( $gift_data, array(
        'price' => 99,
        'credit' => $credit,
        'stock' => 99,
        'max' => 1,
        'express' => 1,
        'content' => '',
        'attachment' => array()
      ) ))));
      
    if ( isset( $_POST['dmeng_exchange_action'] ) ) {
      update_post_meta( $post_id, 'dmeng_exchange_action', htmlspecialchars($_POST['dmeng_exchange_action']) );
    }
  }

  public function gift_init(){
    register_post_type( $this->post_type, $this->post_type_args );
    //~ 礼品页重写规则
    add_rewrite_rule(
      $this->post_type.'/([0-9]+)?$',
      'index.php?post_type='.$this->post_type.'&p=$matches[1]',
      'top' );
    //~ 礼品页重写规则（文章分页）
    add_rewrite_rule(
      $this->post_type.'/([0-9]+)/([0-9]+)?$',
      'index.php?post_type='.$this->post_type.'&p=$matches[1]&page=$matches[2]',
      'top' );
    //~ 礼品页重写规则（评论分页）
    add_rewrite_rule(
      $this->post_type.'/([0-9]+)/comment-page-([0-9]+)?$',
      'index.php?post_type='.$this->post_type.'&p=$matches[1]&cpage=$matches[2]',
      'top' );
    register_taxonomy( $this->taxonomy, $this->post_type, array(
      'label'          => __('礼品目录', 'dmeng'),
      'hierarchical'          => true,
      'show_ui'               => true,
      'show_admin_column'     => true,
      'query_var'             => true
    ) );
  }
  
  public function permalink( $link, $post=0 ) {
    if ( $post->post_type == $this->post_type && get_option('permalink_structure') )
      return home_url( $this->post_type . '/' . $post->ID );

    return $link;
  }
  
  public function gift_tag_link( $termlink, $term, $taxonomy ) {
    if ( $taxonomy==$this->taxonomy )
      $termlink = add_query_arg( array( 't'=>$term->term_id ), get_post_type_archive_link($this->post_type) );
      
    return $termlink;
  }
  
  public function breadcrumb_path( $path ) {
    if ( is_post_type_archive($this->post_type) || is_tax($this->taxonomy) ) {
      global $dmeng_Breadcrumb;
      $path[] = $dmeng_Breadcrumb->item_output( get_bloginfo('name'), home_url('/') );
      $path[] = $dmeng_Breadcrumb->item_output( $this->post_type_args['labels']['name'], get_post_type_archive_link($this->post_type) );
    }
    return $path;
  }
  
  public function nav_class( $classes, $item ) {
    if ( is_singular($this->post_type) || is_post_type_archive($this->post_type) ) {
      if ( in_array( $item->url, array( home_url( '/?post_type='.$this->post_type ), get_post_type_archive_link($this->post_type)	) ) && !in_array( 'active', $classes ) ) {
        $classes[] = 'active';
      }
    }
    return $classes;
  }
  
  public function post_class( $classes ) {
    if (is_main_query() && ( is_post_type_archive($this->post_type) || is_tax($this->taxonomy) )) {
      $classes = array( 'single-post clearfix' );
      if (false===dmeng_is_mobile())
        $classes[] = 'col-lg-3 col-md-4 col-sm-6';
    }
    return $classes;
  }

  public function default_text( $default ) {
    if ( empty($default) ) {
      switch ( str_replace( 'option_dmeng_gift_', '', current_filter() ) ) {
        case 'tips' :
          $default = __( '兑换成功后请留意信息通知，如有兑换后可见的内容可直接查看。', 'dmeng' );
        break;
        case 'notice' :
          $default = __( '兑换后如有隐藏内容或附件下载都将显示在下方，已兑换礼品列表及兑换凭证可在用户中心查看。', 'dmeng' );
        break;
      }
    }
    return $default;
  }
  
  public function info_base_filter( $gift_info ) {
  
    $gift_info['price'] = sprintf("%.2f", intval($gift_info['price']));

    $max = intval($gift_info['max']);
    
    $stock = intval($gift_info['stock']);
    $gift_info['stock'] = $stock==0 ? __('不限量', 'dmeng') : $stock;

    $credit = intval($gift_info['credit']);
    $gift_info['credit'] = $credit===0 ? __('免费', 'dmeng') : $credit;

    $express = dmeng_get_express_array();
    $gift_info['express'] = $express[intval($gift_info['express'])];

    if ( $gift_info['attachment'] ) {
      $atta_html = '';
      foreach( $gift_info['attachment'] as $attachment_id ) {
        $apath = get_attached_file($attachment_id);
        $basename = basename($apath);
        $atta_html .= sprintf( __('%1$s : <a href="%2$s" target="_blank">%3$s (%4$s)</a><br>', 'dmeng'), get_the_title($attachment_id), wp_get_attachment_url($attachment_id), $basename, size_format(filesize($apath))  );
      }
      $gift_info['attachment'] = $atta_html;
    }

    return $gift_info;
  }
  
  public function info_ajax_filter( $post_id, $gift_info='' ) {
  
    if ( empty($gift_info) )
      $gift_info = (array)wp_unslash(json_decode(get_post_meta( $post_id, 'dmeng_gift_info', true )));
    
    $gift_info = $this->info_base_filter($gift_info);

    $exchange_num = $exchange_new = $buyers = 0;
    $buyers = (array)wp_unslash(json_decode(get_post_meta( $post_id, 'dmeng_gift_buyers', true )));
    $current_user_id = get_current_user_id();
    if ( $current_user_id ) {
      $count_buyers = array_count_values($buyers);
      $exchange_num = isset($count_buyers[$current_user_id]) ? intval($count_buyers[$current_user_id]) : 0;
      
      if ( $gift_info['stock']!=0 && $gift_info['stock']<= count($buyers) ) {
        $exchange_title = __('礼品库存不足', 'dmeng');
      } else if ( $gift_info['max']>0 && $exchange_num>=$gift_info['max'] ) {
        $exchange_title = __('已达到最大兑换数', 'dmeng');
      } else if ( intval(get_user_meta($current_user_id, 'dmeng_credit', true)) < $gift_info['credit'] ) {
        $exchange_title = __('积分余额不足', 'dmeng');
      } else {
        $exchange_new = 1;
        $exchange_title = __('立即兑换', 'dmeng');
      }

    } else {
      $exchange_new = 1;
      $exchange_title = __( '请先登录', 'dmeng' );
    }
    
    if ( $exchange_num===0  || empty($gift_info['content']) ) {
      unset($gift_info['content']);
    } else {
      $gift_info['content'] = stripslashes(htmlspecialchars_decode($gift_info['content']));
    }
    
    if ( $exchange_num===0  || empty($gift_info['attachment']) ) {
       unset($gift_info['attachment']);
    }
    
    $gift_info['buyers'] = count($buyers);
    
    return array(
      'info' => $gift_info,
      'exchange' => array(
        'num' => $exchange_num,
        'new' => $exchange_new,
        'title' => $exchange_title
      )
    );

  }
  
  public function ready_ajax_action( $return_array ) {

    if ( 
      !isset($_POST['tracker'])
      || !isset($_POST['tracker']['type'])
      || !isset($_POST['tracker']['pid'])
      || $_POST['tracker']['type']!='single'
      || get_post_type(intval($_POST['tracker']['pid']))!=$this->post_type
    ) {
      return $return_array;
    }
    
    $return_array[$this->post_type] = $this->info_ajax_filter( intval($_POST['tracker']['pid']) );

    return $return_array;
  }

  public function query_vars( $vars ) {
    $vars[] = 't';
    $vars[] = 'max';
    $vars[] = 'min';
    return $vars;
  }
  
  public function archive_posts_where( $where ) {
  
    global $wp_query, $wpdb;
    $max = isset($wp_query->query_vars['max']) ? intval($wp_query->query_vars['max']) : 0;
    $min = isset($wp_query->query_vars['min']) ? intval($wp_query->query_vars['min']) : 0;

    if ( !is_admin() && in_the_loop() && 
          is_post_type_archive($this->post_type) && 
          ($max || $min)
    ) {
      $where .= " AND ( ($wpdb->postmeta.meta_key = 'dmeng_gift_credit' ";
      if ($max)
        $where .= " AND ( $wpdb->postmeta.meta_value + 0 ) <= $max ";
      if ($min)
        $where .= " AND ( $wpdb->postmeta.meta_value + 0 ) >= $min ";
      $where .= " ) ) ";
    }
    return $where;
  }

  public function archive_posts_join( $join ) {
    global $wp_query, $wpdb;
    if ( !is_admin() && in_the_loop() && 
          is_post_type_archive($this->post_type) && 
          (!empty($wp_query->query_vars['max']) || !empty($wp_query->query_vars['min']))
    ) {
      $join .= "LEFT JOIN $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id ";
    }
    return $join;
  }
  
  public function archive_posts_groupby( $groupby ) {
    global $wp_query, $wpdb;
    if ( !is_admin() && in_the_loop() && 
          is_post_type_archive($this->post_type) && 
          (!empty($wp_query->query_vars['max']) || !empty($wp_query->query_vars['min'])) 
    ) {
      $groupby = "{$wpdb->posts}.ID";
    }
    return $groupby;
  }
  
  public function pre_get_gift( $query ) {

    if ( !is_admin() && $query->is_main_query() && $query->is_post_type_archive($this->post_type) ) {
      $query->set( 'posts_per_page', intval(get_option('dmeng_gift_num', 12)) );

      $gift_status = array( 'publish' );
      if ( intval(get_option('dmeng_is_gift_future', 1)) )
        $gift_status[] = 'future';
      
      $query->set( 'post_status', $gift_status );

      if ( !empty($query->query_vars['t']) ) {
        $query->set( 'tax_query', array( array(
            'taxonomy' => $this->taxonomy,
            'field' => 'id',
            'terms' => absint($query->query_vars['t'])
        ) ) );
      }
      
    }
    
  }
  
  public function query_404_error() {
    global $wp_query;
    if ( !empty($wp_query->query_vars['t']) && !get_term_by( 'id', absint($wp_query->query_vars['t']), $this->taxonomy ) ) {
      $wp_query->set_404();
      status_header( 404 );
      nocache_headers();
    }
  }
  
  public function nonce( $user_id, $gift_id, $time ) {
    return $user_id .'P'. $gift_id .'T'. strtotime($time) .'N'. strtoupper(substr( wp_hash( $user_id . $gift_id . $time . NONCE_KEY ), -12, 10 ));
  }
  
  public function unnonce( $nonce ) {
  
    preg_match( '/^(.*)P(.*)T(.*)N(.*)$/', sanitize_text_field($nonce), $matches );

    if ( 
      empty($matches[1]) 
      || empty($matches[2]) 
      || empty($matches[3]) 
      || $this->nonce( $matches[1], $matches[2], date( 'Y-m-d H:i:s', $matches[3] ) ) != $nonce
    ) {
      return false;
    }

    return array(
      'user_id' =>$matches[1],
      'gift_id' => $matches[2],
      'time' => date( 'Y-m-d H:i:s', $matches[3] )
    );
  }

  public function get( $uid=0, $count=0, $limit=0, $offset=0 ) {

    if ( !$uid )
      return;

    global $wpdb, $dmeng_Meta;
    $table_name = $wpdb->prefix . $dmeng_Meta->table_name;
    
    if ($count) {
      $check = $wpdb->get_var( "SELECT COUNT(*) FROM $table_name WHERE `meta_key` LIKE 'exchange_%' AND user_id='$uid' " );
    } else {
      $check = $wpdb->get_results( "SELECT meta_key,meta_value FROM $table_name WHERE `meta_key` LIKE 'exchange_%' AND user_id='$uid' ORDER BY meta_id DESC LIMIT $offset,$limit" );
    }
    
    return isset($check) ? $check : 0;
  }
  
  public function count_gift( $user_id=0 ) {
  
    $count = get_user_meta( $user_id, $this->count_key, true );
    if ( $count=='' ) {
      $count = intval($this->get( $user_id, true ));
      update_user_meta( $user_id, $this->count_key, $count );
    }

    return intval($count);
  }
  
  public function count_posts( $status='publish' ) {
    $count_posts = wp_count_posts( $this->post_type );
    return $count_posts->$status;
  }

  public function exchange( $post_id, $user_id ) {
    if ( add_dmeng_meta( 'exchange_'.absint($post_id), current_time('mysql'), absint($user_id) ) ) {
      update_user_meta( $user_id, $this->count_key, intval($this->get( $user_id, true )) );
      return 1;
    }
    return 0;
  }

  public function exchange_action_callback() {
    
    $post_id = absint($_POST['post_id']);
    $current_user = wp_get_current_user();
    if ( !$current_user->ID )
      return array( 'error'=>__('请先登录！', 'dmeng').'<script>location.href=dmeng.loginurl();</script>' );

    $uid = $current_user->ID;

    $gift = (array)wp_unslash(json_decode(get_post_meta( $post_id, 'dmeng_gift_info', true ), true));
    if ( empty($gift) )
      return array( 'error'=>__('无法查询礼品信息！', 'dmeng') );

    $buyers = (array)wp_unslash(json_decode(get_post_meta( $post_id, 'dmeng_gift_buyers', true ), true));

    $stock = absint($gift['stock']);

    /**
     * 库存检查
     */
    if ( $stock!=0 && $stock<= count($buyers) )
      return array( 'error'=>__('礼品库存不足！', 'dmeng'), $this->post_type=>$this->info_ajax_filter( $post_id, $gift ) );
    
    /**
     * 兑换次数检查
     */
    $count_buyers = array_count_values($buyers);
    $count_exchange = isset($count_buyers[$uid]) ? intval($count_buyers[$uid]) : 0;
    $max = absint($gift['max']);
    if ( $max!=0 && $max<= $count_exchange )
      return array( 'error'=>__('你已达到最大兑换数！', 'dmeng'), $this->post_type=>$this->info_ajax_filter( $post_id, $gift ) );
      
    /**
     * 可用积分余额检查
     */
    $credit = intval($current_user->dmeng_credit);
    $gift_credit = intval($gift['credit']);
    if ( $gift_credit > $credit )
      return array( 'error'=>__('你的积分余额不足！', 'dmeng'), $this->post_type=>$this->info_ajax_filter( $post_id, $gift ) );

    $buyers[] = $uid;
    
    $this->exchange( $post_id, $uid );
    update_post_meta( $post_id, 'dmeng_gift_buyers', json_encode($buyers) );
    
    $gift_title = dmeng_the_title( $post_id );
    if ( $gift_credit > 0 ) {
      dmeng_credit_to_void( $uid , $gift_credit, sprintf(__('花费了%1$s积分兑换%2$s','dmeng') ,$gift_credit, $gift_title) );
    }

    $m_headline = sprintf( __('恭喜，你已成功兑换<a href="%1$s" target="_blank">%2$s</a>','dmeng'), get_permalink($post_id), $gift_title );

    /**
     * 邮件通知
     */
    if ( is_email($current_user->user_email) ) {
      dmeng_mail( $current_user->user_email, strip_tags($m_headline), '<h3>'.sprintf( __('%1$s，你好！','dmeng'), $current_user->display_name ).'</h3><p>'.$m_headline.'<br/>'.sprintf( __('温馨提示：%s','dmeng'), get_option('dmeng_gift_tips') ).'</p>' );
    }

    /**
     * 站内消息
     */
    //~ add_dmeng_message( $uid, 'unread', current_time('mysql'), __('礼品兑换通知', 'dmeng'), $m_headline);
    
    $post = get_post($post_id);
    do_shortcode(stripcslashes(htmlspecialchars_decode(get_post_meta( $post_id, 'dmeng_exchange_action', true ))));

    return array( 'success' =>__('兑换成功！', 'dmeng'), $this->post_type => $this->info_ajax_filter( $post_id, $gift ) );
  }
  
  public function exchange_action() {

    if ( empty($_POST['post_id']) )
      return;
    
    do_action( 'dmeng_before_ajax' );

    wp_send_json( $this->exchange_action_callback() );
  }

  public function main_selection() {
    if ($GLOBALS['tab']!==$this->post_type)
      return;

  ?>

<ul id="uclist" class="noborder" style="display:none"></ul>

<script>
!function($){
  'use strict';
  var $tips = dmengUcenter.$tips,
      user_id = dmengUcenter.user_id,
      privTip = '<?php _e('您无权查看Ta的兑换记录','dmeng'); ?>',
      firstLoad = true,
      load = dmengUcenter.load = function () {
        if (firstLoad===false) {
          dmeng.goHash('#uclist');
          $('#uclist').html('<li><?php _e('加载中…','dmeng'); ?></li>');
        }
        firstLoad = false;
        dmengUcenter.autoLoad();
        dmengUcenter.loaded.success(function (response) {
          if (response==0) {
            $tips.html(privTip);
          } else if (typeof response==='object') {
            $tips.html(response.tips);
            if (response.data) {
              $('#uclist').html(response.data).fadeIn();
            }
          } else {
            $('#uclist').html(response).fadeIn();
          }
        });
      };
  $(document).ready(function () {
    dmeng.readyAjax.complete(function () {
      if (!dmeng.user.ID) {
        $tips.html(privTip);
        return;
      }
      if (dmeng.user.ID===user_id) {
        $tips.html('<?php _e('查看自己的记录请到用户中心。', 'dmeng');?><a href="'+dmeng.ucenter.url_format.replace( '%s', '<?php echo $this->post_type;?>' )+'"><?php _e('点击查看','dmeng');?></a>').fadeIn();
        return;
      }
      load();
    });
  }).on('click', '#get_prev', dmengUcenter.prev
  ).on('click', '#get_next', dmengUcenter.next
  ).on('change', '#paginate', dmengUcenter.selectPaged
  );
}(jQuery);
</script>
  <?php
  }
  
  
  public function get_gift_ajax_action() {

    do_action( 'dmeng_before_ajax' );
    
    if ( empty($_POST['user_id']) )
      die( '<li>'.__('用户ID不能为空', 'dmeng').'</li>' );
    
    $user_id = absint($_POST['user_id']);
    if ( !current_user_can('edit_user', $user_id) )
      die(0);

    $user = get_userdata($user_id);
    $count = $this->count_gift($user->ID);
    
    $paged = empty($_POST['paged']) ? 1 : absint($_POST['paged']);
    $number = 10;
    $offset = ($paged-1)*$number;

    $giftLog = $this->get($user->ID, false, $number,$offset);

    $result = array();
    $result['tips'] = sprintf( __('共兑换了 %1$s 个礼品','dmeng'), $count );
    
    if ($giftLog) {
      
      $result['data'] = '';
      
      foreach( $giftLog as $log ){
        $gift_id = explode('_', $log->meta_key);
        $gift_id = $gift_id[1];
        $title = dmeng_the_title($gift_id);
        $result['data'] .= '<li><span class="num">'.$log->meta_value.'</span> <span class="text">' .  ( empty($title) ? sprintf( __( '兑换了ID是%1$s的礼品（无法查询具体信息，可能已下架）', 'dmeng' ), $gift_id ) : sprintf( __('兑换了%s', 'dmeng'), '<a href="'.get_permalink($gift_id).'" target="_blank">'.esc_html($title).'</a>' ) ) .'</span></li>';
      }

      ob_start();
      global $dmeng_UCenter;
      $dmeng_UCenter->setting_paginate( ceil($count/$number), $paged );
      $result['data'] .= ob_get_contents();
      ob_end_clean();
    }
    
    wp_send_json($result);
  }
  
  public function text_i18n( $array ) {
    $array[$this->post_type] = $this->post_type_args['labels']['singular_name'];
    return $array;
  }
  
  public function ready_ajax_userdata( $userdata, $current_user ) {
    $userdata['user_info'][$this->post_type] = $this->count_gift($current_user->ID);
    return $userdata;
  }
  
  /**
   * 用户中心选项卡
   */
  public function setting_page() {
    
    $current_user = get_userdata(get_current_user_id());
    $count = $this->count_gift($current_user->ID);

    ?>
    <div class="entry-content">
      <ul class="ucenter_list">
        <?php

        $paged = !empty($_POST['paged']) ? intval($_POST['paged']) : 1; 
        $number = 10;
        $num_page = ceil($count/$number);
        $offset = ($paged-1)*$number;

      if ($count)
        echo '<li class="title num">' . sprintf( __('共兑换了 %1$s 个礼品','dmeng'), $count ) . '</li>';
      
      $giftLog = $this->get($current_user->ID, false, $number,$offset);
      
      if ($giftLog) {
        
          foreach( $giftLog as $log ){
            $gift_id = explode('_', $log->meta_key);
            $gift_id = $gift_id[1];
            $title = dmeng_the_title($gift_id);
            echo '<li><span class="num">'.$log->meta_value.'</span> <span class="text">' .  ( empty($title) ? sprintf( __( '兑换了ID是%1$s的礼品（无法查询具体信息，可能已下架）', 'dmeng' ), $gift_id ) : sprintf(
                __('兑换了%1$s，凭证号是%2$s ', 'dmeng'), '<a href="'.get_permalink($gift_id).'" target="_blank">'.esc_html($title).'</a>', '<i class="num">'.$this->nonce($current_user->ID, $gift_id, $log->meta_value).'</i>' ) ) .'</span></li>';
          }
          
        global $dmeng_UCenter;
        $dmeng_UCenter->setting_paginate( $num_page, $paged );
        
      } else { 
        echo '<li>' . __( '还没有兑换过礼品', 'dmeng' ) . '</li>';
      }

        ?>
      </ul>
    </div>
    <?php
    die();
  }
  
}

$dmeng_Gift = new dmeng_Gift;
