<?php

class dmeng_Count {

  public $table_name = 'dmeng_count';

  public function __construct() {
    add_action( 'wp_insert_comment', array( $this, 'comments_cache_flush' ), 10, 2 );
  }
  
  public function get( $type ) {
    return method_exists( $this, $type ) ? absint($this->$type()) : 0;
  }

  public function output( $count ) {
    return $count;
  }

  public function post( $post_type='post' ) {
    return $this->output(wp_count_posts($post_type)->publish);
  }
  
  public function gift() {
    return $this->post('gift');
  }

  public function tax( $taxonomy='category' ) {
    return $this->output(wp_count_terms($taxonomy));
  }
  
  public function cat() {
    return $this->tax('category');
  }
  
  public function tag() {
    return $this->tax('post_tag');
  }

  public function user( $role='' ) {
    $users = count_users();
    
    if (empty($role))
      return $users['total_users'];
    
    return $this->output(isset($users['avail_roles'][$role]) ? $users['avail_roles'][$role] : 0);
  }
  
  public function view() {
    return $this->output(get_dmeng_traffic_all());
  }
  
  public function search() {
    return $this->output(get_dmeng_traffic_all('search'));
  }

  public function comment( $post_id=0, $user_id=0 , $cache=true ) {

    $post_id = absint($post_id);
    $user_id = absint($user_id);
    
    if ($post_id===0 && $user_id===0)
      return wp_count_comments()->total_comments;

    $cache_key = 'comments-'.$post_id.'-'.$user_id;

    $count = 0;

    if ($cache===true) {
      $count = wp_cache_get( $cache_key, $this->table_name );
      if ( false!==$count )
        return $count;
    }

    $query = array( 'status'=>'approve', 'count'=>true );

    if ($post_id)
      $query['post_id'] = $post_id;

    if ($user_id)
      $query['user_id'] = $user_id;

    $count = absint(get_comments($query));
    
    if ($cache===true)
      wp_cache_set( $cache_key, $count, $this->table_name );

    return $this->output($count);
  }

  public function comments_cache_flush( $comment_id, $comment_object ) {
    
    if ( !$comment_id || $comment_object->comment_approved != 1 || !empty($comment_object->comment_type) )
      return;

    wp_cache_delete( 'comments-'.$comment_object->comment_post_ID.'-0', $this->table_name );
    
    if ( $comment_object->user_id )
      wp_cache_delete( 'comments-'.$comment_object->comment_post_ID.'-'.$comment_object->user_id, $this->table_name );

  }
  
  public function exchange( $post_id=0, $user_id=0 ) {
    $post_id = absint($post_id);
    if ( empty($post_id) )
      return 0;
    
    $buyers = (array)wp_unslash(json_decode(get_post_meta( $post_id, 'dmeng_gift_buyers', true ), true));
    
    if ( empty($buyers) )
      return 0;

    $user_id = absint($user_id);
    if ( empty($user_id) )
      return count($buyers);
    
    $count = array_count_values($buyers);
    return $this->output(isset($count[$user_id]) ? absint($count[$user_id]) : 0);
  }

  public function user_gift( $user_id=0 ) {
    global $dmeng_Gift;
    return $dmeng_Gift->count_gift($user_id);
  }
  
  public function user_vote( $user_id=0, $type='', $vote='' ) {
    $user_id = absint($user_id);
    if ($user_id<1)
      return false;

    $type_arg = array('post', 'comment');
    $vote_arg = array('up', 'down');
    
    $type = in_array($type, $type_arg) ? (array)$type : $type_arg;
    $vote = in_array($vote, $vote_arg) ? (array)$vote : $vote_arg;
    
    $count = array();
    
    foreach ($type as $single_type) {
      foreach ($vote as $single_vote) {
        $count[$single_type][$single_vote] = get_dmeng_user_vote($user_id, true, $single_type, $single_vote);
      }
    }
    
    return $this->output($count);
  }

}

$dmeng_Count = new dmeng_Count;
