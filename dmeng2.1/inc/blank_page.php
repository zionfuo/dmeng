<?php

/**
 * 代替 wp_die
 */

function dmeng_die_head() {
  do_action( 'dmeng_die_head' );
}

function dmeng_die( $content, $title='', $load_jQuery=true ) {

  $title = empty($title) ? __( '错误提示', 'dmeng' ) : $title;
  
  ?><!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="UTF-8" />
<meta name="robots" content="noindex,nofollow">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<meta http-equiv="Cache-Control" content="no-transform " /> 
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo esc_html( $title ) . ' - ' . get_bloginfo('name');?></title>
<style type="text/css">

/* Eric Meyer's Reset CSS v2.0 - http://cssreset.com */
html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{border:0;font-size:100%;font:inherit;vertical-align:baseline;margin:0;padding:0}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:none}table{border-collapse:collapse;border-spacing:0}

body{margin:0 auto;padding:30px 0;background:#f1f1f1;font-family:"Helvetica Neue","Luxi Sans","DejaVu Sans",Tahoma,"Hiragino Sans GB",STHeiti,"Microsoft YaHei",Arial,sans-serif;font-size:14px;line-height:1.5;color:#444}
a{color:#21759b;text-decoration:none}
a:hover{color:#d54e21}
.wrapper{max-width:700px;margin:0 auto;padding:30px;background:#fff;-webkit-box-shadow:0 1px 3px rgba(0,0,0,0.13);box-shadow:0 1px 3px rgba(0,0,0,0.13);text-align:center}
h1{font-size:24px;font-weight:100;margin:10px 0 30px}
p{margin:0 0 20px 0}
.small,.copyright{font-size:13px;color:#666;text-decoration:none}
.copyright{margin:25px 0 0}
.copyright a{color:#666 !important}
.danger{color:#a94442 !important}

.button{background:#f7f7f7;border:1px solid #ccc;color:#555;display:inline-block;text-decoration:none;letter-spacing:1px;font-size:14px;line-height:1.42857143;margin:0;padding:6px 12px;cursor:pointer;-webkit-appearance:none;white-space:nowrap;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;vertical-align:top}
.button.button-large{height:29px;line-height:28px;padding:0 12px}
.button:hover,.button:focus{background:#fafafa;border-color:#999;color:#222}
.button:focus{-webkit-box-shadow:1px 1px 1px rgba(0,0,0,.2);box-shadow:1px 1px 1px rgba(0,0,0,.2)}
.button:active{background:#eee;border-color:#999;color:#333;-webkit-box-shadow:inset 0 2px 5px -3px rgba(0,0,0,0.5);box-shadow:inset 0 2px 5px -3px rgba(0,0,0,0.5)}
input[type=text].button{background:none !important;outline:0;cursor:initial}
button[disabled],html input[disabled]{cursor:default}
.button.disabled,.button[disabled]{pointer-events:none;cursor:not-allowed;filter:alpha(opacity=65);-webkit-box-shadow:none;box-shadow:none;opacity:.65}
.top_notice{position:fixed;top:0;left:0;right:0;padding:0;background-color:#fff;border-bottom:1px solid #dd3d36;color:#dd3d36;font-size:15px;line-height:42px;font-family:Microsoft YaHei;font-weight:100;text-align:center}
.top_notice.ok{border-bottom:1px solid #009a61;color:#009a61;background:rgba(255,255,255,.5);}
.justified{display:table;width:100%;table-layout:fixed;border-collapse:separate}
.justified>div{display:table-cell;float:none;width:1%}
.justified>div>input,.justified>div>button{width:100%}
</style>
<?php 

if ($load_jQuery)
  echo '<script type="text/javascript" src="'.dmeng_jquery_uri().'"></script>';

dmeng_die_head(); 

?>
</head>
<body>
  <div class="wrapper">
    <?php echo '<h1>'.$title.'</h1>' . $content;    ?>
    <div class="copyright">&copy; <?php echo current_time('Y');?> <a href="<?php echo home_url('/');?>"><?php bloginfo('name');?></a></div>
  </div>
</body>
</html><?php
  die();
}
