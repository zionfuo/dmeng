<?php

/**
 * 用户中心
 */

class dmeng_UCenter {

  public function get_ucenter_url( $path='' ) {
    global $wp_rewrite;
    if ( $wp_rewrite->using_permalinks() )
      $url = home_url('/') . 'user/'. ( !empty($path) ? $path.'/' : '' );
    else 
      $url = add_query_arg( 'user', ( !empty($path) ? $path : 'home' ), home_url('/') );
      
    return apply_filters( 'dmeng_ucenter_url', $url, $path );
  }
  
  public function is_ucenter_page() {
    
    $var_user = get_query_var('user');
    if ( (! is_home() && ! is_front_page()) || empty($var_user) )
      return false;

    return $var_user;

  }

  public function __construct() {

    if ( dmeng_open_ctrl('ucenter')===false )
      return;
    
    define( 'DMENG_UC_SCRIPT_VER', 150917 );

    add_action( 'init', array( $this, 'ucenter_init' ) );
    
    add_filter( 'dmeng_ready_ajax', array( $this, 'ajax_action' ) );

    add_filter( 'edit_profile_url', array( $this, 'new_profile_url' ) );

    add_action( 'dmeng_no_robots', array( $this, 'no_robots' ) );
    
    add_filter( 'dmeng_tracker_vars', array( $this, 'ucenter_tracker_vars' ) );
    
    add_filter( 'dmeng_scripts_args', array( $this, 'ucenter_scripts_vars' ) );
    add_action( 'wp_enqueue_scripts', array( $this, 'ucenter_scripts' ) );
    
    add_action( 'wp_print_styles', array( $this, 'ucenter_styles' ) );
    
    add_action( 'template_redirect', array( $this, 'ucenter_page' ) );
    
    add_action( 'wp_login', array( $this, 'latest_login' ), 20, 2 );
    add_filter( 'manage_users_columns', array( $this, 'latest_login_column' ) );
    add_action( 'manage_users_custom_column', array( $this, 'latest_login_column_callback' ), 10, 3 );
    
    add_action( 'wp_ajax_dmeng_ucenter_page', array( $this,'setting_ajax_action' ) );
    add_action( 'wp_ajax_nopriv_dmeng_ucenter_page', array( $this,'setting_ajax_action' ) );
    
    add_action( 'wp_ajax_dmeng_profile_form', array( $this,'profile_form_action' ) );
    add_action( 'wp_ajax_dmeng_email_form', array( $this,'email_form_action' ) );
    add_action( 'wp_ajax_dmeng_pass_form', array( $this,'pass_form_action' ) );
    
    add_action( 'wp_ajax_dmeng_destroy_other_sessions', array( $this,'destroy_other_sessions' ) );
    
    add_filter( 'user_search_columns', array( $this,'user_search_columns' ) );
    add_filter( 'manage_users_columns', array( $this,'display_name_column' ) );
    add_filter( 'manage_users_custom_column', array( $this,'display_name_column_callback' ), 10, 3 );
    
    remove_filter( 'authenticate', 'wp_authenticate_username_password', 20, 3 );
    add_filter( 'authenticate', array( $this, 'email_login_authenticate' ), 20, 3 );

    add_filter( 'dmeng_author_template_tabs', array( $this, 'ucenter_author_tab' ) );
    add_filter( 'dmeng_author_main_notice', array( $this, 'notice_selection' ) );
    add_action( 'dmeng_author_main_content', array( $this, 'main_selection' ) );
  }
  
  public function no_robots($no_robots) {
    if ( $this->is_ucenter_page() )
      $no_robots = true;
    return $no_robots;
  }

  public function ucenter_tracker_vars( $vars ) {
    if ( $this->is_ucenter_page() )
      $vars['type'] = 'ucenter';
      
    return $vars;
  }

  public function ucenter_scripts_vars( $scripts_array ) {

    $scripts_array['ucenter'] = array(
      'loginurl' => add_query_arg( array( 'redirect_to'=>'dmeng_current_page_url' ), wp_login_url() ),
      'logouturl' => add_query_arg( array( 'redirect_to'=>'dmeng_current_page_url' ), wp_logout_url() ),
      'text' => apply_filters( 'dmeng_ucenter_text_i18n', array(
        'message' => __( '消息', 'dmeng' ),
        'news' => __( '新消息', 'dmeng' ),
        'news_title' => __( '您有%s条新消息', 'dmeng' ),
        'post' => __( '文章', 'dmeng' ),
        'comment' => __( '评论', 'dmeng' ),
        'like' => __( '赞', 'dmeng' ),
        'login' => __( '登录', 'dmeng' ),
        'logout' => __( '登出', 'dmeng' )
      ) ),
      'url_format' => str_replace( 'home', '%s', $this->get_ucenter_url('home') )
    );
    
    if ( get_query_var( 'user' ) )
      $scripts_array['ucenter']['current'] = get_query_var( 'user' );

    return $scripts_array;
  }
  
  public function ucenter_scripts() {
    if (DMENG_SCRIPT_DEV) {
      wp_enqueue_script( 'dmeng-ucenter', get_template_directory_uri() . '/js/dev/dmeng-ucenter-'.DMENG_UC_SCRIPT_VER.'.js', array( 'dmeng' ), DMENG_VER );
    }
  }
  
  public function ucenter_styles() {
    if ( !$this->is_ucenter_page() )
      return;
    ?>
<style type="text/css">
.ucenter_page{overflow-y:scroll}
.ucenter_page #main{margin-bottom:46px}
.ucenter_page #content{min-height:500px;margin:40px 30px;border:none}
.ucenter_page #content .page-header{margin:20px 0}
.ucenter_page #content #ucenter_loading{margin:0}
.ucenter_page #content #ucenter_nav{text-align:right;padding:0 15px}
.ucenter_page #content #ucenter_menu{margin:15px -15px;padding:0;list-style:none}
.ucenter_page #content #ucenter_menu li a{display:block;padding:0 15px;line-height:38px;color:#888;text-decoration:none}
.ucenter_page #content #ucenter_menu li a.active,.ucenter_page #content #ucenter_menu li a:hover{color:#555;text-decoration:none}
.ucenter_page #content #ucenter_menu li a.active{background:#f5f5f5}
@media (max-width:767px){.ucenter_page #content .ucenter_avatar{float:left;margin:0 15px 0 0}
.ucenter_page #content #ucenter_nav{text-align:left}
.ucenter_page #content #ucenter_menu li{display:inline-block}
.ucenter_page #content #ucenter_menu li .h3{margin:0}
.ucenter_page #content .logout_url{margin:0 0 30px;display:inline-block}
}
.ucenter_page #content .ucenter_list{margin:0;padding:0;list-style:none}
.ucenter_page #content .ucenter_list li{border-bottom:1px solid #eee;padding:15px;line-height:32px;color:#666}
.ucenter_page #content .ucenter_list li:hover{background:#f9f9f9}
.ucenter_page #content .ucenter_list h3{font-size:16px;margin:0 0 8px;padding:0}
.ucenter_page #content .ucenter_list li.title{padding:5px 8px;font-size:12px}
.ucenter_page #content .ucenter_list li#paginate{background:0 0!important;border:none!important}
.ucenter_page #content .ucenter_list li .text{line-height:25px;color:#333}
.ucenter_page #content .ucenter_list li .info{font-size:12px;line-height:22px;color:#999}
.ucenter_page #content .ucenter_list li .info .up_title{margin:0 0 0 6px}
.ucenter_page #content .ucenter_list li .info .glyphicon{margin:0 4px 0 6px}
.ucenter_page #content .ucenter_list .status{font-size:12px;border:1px solid #d9534f;color:#d9534f;vertical-align:bottom;padding:0 2px;margin:0 6px 0 0;border-radius:2px}
.ucenter_page #content .ucenter_list .status.publish{color:#449d44;border-color:#398439}
</style>
    <?php
  }
  
  public function ucenter_page() {
    if ( $this->is_ucenter_page() ) {
      get_template_part( 'ucenter' );
      exit;
    }
  }

  public function new_profile_url( $url ) {
    return is_admin() ? $url : $this->get_ucenter_url('home');
  }
  
   public function ucenter_init(){
     
    if ( 
      is_admin() 
      && is_user_logged_in() 
      && !current_user_can( apply_filters('dmeng_admin_base_cap', 'edit_users') ) 
      && ( !defined('DOING_AJAX') || !DOING_AJAX )  
    ) {
      wp_redirect( is_user_logged_in() ? $this->get_ucenter_url() : home_url() );
      exit;
    }
    
    add_rewrite_tag('%user%', '([^&]+)');
    add_rewrite_rule( '^user/?','index.php?user=home', 'bottom' );
    add_rewrite_rule( '^user/([^/]*)/?','index.php?user=$matches[1]', 'top' );
  }

  /**
   * 用户信息 AJAX
   */
   public function ajax_action( $return_array ) {
     
      if ( !is_user_logged_in() && apply_filters( 'dmeng_is_open_float_ucenter', false ) )
        return $return_array;

     if ( is_user_logged_in() ) {

      remove_filter( 'get_avatar', 'dmeng_lazyload_avatar' );
       
      $current_user = wp_get_current_user();

      $avatar = get_avatar( $current_user->ID, (isset($_POST['tracker']['type']) && $_POST['tracker']['type']=='ucenter' ? 80 : 40 ) );
      preg_match( "/src='(.+?)'/", $avatar, $avatar_matches );

      $return_array['userdata'] = apply_filters( 'dmeng_ready_ajax_userdata', array(
        'ID' => $current_user->ID,
        'display_name' => esc_attr($current_user->display_name),
        'avatar' => $avatar_matches[1],
        'news' => intval(get_dmeng_message($current_user->ID, 'count', "( msg_type='unread' OR msg_type='unrepm' )")),
        'user_info' => array(
          'message' => intval(get_dmeng_message($current_user->ID, 'count', "( msg_type='read' OR msg_type='unread' OR msg_type='repm' OR msg_type='unrepm' )")),
          'post' => intval(count_user_posts($current_user->ID)),
          'comment' => intval(get_comments( array('status' => '1', 'user_id'=>$current_user->ID, 'count' => true) )),
          'like' => ( dmeng_vote_count($current_user->ID, 'post', 'up') + dmeng_vote_count($current_user->ID, 'comment', 'up') )
        )
      ), $current_user );

    } else {

      $return_array['userdata'] = array(
        'welcome' => __( '欢迎登录', 'dmeng' )
      );
      
    }
    
    return $return_array;
  }
  
  public function latest_login( $user_login, $user ){
    update_user_meta( $user->ID, 'dmeng_latest_login', current_time( 'mysql' ) );
  }
  
  public function latest_login_column( $columns ) {
    $columns['dmeng_latest_login'] = '上次登录';
    return $columns;
  }
  
  public function latest_login_column_callback( $value, $column_name, $user_id ) {
    if('dmeng_latest_login' == $column_name){
      $user = get_user_by( 'id', $user_id );
      $value = ( $user->dmeng_latest_login ) ? $user->dmeng_latest_login : $value = __('没有记录','dmeng');
    }
    return $value;
  }

  public function profile_form_action() {
    do_action( 'dmeng_before_ajax' );

    parse_str( $_POST['formdata'], $data );

    if ( empty($data['display_name']) )
      die( __( '昵称不能为空！！！', 'dmeng' ) );

    $display_name = trim(trim($data['display_name']), '  　');
    if ( empty($display_name) )
      die( __( '昵称不能为纯空格或制表符！！！', 'dmeng' ) );
    
    $user = array(
      'ID' => get_current_user_id(),
      'nickname' => $display_name,
      'display_name' => $display_name
    );
    
    if ( isset( $data['url'] ) ) {
      if ( empty ( $data['url'] ) || $data['url'] == 'http://' ) {
        $user['user_url'] = '';
      } else {
        $user['user_url'] = esc_url_raw( $data['url'] );
        $protocols = implode( '|', array_map( 'preg_quote', wp_allowed_protocols() ) );
        $user['user_url'] = preg_match('/^(' . $protocols . '):/is', $user['user_url']) ? $user['user_url'] : 'http://'.$user['user_url'];
      }
    }

    if ( isset( $data['description'] ) )
      $user['description'] = trim( $data['description'] );

    $user_id = wp_update_user( $user );
    if ( is_wp_error( $user_id ) )
      die( $user_id->get_error_message() );
      
    if ( isset( $data['avatar'] ) )
      update_user_meta( $user_id , 'dmeng_avatar', sanitize_text_field( $data['avatar'] ) );
      
    die( __( '个人资料更新成功。', 'dmeng' ) . '<script>location.reload();</script>' );
  }
  
  public function email_form_action() {
    do_action( 'dmeng_before_ajax' );
    
    parse_str( $_POST['formdata'], $data );
    
    global $dmeng_Email;
    $result = $dmeng_Email->verify_send( $data['user_email'], get_current_user_id() );

    echo is_array($result) ? join( ' ', $result ) : $result;

    die();
  }

  public function pass_form_action() {
    do_action( 'dmeng_before_ajax' );
    
    parse_str( $_POST['formdata'], $data );
    
    $dmeng_Security = dmeng_Security();

    $pass1 = $pass2 = '';
    if ( isset( $data['pass1'] ) )
      $pass1 = $dmeng_Security->maybe_RSA($data['pass1']);
    if ( isset( $data['pass2'] ) )
      $pass2 = $dmeng_Security->maybe_RSA($data['pass2']);

    $current_user = get_userdata(get_current_user_id());
    
    do_action_ref_array( 'check_passwords', array( $current_user->user_login, $pass1, $pass2 ) );

    if ( empty($pass1) || empty($pass2) )
      die( __( '新密码不能为空！！！', 'dmeng' ) );
    
    if ( $pass1 != $pass2 )
      die( __( '两次输入的密码不一样。', 'dmeng' ) );
    
    if ( false !== strpos( wp_unslash( $pass1 ), "\\" ) )
      die( __( '密码中不能包含 "\\" 字符。', 'dmeng' ) );

    if ( 8>strlen($pass1) )
      die( __( '密码至少8个字符', 'dmeng' ) );
    
    wp_set_password( $pass1, $current_user->ID );
    
    die( __( '密码更新成功。', 'dmeng' ) . '<script>location.reload();</script>' );
  }
  
  public function destroy_other_sessions() {
    wp_destroy_other_sessions();
    die( __( '其他会话已清理', 'dmeng' ) );
  }
  
  public function setting_ajax_action() {

    if ( ! is_user_logged_in() )
      die( __( '你还没登录。', 'dmeng' ) . '<script>location.href=dmeng.loginurl();</script>' );
    
    do_action( 'dmeng_before_ajax' );
    
    $page = sanitize_key($_POST['page']);
    $page_func = 'setting_page_'.$page;
    
    if ( method_exists( $this, $page_func ) ) {
    
      $this->$page_func();
      die();
      
    } else {
    
      do_action('dmeng_ucenter_setting_page_'.$page);
    }
    
    _e( '页面不存在', 'dmeng' );

    die();
  }
  
  public function setting_paginate( $num_page, $paged ) {
    if ( $num_page <= 1 )
      return;
    $paginate = '';
    for( $i=1; $i<=$num_page; $i++ ) {
      $paginate .= '<option ' . ( $i==$paged ? 'selected="selected" ' : '') . ' value="'.$i.'">'.$i.'</option>';
    }
    echo '<li id="paginate">' . sprintf( 
      __( '当前第 %1$s 页，共 %2$s 页，%3$s ', 'dmeng' ), 
      '<select class="paginate">' . $paginate . '</select>', 
      ceil($num_page),
      ( $paged<$num_page ? '<a href="javascript:;" id="get_next">' . __( '下一页', 'dmeng' ) . '</a>' : '<a href="javascript:;" id="get_prev">' . __( '上一页', 'dmeng' ) . '</a>' )
    ) . '</li>';
  }
  
  
  public function setting_page_home() {
    
    global $current_user;
    get_currentuserinfo();
    
    ?>
    <div class="entry-content">

      <form id="profile_form" class="form-horizontal" role="form" method="post">
        
        <div class="page-header text-center">
          <h1><?php _e( '个人资料', 'dmeng' );?> <small class="smaller">(<a href="<?php echo get_author_posts_url($current_user->ID);?>" target="_blank"><?php _e( '查看主页', 'dmeng' );?></a>)</small></h1>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 text-right"><?php _e('ID','dmeng');?></label>
          <div class="col-sm-9">
            <p><?php echo $current_user->ID;?></p>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 text-right"><?php _e('头像','dmeng');?></label>
          <div class="col-sm-9">
            <?php
              $avatars = apply_filters( 'dmeng_avatar_radio', array(
                'default' => array(
                  'name' => __( 'Gravatar', 'dmeng' ),
                  'img' => "http://cn.gravatar.com/avatar/" . md5(strtolower( $current_user->user_email )) . "?d=" . get_option('avatar_default') . "&r=" . get_option('avatar_rating') . "&s=40"
                )
              ) );
              
              $avatar_checked = apply_filters( 'dmeng_avatar_checked', 'default' );
              
              foreach( $avatars as $avatar_key=>$avatar ) {
                ?>
                  <div class="radio">
                    <img src="<?php echo $avatar['img']; ?>" width="40" height="40" class="avatar" />
                    <label>
                      <input type="radio" name="avatar"  value="<?php echo $avatar_key;?>" <?php if ( $avatar_key==$avatar_checked ) echo 'checked';?>> <?php echo $avatar['name'];?>
                    </label>
                  </div>
                <?php
              }
            ?>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 text-right"><?php _e( '用户名', 'dmeng' );?></label>
          <div class="col-sm-9">
            <p><?php echo $current_user->user_login;?></p>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 text-right"><?php _e( '昵称', 'dmeng' );?></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="display_name" name="display_name" value="<?php echo $current_user->display_name;?>">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 text-right"><?php _e( '站点', 'dmeng' );?></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="url" name="url" value="<?php echo esc_url($current_user->user_url);?>">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 text-right"><?php _e( '个人说明', 'dmeng' );?></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="description" name="description" value="<?php echo $current_user->description;?>">
          </div>
        </div>

        <?php
          global $dmeng_Open;
          $open = apply_filters( 'dmeng_open_loginform', array() );
          if ( $open ) {
        ?>
          <div class="form-group">
            <label class="col-sm-3 text-right"><?php _e( '第三方登录', 'dmeng' );?></label>
            <div class="col-sm-9">
              <?php $dmeng_Open->open_login_form();  ?>
            </div>
          </div>
        <?php
          }
        ?>
        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <p class="help-block"><?php _e( '修改后请注意保存更改。', 'dmeng' );?></p>
            <button type="submit" class="btn btn-primary" data-loading-text="<?php _e( '提交中…', 'dmeng' );?>"><?php _e( '保存更改', 'dmeng' );?></button> 
            <?php
              global $wp_version;
              if ( version_compare($wp_version, '4.0.0', '>=') ) {
                $sessions = wp_get_all_sessions();
                $count_sessions = count($sessions);
                if( $count_sessions > 1){
                ?>
                  <button type="button" class="btn btn-default" id="destroy_other_sessions" title="退出在其他地方的登录" data-loading-text="<?php _e( '提交中…', 'dmeng' );?>"><?php printf(__( '登出其他会话', 'dmeng' ), $count_sessions-1 ); ?></button>
              <?php
                }
              ?>
            <div class="small" style="margin:20px 0">
              <p class="help-block"><?php _e( '当前登录在线的会话信息', 'dmeng' );?></p>
              <p>
                <?php
                  global $dmeng_UA_parse;
                  foreach ( $sessions as $session ) {
                    $dmeng_UA_parse->user_agent = $session['ua'];
                    echo sprintf( __( '在 %1$s 使用 %2$s (%3$s) 登录', 'dmeng' ), get_date_from_gmt(date( 'Y-m-d H:i:s', $session['login'] )), join( ' ', $dmeng_UA_parse->result() ), $session['ip'] ).'<br>';
                  }
                ?>
              </p>
            </div>
        <?php
              }
            ?>
          </div>
        </div>
  
      </form>
      
      <p class="text-right"><a href="javascript:;" id="more_form-toggle"><?php _e( '更多设置（更改邮箱/密码）', 'dmeng' );?></a></p>

    <div id="more_form" style="display:none">
      <form id="email_form" class="form-horizontal" role="form" method="post">
        
        <div class="page-header text-center">
          <h2><?php _e( '更改邮箱', 'dmeng' );?></h2>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 text-right"><?php _e( '邮箱', 'dmeng' );?></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="user_email" name="user_email" value="<?php echo $current_user->user_email;?>" autocomplete="off" required>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <p class="help-block"><?php _e( '更改邮箱需要获取验证链接验证后方可生效。', 'dmeng' );?></p>
            <button type="submit" class="btn btn-default" data-loading-text="<?php _e( '提交中…', 'dmeng' );?>"><?php _e( '获取验证邮件', 'dmeng' );?></button>
          </div>
        </div>
        
      </form>
      
      <form id="pass_form" class="form-horizontal" role="form" method="post">
        
        <div class="page-header text-center">
          <h2><?php _e( '更改密码', 'dmeng' );?></h2>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 text-right"><?php _e( '新密码', 'dmeng' );?></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="pass1" name="pass1" value="" autocomplete="off" required>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 text-right"><?php _e( '重复新密码', 'dmeng' );?></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="pass2" name="pass2" value="" autocomplete="off" required>
            
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <p class="help-block"><?php _e( '再输入一遍新密码。 提示：您的密码最好至少包含7个字符。为了保证密码强度，使用大小写字母、数字和符号（例如! " ? $ % ^ &amp; )）。', 'dmeng' );?></p>
            <button type="submit" class="btn btn-primary" data-loading-text="<?php _e( '提交中…', 'dmeng' );?>"><?php _e( '更改密码', 'dmeng' );?></button>
          </div>
        </div>

      </form>
    </div>
    
    </div>  
    <?php
  }
  
  public function setting_page_message() {
    ?>
    <div class="entry-content">
      <ul class="ucenter_list">
        <?php
    
        $current_user = get_userdata(get_current_user_id());
        
        $unread_sql = " msg_type='unread' OR msg_type='unrepm' ";
        $all_sql = $unread_sql . " OR msg_type='read' OR msg_type='repm' ";

        $all = get_dmeng_message($current_user->ID, 'count', " ( $all_sql )");

        $paged = !empty($_POST['paged']) ? intval($_POST['paged']) : 1; 
        $number = 10;
        $num_page = ceil($all/$number);
        $offset = ($paged-1)*$number;
        
        $unread = intval(get_dmeng_message($current_user->ID, 'count', " ( $unread_sql )"));

        $get_news = !empty($_POST['paged']) && $_POST['paged']=='news';
        if ( $get_news )
          $num_page = ceil($unread/$number);
          
        $mLog = get_dmeng_message($current_user->ID, '', ($get_news ? " ( $unread_sql )" : " ( $all_sql )"), $number, ($get_news ? 0 : $offset) );
        
        if ( $mLog ) {
          
          echo '<li class="title">' . ( $get_news ? sprintf( __( '当前只查看新消息，共有 %1$s 条 %2$s 页。', 'dmeng' ), $unread, $num_page ) : sprintf(__('共有 %1$s 条消息，其中 %2$s 条是新消息。','dmeng'), $all, $unread) ). '</li>';

          foreach( $mLog as $log ){
            $unread_tip = $unread_class = '';
            if ( in_array($log->msg_type, array('unread', 'unrepm')) ) {
              $unread_tip = '<span class="status">'.__('新消息', 'dmeng').'</span>';
              $unread_class = ' unread';
              update_dmeng_message_type( $log->msg_id, $current_user->ID , ltrim($log->msg_type, 'un') );
              $unread--;
            }
            $msg_title =  $log->msg_title;
            if ( in_array($log->msg_type, array('repm', 'unrepm')) ) {
              $msg_title_data = json_decode($log->msg_title);
              $msg_title = get_the_author_meta('display_name', intval($msg_title_data->from));
              $msg_title = sprintf(__('%s发来的私信','dmeng'), $msg_title).' <a href="'.add_query_arg('tab', 'message', get_author_posts_url(intval($msg_title_data->from))).'">'.__('查看对话','dmeng').'</a>';
            }
            echo '<li class="msg'.$unread_class.'"><p class="text">'.strip_tags(htmlspecialchars_decode($log->msg_content), '<a><br><p>').' </p><div class="info">'.$unread_tip.'  '.$msg_title.'  '.$log->msg_date.'</div></li>';
          }

        if ( $get_news ) {
          echo '<li>' . ( $unread>0 ? sprintf( __( '还有 %1$s 条新消息，你可以 %2$s 或 %3$s ', 'dmeng' ), $unread, '<a href="javascript:;" id="get_message">' . __( '返回查看全部', 'dmeng' ) . '</a>', '<a href="javascript:;" id="get_news">' . __( '加载下一页', 'dmeng' ) . '</a>' ) : sprintf( __( '没有新消息了，你可以 %1$s ', 'dmeng' ), '<a href="javascript:;" id="get_message">' . __( '返回查看全部', 'dmeng' ) . '</a>' ) ). '</li>';
        } else if ( $unread ) {
          echo '<li>' . sprintf( __( '分页里包含 %1$s 条新消息，你可以：%2$s', 'dmeng' ), $unread, '<a href="javascript:;" id="get_news">' . __( '只查看新消息', 'dmeng' ) ). '</a></li>';
        }

        if ( ! $get_news ) {
          $this->setting_paginate( $num_page, $paged );
        }

      } else {
        echo '<li>' . __( '没有找到记录', 'dmeng' ) . '</li>';
      }

        ?>
      </ul>
    </div>
    <?php
    
  }
  
  public function setting_page_post() {

  $post_statuses = get_post_statuses();

    ?>
    <div class="entry-content">
      <ul class="ucenter_list">
        <?php

          $paged = !empty($_POST['paged']) ? intval($_POST['paged']) : 1; 
          $number = 10;

          $query = new WP_Query( array( 'author' => get_current_user_id(), 'posts_per_page' => $number, 'paged' => $paged, 'post_status' => array( 'publish', 'pending', 'draft' ) ) );
          $count = $query->found_posts;
          $num_page = $count/$number;
          
          if ( $query->have_posts() ) {
              
              echo '<li class="title">' . sprintf( __( '共有 %1$s 篇文章，其中 %2$s 篇已发布', 'dmeng' ), $count, count_user_posts( get_current_user_id(), 'post' ) ) . '</li>';

              while ( $query->have_posts() ) : $query->the_post();
                $status = get_post_status();
                echo '<li><h3><a href="'.( $status=='publish' ? get_permalink() : get_edit_post_link() ).'" target="_blank">'.get_the_title().'</a></h3><span class="status '.$status.'">'.$post_statuses[$status].'</span><time class="num">'.get_the_date( 'Y-m-d H:i:s' ).'</time></li>';

              endwhile;

              $this->setting_paginate( $num_page, $paged );

          } else { 
          
            echo '<li>' . __( '没有找到文章', 'dmeng' ) . '</li>';
            
          }
        ?>
      </ul>
    </div>
    <?php

    wp_reset_postdata();
  }
  
  public function setting_page_comment() {
    
    $current_user_id = get_current_user_id();
    $all = get_comments( array('status' => '', 'user_id'=>$current_user_id, 'count' => true) );
    $approve = get_comments( array('status' => '1', 'user_id'=>$current_user_id, 'count' => true) );

    $paged = !empty($_POST['paged']) ? intval($_POST['paged']) : 1; 
    $number = 10;
    $num_page = ceil($all/$number);
    $offset = ($paged-1)*$number;

    $comments = get_comments(array(
      'status' => '',
      'order' => 'DESC',
      'number' => $number,
      'offset' => $offset,
      'user_id' => $current_user_id
    ));

    ?>
    <div class="entry-content">
      <ul class="ucenter_list">
        <?php

          if ( $comments ) {

            echo '<li class="title">' . sprintf( __( '共有 %1$s 条评论，其中 %2$s 条已获准， %3$s 条正等待审核。', 'dmeng' ), $all, $approve, $all-$approve ) . '</li>';
            
            foreach( $comments as $comment ){

              $reply = '';
              if ( $comment->comment_parent ) {
                $parent = get_comment($comment->comment_parent);
                if ( $parent->user_id!=$comment->user_id )
                  $reply = '<span class="info">' . sprintf( __( '回复%s：', 'dmeng' ), dmeng_auto_comment_author_name($parent) ) . '</span>';
              }

              echo '<li><div class="text">' . $reply . get_comment_text($comment->comment_ID) . '</div>'.
                ( $comment->comment_approved!=1 ? '<span class="status pending">'.__( '待审', 'dmeng' ).'</span>' : '' ) . 
                '<a class="info" href="'.htmlspecialchars( get_comment_link( $comment->comment_ID) ).'" target="_blank">'.sprintf(__('%1$s 发表在 %2$s','dmeng'),$comment->comment_date,get_the_title($comment->comment_post_ID)).'</a></li>';
              
            }
            
            $this->setting_paginate( $num_page, $paged );

          } else { 
          
            echo '<li>' . __( '没有找到评论', 'dmeng' ) . '</li>';

          }
        ?>
      </ul>
    </div>
    <?php
  }
  
  public function setting_page_like() {
    
    $paged = empty($_POST['paged']) ? 1 : absint($_POST['paged']);
    $number = 10;
    $up = dmeng_get_user_like( get_current_user_id(), $number, $paged );
    $count = $up['posts_count']+$up['comments_count'];
    $up_data = $up['data'];
    
    ?>
    <div class="entry-content">
      <ul class="ucenter_list">
        <?php
              
          if( $up_data ){
            
            echo '<li class="title">' . sprintf(__('赞了 %1$s 次，其中包括 %2$s 篇文章， %3$s 条评论。','dmeng'), $count, $up['posts_count'], $up['comments_count']) . '</li>';

            foreach( $up_data as $up_term ){
              echo '<li><div class="text">'.$up_term['excerpt'].'</div><p class="info">['.$up_term['type_label'].']<a class="up_title" href="'.$up_term['url'].'" target="_blank">'.$up_term['title'].'</a> <span class="glyphicon glyphicon-thumbs-up"></span>'.intval(get_metadata($up_term['type'], $up_term['id'], 'dmeng_votes_up', true)).'<span class="glyphicon glyphicon-thumbs-down"></span>'.intval(get_metadata($up_term['type'], $up_term['id'], 'dmeng_votes_down', true)).'</p></li>';
            }
            
            $this->setting_paginate( ceil($count/$number), $paged );
            
          } else {
            echo '<li>' . __( '没有找到记录，要做一个有态度的人，快去点赞吧！', 'dmeng' ) . '</li>';
          }
          
        ?>
      </ul>
    </div>
    <?php
  }
  
  public function email_login_authenticate( $user, $username, $password ) {
    if ( is_a( $user, 'WP_User' ) )
      return $user;

    if ( !empty( $username ) && is_email( $username ) ) {
      $username = str_replace( '&', '&amp;', stripslashes( $username ) );
      $user = get_user_by( 'email', $username );
      if ( isset( $user, $user->user_login, $user->user_status ) && 0 == (int) $user->user_status )
        $username = $user->user_login;
    }

    return wp_authenticate_username_password( null, $username, $password );
  }
  
  public function user_search_columns($search_columns) {
    if ( !in_array('display_name', $search_columns) ) {
      $search_columns[] = 'display_name';
    }
    return $search_columns;
  }

  public function display_name_column( $columns ) {
    $columns['dmeng_display_name'] = __( '显示名称', 'dmeng' );
    unset($columns['name']);
    return $columns;
  }
  
  public function display_name_column_callback( $value, $column_name, $user_id ) {
    if ( 'dmeng_display_name' == $column_name )
      $value = get_the_author_meta( 'display_name', $user_id );
    return $value;
  }

  public function ucenter_author_tab($array) {
    $array['message'] = __('消息', 'dmeng');
    $array['profile'] = __('资料', 'dmeng');
    return $array;
  }
  
  public function notice_selection( $notice ) {
    global $tab;
    if ('profile'===$tab) {
      return sprintf(__('%s的个人资料', 'dmeng') , $GLOBALS['curauth']->display_name);
    }
    return $notice;
  }

  public function main_selection() {
    global $tab;
    if ( in_array($tab, array('message', 'profile'))===false )
      return;
    $callback = 'main_'.$tab;
    $this->$callback();
  }
  
  public function main_message() {
    global $curauth;
    ?>
  <div id="pm-box" style="display:none">
    <form id="pmform" class="ucform" role="form" method="post">
      <p>
        <textarea class="form-control" rows="3" name="pm" id="pm" required></textarea>
      </p>
      <p class="clearfix">
        <a class="btn btn-link pull-left" href="<?php echo $this->get_ucenter_url('message');?>"><?php _e('查看我的消息','dmeng');?></a>
        <button type="submit" class="btn btn-primary pull-right" data-loading-text="<?php _e('发送中…','dmeng');?>"><?php _e('确定发送','dmeng');?></button>
      </p>
    </form>
    <ul id="uclist">
      <li><?php _e('正在加载对话记录……','dmeng');?></li>
    </ul>
  </div>
<script>
!function($){
  'use strict';
  var $tips = dmengUcenter.$tips,
      user_id = dmengUcenter.user_id,
      load = function () {
        dmengUcenter.autoLoad();
        dmengUcenter.loaded.success(function (response) {
          var loadMore = $('#load-more');
          if (loadMore.length>0) {
            loadMore.parent().before(response).remove();
          } else {
            $('#uclist').html(response).fadeIn();
          }
          dmengUcenter.paged++;
        });
      };
  $(document).ready(function () {
    dmeng.readyAjax.complete(function () {
      if (!dmeng.user.ID) {
        $tips.html('<?php printf(__('要给Ta发送私信请先<a href="%s">登录</a>'), wp_login_url() ); ?>');
        return;
      }
      if (dmeng.user.ID===user_id) {
        $tips.html('<?php _e('查看自己的记录请到用户中心。', 'dmeng');?><a href="'+dmeng.ucenter.url_format.replace( '%s', 'message' )+'"><?php _e('点击查看','dmeng');?></a>').fadeIn();
        return;
      }
      $tips.html('<?php _e('正在给Ta发私信','dmeng');?>');
      var $pmform = $('#pm-box');
      $pmform.fadeIn('normal', function () {
        load();
      });
    });
  }).on('click', '#load-more', function () {
    $(this).button('loading');
    load();
  }).on("submit","#pmform",function () {
    var $pm = $('#pm'), pm = $.trim($pm.val());
    if (pm=='') {
      return dmengUcenter.errorTips('<?php _e('内容不能为空！','dmeng');?>');
    }
    $pm.addClass('disabled').prop('disabled', true);
    var $btn = $(this).find('[type=submit]').button('loading');
    $.ajax({
      type: 'POST',
      url: dmeng.ajaxurl,
      data: {
        action: 'dmeng_pm',
        pm_to: user_id,
        pm: pm,
        _wpnonce: dmeng._wpnonce
      },
      success: function (response) {
        if (response==0) {
          location.reload();
          return;
        } else if (response.success) {
          $('#uclist').html(response.result);
          dmengUcenter.paged = 2;
          $pm.val('');
          dmengUcenter.errorTips('<?php _e('发送成功','dmeng');?>');
        } else {
          dmengUcenter.errorTips(response);
        }
        $pm.removeClass('disabled').prop('disabled', false);
        $btn.button('reset');
      }
    });
    return false;
  });
}(jQuery);
</script>
  <?php
    return;
  }
  
  public function main_profile() {
    
    global $curauth;
		$author_profile = array(
			__('ID','dmeng') => $curauth->ID,
			__('用户名','dmeng') => $curauth->user_login,
			__('昵称','dmeng') => $curauth->display_name,
			__('站点','dmeng') => $curauth->user_url,
			__('个人说明','dmeng') => $curauth->description
		);
    
    echo '<table class="table"><caption></caption><tbody>';
    foreach ($author_profile as $pro_name=>$pro_content) {
      echo '<tr><th scope="row">'.$pro_name.'</th><td>'.$pro_content.'</td></tr>';
    }
    echo '</tbody></table>';
  }

}

$dmeng_UCenter = new dmeng_UCenter;
