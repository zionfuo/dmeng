<?php

class dmeng_Sitemap {
  
  public $cache_prefix = 'dmeng_sitemap_';
  public $number = 1000;
  public $paged = 1;
  public $expiration = 3600;

  public $setting_field_name = 'dmeng_sitemap_auto_ping_baidu';
  public $baidu_api_field_name = 'dmeng_sitemap_baidu_api_url';
  
  public function __construct() {
    $this->paged = max( 1, (empty($_GET['page']) ? 1 : absint($_GET['page'])) );

    add_action('template_redirect', array($this, 'output'), 1);

    add_action('wp_ajax_'.$this->setting_field_name, array($this, 'ping_baidu_action'));

    add_action( 'publish_post', array($this, 'auto_ping_baidu_action') );
  }

  public function get_sitemap_index_url() {
    return add_query_arg(array('xml'=>'sitemap-index.xml'), home_url('/'));
  }

  public function get_sitemap_url($args) {
    return htmlentities(add_query_arg(array_merge($args, array('xml'=>'sitemap.xml')), home_url('/')));
  }
  
  public function output() {
    
    if ( 
      ( is_home()===false && is_front_page()===false ) 
      || empty($_GET['xml']) 
      || in_array($_GET['xml'], array('sitemap.xml', 'sitemap-index.xml'))===false
    ) {
      return;
    }
    
    if ($_GET['xml']=='sitemap-index.xml') {
      
      /**
       * 地图索引
       * 存在多个地图分页，如果逐个地址提交就太麻烦了，使用地图索引只需要提交索引地址就可以
       * http://www.sitemaps.org/protocol.html#index
       */

      $xml = $this->sitemap_index();
      if (!empty($xml))
        $xml = '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.$xml.'</sitemapindex>';

    } else {
      
      $page_func = 'sitemap_'.(!empty($_GET['type'])&&in_array($_GET['type'], array('post', 'page', 'tax')) ? $_GET['type'] : 'page');
      $xml = $this->$page_func();
      if (!empty($xml))
        $xml = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.$xml.'</urlset>';

    }
    
    if (empty($xml)) {
      header('Content-type:text/html;Charset=UTF-8');
      wp_die( __('找不到网站地图，可能内容为空，或者链接有误。', 'dmeng'), __('未找到地图', 'dmeng'), array('response'=>404, 'back_link'=>true) );
    }

		header('Content-type:text/xml;Charset=UTF-8');
		echo '<?xml version="1.0" encoding="utf-8"?>'.$xml;

    exit;
  }
  
  public function sitemap_index() {

    $links = '<sitemap><loc>'.$this->get_sitemap_url(array('type'=>'page')).'</loc></sitemap>';
    
    // 分类法
    foreach ($this->get_tax_names() as $taxonomy ) {
      $count = wp_count_terms($taxonomy, array('hide_empty' => true));
      for($i=1; $i<=ceil($count/$this->number); $i++) {
        $links .= '<sitemap><loc>'.$this->get_sitemap_url(array('type'=>'tax', 'tax_name'=>$taxonomy, 'page'=>$i)).'</loc></sitemap>';
      }
    }
  
    // 文章类型
    foreach ($this->get_post_type_names() as $post_type ) {
      
      // 排除页面，因为页面和首页放一起了
      if ('page'==$post_type)
        continue;
      
      $count = wp_count_posts($post_type)->publish;
      for($i=1; $i<=ceil($count/$this->number); $i++) {
        $links .= '<sitemap><loc>'.$this->get_sitemap_url(array('type'=>'post', 'post_type_name'=>$post_type, 'page'=>$i)).'</loc></sitemap>';
      }
    }
    
    return $links;
  }
  
  public function sitemap_page() {
    
    // 首页
    $links = '<url><loc>'.home_url('/').'</loc><changefreq>daily</changefreq></url>';

    // 文章类型归档页
    foreach ($this->get_post_type_names() as $post_type ) {
      $archive_link = get_post_type_archive_link($post_type);
      if ($archive_link)
        $links .= '<url><loc>'.$archive_link.'</loc><changefreq>daily</changefreq></url>';
    }
    
    // 页面
    $this->number = -1;
    $page_links = $this->get_post_urls('page');
    if ($page_links)
      $links .= $page_links;
    
    return $links;
  }
  
  public function sitemap_post() {
    return $this->get_post_urls( empty($post_type) ? (empty($_GET['post_type_name']) ? 'post' : $_GET['post_type_name']) : $post_type );
  }
  
  public function sitemap_tax() {
    return $this->get_tax_urls( empty($tax) ? (empty($_GET['tax_name']) ? 'category' : $_GET['tax_name']) : $tax );
  }
  
  public function get_tax_urls($tax='', $paged=0) {

    if (in_array($tax, $this->get_tax_names())===false)
      return false;

    if (absint($paged)<1)
      $paged = $this->paged;
    
    $cache_key = $this->cache_prefix.'tax_'.$tax.'_'.$paged;
    $links = get_transient($cache_key);
    
    if ( false === $links ) {

      $links = array();
      
      $ids = (array)get_terms($tax, array(
        'number' => $this->number,
        'offset' => (($paged-1)*$this->number),
        'fields' => 'ids',
        'orderby' => 'count',
        'hide_empty' => true
      ));

      if (!empty($ids)) {
        foreach( $ids as $id ) {
          $term_link = get_term_link(absint($id), $tax);
          if ( is_wp_error($term_link) )
            continue;
          $links[] = $term_link;
        }
        set_transient($cache_key, json_encode($links), $this->expiration);
      }
      
    } else {
      $links = wp_unslash((array)json_decode($links, true));
    }

    if (empty($links))
      return '';

    return '<url><loc>'.join('</loc><changefreq>daily</changefreq></url><url><loc>', $links).'</loc><changefreq>daily</changefreq></url>';
  }

  public function get_post_urls($post_type='', $paged=0) {

    if (in_array($post_type, $this->get_post_type_names())===false)
      return false;

    if (absint($paged)<1)
      $paged = $this->paged;

    $cache_key = $this->cache_prefix.'post_'.$post_type.'_'.$paged;
    $posts = get_transient($cache_key);

    if ( false === $posts ) {
      
      $posts = array();
      
      $query = new WP_Query( array(
        'ignore_sticky_posts ' => 1,
        'posts_per_page' => $this->number,
        'paged' => $paged,
        'post_type' => $post_type,
        'orderby ' => 'date',
        'order' => 'DESC',
        'fields' => 'ids'
      ) );
      
      if ( !empty($query->posts) ) {
        foreach( $query->posts as $post_id ) {
          $posts[] = array(
            'url' => get_permalink($post_id),
            'date' => get_post_modified_time('U', false, $post_id)
          );
        }
        set_transient($cache_key, json_encode($posts), $this->expiration);
      }
      
      wp_reset_postdata();

    } else {
      $posts = wp_unslash((array)json_decode($posts, true));
    }

    if (empty($posts))
      return '';

    $links = '';
    foreach( $posts as $post ) {
      $links .= '<url><loc>'.esc_url($post['url']).'</loc><lastmod>'.date('c', $post['date']).'</lastmod><changefreq>monthly</changefreq></url>';
    }

    return $links;
  }
  
  public function get_tax_names() {
    return array_merge(
      // 默认分类法
      array('category', 'post_tag'), 
      // 自定义分类法
      array_values(get_taxonomies(array(
        'public' => true,
        '_builtin' => false
      )))
    );
  }
  
  public function get_post_type_names() {
    return array_merge(
      // 默认文章类型
      array('post', 'page'), 
      // 自定义文章类型
      array_values(get_post_types(array(
        'public' => true,
        '_builtin' => false
      )))
    );
  }
  
  public function ping_baidu($urls='', $api_url='') {

    $baidu_api_url = $api_url ? $api_url : get_option($this->baidu_api_field_name);
  
    if ( empty($baidu_api_url) || empty($urls) )
      return;

    return wp_remote_retrieve_body(wp_remote_post( $baidu_api_url, array(
      'method' => 'POST',
      'timeout' => 45,
      'redirection' => 5,
      'httpversion' => '1.0',
      'blocking' => true,
      'headers' => array( 'Content-Type'=>'text/plain' ),
      'body' => (is_array($urls) ? join("\n", $urls) : $urls),
      'cookies' => array()
      )
    ));

  }
  
  public function ping_baidu_action() {
    
    if ( empty($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], $_POST['action']) ) {
      die( __('验证无效，请刷新重试', 'dmeng') ); 
    }
    
    if ( current_user_can('manage_options')===false )
      die( __('你无权进行操作', 'dmeng') ); 
    
    $api_url = get_option($this->baidu_api_field_name);
    if ( empty($api_url) )
      die( __('接口地址不能为空', 'dmeng') ); 
    
    if (!empty($_POST['index'])&&$_POST['index']=='sitemap') {

      $urls = $this->get_sitemap_index_url();

    } else {
      
      $today = getdate();
      $query = new WP_Query( array(
        'ignore_sticky_posts ' => 1,
        'posts_per_page' => $this->number,
        'post_type' => $this->get_post_type_names(),
        'date_query' => array(
          array(
            'column' => 'post_modified',
            'year'  => $today['year'],
            'month' => $today['mon'],
            'day'   => $today['mday'],
          ),
        ),
        'fields' => 'ids'
      ) );

      if (empty($query->posts))
        die( __('今天没有更新文章', 'dmeng') ); 

      $urls = array_map('get_permalink', $query->posts);
    }

    $response = wp_unslash((array)json_decode($this->ping_baidu($urls, $api_url), true));
    if( !empty($response['message']) )
      die($response['message']); 
    
    if( !empty($response['success']) )
      die( sprintf(__('成功推送%s条', 'dmeng'), $response['success'] ) );
    
    if( !empty($response['not_same_site']) )
      die( sprintf(__('不是本站url而未处理%s条', 'dmeng'), count($response['not_same_site']) ) ); 

    die( '出错了，请刷新重试' ); 
  }

  public function auto_ping_baidu_action($ID) {
    if (
      (int)get_option('blog_public')===0
      || (int)get_option($this->setting_field_name)===0
    ) {
      return;
    }
    $this->ping_baidu(get_permalink($ID));
  }

}

$dmeng_Sitemap = new dmeng_Sitemap;