<?php

/**
 * 邮件通知
 */

class dmeng_Email {
  
  public function __construct() {

    add_action( 'phpmailer_init', array( $this, 'phpmailer_option' ) );

    add_filter( 'dmeng_email_content', array( $this, 'email_signature' ) );
    add_filter( 'comment_moderation_text', array( $this, 'pre_email_message' ) , 10, 1);

    add_action( 'comment_unapproved_to_approved', array( $this, 'comment_unapproved_to_approved' ) );
    
    add_action( 'wp_insert_comment', array( $this, 'comment_notify' ), 99, 2 );

    add_action( 'dmeng_comment_unapproved_to_approved', array( $this, 'comment_notify' ), 99, 2 );
    
    add_action( 'init', array( $this, 'verify_template' ) );
    
    add_action( 'template_redirect', array( $this, 'auto_msg_status' ), 999);
    
    add_filter( 'comment_notification_recipients', '__return_empty_array' );

    add_filter( 'retrieve_password_message', array( $this, 'retrieve_password_message' ), 10,  2);
    
    add_action( 'delete_user', array( $this, 'delete_user_notify' ) );
    
    add_action( 'transition_post_status', array( $this, 'transition_post_status_notify' ), 10, 3 );
  }

  public function phpmailer_option( $mail ) {
    $smtp = (object)wp_parse_args(
      (array)wp_unslash(json_decode(get_option('dmeng_smtp'), true)),
      array(
        'option' => 0,
        'host' => '',
        'ssl' => 0,
        'port' => 25,
        'user' => '',
        'pass' => '',
        'name' => ''
      )
    );
    if ( intval($smtp->option) ) {
      $mail->IsSMTP();
      $mail->SMTPAuth = true; 
      $mail->isHTML(true);
      //~ 发信服务器
      $mail->Host = sanitize_text_field($smtp->host);
      //~ 端口
      $mail->Port = intval($smtp->port);
      //~ 发信用户
      $mail->Username = sanitize_text_field($smtp->user);
      //~ 密码
      $mail->Password = sanitize_text_field($smtp->pass);
      //~ SSL
      if(intval($smtp->ssl)) $mail->SMTPSecure = 'ssl';
      //~ 来源（显示发信用户）
      $mail->From = sanitize_text_field($smtp->user);
      //~ 昵称
      $mail->FromName = sanitize_text_field($smtp->name);
    }
  }

  public function verify_field_name( $user_id ) {
    $user_id = $user_id ? $user_id : get_current_user_id();
    return ( is_int($user_id) ? 'dmeng_email_verify_'.$user_id : 'dm_ev_'.$user_id );
  }
  
  public function verify_nonce( $ehash, $user_id=0 ) {
    return substr( wp_hash( ($user_id ? $user_id : get_current_user_id()) . $ehash . NONCE_KEY ), -12, 10 );
  }

  public function verify_url( $email, $code=0 ) {
    $ehash = wp_hash($email);
    return add_query_arg(
      array(
        'action' => 'email_verify',
        'code' => ( empty($code) ? null : $code ),
        'ehash' => $ehash,
        'nonce' => $this->verify_nonce($ehash, $code),
        'redirect_to' => urlencode(dmeng_get_redirect_url())
      ),
      wp_login_url()
    );
  }
  
  public function verify_send( $email='', $user_id=0, $data=array() ) {

    $dmeng_Security = dmeng_Security();
    $email = $dmeng_Security->maybe_RSA($email);
  
    if ( ! is_email($email) )
      return array( 'error' => __( '请输入一个有效的邮箱地址！！！', 'dmeng' ) );

    $code = $user_id ? $user_id : get_current_user_id();
    $user_id = is_int($code) ? $code : 0;
    
    $email_exists = email_exists($email);
    
    if ( $email_exists && $email_exists != $user_id )
      return array( 'error' => sprintf( __('这个邮箱地址（%s）已经被使用，请换一个。','dmeng'),  $email ) );
      
    $user_email = $user_id ? get_the_author_meta( 'user_email', $user_id ) : '';

    if ( $email == $user_email )
      return array( 'error' => sprintf( __('这个邮箱地址（%s）你已验证，无需再次验证。','dmeng'),  $email ) );

    $expired_time = intval(apply_filters( 'dmeng_email_verify_time', 300 ));
    $expired_time = max( $expired_time, 300 );
    $human_expired_time = ($expired_time/60);
    
    $transient_key = $this->verify_field_name($code);
    $transient = (array)wp_unslash(json_decode(get_transient( $transient_key ), true));
    if ( !empty($transient) && !empty($transient['time']) && !empty($transient['email']) ) {
      return array( 'error' => sprintf( __( '验证邮件申请间隔至少%1$s分钟，你在 %2$s 申请了邮箱（%3$s）验证，请验证这个邮箱或稍后再试。', 'dmeng' ), $human_expired_time, $transient['time'], $transient['email'] ) );
    }
    
    $current_time = current_time('mysql');
    
    if ( set_transient(
      $transient_key,
      json_encode(
        wp_parse_args(
          array(
            'time' => $current_time,
            'email' => $email
          ), 
          (array)$data
        )
      ),
      $expired_time )
    ) {
      
      $verify_url = $this->verify_url( $email, ( empty($user_id) ? $code : null ) );

      $this->send_mail(
        $email,
        sprintf( __( '%1$s用户邮箱验证', 'dmeng'), get_bloginfo('name') ),
        '<h3>'.sprintf( __('%1$s，你好！', 'dmeng'), ( $user_id ? get_the_author_meta( 'display_name', $user_id ) : __( '亲', 'dmeng' ) ) ).'</h3>'.
        '<p>'.sprintf(__('请点击下面的链接来验证你的邮箱地址（本链接从 %1$s 开始 %2$s 分钟内有效）', 'dmeng' ), $current_time, $human_expired_time ).'</p>'.
        '<p><a href="'.$verify_url.'" target="_blank">'.$verify_url.'</a></p>'.
        '<p>'.__( '如果通过点击以上链接无法访问，请将该网址复制并粘贴至新的浏览器窗口中', 'dmeng' ) . '</p>'
      );

      return array( 'success' => sprintf( __( '验证邮件已发送到（%1$s）邮箱，请尽快验证（链接在%2$s分钟内有效）。', 'dmeng' ), $email, $human_expired_time ) );

    }
    
    return array( 'error' => __( '操作失败，请重试。', 'dmeng' ) );

  }
  
  public function verify_template() {
    
    global $pagenow;
    if ( $pagenow!='wp-login.php' || 
        empty($_GET['action']) || $_GET['action']!='email_verify' || 
        empty($_GET['ehash']) || empty($_GET['nonce'])
    ) {
      return;
    }

    $code_verify = ( !empty($_GET['code']) && $this->verify_nonce($_GET['ehash'], $_GET['code']) == $_GET['nonce'] );
    if ($code_verify) {
      $transient = (array)wp_unslash(json_decode(get_transient( $this->verify_field_name($_GET['code']) ), true));
      do_action( 'dmeng_email_verify_by_code', $transient );
      die();
    }

    $verify_title = __( '邮箱验证', 'dmeng' );
    $failed_title = __( '邮箱验证失败', 'dmeng' );

    $current_user_id = get_current_user_id();
  
    if ( !$current_user_id )
      dmeng_die(
        sprintf( __('要验证邮箱，请先<a href="%1$s">登录</a>，已在别的页面登录<a href="%2$s">点击刷新</a>即可。', 'dmeng'), wp_login_url( dmeng_get_current_page_url() ), 'javascript:location.reload()')
      , $verify_title );
    
    $profile_url = '<a href="'.get_edit_profile_url() .'">&laquo; '.__( '返回个人资料', 'dmeng' ).'</a>';
    
    if ( $this->verify_nonce($_GET['ehash']) != $_GET['nonce']  )
      dmeng_die(
        '<p>' . __( '验证链接安全码无效，请重新获取验证链接。', 'dmeng' ) . '</p>' . $profile_url
      , $failed_title );
      
    $transient = (array)wp_unslash(json_decode(get_transient( $this->verify_field_name($current_user_id) ), true));
    if ( $transient && isset($transient['email']) && is_email($transient['email']) && wp_hash($transient['email'])==$_GET['ehash'] ) {

      if ( email_exists( $transient['email'] ) )
        dmeng_die(
          '<p>' . sprintf( __( '邮箱地址（%s）已被其他帐号使用，请更换邮箱地址，或通过此邮箱找回密码。', 'dmeng' ), $transient['email'] ) . '</p>' . $profile_url
        , $verify_title );
      
      $user_id = wp_update_user( array( 'ID' => $current_user_id, 'user_email' => $transient['email'] ) );
      if ( is_wp_error( $user_id ) )
        dmeng_die(
          '<p>' . __( '更新用户信息出错，请重试。', 'dmeng' ) . '</p><p>'. $user_id->get_error_message() .'</p>' . $profile_url
        , $verify_title );

      delete_transient( $this->verify_field_name($current_user_id) );
      dmeng_die(
          '<p>' . sprintf( __( '恭喜，邮箱地址（%s）验证成功！', 'dmeng' ), $transient['email'] ) . '</p>' . $profile_url
      , $verify_title );
      
    }
    
    dmeng_die(
      '<p>' . __( '验证信息无效，请重新获取验证链接。', 'dmeng' ) . '</p>' . $profile_url
    , $failed_title );
    
  }

  public function pre_email_message( $message ) {
    return '<pre>'.apply_filters( 'dmeng_email_content', $message ).'</pre>';
  }

  public function email_signature($content) {
    return '<style type="text/css">.email_wrapper{background:#fcfcfc;border:1px solid #eee;border-radius: 4px}.email_wrapper h3 {color: #2a6496;}</style><div class="email_wrapper"><div style="margin:15px">'.$content.'<br><p style="color: #777777;">'.sprintf(__('本邮件由<a href="%1$s" target="_blank">%2$s</a>发送','dmeng') , get_bloginfo('url') , get_bloginfo('name')).'</p></div></div>';
  }
  
  public function send_mail( $email, $title, $content ) {

    $title = trim(apply_filters( 'dmeng_email_title', $title ));
    $content = trim(apply_filters( 'dmeng_email_content', $content ));
    
    $send = apply_filters( 'dmeng_send_mail', false, $email, $title, $content );
    if (true===$send)
      return true;

    return wp_mail( $email, $title, $content );
  }
  
  public function comment_unapproved_to_approved( $comment) {
    do_action( "dmeng_comment_unapproved_to_approved", $comment->comment_ID, $comment );
  }

  public function get_comment_notify_recipients( $comment='', $recipients=array() ) {
    
    if ( $comment && $comment->comment_approved==1 && empty($comment->comment_type) ) {

      /**
       * 有 user_id 的，以 user_id 作为数组 key
       * 没有 user_id 但有 comment_author_email 的，以 comment_author_email 作为数组 key
       * 既没有 user_id 也没有 comment_author_email 的就排除在外了
       */

      if ( $comment->user_id && !array_key_exists( $comment->user_id, $recipients ) ) {
        $recipients[$comment->user_id] = array(
          'uid' => $comment->user_id,
          'name' => get_the_author_meta( 'display_name', $comment->user_id ),
          'email' => get_the_author_meta( 'user_email', $comment->user_id )
        );
      }

      if ( !$comment->user_id && is_email($comment->comment_author_email) && !array_key_exists( $comment->comment_author_email, $recipients )  ) {
        $recipients[$comment->comment_author_email] = array(
          'name' => $comment->comment_author,
          'email' => $comment->comment_author_email
        );  
      }

    }
    
    /**
     * 向上循环
     */
    if ( $comment->comment_parent )
      $recipients = $this->get_comment_notify_recipients( get_comment($comment->comment_parent), $recipients );
    
    return $recipients;
  }

  public function comment_notify_send( $email, $comment_id, $comment_link ) {

    $content = '<h3>'.$email['headline'].'</h3>';
    $content .= '<p>'.$email['content'].'</p>';
        
    if ( $email['uid'] ) {
      $msg_id = add_dmeng_message( $email['uid'], 'unread', current_time('mysql'), $email['title'], $email['content'] );
      if ($msg_id)
        $content = str_replace($comment_link, htmlspecialchars(add_query_arg(array('msg_action'=>'read', 'msg_id'=>$msg_id, 'msg_nonce'=>$this->verify_nonce('read'.$msg_id, $email['uid'])), htmlspecialchars_decode($comment_link) )), $content );
    }

    //~ 如果有设置邮箱而且开启评论通知就发送邮件通知
    if ( is_email( $email['email'] ) && get_option( 'comments_notify' ) ) {
      $this->send_mail( $email['email'], $email['title'], apply_filters( 'comment_notification_text', $content, $comment_id ) );
    }
  }
  
  public function comment_notify( $comment_id, $comment_object ) {
    
    if ( !$comment_id || $comment_object->comment_approved != 1 || !empty($comment_object->comment_type) )
      return;

    $post = get_post($comment_object->comment_post_ID);
    $post_author_email = get_the_author_meta('user_email', $post->post_author);

    $comment_link = htmlspecialchars( get_comment_link( $comment_id ) );

    //~ 移除这个替换表情的钩子
    remove_filter( 'get_comment_text', 'dmeng_look_replace' );
    
    remove_shortcode( 'pem' );

    add_shortcode( 'pem', array( 'dmeng_Shortcode', 'pem_tips_for_email' ) );
    
    $comment_text = wp_trim_words( apply_filters( 'get_comment_text', $comment_object->comment_content, $comment_object, array() ), 140, '…' );
    
    $recipients = array();
    
    /**
     * 文章作者
     */
    if ( $comment_object->user_id != $post->post_author ) {
      $recipients[$post->post_author] = array(
        'uid' => $post->post_author,
        'name' => get_the_author_meta( 'display_name', $post->post_author ),
        'email' => $post_author_email
      );
    }
    
    /**
     * 回复的所有上级评论作者
     */
    $recipients = $this->get_comment_notify_recipients( $comment_object, $recipients );
    
    /**
     * 管理员
     */
    $admin_email = get_option('admin_email');
    
    /**
     * 文章和评论作者的通知
     */
    foreach( $recipients as $recipient ) {
      
      /**
       * 如果管理员邮箱已经在列表里就不再单独发送邮件给管理员了
       */
      if ( $recipient['email']==$admin_email )
        $admin_email = '';
      
      /**
       * 如果收件人就是评论发布者，则跳过
       */
      if ( !empty($recipient['uid']) && $recipient['uid']==$comment_object->user_id )
        continue;
      
      $this->comment_notify_send(
        array(
          'uid' => ( isset($recipient['uid']) ? intval($recipient['uid']) : 0 ), 
          'email' => $recipient['email'],
          'title' => sprintf( __('%1$s在%2$s中回复你','dmeng'), $comment_object->comment_author, $post->post_title ),
          'headline' => sprintf( __('%1$s，你好！','dmeng'), $recipient['name'] ),
          'content' => sprintf( __('%1$s在文章<a href="%2$s" target="_blank">%3$s</a>中回复你了，快去看看吧：<br> %4$s','dmeng'), $comment_object->comment_author, $comment_link, $post->post_title, $comment_text )
        ), $comment_id, $comment_link
      );

    }
    
    if ( is_email($admin_email) ) {
      $this->comment_notify_send(
        array(
          'uid' => 0, 
          'email' => $admin_email,
          'title' => sprintf( __('%1$s上的文章有了新的回复','dmeng'), get_bloginfo('name') ),
          'headline' => sprintf( __('%1$s管理员，你好！','dmeng'), get_bloginfo('name') ),
          'content' => sprintf( __('%1$s回复了文章<a href="%2$s" target="_blank">%3$s</a>，快去看看吧：<br> %4$s','dmeng'), $comment_object->comment_author, $comment_link, $post->post_title, $comment_text )
        ), $comment_id, $comment_link
      );
    }

    //~ 重新挂载替换表情的钩子
    add_filter( 'get_comment_text', 'dmeng_look_replace' );
    
    remove_shortcode( 'pem' );
    add_shortcode( 'pem', array( 'dmeng_Shortcode', 'pem_callback' ) );

  }
  
  public function auto_msg_status() {
    if ( isset($_GET['msg_action']) && !empty($_GET['msg_id']) && !empty($_GET['msg_nonce']) ) {
      if ( $this->verify_nonce( $_GET['msg_action'] . $_GET['msg_id'] )==$_GET['msg_nonce'] ) {
        update_dmeng_message_type( $_GET['msg_id'], get_current_user_id(), htmlspecialchars($_GET['msg_action']) );
      }
    }
  }
  
  public function retrieve_password_message( $message, $key ) {

    if ( strpos( $_POST['user_login'], '@' ) ) {
      $user_data = get_user_by( 'email', trim( $_POST['user_login'] ) );
    } else {
      $login = trim($_POST['user_login']);
      $user_data = get_user_by('login', $login);
    }
    
    $user_login = $user_data->user_login;
    
    $message = '<h3>'.sprintf( __('%1$s，你好！','dmeng'), $user_data->display_name ).'</h3>';
    $message .= '<p>';
    $message .= sprintf( __('有人要求重设您在%1$s的帐号密码（用户名：%2$s）。若这不是您本人要求的，请忽略本邮件，一切如常。 要重置您的密码，请打开下面的链接：%3$s','dmeng'),
                          get_bloginfo('name'),
                          $user_login,
                          add_query_arg( array( 'action'=>'rp', 'key'=>$key, 'login'=>rawurlencode($user_login) ), wp_login_url() )
                          );
    $message .= '</p>';

    $content = apply_filters( 'dmeng_email_content', $message );
    return $content;
  }

  public function delete_user_notify( $user_id ) {
		$user_email = get_the_author_meta( 'user_email', $user_id );
		if ( ! is_email( $user_email ) )
      return;
      
		$email_title = sprintf(__('你在%1$s上的账号已被注销','dmeng'), get_bloginfo('name'));
    $email_content = sprintf(__('<h3>%1$s，你好！</h3><p>你在%2$s上的账号已被注销！</p>','dmeng'), get_the_author_meta( 'display_name', $user_id ), get_bloginfo('name'));
		$this->send_mail( $user_email , $email_title, $email_content );
  }

  public function transition_post_status_notify( $new_status, $old_status, $post ) {
  
    $current_user_id = get_current_user_id();
    if ( $new_status==$old_status || $post->post_author==$current_user_id )
      return;

    $user_email = get_the_author_meta( 'user_email', $post->post_author );
		if ( ! is_email( $user_email ) )
      return;
      
    $statuses = get_post_statuses();
    $email_title = sprintf( __('你在%1$s上的文章状态发生了改变','dmeng'), get_bloginfo('name') );
    $email_content = sprintf(
                                        __( '<h3>%1$s，你好！</h3><p>%2$s（用户ID:%3$s）将你的文章《%4$s》状态由"%5$s"改为"%6$s"，快去看看吧！</p>', 'dmeng' ),
                                        get_the_author_meta( 'display_name', $post->post_author ), 
                                        get_the_author_meta( 'display_name', $current_user_id ),
                                        $current_user_id,
                                        '<a href="'.get_permalink($post->ID).'" target="_blank">'.$post->post_title.'</a>',
                                        '<b>'.$statuses[$old_status].'</b>',
                                        '<b>'.$statuses[$new_status].'</b>'
                                      );

    $this->send_mail( $user_email , $email_title, $email_content );
  }

}

$dmeng_Email = new dmeng_Email;
