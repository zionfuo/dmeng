<?php
/**
 * 自定义评论列表 
 * https://codex.wordpress.org/Function_Reference/wp_list_comments
 */
function dmeng_comment($comment, $args, $depth) {

  $GLOBALS['comment'] = $comment;
  extract($args, EXTR_SKIP);

  $add_below = 'comment';
  
  $author = $comment->comment_author;
  $author_url = $author_posts_url = $comment->comment_author_url;
  $author_posts_url = $author_posts_url ? $author_posts_url : 'javascript:;';

  $comment->user_id = (int)$comment->user_id;
  
  $user = (object)array();

  if ( $comment->user_id && $user = get_userdata( $comment->user_id ) ) {

    $author_posts_url = get_author_posts_url( $user->ID );

    $author = $user->display_name;
    $author_url = esc_url($user->user_url);
    if ( empty( $author_url ) || 'http://' == $author_url )
      $author_url = $author_posts_url;

  }

  $author = dmeng_trim_username($author);

  if ( $author_url ) {
    $author = apply_filters( 'get_comment_author_link', '<a href="'.esc_url($author_url).'" rel="external nofollow" target="_blank">'.$author.'</a>', $author, $comment->comment_ID );
  }
  
  $name_title = '';

  if ( !empty($user->roles) && is_array($user->roles) ) {
    if (in_array('administrator', $user->roles)) {
      $name_title = __('[管理员]', 'dmeng');
    } else if (in_array('editor', $user->roles)) {
      $name_title = __('[编辑]', 'dmeng');
    }
  }
  
  if (empty($name_title)) {
    global $post;
    if ($comment->user_id===(int)$post->post_author)
      $name_title = __('[本文作者]', 'dmeng');
  }

  if ($name_title)
    $author .= ' <i class="v">'.$name_title.'</i>';
  
  $author_link = '<b class="fn">'.$author.'</b>';

  $is_stick_comments = apply_filters( 'dmeng_is_stick_comments', false );

  $sticky = get_comment_meta( $comment->comment_ID, 'dmeng_sticky_comment', true );
  
  $time = '<time class="num" datetime="'.get_comment_time( 'c' ).'" title="'.sprintf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ).'">'.dmeng_human_time_diff( get_comment_time('U') ).'</time>';
?>
<li <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?>>
  <?php
    if ( in_array($comment->comment_type, array('pingback', 'trackback') ) ) {
  ?>
<article id="<?php echo $comment->comment_type.'-'.get_comment_ID() ?>"><?php echo $time;?><span class="commenttype"><?php echo $comment->comment_type;?></span><?php comment_author_link();?></article>
  <?php
    return;
  }
  ?>
  <article id="<?php echo $add_below.'-'.get_comment_ID() ?>">
    <?php 
      if ( !$is_stick_comments ) { 
    ?>
    <a class="comment-author" href="<?php echo $author_posts_url;?>">
      <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, ( $depth<=1 ? 50 : 30 ) ); ?>
    </a>
    <?php 
      } 
    ?>
    <div class="comment-body">
      <?php 
        if ( $depth<=1 && !$is_stick_comments )
          echo $author_link;
      ?>
      <div class="comment-content">
        <?php
        
        global $comment_depth;
        $comment_parent = '';
        if ( $comment_depth>=$args['max_depth'] && !$is_stick_comments ) {
          $comment_parent = '<span class="top-level">' . __( 'Reply' ) . dmeng_auto_comment_author_name( get_comment($comment->comment_parent) ).' :</span> ';
        }
        
        echo $comment_parent . wpautop(get_comment_text());
  
        ?>
        <?php if ( $comment->comment_approved == '0' ) : ?>
          <small class="text-danger"><?php _e( 'Your comment is awaiting moderation.' ); ?></small>
        <?php endif; ?>
      </div>

      <footer class="comment-meta"><?php 
      
          if ( $depth>1 || $is_stick_comments )
            echo $author_link;

          echo $time;

        dmeng_vote_html( $comment->comment_ID, 'comment');
  
        comment_reply_link( wp_parse_args( array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => 999999999 ), $args ), $comment->comment_ID );

        edit_comment_link( __( 'Edit' ), '  ', '' );  
        
      ?></footer>
    </div>
  </article>
<?php
}
