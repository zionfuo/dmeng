<?php

/**
 * 积分
 * 
 * @user_meta dmeng_credit 当前可用积分
 * @user_meta dmeng_credit_void 无效（已消费）积分
 * 
 * @user_meta dmeng_rec_view 访问推广数据，每天0点清空
 * @user_meta dmeng_rec_reg 注册推广数据，每天0点清空
 * @user_meta dmeng_rec_post 投稿数据，每天0点清空
 * @user_meta dmeng_rec_comment 评论数据，每天0点清空
 * @user_meta dmeng_friend 注册推广人
 * 
 * @option dmeng_reg_credit 注册奖励积分，默认是50分
 * @option dmeng_rec_view_credit 访问推广一次可得积分，默认是5分
 * @option dmeng_rec_reg_credit 注册推广一次可得积分，默认是50分
 * @option dmeng_rec_post_credit 投稿一次可得积分，默认是50分
 * @option dmeng_rec_comment_credit 评论一次可得积分，默认是5分
 * 
 * @option dmeng_rec_view_num 每天可得积分访问推广次数，默认是50次
 * @option dmeng_rec_reg_num 每天可得积分注册推广次数，默认是5次
 * @option dmeng_rec_post_num 每天可得积分投稿次数，默认是5次
 * @option dmeng_rec_comment_num 每天可得积分评论次数，默认是50次
 * 
 */

class dmeng_Credit {

  public function __construct() {

    if ( dmeng_open_ctrl('credit')===false || dmeng_open_ctrl('ucenter')===false )
      return;

    add_action( 'wp_insert_comment', array( $this, 'comment_credit' ), 10, 2 );
    add_action( 'transition_post_status', array( $this, 'publish_post_credit' ), 10, 3 );
    
    add_action( 'user_register', array( $this, 'user_register_credit' ) );
    add_filter( 'dmeng_ready_ajax', array( $this, 'dmeng_friend_check' ) );
    
    add_filter( 'manage_users_columns', array( $this, 'credit_column' ) );
    add_action( 'manage_users_custom_column', array( $this, 'credit_column_callback' ), 10, 3 );
    
    add_action( 'show_user_profile', array( $this, 'profile_fields' ) );
    add_action( 'edit_user_profile', array( $this, 'profile_fields' ) );

    add_action( 'edit_user_profile_update', array( $this, 'update_profile_fields' ) );
    add_action( 'personal_options_update', array( $this, 'update_profile_fields' ) );
    
    add_action( 'dmeng_author_main_content', array( $this, 'main_selection' ) );
    add_action( 'wp_ajax_dmeng_get_credit', array( $this, 'get_credit_ajax_action' ) );
    
    add_filter( 'dmeng_author_template_tabs', array( $this, 'text_i18n' ) );
    
    add_filter( 'dmeng_ucenter_text_i18n', array( $this, 'text_i18n' ) );
    add_filter( 'dmeng_ready_ajax_userdata', array( $this, 'ready_ajax_userdata' ), 10, 2 );
    add_action( 'dmeng_ucenter_setting_page_credit', array( $this, 'setting_page' ) );
    
    add_action( 'dmengaction_callback', array( $this, 'dmengaction_callback' ), 10, 4 );
  }

  public function text_i18n( $array ) {
    $array['credit'] = __( '积分', 'dmeng' );
    return $array;
  }

  public function main_selection() {
    if ($GLOBALS['tab']!=='credit')
      return;

  ?>

<ul id="uclist" class="noborder" style="display:none"></ul>

<script>
!function($){
  'use strict';
  var $tips = dmengUcenter.$tips,
      user_id = dmengUcenter.user_id,
      privTip = '<?php _e('您无权查看Ta的积分记录','dmeng'); ?>',
      firstLoad = true,
      load = dmengUcenter.load = function () {
        if (firstLoad===false) {
          dmeng.goHash('#uclist');
          $('#uclist').html('<li><?php _e('加载中…','dmeng'); ?></li>');
        }
        firstLoad = false;
        dmengUcenter.autoLoad();
        dmengUcenter.loaded.success(function (response) {
          if (response==0) {
            $tips.html(privTip);
          } else if (typeof response==='object') {
            $tips.html(response.tips);
            if (response.data) {
              $('#uclist').html(response.data).fadeIn();
            }
          } else {
            $('#uclist').html(response).fadeIn();
          }
        });
      };
  $(document).ready(function () {
    dmeng.readyAjax.complete(function () {
      if (!dmeng.user.ID) {
        $tips.html(privTip);
        return;
      }
      if (dmeng.user.ID===user_id) {
        $tips.html('<?php _e('查看自己的记录请到用户中心。', 'dmeng');?><a href="'+dmeng.ucenter.url_format.replace( '%s', 'credit' )+'"><?php _e('点击查看','dmeng');?></a>').fadeIn();
        return;
      }
      load();
    });
  }).on('click', '#get_prev', dmengUcenter.prev
  ).on('click', '#get_next', dmengUcenter.next
  ).on('change', '#paginate', dmengUcenter.selectPaged
  );
}(jQuery);
</script>
    <table class="table table-bordered credit-table">
      <thead>
        <tr class="active">
          <th><?php _e('积分方法','dmeng');?></th>
          <th><?php _e('一次得分','dmeng');?></th>
          <th><?php _e('可用次数','dmeng');?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php _e('注册奖励','dmeng');?></td>
          <td><?php printf( __('%1$s 分','dmeng'), get_option('dmeng_reg_credit','50'));?></td>
          <td><?php _e('只有 1 次','dmeng');?></td>
        </tr>
        <tr>
          <td><?php _e('文章投稿','dmeng');?></td>
          <td><?php printf( __('%1$s 分','dmeng'), get_option('dmeng_rec_post_credit','50'));?></td>
          <td><?php printf( __('每天 %1$s 次','dmeng'), get_option('dmeng_rec_post_num','5'));?></td>
        </tr>
        <tr>
          <td><?php _e('评论回复','dmeng');?></td>
          <td><?php printf( __('%1$s 分','dmeng'), get_option('dmeng_rec_comment_credit','5'));?></td>
          <td><?php printf( __('每天 %1$s 次','dmeng'), get_option('dmeng_rec_comment_num','50'));?></td>
        </tr>
        <tr>
          <td><?php _e('访问推广','dmeng');?></td>
          <td><?php printf( __('%1$s 分','dmeng'), get_option('dmeng_rec_view_credit','5'));?></td>
          <td><?php printf( __('每天 %1$s 次','dmeng'), get_option('dmeng_rec_view_num','50'));?></td>
        </tr>
        <tr>
          <td><?php _e('注册推广','dmeng');?></td>
          <td><?php printf( __('%1$s 分','dmeng'), get_option('dmeng_rec_reg_credit','50'));?></td>
          <td><?php printf( __('每天 %1$s 次','dmeng'), get_option('dmeng_rec_reg_num','5'));?></td>
        </tr>
      </tbody>
    </table>
  <?php
  }
  
  public function get_credit_ajax_action() {

    do_action( 'dmeng_before_ajax' );
    
    if ( empty($_POST['user_id']) )
      die( '<li>'.__('用户ID不能为空', 'dmeng').'</li>' );
    
    $user_id = absint($_POST['user_id']);
    if ( !current_user_can('edit_user', $user_id) )
      die(0);
      
    $paged = empty($_POST['paged']) ? 1 : absint($_POST['paged']);

    $user = get_userdata($user_id);
    $credit = intval($user->dmeng_credit);
    $credit_void = intval($user->dmeng_credit_void);

    $result = array();
    $result['tips'] = sprintf(__('共有 %1$s 个积分，其中 %2$s 个已消费， %3$s 个可用。','dmeng'), ($credit+$credit_void), $credit_void, $credit);

    $all = get_dmeng_message($user_id, 'count', "msg_type='credit'");
    
    $number = 10;
    $offset = ($paged-1)*$number;
    
    $creditLog = get_dmeng_credit_message($user_id, $number, $offset);
    
    if ( $creditLog ) {
      
      $result['data'] = '';
      
      foreach( $creditLog as $log ){
        $result['data'] .= '<li><span class="num">'.$log->msg_date.'</span> <span class="text">'.$log->msg_title.'</span></li>';
      }

      ob_start();
      global $dmeng_UCenter;
      $dmeng_UCenter->setting_paginate( ceil($all/$number), $paged );
      $result['data'] .= ob_get_contents();
      ob_end_clean();

    }

    wp_send_json($result);
  }
  
  public function ready_ajax_userdata( $userdata, $current_user ) {
    $userdata['user_info']['credit'] = intval($current_user->dmeng_credit);
    return $userdata;
  }
  
  public function dmengaction_callback( $atts, $time, $post, $in_comment ) {
  
    $current_user_id = get_current_user_ID();
    if ( !$current_user_id || $atts['do']!='credit' )
      return;

    $atts['plus'] = isset($atts['plus']) ? absint($atts['plus']) : 0;
    $atts['minus'] = isset($atts['minus']) ? absint($atts['minus']) : 0;

    $msg = ( $atts['plus'] || $atts['minus'] ) ? ( $time>1 ? __( '第%1$s次', 'dmeng' ) : '' ) . ( $in_comment ? __( '评论《%2$s》时%3$s%4$s积分%5$s', 'dmeng' ) : __( '兑换《%2$s》时%3$s%4$s积分%5$s', 'dmeng' ) ) : '';

    $post_title = dmeng_the_title($post->ID);

    if ( $atts['plus'] ) {
      $this->update(
        $current_user_id, 
        $atts['plus'], 
        'plus', 
        'dmeng_credit', 
        sprintf( $msg, $time, $post_title, __( '获得奖励', 'dmeng' ), $atts['plus'], ( !empty($atts['title']) ? sprintf( __( '（%s）', 'dmeng' ), sanitize_text_field($atts['title']) ) : '' ) )
      );
    } else if ( $atts['minus'] ) {
      $this->to_void(
        $current_user_id,
        $atts['minus'],
        sprintf( $msg, $time, $post_title, __( '消费了', 'dmeng' ), $atts['minus'], ( !empty($atts['title']) ? sprintf( __( '（%s）', 'dmeng' ), sanitize_text_field($atts['title']) ) : '' ) )
      );
    }

    return;
  }

  /**
   * 更新用户积分
   */
   public function update( $user_id, $num, $method='plus', $field='dmeng_credit', $msg='' ) {

    if ( !is_numeric($user_id)  )
      return;

    $field = $field=='dmeng_credit' ? $field : 'dmeng_credit_void';
    
    $credit = (int)get_user_meta( $user_id, $field, true );
    $num = (int)$num;

    if ( $method=='plus' ) {
      $new_credit = ($credit+$num)>0 ? ($credit+$num) : 0;
      $add = update_user_meta( $user_id , $field, $new_credit );
      if ( $add ) {
        add_dmeng_message( $user_id,  'credit', current_time('mysql'), apply_filters( 'dmeng_credit_log', ($msg ? $msg : sprintf( __('获得%s积分','dmeng') , $num )), $new_credit, $credit, 'plus' ) );
        return $new_credit;
      }
    }
    
    if ( $method=='minus' ) {
      $new_credit = ($credit-$num)>0 ? ($credit-$num) : 0;
      $cut = update_user_meta( $user_id , $field, $new_credit );
      if ( $cut ) {
        add_dmeng_message( $user_id,  'credit', current_time('mysql'), apply_filters( 'dmeng_credit_log', ($msg ? $msg : sprintf( __('减去%s积分','dmeng') , $num )), $new_credit, $credit, 'minus' ) );
        return $new_credit;
      }
    }

    $update = update_user_meta( $user_id , $field, $num );
    if ( $update ) {
      add_dmeng_message( $user_id,  'credit', current_time('mysql'), apply_filters( 'dmeng_credit_log', ($msg ? $msg : sprintf( __('更新积分为%s','dmeng') , $num )), $num, $credit, 'update' ) );
      return $num;
    }

  }

  public function to_void( $user_id, $num, $msg='' ) {
     
    if ( !is_numeric($user_id) || !is_numeric($num) )
      return;

    $credit = (int)get_user_meta( $user_id, 'dmeng_credit' , true );
    $num = absint($num);
    
    if ($credit<$num)
      return 'less';
    
    $new_credit = $credit-$num;
    $cut = update_user_meta( $user_id , 'dmeng_credit', $new_credit );

    $credit_void = (int)get_user_meta( $user_id, 'dmeng_credit_void' , true );
    $add = update_user_meta( $user_id , 'dmeng_credit_void' , ($credit_void+$num) );
    
    add_dmeng_message( $user_id, 'credit', current_time('mysql'), apply_filters( 'dmeng_credit_log', ($msg ? $msg : sprintf( __('消费了%s积分','dmeng'), $num )), $new_credit, $credit, 'void' ) );
    
    return 0;
      
  }

  /**
   * 评论回复奖励积分
   */
  public function comment_credit( $comment_id, $comment_object ) {
    
    $user_id = intval($comment_object->user_id);
    if ( $user_id<1 )
      return;
      
    $rec_comment_num = intval(get_option( 'dmeng_rec_comment_num', '50' ));
    $rec_comment_credit = intval(get_option( 'dmeng_rec_comment_credit', '5' ));
    $rec_comment = intval(get_user_meta( $user_id, 'dmeng_rec_comment', true ));
      
    if ( $rec_comment<$rec_comment_num && $rec_comment_credit ) {
      $this->update( $user_id , $rec_comment_credit , 'plus' , 'dmeng_credit' , sprintf(__('获得评论回复奖励%1$s积分','dmeng') ,$rec_comment_credit) );
      update_user_meta( $user_id, 'dmeng_rec_comment', $rec_comment+1);
    }

  }

  public function publish_post_credit( $new_status, $old_status, $post ) {
  
    if ( $new_status == $old_status || $new_status != 'publish' || $post->post_type != 'post' || $post->post_author==get_current_user_id() )
      return;

    $rec_post_num = intval(get_option( 'dmeng_rec_post_num', '5' ));
    $rec_post_credit = intval(get_option( 'dmeng_rec_post_credit', '50' ));
    $rec_post = intval(get_user_meta( $post->post_author, 'dmeng_rec_post', true ));
    
    if ( $rec_post<$rec_post_num && $rec_post_credit ) {
      $this->update( $post->post_author, $rec_post_credit, 'plus', 'dmeng_credit', sprintf(__('获得文章投稿奖励%1$s积分','dmeng') ,$rec_post_credit) );
      update_user_meta( $post->post_author, 'dmeng_rec_post', $rec_post+1);
    }

  }

  /**
   * 用户注册时添加推广人和奖励积分
   */
  public function user_register_credit( $user_id ) {
    
    $friend_id = isset($_COOKIE['dm_fid']) ? (int)$_COOKIE['dm_fid'] : 0;
    
    if ($friend_id && get_user_option('show_admin_bar_front', $friend_id)!==false) {

      update_user_meta($user_id, 'dmeng_friend', $friend_id);
      $rec_reg_num = (int)get_option('dmeng_rec_reg_num','5');
      $rec_reg = json_decode(get_user_meta($friend_id, 'dmeng_rec_reg', true));
      $ua = $_SERVER["REMOTE_ADDR"].'&'.$_SERVER["HTTP_USER_AGENT"];
      if (!$rec_reg) {
        $rec_reg = array();
        $new_rec_reg = array($ua);
      } else {
        $new_rec_reg = $rec_reg;
        array_push($new_rec_reg , $ua);
      }
      if ((count($rec_reg) < $rec_reg_num) &&  !in_array($ua,$rec_reg)) {
        update_user_meta($friend_id , 'dmeng_rec_reg' , json_encode($new_rec_reg));
        $reg_credit = (int)get_option('dmeng_rec_reg_credit','50');
        if ($reg_credit) {
          $this->update( $friend_id , $reg_credit , 'plus' , 'dmeng_credit' , sprintf(__('获得注册推广（来自%1$s的注册）奖励%2$s积分','dmeng') , get_the_author_meta('display_name', $user_id) ,$reg_credit) );
        }
      }
    }
    
    $credit = get_option('dmeng_reg_credit','50');
    if ($credit) {
      $this->update( $user_id , $credit , 'plus' , 'dmeng_credit' , sprintf(__('获得注册奖励%s积分','dmeng') , $credit) );
    }

  }

  /**
   * 访问推广检查
   */
  public function dmeng_friend_check($vars) {
    
    $friend_id = isset($_POST['fid']) ? (int)$_POST['fid'] : 0;

    /**
     * 用户检查
     */
    if ($friend_id<1 || get_user_option('show_admin_bar_front', $friend_id)===false)
      return $vars;

    $rec_view_num = (int)get_option('dmeng_rec_view_num', '50');
    if ($rec_view_num<1)
      return $vars;

    $rec_view = (array)wp_unslash(json_decode(get_user_meta($friend_id, 'dmeng_rec_view', true), true));
    $current_timestamp = current_time('timestamp');
    foreach ($rec_view as $timestamp=>$ua) {
      if ( ($timestamp+DAY_IN_SECONDS)<$current_timestamp )
        unset($rec_view[$timestamp]);
    }

    /**
     * 最多推广次数检查
     */
    if (count($rec_view) >= $rec_view_num)
      return $vars;

    /**
     * 重复访问检查
     */
    $ua = $_SERVER['REMOTE_ADDR'].'&'.$_SERVER["HTTP_USER_AGENT"];
    if (in_array($ua, $rec_view))
      return $vars;

    /**
     * 都没问题，就可以添加到最近推广了
     */
    $rec_view[$current_timestamp] = $ua;
    update_user_meta($friend_id , 'dmeng_rec_view' , json_encode($rec_view));

    $view_credit = (int)get_option('dmeng_rec_view_credit', '5');
    if ($view_credit) {
      $this->update( $friend_id , $view_credit , 'plus' , 'dmeng_credit' , sprintf(__('获得访问推广奖励%1$s积分','dmeng') ,$view_credit) );
    }

    return $vars;
  }

  public function credit_column($columns) {
    $columns['dmeng_credit'] = '积分';
    return $columns;
  }
  
  public function credit_column_callback($value, $column_name, $user_id) {

    if( 'dmeng_credit' == $column_name ){
      $credit = intval(get_user_meta($user_id,'dmeng_credit',true));
      $void = intval(get_user_meta($user_id,'dmeng_credit_void',true));
      $value = sprintf(__('总积分 %1$s 已消费 %2$s <br> <a href="%3$s" target="_blank">修改积分</a>','dmeng'), ($credit+$void), $void, '#' );
    }

    return $value;
  }
  
  public function profile_fields( $user ) {
    
    $dmeng_credit = (int)get_user_meta( $user->ID, 'dmeng_credit', true );
    $dmeng_credit_void = (int)get_user_meta( $user->ID, 'dmeng_credit_void', true );
    
    $tips = current_user_can('edit_user') ? __( '不修改请保持默认，否则会覆盖最新数据' , 'dmeng' ) : __( '您无权自行修改积分' , 'dmeng' );
  ?>
  <table class="form-table">
    <tr>
      <th>
        <label for="dmeng_credit"><?php _e( '可用积分' , 'dmeng' ); ?></label>
      </th>
      <td>
        <input type="text" name="dmeng_credit" id="dmeng_credit" value="<?php echo $dmeng_credit; ?>" class="regular-text" <?php if ( !current_user_can('edit_user') ) echo 'disabled="disabled"';?> />
        <input type="hidden" name="dmeng_credit_default" value="<?php echo $dmeng_credit; ?>" />
        <p class="description"><?php echo $tips; ?></p>
      </td>
    </tr>
    <tr>
      <th>
        <label for="dmeng_credit_void"><?php _e( '已消费积分' , 'dmeng' ); ?></label>
      </th>
      <td>
        <input type="text" name="dmeng_credit_void" id="dmeng_credit_void" value="<?php echo $dmeng_credit_void; ?>" class="regular-text" <?php if ( !current_user_can('edit_user') ) echo 'disabled="disabled"';?> />
        <input type="hidden" name="dmeng_credit_void_default" value="<?php echo $dmeng_credit_void; ?>" />
        <p class="description"><?php echo $tips; ?></p>
      </td>
    </tr>
  </table>
  <?php
  }

  public function update_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user' ) )
      return;

    foreach ( array( 'dmeng_credit', 'dmeng_credit_void' ) as $field ) {
      
      if ( !isset($_POST[$field]) || !isset($_POST[$field.'_default']) )
        continue;
        
      if ( $_POST[$field] == $_POST[$field.'_default'] )
        continue;
        
      $credit = intval($_POST[$field]);

      $current_user = wp_get_current_user();
      $this->update( $user_id , $credit , 'update' , $field , sprintf( __( '%1$s（管理员ID:%2$s）将你的%3$s积分更新为%4$s', 'dmeng' ), $current_user->display_name, $current_user->ID, ( $field=='dmeng_credit' ? '可用' : '已消费' ), $credit ) );
    }
  }
  
  /**
   * 用户中心积分选项卡
   */
  public function setting_page() {
    
    global $current_user;
    get_currentuserinfo();

    $credit = intval($current_user->dmeng_credit);
    $credit_void = intval($current_user->dmeng_credit_void);

    ?>
    <div class="entry-content">
      <ul class="ucenter_list">
        <?php
        
        echo '<li class="title num">' . sprintf(__('共有 %1$s 个积分，其中 %2$s 个已消费， %3$s 个可用。','dmeng'), ($credit+$credit_void), $credit_void, $credit) . '</li>';
        
        $all = get_dmeng_message($current_user->ID, 'count', "msg_type='credit'");
        
        $paged = !empty($_POST['paged']) ? intval($_POST['paged']) : 1; 
        $number = 10;
        $num_page = ceil($all/$number);
        $offset = ($paged-1)*$number;

        $creditLog = get_dmeng_credit_message($current_user->ID, $number, $offset);

        if ( $creditLog ) {
          foreach( $creditLog as $log ){
            echo '<li><span class="num">'.$log->msg_date.'</span> <span class="text">'.$log->msg_title.'</span></li>';
          }
          
          global $dmeng_UCenter;
          $dmeng_UCenter->setting_paginate( $num_page, $paged );
        
        } else {
          echo '<li>' . __( '没有找到记录', 'dmeng' ) . '</li>';
        }
        ?>
      </ul>
    </div>
    <?php
    die();
  }
}

$dmeng_Credit = new dmeng_Credit;
