<?php

/**
 * 
 */

class dmeng_Message {
  
  public function __construct() {
    add_action( 'load-themes.php', array( $this, 'install' ) );
    add_action( 'delete_user', array( $this, 'delete_by_user' ) );
    
    add_action( 'wp_ajax_dmeng_get_message', array( $this, 'get_message_ajax_action' ) );
    add_action( 'wp_ajax_dmeng_pm', array( $this, 'pm_ajax_action' ) );
  }
  
  /**
   * 数据库表格名称
   */
  public $table_name = 'dmeng_message';   

  /**
   * 启用主题时创建数据库表格
   * https://codex.wordpress.org/Plugin_API/Action_Reference/load-themes.php
   */
  /**
   * msg_id 自动增长主键
   * user_id 用户ID
   * msg_type 类型
   * msg_date 日期
   * msg_title 标题
   * msg_content 内容
   * 
   */
  public function install() {
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    dmeng_create_db_table(
      $this->table_name,
      " CREATE TABLE `$this->table_name` (
        `msg_id` int NOT NULL AUTO_INCREMENT, 
        `user_id` int,
        `msg_type` varchar(20),
        `msg_date` datetime,
        `msg_title` tinytext,
        `msg_content` text,
        PRIMARY KEY(user_id),
        KEY msg_type (msg_type)
      ) $charset_collate;"
    );
  }

  public function add( $uid=0, $type='', $date='', $title='', $content='' ) {

    $uid = intval($uid);
    $title = sanitize_text_field($title);
    
    if ( !$uid || empty($title) )
      return;

    $type = $type ? sanitize_text_field($type) : 'unread';
    $date = $date ? $date : current_time('mysql');
    $content = htmlspecialchars($content);
    
    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;

    if ( $wpdb->insert( $table_name, array( 'user_id'=> $uid, 'msg_type'=> $type, 'msg_date'=> $date, 'msg_title'=> $title, 'msg_content'=> $content ) ) )
      return $wpdb->insert_id;
    
    return 0;
    
  }

  public function delete( $id=0, $uid=0, $type='', $title='' ) {

    $id = intval($id);
    $uid = intval($uid);

    if ( !$id && !$uid )
      return;

    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;

    $where = array();
    if ($id)
      $where['msg_id'] = $id;
    if ($uid)
      $where['user_id'] = $uid;
    if ($type)
      $where['msg_type'] = $type;
    if ($title)
      $where['msg_title'] = $title;

    return $wpdb->delete( $table_name, $where );

  }

  public  function delete_by_user( $user_id ) {
    $this->delete( 0, $user_id );
  }

  public function update_type( $id=0, $uid=0, $type='' ) {

    $id = intval($id);
    $uid = intval($uid);

    if ( ( !$id || !$uid) || empty($type) )
      return;

    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;

    if ( $id===0 ) {
      $sql = " UPDATE $table_name SET msg_type = '$type' WHERE user_id = '$uid' ";
    } else {
      $sql = " UPDATE $table_name SET msg_type = '$type' WHERE msg_id = '$id' ";
    }

    if ($wpdb->query( $sql ))
      return 1;
    
    return 0;
    
  }

  public function get( $uid=0 , $count=0, $where='', $limit=0, $offset=0 ) {
    
    $uid = intval($uid);
    
    if ( !$uid )
      return;

    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;
    
    if ($count) {
      if($where) $where = " AND $where";
      $check = $wpdb->get_var( "SELECT COUNT(*) FROM $table_name WHERE user_id='$uid' $where" );
    } else {
      $check = $wpdb->get_results( "SELECT msg_id,msg_type,msg_date,msg_title,msg_content FROM $table_name WHERE user_id='$uid' AND $where ORDER BY msg_date DESC LIMIT $offset,$limit" );
    }
    
    if ($check)
      return $check;

    return 0;

  }

  public function get_credit_message( $uid=0 , $limit=0, $offset=0 ) {
    
    $uid = intval($uid);
    
    if ( !$uid )
      return;

    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;
    
    $check = $wpdb->get_results( "SELECT msg_id,msg_date,msg_title FROM $table_name WHERE msg_type='credit' AND user_id='$uid' ORDER BY msg_date DESC LIMIT $offset,$limit" );

    if ($check)
      return $check;

    return 0;

  }

  public function get_pm( $pm=0, $from=0, $count=false, $single=false, $limit=0, $offset=0 ) {
    
    $pm = intval($pm);
    $from = intval($from);
    
    if ( !$pm || !$from )
      return;

    global $wpdb;
    $table_name = $wpdb->prefix . $this->table_name;
    
    $title_sql = $single ? "msg_title='{\"pm\":$pm,\"from\":$from}'" : "( msg_title='{\"pm\":$pm,\"from\":$from}' OR msg_title='{\"pm\":$from,\"from\":$pm}' )";
    
    if ($count) {
      $check = $wpdb->get_var( "SELECT COUNT(*) FROM $table_name WHERE ( msg_type='repm' OR msg_type='unrepm' ) AND $title_sql" );
    } else {
      $check = $wpdb->get_results( "SELECT msg_id,msg_date,msg_title,msg_content FROM $table_name WHERE ( msg_type='repm' OR msg_type='unrepm' ) AND $title_sql ORDER BY msg_date DESC LIMIT $offset,$limit" );
    }
    
    return $check ? $check : 0;
  }

  public function get_message_ajax_action_callback($pm_to, $paged) {
    $number = 10;
    $offset = ($paged-1)*$number;

    $current_user_id = get_current_user_id();

    $all = absint($this->get_pm($pm_to, $current_user_id, true));

    if ($all<1)
      return '<li>'.__('没有记录', 'dmeng').'</li>';

    $logs = $this->get_pm($pm_to, $current_user_id, false, false, $number, $offset);
    if (empty($logs))
      return '';
    
    $output = '';
    
    foreach ($logs as $log) {
      $pm_data = json_decode($log->msg_title);
			if( $pm_data->from==$pm_to ){
        $this->update_type($log->msg_id, $pm_to, 'repm');
			}
			$output .= '<li class="msg'.( $pm_data->from==$current_user_id ? ' highlight' : '' ).'"><div class="message-content clearfix"><span class="'.( $pm_data->from==$current_user_id ? 'pull-right' : 'pull-left' ).'">'.get_avatar( $pm_data->from, 34 ).'</span><div class="pm-box"><div class="pm-content">'.wpautop(wp_strip_all_tags(htmlspecialchars_decode($log->msg_content))).'</div><p class="pm-date">'.date_i18n( get_option( 'date_format' ).' '.get_option( 'time_format' ), strtotime($log->msg_date)).'</p></div></div></li>';
    }

    if ($paged<ceil($all/$number))
      $output .= '<li class="text-center"><a href="javascript:;" data-loading-text="'.__('加载中…', 'dmeng').'" id="load-more">'.__('加载更早的记录', 'dmeng').'</a></li>';
    
    return $output;
  }
  
  public function get_message_ajax_action() {

    do_action( 'dmeng_before_ajax' );
    
    if ( empty($_POST['user_id']) )
      die( '<li>'.__('用户ID不能为空', 'dmeng').'</li>' );
    
    $paged = empty($_POST['paged']) ? 1 : absint($_POST['paged']);

    die( $this->get_message_ajax_action_callback(absint($_POST['user_id']), $paged) );
  }

  public function pm_ajax_action() {
    do_action( 'dmeng_before_ajax' );

    if ( empty($_POST['pm_to']) || empty($_POST['pm']) || wp_strip_all_tags(trim($_POST['pm']))=='' )
      die( __('收件人和私信内容都不能为空', 'dmeng') );

    $pm = wp_strip_all_tags(trim($_POST['pm']));
    $pm_to = absint($_POST['pm_to']);
    
    $user_info = get_userdata($pm_to);
    if (false===$user_info)
      die( __('收件用户不存在', 'dmeng') );

    global $current_user;
    get_currentuserinfo();
    
    $pm_check = $this->get_pm( $user_info->ID, $current_user->ID, false, true, 1 );
    if ( $pm_check && !empty($pm_check[0]) && !empty($pm_check[0]->msg_content) && $pm_check[0]->msg_content==$pm )
      die( __('请不要提交重复内容', 'dmeng') );
    
    if( $this->add(
          $user_info->ID, 
          'unrepm', 
          '', 
          json_encode(array(
            'pm' => $user_info->ID,
            'from' => $current_user->ID
          )), 
          $pm
        )
    ) {
				// 发邮件通知
				if (is_email($user_info->user_email)) {
					dmeng_mail(
            $user_info->user_email, 
            sprintf(__('%s给你发来了私信','dmeng'), $current_user->display_name), 
            '<h3>'.sprintf( __('%1$s，你好！','dmeng'), $user_info->display_name ).'</h3><p>'.sprintf( __('%1$s给你发来了<a href="%2$s" target="_blank">私信</a>，快去看看吧：<br> %3$s','dmeng'), $current_user->display_name, htmlspecialchars( add_query_arg('tab', 'message', get_author_posts_url( $user_info->ID )) ), wp_trim_words( $pm, 140, '…' )).'</p>'
          );
				}
        wp_send_json(array('success'=>1, 'result'=>$this->get_message_ajax_action_callback($user_info->ID, 1)));
    }
    die();
  }

}

$dmeng_Message = new dmeng_Message;
