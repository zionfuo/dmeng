<?php

class dmeng_CodePrettify {
  
  public function __construct() {
    add_filter( 'the_content', array( $this, 'auto_linenums' ), 1 );
    add_filter( 'dmeng_scripts_args', array( $this, 'output_script_uri' ) );
  }
  
  public function auto_linenums($content) {
    return preg_replace_callback(
            '/(<code[^>]*>)(.*?)<\/code>/is', 
            array($this, 'auto_linenums_callback'), 
            $content
           );
  }

  public function auto_linenums_callback($matches) {
    if (strpos($matches[2], PHP_EOL) || strpos($matches[2], "\n"))
      $matches[1] = '<code data-linenums="true"'.ltrim($matches[1], '<code');

    return $matches[1].esc_html($matches[2]).'</code>';
  }
  
  public function output_script_uri($args) {
    if (empty($args['cpfuri']))
      $args['cpfuri'] = apply_filters('dmeng_codeprettify_uri', get_template_directory_uri() . '/google-code-prettify/prettify.js');
    
    return $args;
  }

}

$dmeng_CodePrettify = new dmeng_CodePrettify;