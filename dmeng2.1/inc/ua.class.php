<?php

/**
 * UA
 */

class dmeng_UA_parse {
  
  public $user_agent;
  public $os_array;
  public $browser_array;
  
  public function __construct() {
    $this->os_array = $this->default_os_array();
    $this->browser_array = $this->default_browser_array();
  }
  
  public function default_os_array() {
    return array(
      '/windows nt 10/i'     =>  'Windows 10',
      '/windows nt 6.3/i'     =>  'Windows 8.1',
      '/windows nt 6.2/i'     =>  'Windows 8',
      '/windows nt 6.1/i'     =>  'Windows 7',
      '/windows nt 6.0/i'     =>  'Windows Vista',
      '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
      '/windows nt 5.1/i'     =>  'Windows XP',
      '/windows xp/i'         =>  'Windows XP',
      '/windows nt 5.0/i'     =>  'Windows 2000',
      '/windows me/i'         =>  'Windows ME',
      '/win98/i'              =>  'Windows 98',
      '/win95/i'              =>  'Windows 95',
      '/win16/i'              =>  'Windows 3.11',
      '/macintosh|mac os x/i' =>  'Mac OS X',
      '/mac_powerpc/i'        =>  'Mac OS 9',
      '/linux/i'              =>  'Linux',
      '/ubuntu/i'             =>  'Ubuntu',
      '/iphone/i'             =>  'iPhone',
      '/ipod/i'               =>  'iPod',
      '/ipad/i'               =>  'iPad',
      '/android/i'            =>  'Android',
      '/blackberry/i'         =>  'BlackBerry',
      '/webos/i'              =>  'Mobile'
    );
  }
  
  public function default_browser_array() {
    return array(
      '/msie/i'       =>  'Internet Explorer',
      '/firefox/i'    =>  'Firefox',
      '/safari/i'     =>  'Safari',
      '/chrome/i'     =>  'Chrome',
      '/opera/i'      =>  'Opera',
      '/netscape/i'   =>  'Netscape',
      '/maxthon/i'    =>  'Maxthon',
      '/konqueror/i'  =>  'Konqueror',
      '/mobile/i'     =>  'Handheld Browser'
    );
  }
  
  public function match($array) {
    $result = false;
    foreach ($array as $regex => $value) {
        if (preg_match($regex, $this->user_agent)) {
            $result = $value;
        }
    }
    return $result;
  }

  public function result() {
    return array(
      'os' => $this->match($this->os_array),
      'browser' => $this->match($this->browser_array)
    );
  }
  
}

$dmeng_UA_parse = new dmeng_UA_parse;
