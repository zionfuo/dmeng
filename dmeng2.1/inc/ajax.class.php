<?php

/**
 * AJAX
 */

class dmeng_AJAX {
  
  public function __construct() {

    add_action( 'wp_ajax_dmeng_ready', array( $this,'dmeng_ready' ) );
    add_action( 'wp_ajax_nopriv_dmeng_ready', array( $this, 'dmeng_ready' ) );
    
    add_action( 'dmeng_before_ajax', array( $this, 'security_check' ) );
    
    add_action( 'wp_ajax_get_comments_template', array( $this,'get_comments_template' ) );
    add_action( 'wp_ajax_nopriv_get_comments_template', array( $this, 'get_comments_template' ) );

    add_filter( 'comments_template', array( $this, 'ajax_comments_template' ) );

    add_action( 'set_comment_cookies', array( $this, 'return_comments_template' ) );
    
  }
  
  /**
   * 网页加载完成后进行的AJAX动作，如流量统计、获取当前登录用户信息等
   */
  public function dmeng_ready() {

    $vars = array();

    if ( 
      isset($_POST['tracker']) 
      && isset($_POST['tracker']['type']) 
      && isset($_POST['tracker']['pid']) 
      && $_POST['tracker']['type']=='single' 
      && comments_open((int)$_POST['tracker']['pid'])
    ) {
      $comments_number = (int)get_comments_number((int)$_POST['tracker']['pid']);
      $vars['comments'] = $comments_number>0 ? number_format_i18n($comments_number) : 0;
    }

    wp_send_json( apply_filters( 'dmeng_ready_ajax', $vars ) );
  }
  
  public function security_check() {
    if ( 
      empty($_POST) 
      || empty($_POST['_wpnonce']['hash']) 
      || empty($_POST['_wpnonce']['nonce']) 
      || !wp_verify_nonce( $_POST['_wpnonce']['nonce'], $_POST['_wpnonce']['hash'] )
    ) {
      die( __( '安全验证码无效，请刷新页面重试。', 'dmeng' ) );
    }
  }
  
  /**
   * 评论模板
   */
  public function return_comments_template( $comment ) {
    
    if ( empty($_POST['dmeng_ajax_comment']) )
      return;

    $this->get_comments_template();
    
    die();
  }
  
  /**
   * 评论模板
   */
  public function get_comments_template() {

    global $comment;
    
    if ( empty($comment) ) {
      
      $post_id = $_POST['post_id'];
      $cpage = $_POST['cpage'];
      
    } else {
      
      $args = array();
      
      if (get_option('page_comments'))
        $args['per_page'] = get_option('comments_per_page');
    
      $post_id = $comment->comment_post_ID;
      $cpage = get_page_of_comment( $comment->comment_ID, $args );
    }

    $post_id = intval($post_id);
    $cpage = intval($cpage);
    
    if ( empty($post_id) )
      return;

    add_filter( 'dmeng_ajax_comments_template', '__return_false' );

    query_posts( array( 
      'post_type' => get_post_type($post_id),
      'p' => $post_id
     ) );
      if ( have_posts() ) : 
        while ( have_posts() ) : the_post();
        
          if ($cpage) {
            global $wp_query;
            $wp_query->set( 'cpage', $cpage );
          }

          comments_template( '', true );
          
        endwhile;
      endif;
    wp_reset_query();
    
    add_filter( 'dmeng_ajax_comments_template', '__return_true' );
    
    die();
  }

  /**
   * AJAX 加载评论
   */
  public function ajax_comments_template( $template ) {

    /**
     * UA带有类似搜索引擎标识时直接输出评论，其他情况使用 AJAX 加载
     */
    if ( apply_filters( 'dmeng_ajax_comments_template', true ) && !(isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|slurp|spider/i', $_SERVER['HTTP_USER_AGENT'])) )
      $template = get_template_directory() . '/ajax-comments.php';
      
    return $template;

  }
  
}

$dmeng_AJAX = new dmeng_AJAX;
