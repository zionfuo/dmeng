<?php

class dmeng_Open_connect_sms extends dmeng_Open_connect {
  
  public $connect = 'sms';

  public $avatar_format = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAGZ0lEQVRYR+2Xa2wcVxXHf3dm17vr9Wb9WL+duEnd4FdCYgualKoSUKSmTRMlVYUqoSpp2gil4gMviS9USFEkVCGoECJ9uZRIUIQQVVuqIiGEBBUkJW4Ss340iVMcx43tfdjr3ZmdmZ2Zi2bWryROBB9CvnQ+rOY+zj3/+Z9z/ves4A4/4g7751MAYurFiIzGiyuRkNcFRYA/JfHpkou/3pRAIMurSH+9vMMfL716Q7GKaFneoS9EWH9EF2Lu18jqr/ym7DX/4e1PiVif7yPz3hMknpRCzB4Xsmbn10AbA0e7/QAWPcxfGqP+gCtE+pdCujrU3v8UUhsBO/9/AZG9MErjQacMIBCuxEjrNO76Lu78KSjlbjuIVDJJ0yFbiNnXVRlrWY+RSVFMFWne/R3c+TPIJSYc/baASSVHaH7GFmJmICBruzqx8zn0dBYjZdCy59u4uSGkXfCdr+R2Gcv14yWEkyWd0WKay7bFvOv409WKSnuggp6KMM1KsGwMzA6dp/XrJSGuvhKQie7Nfh1ZC3mKmTmKMyZt+7+FuzCGtLVFI69+Fq2Xq6pcU1nX4o8LV0gFFXZ0PUoivokNPO07usyrpHLjvJ98kxbb5uFwjLiiMj00zvojlhBTLwVlfdcmcF3fkZXX0NM5tBmLux7/Jm5+HGnry8ivj8ekled3+Ut03NVJZaSS2WwGyzZwXAcpJaFgmMpwJU2JBFMz00xNXuSxUBQlOUX7N0whrvy8QtZ3tyOdMmXex1kFHS2dL4N45MtIN4IQK6KzREDWNjiR+xdbu7YzcXUKiX3TfAmoAQzTob25heToILvG82w5UhTi8s9CsqGzDWmXrjG2CgZaVkOfsdi471EQsRsOf23677R03M1sNoVlWxTzaRQ1eMO+A9uTvH6ml1BlnFLJpbW+kU+GzvDsY3NC/PunYdnU2YJrWYt662mvp6sCs2CiZ4rkZy3u2fclRKRj+fAx7QqnmERVgxT0AqaWYW/v70lEetZkIV0c5q3kfkLROja2buCjiYtkC9nd4uOfhGVTdxNO0ShrdlnUlzPd0koUMiZayuKevZ9Drd7pOzgx+SfCjXFSc2l/7Fg6B/uGOJHcsSaAJ3tP8tpgD4FQjEg4TFWkiuTHY2+Liz+KyJbeRhxt7Xp3JRiFElapmsLklA/CBY59Ms6GllYKetlOOgZP9Z3jF+fuXRPAwc+eYuB0N0qwHMqGugSjlz4yxIXnK2Xrlgbs/LUS7CVaqSQwLUHDfY+T++cb5LMWerqEHlR4c2uC5oYaTD90HgUmh/rPMXD282sCOLTtA1493Y0IVPnrNbF1jE9OIcZ+GJUbttZTmvfkd0liBKYp0IuCtgd2oQ29S0VIoBdsCvMlUkGVt9pqqKuuwrZtDEMQDhl8tfcPxELrb1oJA6d7kEq0zJgbIqfPI0aPVcn27Qms7NwyAF1TKeQVNj54P1ryL77zJWiG5pBH8FK8hsa6KIZlYfoATJ7uP3tT597CwGAvrqj097hOBXldQwwfrZIb+xOY6awPoLAgmM+odD6yjcLIaUJBLykl0lNBXwC8lkRwVNSwribgH2aaCqGQy+H+D24J4OXBlfCYhoJulDSR/EFMbrq3HnMmTW5OIZtS2LJ3M7nhERQFQkFnzcbxRRknXxMhoEp0TaEy6vJM38lbAnjlw5UK0QoKRsn+mxh6Li47dtYxPZwhPa2yfX8bc8lLKKogIFbUcRUBPhMnRZg/R2qJxRyKukqk0uFw/60BvDy4AmAuG0Qq5rPi7Pfjsm59FVfPF9m2N8H8yJT/FYptIRRQFIHr1eLS40lFGRc/jrQiqlUqKhw8Snd95nnurrtvTRZy5gS/HX4Cu6RQLAbwLtoXDp8XYvB71dKxFbbtiaNd0QlXhW9J4+rFqw4c18PEa11ct4Rtg+MIbFtZTLRru/6KCohGFbKzgocuZHjwaFqI1sbgjncORv+x3MX6pouGfkdb7mLFKhKWXyVMRIO8354g0egQjkgMwyaTUq9hzGNRVRR/PT+n8oWJNF1Cp/u5gt8vf/G//mS48aYB9hx4YF9938zumgRtTU0Kqir9sHmne7dooSBIZ1y0HFPF8Y7jv3rh3WOrIvo/uL9xq6cqdcAmoLZ/z7qejp3rHq6KhTcrCn6NulKWtAXzwthf8++cfS/n9f2e4nmJlgG0T/+a3XEG/gNuRtaze5mcUQAAAABJRU5ErkJggg==';

  public function __construct() {

    if ((int)get_option('dmeng_open_sms')===0)
      return;
    
    parent::__construct();
    add_action( 'wp_ajax_dmeng_sms', array( $this,'sms_ajax' ) );
    add_action( 'wp_ajax_nopriv_dmeng_sms', array( $this, 'sms_ajax' ) );
  }
  
  public function name() {
    return __( '短信' ,'dmeng' );
  }

  public function loginform_style() {
    return 'color:#fff;background:#8bcf7b;';
  }
  
  public function openid_field_name( $connect = '' ) {
    return $this->prefix . 'phone';
  }
  
  public function code_field_name( $user_id=0 ) {
    return $this->prefix.'sms_code_'.$user_id;
  }
  
  public function avatar_radio( $avatars ) {
    return $avatars;
  }
  
  public function open_avatar( $avatar, $user_id, $connect ) {
    if ( $this->connect == $connect && $this->is_open() && get_user_meta( $user_id, $this->openid_field_name() ) ) {
      $avatar = $this->avatar_format;
    }
    return $avatar;
  }

  public function get_auth() {

    $current_user_id = get_current_user_id();

    /**
     * 处理短信登录跳转
     */
    if (
      !empty($_GET['validate'])
      && !empty($_GET['phone'])
      && !empty($_GET['code'])
      && wp_verify_nonce($_GET['validate'], $_GET['phone'])
      && !$current_user_id
    ) {
      $phone_user_id = $this->sms_check_code( 0, $_GET['phone'], absint($_GET['code']) );
      if ($phone_user_id) {
        $this->empty_user_code($phone_user_id);
        $this->login($phone_user_id);
      }
    }

    $phone = $current_user_id ? get_the_author_meta( $this->openid_field_name(), $current_user_id ) : '';

    /**
     * 主要是要调用RSA
     */
    $dmeng_Security = dmeng_Security();

    ob_start();
?>

    <style>
      #smsform{width:300px;max-width:100%;margin:0 auto;text-align:left}
      #smsform label{display:block;margin:10px 0;font-size:13px;color:#666}
    </style>
    
    <div id="tips"><?php _e( '请稍后，表单加载中…', 'dmeng' );?></div>
    <form method="post" id="smsform" style="display:none">
      <?php 

      if ($phone)
        echo '<p>' . sprintf( __('你好，%1$s，你已绑定 %2$s ，更换绑定号码，将会取消这个号码的绑定。'), get_the_author_meta( 'display_name', $current_user_id ), substr_replace($phone, '****', 3, 4) ) .'</p>';

      ?>
      <input type="hidden" id="nonce" name="nonce" value="<?php echo wp_create_nonce('dmeng_sms');?>" />
      <label for="phone" id="phone_label"><?php _e('手机号码', 'dmeng');?></label>
      <div class="justified">
        <div><input type="text" class="button" name="phone" id="phone" placeholder="<?php _e('请输入手机号码', 'dmeng');?>" autocomplete="off" required></div>
      </div>
      <label for="code" id="code_label"><?php _e('短信验证码', 'dmeng');?></label>
      <div class="justified">
        <div><input type="text" class="button" name="code" id="code" placeholder="<?php _e('验证码', 'dmeng');?>" autocomplete="off" required></div>
        <div><button id="get_code" class="button"><?php _e('发送验证码', 'dmeng');?></button></div>
      </div>
      <label id="submit_label"><?php _e('验证登录', 'dmeng');?></label>
      <div class="justified">
        <div><input type="submit" id="submit" class="button" value="<?php echo $current_user_id ? __('提交验证', 'dmeng') : __('确认登录', 'dmeng'); ?>" /></div>
      </div>
    </form>
    <br>
    <p class="small"><?php _e('验证码不论前后，在30分钟内全部有效。', 'dmeng');?></p>

    <?php 

      echo $this->black_link() . ( $current_user_id ? '' : ' - <a href="'.wp_login_url().'">'.__('其他登录方式','dmeng').' &raquo;</a> ');
    
  if ($dmeng_Security->maybe_auto_RSA()) {

    echo $dmeng_Security->RSA_public_key(true);
    
?>

<script>
(function($){
  $(document).ready(function(){
    var notice_id = '#rsa_notice';
    $('body').css('padding-top', 50).prepend($('<div />').attr({'id':notice_id.replace('#',''), 'class':'top_notice'}).html('<?php _e( '正在强制启用加密模式…', 'dmeng' );?>'));

    $.getScript('<?php echo dmeng_jsencrypt_uri();?>', function(){
      var public_key = $('#RSA_PUBLIC_KEY').val(),
          crypt = new JSEncrypt()
      crypt.setKey(public_key);
      if (false===crypt.encrypt('test')) {
        $(notice_id).html('<?php _e('加密程序启用异常，请刷新重试', 'dmeng');?>');
        return;
      }
      setTimeout(function(){
        $(notice_id).addClass('ok').html('<?php _e('正在使用RSA公钥加密算法防止手机号码被窃取', 'dmeng');?>');
      }, 500);
      window.RSAencrypt = function(phone) {
        var pwd = crypt.encrypt('<?php echo $dmeng_Security->RSA_text_prefix();?>'+phone);
        if (pwd) {
          phone = '<?php echo $dmeng_Security->RSA_text_prefix();?>'+pwd;
        }
        return phone;
      };
    }).fail(function(){
      $(notice_id).html('<?php _e('加载加密程序失败，请刷新重试', 'dmeng');?>');
    });
  });
})(jQuery);
</script>
<?php
  }
?>
    <script>
      (function($){

        $(document).ready(function(){
          $('#tips').hide();
          $('#smsform').fadeIn();
        });

        var $phone_label = $('#phone_label'),
            phone_label_text = $phone_label.html(),
            $code_label = $('#code_label'),
            code_label_text = $code_label.html(),
            $submit_label = $('#submit_label'),
            submit_label_label_text = $submit_label.html(),
            $phone = $('#phone'),
            $get_code = $('#get_code'),
            get_code_text = $get_code.html(),
            $submit = $('#submit'),
            submit_text = $submit.val(),
            wait_sec = 0,
            wait_text = '<?php _e( '%s 秒后可重发', 'dmeng' ); ?>';

        function wait_tips(){
          if ( wait_sec > 1 ) {
            wait_sec--;
            $get_code.html(wait_text.replace('%s', wait_sec));
            setTimeout( function(){
              wait_tips();
            }, 1000);
            return;
          }
          $get_code.removeAttr( 'disabled' ).html(get_code_text);
        }

        function response_callback(response) {
          response = response.responseJSON;
          if ( response ) {
            if ( response.phone ) {
              $phone_label.addClass('danger').html(response.phone);
            }
            if ( response.submit ) {
              $submit_label.addClass('danger').html(response.submit);
            }
            if ( response.wait ) {
              wait_sec = parseInt(response.wait);
            }
          }
          wait_tips();
          $submit.removeAttr('disabled').val(submit_text);
        }
        
        $(document).on('click', '#get_code', function() {
          var phone_val = $phone.val(),
            phoned = phone_val.match(/\d/g);
          if ( !phoned || phoned.length !==11 ) {
            $phone_label.addClass('danger').html('<?php _e( '请输入正确的手机号码', 'dmeng' );?>');
            $phone.focus();
            return false;
          }
          $phone_label.removeClass('danger').html(phone_label_text);
          $get_code.attr( 'disabled', 'disabled' ).html('<?php _e( '发送中…', 'dmeng' );?>');
            $.ajax({ 
              type: 'POST', 
              url: '<?php echo admin_url( 'admin-ajax.php' );?>', 
              data: {
                action: 'dmeng_sms',
                nonce: $('#nonce').val(),
                phone: (typeof RSAencrypt==='function' ? RSAencrypt(phone_val) : phone_val)
              }, 
              complete: response_callback
            });
          return false;
        });
        
        $(document).on('submit', '#smsform', function() {
          $submit.attr( 'disabled', 'disabled' ).val('<?php _e( '正在验证…', 'dmeng' );?>');
            $.ajax({ 
              type: 'POST', 
              url: '<?php echo admin_url( 'admin-ajax.php' );?>', 
              data: {
                action: 'dmeng_sms',
                validate: 1,
                nonce: $('#nonce').val(),
                phone: (typeof RSAencrypt==='function' ? RSAencrypt($phone.val()) : $phone.val()),
                code: $('#code').val()
              }, 
              complete: response_callback
            });
          return false;
        });
      })(jQuery);
    </script>
    <?php
    $html = ob_get_contents();
    ob_end_clean();

    dmeng_die(
      $html,
      ( $current_user_id ? __( '绑定登录手机号', 'dmeng' ) : __( '短信登录', 'dmeng' ) )
    );

  }

  public function is_phone_number( $phone=0 ) {
    return is_numeric($phone) && strlen($phone)===11;
  }

  /**
   * 通过手机号码（或手机号的MD5值）获取用户ID
   */
  public function get_user_id_by_phone( $phone=0, $md5=false ) {
    $id_field = $this->openid_field_name();
    global $wpdb;
    
    if ($md5)
      $where = " WHERE `meta_key`='$id_field' AND md5(`meta_value`)='$phone' ";
    else
      $where = " WHERE `meta_key`='$id_field' AND `meta_value`='$phone' ";

    $user_exists = $wpdb->get_var( "SELECT user_id FROM $wpdb->usermeta " . $where );

    return isset($user_exists) ? absint($user_exists) : 0;
  }
  
  /**
   * 清空某个用户的验证码
   * 为什么是清空而不是直接删除呢？
   * 因为需要防止滥用短信接口，直接删除没有发送记录了
   */
  public function empty_user_code( $user_id=0 ) {
    $transient_key = $this->code_field_name($user_id);
    $transient = (array)wp_unslash(json_decode(get_transient($transient_key), true));
    if ( empty($transient) )
      return false;

    foreach ($transient as $timestamp=>$data) {
      $transient[$timestamp] = array(
        'code' => '',
        'phone' => ''
      );
    }

    $exp = max(array_keys($transient))-current_time('timestamp');
    set_transient($transient_key,  json_encode($transient), $exp);
  }

  /**
   * 检查验证码是否可用
   */
  public function sms_check_code( $user_id=0, $phone=0, $code=0 ) {
    
    $use_md5 = false===$this->is_phone_number($phone);
    
    if ( absint($user_id)<=0 )
      $user_id = $this->get_user_id_by_phone($phone, $use_md5);

    $transient_key = $this->code_field_name($user_id);
    $transient = (array)wp_unslash(json_decode(get_transient($transient_key), true));
    if ( !empty($transient) ) {
      $now = current_time('timestamp');
      foreach( $transient as $timestamp=>$data ) {
        if ( $timestamp <= $now )
          continue;
        if (
          !empty($data['phone']) 
          && ($use_md5 ? md5($data['phone']) : $data['phone'])==$phone 
          && !empty($data['code'])
          && $data['code']==$code
        )
          return $user_id;
      }
    }

    return false;
  }
  
  /**
   * 获取新的验证码
   */
  public function sms_new_code( $user_id=0, $phone=0 ) {
    $transient_key = $this->code_field_name( $user_id );
    $transient = (array)wp_unslash(json_decode(get_transient( $transient_key ), true));
    $now = current_time('timestamp');
    $half_an_hour_send = 0;
    if ( !empty($transient) ) {

      $last = max(array_keys($transient)) - 1800;
      if ( ( $now - $last ) < 60 )
        return array( 'wait'=>0, 'submit'=>__( '1分钟内只能发1条，请稍后再试。', 'dmeng' ) );

      foreach( $transient as $timestamp=>$code ) {
        if ( $timestamp <= $now ) {
          unset( $transient[$timestamp] );
          continue;
        }

        if ( ( $now - $timestamp ) < 1800 )
          $half_an_hour_send++;

      }
    }
    
    if ( $half_an_hour_send >= 3 ) {
      
      unset( $transient[max(array_keys($transient))] );
      $second_last = max(array_keys($transient)) - 1800;
      
      unset( $transient[max(array_keys($transient))] );
      $third_last = max(array_keys($transient)) - 1800;
      
      return array( 
        'wait'=>absint( max(array_keys($transient)) - $now ),
        'submit'=>sprintf( __( '合理使用短信资源，每个用户每30分钟只能发3条，最近三次发送是%s。', 'dmeng' ), dmeng_human_time_diff($last).' / '.dmeng_human_time_diff($second_last).' / '.dmeng_human_time_diff($third_last) ));
    }
    
    $numbers = range(1, 9);
    shuffle($numbers);
    $new_code = join( '', array_slice($numbers, 0, 6) );

    $send = $this->sms_send( $phone, $new_code );
    if ( empty($send) || empty($send['reason']) )
      return array( 'wait'=>0, 'submit'=>__( '发送请求出错，请重试。', 'dmeng' ));
    
    // $send = array(
      // 'reason' => '测试发送成功'
    // );
    
    if ( empty($send['error_code']) ) {
      
      $transient[$now+1800] = array(
        'code' => $new_code,
        'phone' => $phone
      );

      set_transient($transient_key,  json_encode($transient), 1800);
    }
    
    return  array( 'wait'=> (empty($send['error_code']) ? 60 : 0), 'submit'=>$send['reason'] );
  }

  /**
   * 发送短信请求
   */
  public function sms_send( $phone=0, $code=0 ) {
    return (array)json_decode(wp_remote_retrieve_body(wp_remote_get(add_query_arg(
      array(
        'mobile' => $phone,
        'dtype' => 'json',
        'key' => get_option('open_sms_key'),
        'tpl_id' => '3176',
        'tpl_value' => urlencode( '#code#='.$code )
      ), 'http://v.juhe.cn/sms/send'
    ))), true);
  }
  
  /**
   * 检查手机号是否可用
   */
  public function sms_ajax_phone_check() {

    if ( !isset( $_POST['nonce'] ) || !wp_verify_nonce( $_POST['nonce'], 'dmeng_sms' ) )
      return __( '表单已失效，请刷新页面重试', 'dmeng' );

    if ( empty($_POST['phone']) || false===$this->is_phone_number($_POST['phone']) )
      return __( '请输入正确的手机号码', 'dmeng' );

    $current_user_id = get_current_user_id();
    $user_exists = $this->get_user_id_by_phone($_POST['phone']);
    
    if ( $current_user_id && $user_exists && $user_exists==$current_user_id )
      return __( '您已绑定这个手机号，无需重复绑定', 'dmeng' );
    
    if ( $user_exists )
      return $current_user_id ? __( '这个手机号码已绑定其他帐号', 'dmeng' ) : $user_exists;

    return $current_user_id ? 0 : __( '这个手机号码还没有绑定任何帐号', 'dmeng' );
  }
  
  /**
   * 处理AJAX请求
   */
  public function sms_ajax() {
    
    if (!empty($_POST['phone'])) {
      $dmeng_Security = dmeng_Security();
      $_POST['phone'] = $dmeng_Security->maybe_RSA($_POST['phone']);
    }

    $current_user_id = get_current_user_id();

    $result = array();

    $check = $this->sms_ajax_phone_check();

    if ( is_numeric($check) ) {

      if ( isset($_POST['validate']) && !empty($_POST['code']) ) {

        $phone_user_id = $this->sms_check_code( $current_user_id, $_POST['phone'], absint($_POST['code']) );
        if ( ! $phone_user_id ) {

          $result['submit'] = __( '请输入正确的验证码', 'dmeng' );

        } else {

          if ( !$current_user_id ) {

            $phone_md5 = md5($_POST['phone']);
            $result['submit'] = __( '验证成功，跳转中……', 'dmeng' ) . '<script> top.location.href="' . add_query_arg( array( 'validate'=>wp_create_nonce($phone_md5), 'phone'=>$phone_md5, 'code'=>absint($_POST['code']) ), dmeng_open_url( $this->connect ) ) . '"</script>';

          } else {

            // 更新绑定的手机号
            update_user_meta($phone_user_id, $this->openid_field_name(), $_POST['phone']);
            
            // 清空验证码信息，让验证码失效
            $this->empty_user_code($phone_user_id);

            $result['submit'] = sprintf(__('恭喜，手机号 %s 绑定成功。', 'dmeng'), substr_replace($_POST['phone'], '****', 3, 4));
            $result['wait'] = 1;

          }
        }
      } else {
        $result = $this->sms_new_code( ( $check==0 ? get_current_user_id() : $check ), $_POST['phone'] );
      }

    } else {
      $result['phone'] = $check;
    }

    wp_send_json( $result );
  }

}
$dmeng_Open_connect_sms = new dmeng_Open_connect_sms;
