<?php

function dmeng_adsense($type='single', $local='top'){

	$content = $adsense = '';
	
	if($type=='single')
    $adsense = json_decode(get_option('dmeng_adsense_single','{"top":"","comment":"","bottom":""}'));
	else if($type=='archive')
    $adsense = json_decode(get_option('dmeng_adsense_archive','{"top":"","bottom":""}'));
	else if($type=='author')
    $adsense = json_decode(get_option('dmeng_adsense_author','{"top":"","bottom":""}'));

	if($adsense)
    $content = stripslashes(htmlspecialchars_decode($adsense->$local));
	
	if($content)
    return '<div class="adsense">'.do_shortcode(trim($content)).'</div>';

}