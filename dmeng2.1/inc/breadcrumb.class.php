<?php

class dmeng_Breadcrumb {

  public $path = array();
  public $separator;

  public function item_output( $name, $url='' ) {
    $html = '<span itemprop="title">'.$name.'</span>';
    if ( !empty($url) )
      $html = '<a href="'.esc_url($url).'" title="'.$name.'" itemprop="url">'.$html.'</a>';
    return '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.$html.'</span>';
  }
  
  public function output() {

    $this->path = apply_filters( 'dmeng_breadcrumb_path', $this->path );
    
    if ( $this->path )
      return join( $this->separator ,$this->path );

    $this->path[] = $this->item_output( get_bloginfo('name', 'display'), home_url('/') );
  
    if ( in_the_loop() ) {

      $post_type = get_post_type();
      $post_type_link = get_post_type_archive_link($post_type);
      $post_type_obj = get_post_type_object( $post_type );
      
      if ( $post_type_link )
        $this->path[] = $this->item_output( $post_type_obj->labels->name, $post_type_link );

      $this->join_taxonomies( $post_type );
      
      $this->path[] = $this->item_output( $post_type_obj->labels->singular_name );

    } else {
    
      if ( is_home() ) {
        $this->path[] = $this->item_output( is_home()&&is_front_page()===false ? __( '文章', 'dmeng' ) : __( '首页', 'dmeng' ) );
      } else if ( is_author() ) {
        $this->path[] = $this->item_output( __( '用户', 'dmeng' ) );
      } else if ( is_search() ) {
        $this->path[] = $this->item_output( __( '搜索结果', 'dmeng' ) );
      } else if ( is_category() || is_tag() || is_tax() ) {
        $term = get_queried_object();
        $term_parents = $this->get_term_tree_path( $term, true );
        if ( !empty($term_parents) )
          $this->path[] = join( $this->separator, $term_parents );
        $this->path[] = $this->item_output( get_taxonomy($term->taxonomy)->labels->singular_name );
      } else if ( is_post_type_archive() ) {
        $this->path[] = $this->item_output( get_queried_object()->labels->name, get_post_type_archive_link(get_post_type()) );
      }

    }
    
    global $paged, $page;
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
      $this->path[] = sprintf( __( '第%s页', 'dmeng' ), max( $paged, $page ) );

    return join( $this->separator ,$this->path );
  }

  public function get_term_tree( $term, $visited = array() ) {
    $visited[$term->term_id] = $term;
    if ( $term->parent && !isset($visited[$term->parent]) ) {
      $parent = get_term( $term->parent, $term->taxonomy );
      if ( ! is_wp_error( $parent ) )
        $visited = $this->get_term_tree( $parent, $visited );
    }
    return $visited;
  }

  public function get_term_tree_path( $term, $only_parents=false ) {
    $tree = array_reverse($this->get_term_tree($term), true);
    $tree_path = array();
    foreach( $tree as $tree_term ) {
      if ( $only_parents===false || $tree_term->term_id!=$term->term_id )
        $tree_path[] = $this->item_output( $tree_term->name, get_term_link( $tree_term->term_id, $tree_term->taxonomy ) );
    }
    return $tree_path;
  }
  
  public function join_taxonomies( $post_type ) {

    $tax = array();

    $taxonomies = get_object_taxonomies( $post_type, 'objects' );

    foreach( $taxonomies as $name=>$taxonomy ) {
      if ( $taxonomy->hierarchical )
        $tax[] = $name;
    }
    
    if ( empty($tax) )
      return;
      
    $tax_path = array();

    foreach( $tax as $taxonomy ) {
    
      $terms_path = array();
      
      $terms = get_the_terms( get_the_ID(), $taxonomy );
      if ( empty($terms) )
        continue;

      foreach( $terms as $term ) {
        if ( get_term_children( $term->term_id, $term->taxonomy ) )
          continue;
        $terms_path[] = join( '/', $this->get_term_tree_path($term) );
      }
      
      $tax_path[] = join( ',', $terms_path );
    }
    
    $this->path[] =  join( ' ; ', $tax_path );
  }

}

$dmeng_Breadcrumb = new dmeng_Breadcrumb;
