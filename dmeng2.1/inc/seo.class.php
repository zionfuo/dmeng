<?php

/**
 * SEO
 */

class dmeng_SEO {
  
  public function __construct() {

    add_filter( 'user_trailingslashit', array( $this, 'nice_trailingslashit' ), 10, 2);

    add_filter( 'wp_title', array( $this, 'page_title' ), 10, 2 );

    if ( (int)get_option('dmeng_seo_meta', 1)===1 ) {
      add_action( 'wp_head', array( $this, 'auto_description' ) );
    }
    
    add_action( 'wp', array( $this, 'auto_robots' ) );
    add_filter( 'dmeng_no_robots', array( $this, 'no_robots' ) );
  }

  /**
   * 归档页后加斜杆 
   */
  public function nice_trailingslashit($string, $type_of_url) {
    if ( $type_of_url != 'single' )
      $string = trailingslashit($string);
    return $string;
  }
  
  /**
   * 网页标题
   */
  public function page_title( $title, $sep ) {

    if ( is_feed() )
      return $title;

    if ( is_dmeng_ucenter() )
      return __( '用户中心', 'dmeng' ) . " $sep " . get_bloginfo( 'name', 'display' );

    global $paged, $page;

    if ( is_singular('post') ) {

      $cat = array();
      foreach( (get_the_category() ) as $category ) {
        $cat[] = esc_html($category->cat_name); 
      }

      $cat = empty($cat) ? '' : join(',',$cat);
      
      $title = esc_html(get_the_title())." $sep ".$cat." $sep ";

    } else if ( is_search() ) {

      $title = __('搜索结果', 'dmeng')." $sep ";

    } else if ( 'post'===get_query_var('post_type') ) {
      
      $title = __('最新文章', 'dmeng')." $sep "; 

    }

    $title .= get_bloginfo( 'name', 'display' );

    if (( is_home() || is_front_page() ) && 'post'!==get_query_var('post_type') ) {
      $site_description = get_bloginfo( 'description', 'display' );
      if ($site_description)
        $title .= " $sep $site_description";
    }

    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
      $title .= " $sep " . sprintf( __( '第%s页', 'dmeng' ), max( $paged, $page ) );

    return $title;
  }
  
  /**
   * 网页关键词和描述
   */
  public function auto_description() {
  
    $keywords = array();
    $description = '';
    
    if ( is_home() || is_front_page() ) {
      $home_seo = wp_unslash(json_decode(get_option('dmeng_home_seo','{"keywords":"","description":""}')));
      $keywords[] = $home_seo->keywords;
      $description = $home_seo->description;
    }
    
    if ( is_single() ) {

      if ( has_tag() ) {
        foreach( (get_the_tags()) as $tag ) {
           $keywords[] = $tag->name; 
        }
      }
      
      foreach( (get_the_category() ) as $category ) { 
        $keywords[] = $category->cat_name; 
      }
      
      $description = get_the_excerpt();
      
    }
    
    if ( is_category() || is_tag() || is_tax() ) {
      $keywords[] = single_term_title("", false);
      $description = term_description();
    }
    
    if ( is_search() ) {
      $description = __('搜索','dmeng');
      $keywords[] = $description;
    }

    $keywords = esc_attr(join(',', array_map('wp_strip_all_tags', $keywords)));
    $description = esc_attr(wp_strip_all_tags($description));
    
    if ($keywords)
      echo '<meta name="keywords" content="'.$keywords.'" />';
  
    if ($description)
      echo '<meta name="description" content="'.$description.'" />';

  }

  /**
   * 在 wp_head 中输出 noindex,follow
   */
  public function auto_robots() {
    if (apply_filters('dmeng_no_robots', false) && has_action('wp_head', 'wp_no_robots')===false) {
      add_action('wp_head', 'wp_no_robots');
    }
  }
  
  /**
   * 在 wp_head 中输出 noindex,follow 的前提条件
   */
  public function no_robots($no_robots) {
    
    /**
     * 拒绝收录的条件
     */
    if (
        // 作者页选项卡
        (is_author() && !empty($_GET['tab'])) ||
        // 评论分页
        (is_singular() && absint(get_query_var('cpage'))>1)
    ) {
      $no_robots = true;
    }
    
    if (is_home() && is_front_page() && 'post'===get_query_var('post_type'))
      $no_robots = true;
    
    return $no_robots;
  }

}

$dmeng_SEO = new dmeng_SEO;
