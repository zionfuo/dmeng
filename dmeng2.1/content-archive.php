<?php

$title = esc_html(get_the_title());
$excerpt = apply_filters( 'the_excerpt', get_the_excerpt() );

if (is_search()) {
  $keyword = get_search_query();
  $title = dmeng_highlight_keyword($keyword, $title);
  $excerpt = dmeng_highlight_keyword($keyword, $excerpt);
}
?>
<article <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/Article">
  <?php echo dmeng_thumbnail(); ?>
  <header class="entry-header">
    <h3 class="entry-title" itemprop="name"><?php 
      
      if ( is_sticky() )
        echo '<span class="sticky-tag">'.__( '置顶', 'dmeng' ).'</span>';
        
      echo apply_filters( 'dmeng_the_title', '<a href="'.get_permalink().'" rel="bookmark" itemprop="url"><span itemprop="name">'.$title.'</span></a>' );
      
    ?></h3>
    <?php dmeng_post_meta(); ?>
  </header>
  <div class="entry-excerpt"  itemprop="articleBody"><?php echo $excerpt;?></div>
</article>
