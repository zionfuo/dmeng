<?php
get_header();
get_header( 'masthead' ); ?>
<div id="main" class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
  <div class="row">
    <article id="content" class="col-lg-8 col-md-8 archive author-archive" role="article" itemscope="" itemtype="http://schema.org/Article">
      <header class="entry-header">
        <div class="entry-location"><?php echo dmeng_breadcrumb_html();?></div>
        <?php
          global  $wp_query;
          $curauth = $wp_query->get_queried_object();
        ?>
        <div class="entry-author clearfix">
          <div class="author-avatar">
            <?php echo get_avatar( $curauth->ID, 80 );?>
          </div>
          <div class="author-info">
            <h1 class="vcard"><?php echo filter_var($curauth->user_url, FILTER_VALIDATE_URL) ? '<a href="'.$curauth->user_url.'" target="_blank" rel="external">'.$curauth->display_name.'</a>' : $curauth->display_name;?></h1>
            <p class="author-description"><?php 
						echo $curauth->description ? $curauth->description : __('没有个人说明','dmeng'); ?></p>
            <p class="small author-register-time"><?php

  if ($curauth->dmeng_latest_login) {
    printf(
      __( '在%1$s注册，最后登录于%2$s', 'dmeng' ),
      date( __('Y年m月d号','dmeng'), strtotime( $curauth->user_registered ) ),
      date( __('Y年m月d号','dmeng'), strtotime( $curauth->dmeng_latest_login ) )
    );
  } else {
    printf(
      __( '在%s注册', 'dmeng' ),
      date( __('Y年m月d号','dmeng'), strtotime( $curauth->user_registered ) )
    );
  }
				 ?></p>
          </div>
        </div>
<?php

global $dmeng_Author_Template;
$tabs = $dmeng_Author_Template->tabs();
$tab = $dmeng_Author_Template->get_tab();

?>
        <div class="entry-tab">
          <ul>
<?php
  foreach( $tabs as $key=>$value ) {
    echo '<li'.($tab==$key ? ' class="active"' : '').'><a href="'.add_query_arg( array('tab'=>($key=='post' ? null : $key)), get_author_posts_url($curauth->ID) ).'">'.$value.'</a></li>';
  }
?>
          </ul>
        </div>
        
        <div class="entry-meta"><?php echo apply_filters( 'dmeng_author_main_notice', dmeng_loading_tips() ); ?></div>
 
      </header>
      <div class="entry-content" itemprop="articleBody">
        <?php do_action( 'dmeng_author_main_content' ); ?>
      </div>
      <footer class="entry-footer">
        <?php do_action( 'dmeng_author_main_footer' ); ?>
      </footer>
    </article>
    <?php get_sidebar(); ?>
  </div>
 </div><!-- #main -->
<?php get_footer('colophon'); ?>
<?php get_footer(); ?>
