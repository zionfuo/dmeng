<?php
/**
 * 加密文章返回空
 */
if ( post_password_required() ) {
  return;
}
?>
<div id="respond">
  <?php
  if ( have_comments() ) {

    global $post;
    
    $s_comments = get_comments(array(
        'status' => 'approve',
        'post_id'=> $post->ID,
        'meta_key' => 'dmeng_sticky_comment',
        'orderby' => 'meta_value_num',
        'order' => 'ASC',
      ));
    
    add_filter( 'dmeng_is_stick_comments', '__return_true' );
    
    $s_comments_list = wp_list_comments( "type=comment&callback=dmeng_comment&depth=1&echo=0&per_page=0", $s_comments );
    if ($s_comments_list || ( get_current_user_id()==get_the_author_meta('ID') || current_user_can('moderate_comments') ) ) {
      
    ?>
    <ul id="sticky-comments">
      <li class="h4 sticky-title<?php if(!$s_comments_list) echo ' hide';?>"><?php echo get_option('dmeng_sticky_comment_title', __('置顶评论','dmeng')); ?></li>
      <li>
        <ul class="commentlist"><?php echo $s_comments_list;?></ul>
      </li>
    </ul>
    <?php }
  } 
  
?><h4 class="respond-title"><?php 
  
   if ( comments_open() ) {
     
    comment_form_title( __('Leave a Reply'), __('Leave a Reply to %s' ) ); 
    
    $cancel_comment_reply_link = get_cancel_comment_reply_link();
    if ($cancel_comment_reply_link)
      echo '<small id="cancel-comment-reply">'.$cancel_comment_reply_link.'</small>';
    
  } else {
    _e( '评论已关闭。', 'dmeng' );
  }
  
  ?></h4>
  
  <?php if ( comments_open() ) { ?>

  <?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
  <p class="commenttips"><?php printf(__('要发表评论，您必须先<a href="%s" class="login_url">登录</a>。'), wp_login_url( get_permalink() )); ?></p>
  <?php else : ?>

  <form action="<?php echo site_url(); ?>/wp-comments-post.php" method="post" id="commentform">

  <?php if ( is_user_logged_in() ) : ?>

  <p><?php printf(__('Logged in as <a href="%1$s">%2$s</a>.'), get_edit_user_link(), $user_identity); ?> <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php esc_attr_e('Log out of this account'); ?>"><?php _e('Log out &raquo;'); ?></a></p>

  <?php else : ?>

  <div class="row">
    <div class="col-md-4">
      <label for="author"><small><?php _e( '姓名', 'dmeng' ); ?> <?php if ($req) _e( '(必填)', 'dmeng' ); ?></small></label>
      <input type="text" name="author" class="form-control input-sm" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" <?php if ($req) echo 'aria-required="true" required'; ?> />
    </div>

    <div class="col-md-4">
      <label for="email"><small><?php _e( '邮箱（不会被公开）', 'dmeng' ); ?> <?php if ($req) _e( '(必填)', 'dmeng' ); ?></small></label>
      <input type="text" name="email" class="form-control input-sm" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" <?php if ($req) echo 'aria-required="true" required'; ?> />
    </div>

    <div class="col-md-4">
      <label for="url"><small><?php _e( '站点', 'dmeng' ); ?></small></label>
      <input type="text" name="url" class="form-control input-sm" id="url" value="<?php echo  esc_attr($comment_author_url); ?>" size="22" tabindex="3" />
    </div>
  </div>

  <?php endif; ?>

  <div id="commentaction">
    <textarea name="comment" class="form-control" id="comment" cols="58" rows="3" tabindex="4" required></textarea>
    <div class="btn-toolbar clearfix" role="toolbar">
      <div class="btn-group" role="group">
        <span  class="btn btn-default" id="looks-image"><?php _e( '表情', 'dmeng' );?></span>
        <span class="btn btn-default" id="privacy-action" title="<?php _e( '隐私内容标签', 'dmeng' );?>" data-endtag-text="[/pem]">[pem]</span>
      </div>
      <div class="btn-group pull-right" role="group">
        <input name="submit" type="submit"  class="btn btn-default" id="commentsubmit" tabindex="5" data-loading-text="<?php _e( '提交中...', 'dmeng' );?>" value="<?php _e( '发表评论', 'dmeng' );?>" />
      </div>
    </div>
    <input type="hidden" name="dmeng_ajax_comment" value="1" />
    <?php comment_id_fields(); ?>
  </div>
  
  <div id="comment-error-alert" class="alert alert-warning" style="display:none;" role="alert"></div>
  <?php  do_action( 'comment_form', $post->ID );  ?>
  </form>

  <?php endif; ?>

  <?php } ?>

</div>

<?php
if ( have_comments() ) {
  
  add_filter( 'dmeng_is_stick_comments', '__return_false' );
  
      /**
       * 评论列表
       * https://codex.wordpress.org/Function_Reference/wp_list_comments
       */
      $comments = wp_list_comments( array(
        'callback' => 'dmeng_comment',
        'style' => 'ul',
        'echo' => 0
      ) );
?>
  <ul>
    <li class="help-block commentnumber"><?php
      printf( __( '评论%1$s则，共%2$s页，当前第%3$s页', 'dmeng' ), '<i class="num">'.number_format_i18n( get_comments_number() ).'</i>', '<i class="num">'.get_comment_pages_count().'</i>', '<i class="num">'.max( get_query_var('cpage'), 1 ).'</i>' );
    ?></li>
    <li>
      <ul id="thread-comments" class="commentlist"><?php echo $comments; ?></ul>
    </li>
    <li class="text-center">
      <?php echo dmeng_paginate_comments();?>
    </li>
  </ul>
<?php
}

