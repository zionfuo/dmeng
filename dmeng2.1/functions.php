<?php

/**
 * 多梦主题2.1说明
 * 
 * 作者
 * 
 * 多梦 steven.chan.chihyu
 * http://duomeng.me/ 
 * 
 * 1500213442@qq.com
 * chihyu@aliyun.com
 * 
 * 官网
 * http://www.dmeng.net/
 * 
 * 支付宝捐赠账户
 * 
 * 陈志宇
 * chihyu@aliyun.com
 * 
********** 注释魅力不可挡 **********
 * 
 * 代码说明
 * 
 * 变量、方法、字段等统一英文小写前缀 : dmeng
 * 
 * 注释中的网址为代码相关方法对应的官方文档链接，如：
 * https://codex.wordpress.org/Main_Page
 * 
 * 没有注释的一般直接 Google 一下以下函数名称或者是动作钩子名称都能查到文档
 * 访问不了 Google 就百度一下 Google IP 然后使用 IP 访问就可以
 * 
 * 全局性功能应该封装成函数，因为变量是有前后顺序的，函数则是全局的
 * 
 * 以下是一些比较常用的方法文档地址
 * 
 * 模板文件路径 : get_template_directory()
 * https://codex.wordpress.org/Function_Reference/get_template_directory
 * 
 * 模板访问路径 : get_template_directory_uri()
 * https://codex.wordpress.org/Function_Reference/get_template_directory_uri
 * 
 * 移动端判断 : wp_is_mobile()
 * https://codex.wordpress.org/Function_Reference/wp_is_mobile
 * 
 * 自定义文章查询
 * https://codex.wordpress.org/Function_Reference/get_posts
 * https://codex.wordpress.org/Function_Reference/query_posts
 * https://codex.wordpress.org/Class_Reference/WP_Query
 * 
 * 自定义分类查询
 * https://codex.wordpress.org/Function_Reference/get_categories
 * https://codex.wordpress.org/Function_Reference/get_tags
 * https://codex.wordpress.org/Taxonomies
 * 
 * 自定义评论查询
 * https://codex.wordpress.org/Class_Reference/WP_Comment_Query
 * 
 * 自定义用户查询
 * https://codex.wordpress.org/Class_Reference/WP_User_Query
 * 
 * 自定义数据库查询
 * https://codex.wordpress.org/Class_Reference/wpdb
 * 
 * 动作和钩子
 * https://codex.wordpress.org/Plugin_API
 */
 
/**
 * 可能用到的 WordPress 内置有的简单函数
 * 
 * 返回当前时间
 * current_time()
 * 
 * 国际化时间
 * date_i18n()
 * 
 * 国际化数字
 * number_format_i18n()
 * 
 * 尺寸格式
 * size_format()
 * 
 * 返回真
 * __return_true()
 * 
 * 返回假
 * __return_false()
 * 
 * 返回数字0
 * __return_zero()
 * 
 * 返回空的数组
 * __return_empty_array()
 * 
 * 返回null
 * __return_null()
 * 
 * 返回空字符串
 * __return_empty_string()
 * 
 * 获取某个HTML标签的正则表达式
 * get_tag_regex( $tag ) 
 * 
 */

define( 'DMENG_SCRIPT_DEV', apply_filters( 'dmeng_enqueue_script_dev', false ) );

/**
 * CSS/JS 代码版本号，和对应文件名中的版本号同步
 */
define( 'DMENG_CSS_VER', 150917 );
define( 'DMENG_SCRIPT_VER', 150917 );

/**
 * 当前主题代码版本
 * !注意! 不能改，修改会影响主题功能
 */
 
define( 'DMENG_VER', '2.1.d11' );


/**
 * 载入主题目录下 /inc 目录的所有 PHP 文件 
 */

foreach ( glob( get_template_directory() . '/inc/*.php' ) as $filename ) {
  require $filename;
}

/**
 * 和 wp_is_mobile() 不同的是排除ipad
 */
if (false===function_exists('dmeng_is_mobile')) {
  function dmeng_is_mobile() {
    static $is_mobile;
    
    if ( isset($is_mobile) )
      return $is_mobile;

    if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
      $is_mobile = false;
    } elseif (
      strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
      || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
      || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
      || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
      || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false ) {
        $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') == false) {
      $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
      $is_mobile = false;
    } else {
      $is_mobile = false;
    }

    return $is_mobile;
  }
}

/**
 * 初始化
 */ 
$dmeng = new dmeng_Setup;

if (is_admin()) {
  require get_template_directory() . '/inc/admin/options.php';
}

function dmeng_setting($name='') {
  static $setting;
  if ( false===isset($setting) ) {
    $setting = (array)wp_unslash(json_decode(get_option('dmeng_general_setting'), true));
  }
  $filed = empty($name) ? $setting : ( isset($setting[$name]) ? $setting[$name] : '' );
  return $filed;
}

function dmeng_human_time_diff( $from, $to = '' ) {
  
  if ( empty( $to ) ) {
    $to = current_time('timestamp');
  }
  
  $diff = (int) abs( $to - $from );
  
  if ( $diff < DAY_IN_SECONDS ) {
    $since = human_time_diff( $from, $to ) . __( '前', 'dmeng' );
  } else {
    $since = date( ( date('Y', $from) < date('Y', $to) ? 'Y-' : '' ) . 'm-d', $from );
  }
  
  return $since;
  
}

function dmeng_trim_username($name) {
  return trim(trim($name), '　') ? $name : __( '匿名', 'dmeng' );
}

function dmeng_get_current_page_url() {

  static $current_url;
  
  if ( isset($current_url) )
    return $current_url;
  
  $ssl = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? true : false;
  $sp = strtolower($_SERVER['SERVER_PROTOCOL']);
  $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
  $port  = $_SERVER['SERVER_PORT'];
  $port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
  $host = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];
  $current_url = $protocol . '://' . $host . $port . $_SERVER['REQUEST_URI'];

  return $current_url;
}

function dmeng_get_redirect_url( $default=null ) {
  return !empty($_GET['redirect_to']) ? esc_url($_GET['redirect_to']) : $default;
}

function dmeng_create_db_table( $table_name, $sql ) {
  global $pagenow;   
  if ( is_admin() && 'themes.php' == $pagenow && isset( $_GET['activated'] ) ) {
    global $wpdb;
    $table_name = $wpdb->prefix . $table_name;
    if ( $wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name ) {
      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');   
      dbDelta($sql);
    }
  }
}

/**
 * 没有开启SSL而且openssl函数可用的情况下，使用RSA公钥加密算法保护密码和手机号等私密信息的传输安全
 */
function dmeng_maybe_RSA() {
  static $maybe_RSA;
  if (false===isset($maybe_RSA)) {
    $maybe_RSA = false===is_ssl() && function_exists('openssl_private_decrypt');
  }
  return $maybe_RSA;
}

function get_dmeng_tracker_vars( $hash=false ) {
  global $dmeng_Tracker;
  return $dmeng_Tracker->vars($hash);
}

function get_dmeng_traffic( $type='' , $pid='' ) {
  global $dmeng_Tracker;

  $vars = $dmeng_Tracker->vars();
  if ($type=='')
    $type = $vars['type'];

  if ($pid=='')
    $pid = $vars['pid'];

  return $dmeng_Tracker->get_var( $type , $pid );
}

function get_dmeng_traffic_all( $type='' ) {
  global $dmeng_Tracker;
  return $dmeng_Tracker->get($type);
}

function get_dmeng_traffic_rank( $type='single', $limit=10, $exclude=array() ) {
  global $dmeng_Tracker;
  return $dmeng_Tracker->rank( $type, $limit, $exclude );
}

function dmeng_print_scripts() {
  global $pagenow;
  if ( is_admin() || 'wp-login.php'==$pagenow )
     return;
  echo '<script type="text/javascript">var dmeng = ' . json_encode( (array) apply_filters( 'dmeng_scripts_args', array(
    'current_page_url' => dmeng_get_current_page_url(),
    'is_mobile' => dmeng_is_mobile(),
    'ajaxurl' => admin_url( '/admin-ajax.php' ),
    'post_id' =>  ( is_singular() ? get_queried_object_id() : 0 ),
    'cpage' =>  get_query_var('cpage')
  )) ) . '</script>';
}
add_action( 'wp_print_scripts', 'dmeng_print_scripts' );

/**
 * meta
 */

function get_dmeng_meta( $key , $uid=0, $limit=0, $offset=0 ) {
  global $dmeng_Meta;
  return $dmeng_Meta->get_var( $key , $uid, $limit, $offset );
}

function add_dmeng_meta( $key, $value='', $uid=0 ) {
  global $dmeng_Meta;
  return $dmeng_Meta->add( $key, $value, $uid );
}

function update_dmeng_meta( $key, $value='', $uid=0 ) {
  global $dmeng_Meta;
  return $dmeng_Meta->update( $key, $value, $uid );
}

function get_dmeng_meta_count( $key, $value=0, $uid='all' ) {
  global $dmeng_Meta;
  return $dmeng_Meta->count( $key, $value, $uid );
}

/**
 * message
 */

function add_dmeng_message( $uid=0, $type='', $date='', $title='', $content='' ) {
  global $dmeng_Message;
  return $dmeng_Message->add( $uid, $type, $date, $title, $content );
}

function delete_dmeng_message( $id, $uid=0, $type='', $title='' ) {
  global $dmeng_Message;
  return $dmeng_Message->delete( $id, $uid, $type, $title );
}

function update_dmeng_message_type( $id=0, $uid=0, $type='' ) {
  global $dmeng_Message;
  return $dmeng_Message->update_type( $id, $uid, $type );
}

function get_dmeng_message( $uid=0 , $count=0, $where='', $limit=0, $offset=0 ) {
  global $dmeng_Message;
  return $dmeng_Message->get( $uid, $count, $where, $limit, $offset );
}

function get_dmeng_credit_message( $uid=0 , $limit=0, $offset=0 ) {
  global $dmeng_Message;
  return $dmeng_Message->get_credit_message( $uid, $limit, $offset );
}

function get_dmeng_pm( $pm=0, $from=0, $count=false, $single=false, $limit=0, $offset=0 ) {
  global $dmeng_Message;
  return $dmeng_Message->get_pm( $pm, $from, $count, $single, $limit, $offset );
}

/**
 * open
 */

function dmeng_open_url( $connect='', $action='login', $redirect_to='' ) {
  $connect = sanitize_key( ( empty($connect) && !empty($_GET['connect']) ) ? $_GET['connect'] : $connect );
  $action = $action=='login' ? $action : 'logout';
  $redirect_to = empty($redirect_to) ? dmeng_get_redirect_url() : $redirect_to;
  return add_query_arg( array( 
    'connect' => $connect, 
    'action' => $action, 
    'nonce' => wp_create_nonce( $connect.$action ), 
    'redirect_to' => ( $redirect_to ? urlencode( $redirect_to ) : null )
  ), wp_login_url()  );
}

/**
 * mail
 */
function dmeng_mail( $email, $title, $content ) {
  global $dmeng_Email;
  return $dmeng_Email->send_mail( $email, $title, $content );
}

function dmeng_verify_mail( $email='', $user_id=0, $data=array(), $sinlge=true ) {
  global $dmeng_Email;
  $result = $dmeng_Email->verify_send( $email, ( $user_id ? $user_id : wp_generate_password( 8, false ) ), $data );
  return $sinlge ? join( ' ', $result ) : $result;
}

/**
 * credit
 */ 
function update_dmeng_credit( $user_id, $num, $method='plus', $field='dmeng_credit', $msg='' ) {
  global $dmeng_Credit;
  return $dmeng_Credit->update( $user_id, $num, $method, $field, $msg ); 
}

function dmeng_credit_to_void( $user_id , $num, $msg='' ) {
  global $dmeng_Credit;
  return $dmeng_Credit->to_void( $user_id , $num, $msg );
}

function dmeng_paginate( $wp_query='' ){
  
  if ( empty($wp_query) )
    global $wp_query;
    
  $pages = $wp_query->max_num_pages;
  if ( $pages < 2 )
    return;
  
  $big = 999999999;
  $paginate = paginate_links( array(
    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total' => $pages,
    'type' => 'array'
  ) );
  echo '<div id="pagination"><ul class="pagination" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">';
  foreach ($paginate as $value) {
    echo '<li itemprop="name">'.$value.'</li>';
  }
  echo '</ul></div>';
}

function dmeng_post_page_nav( $echo=true ) {

  return wp_link_pages( array(
    'before'      => '<nav class="pager" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement"><span>'.__('分页','dmeng').'</span>',
    'after'       => '</nav>',
    'link_before' => '<span itemprop="name">',
    'link_after'  => '</span>',
    'pagelink' => __('%','dmeng'),
    'echo' => $echo
  ) );

}

function dmeng_auto_comment_author_name( $comment ) {
  if ( !$comment->user_id )
    return $comment->comment_author;
  return get_the_author_meta( 'display_name', $comment->user_id );
}

function dmeng_paginate_comments() {

  global $wp_rewrite;

  if ( !is_singular() || !get_option('page_comments') )
    return;

  $page = max( get_query_var('cpage'), 1 );
  $max_page = get_comment_pages_count();
  $defaults = array(
    'base' => add_query_arg( 'cpage', '%#%', get_permalink() ),
    'format' => '',
    'total' => $max_page,
    'current' => $page,
    'echo' => false,
    'add_fragment' => '#comments',
    'mid_size' => 4,
    'prev_next' => false
  );
  
  $page_links = paginate_links( $defaults );

  if ( $max_page >= 2 )
    return '<div id="pagination-comments" role="navigation">'.$page_links.'</div>';
  
}

function dmeng_post_meta() {

  $output = '<div class="entry-meta">';

  //~ 字体设置按钮
  if (( is_single() || is_page() ) && is_main_query())
    $output .= apply_filters('dmeng_post_meta_set_font', '<div class="entry-set-font"><span id="set-font-small" class="disabled">A<sup>-</sup></span><span id="set-font-big">A<sup>+</sup></span></div>');
  
  $output .= apply_filters('dmeng_post_meta_author', '<a href="'.esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ).'" itemprop="author">'.get_the_author().'</a>');

  $time = get_the_time('U');
  $output .= apply_filters('dmeng_post_meta_date', '<time class="entry-date num" title="'.sprintf( __('发布于 %1$s 最后编辑于 %2$s ', 'dmeng'), date('Y-m-d H:i:s', $time), get_the_modified_time('Y-m-d H:i:s') ).'" datetime="'.get_the_date( 'c' ).'"  itemprop="datePublished">'.date( ( date('Y', $time)===current_time('Y') ? '' : 'Y-').'m-d H:i:s', $time).'</time>');
  
  $comments_number = get_comments_number();
  if ( $comments_number )
    $output .= apply_filters('dmeng_post_meta_comments_number', '<a href="' . get_comments_link() .'" itemprop="discussionUrl" itemscope itemtype="http://schema.org/Comment">'.sprintf( __( '%s条评论', 'dmeng' ), '<i '.( is_singular() ? 'data-num-comments="true" ' : '' ).'class="num">'.$comments_number.'</i>' ).'</a>');
  
  $traffic = number_format_i18n(get_dmeng_traffic('single', get_the_ID()));

  $output .= apply_filters('dmeng_post_meta_traffic', '<span>' . sprintf( __( '%s次浏览', 'dmeng' ) , '<i '.( is_singular() ? 'data-num-views="true" ' : '' ).'class="num">'.$traffic.'</i>') . '</span>' );

  if( get_post_type()=='post' ) {
    if( apply_filters('dmeng_post_meta_tag_show', true) ){
      $tags = get_the_tags();
      if ($tags) {
        $post_tags = array();
        foreach ( $tags as $tag ){
          if ( $tag->term_id !== get_queried_object_id() || !is_tag() )
            $post_tags[] = '<span itemprop="keywords"><a href="'.get_tag_link($tag->term_id).'" rel="tag">'.$tag->name.'</a></span>';
        }
        $output .= apply_filters( 'dmeng_post_meta_tag', join('',$post_tags) );
      }
    }

    if ( ( is_archive() || is_search() ) && apply_filters('dmeng_post_meta_cat_show', true) ) {
      $categories = get_the_category();
      if ($categories) {
        $cats = array();
        foreach ( $categories as $category ) {
          if ( !is_category() || $category->term_id !== get_queried_object_id() )
            $cats[] = '<a href="'.get_category_link( $category->term_id ).'" rel="category" itemprop="articleSection">'.$category->name.'</a>';
        }
        $output .= apply_filters('dmeng_post_meta_cat', join('',$cats) );
      }
    }
  }

  $output .= '</div>';
  
  echo $output;

}

function dmeng_post_copyright($post_id) {
  
  $post_id = (int)$post_id;
  
  if (!$post_id)
    return;

  if ( (int)get_option('dmeng_copyright_status_all',1)===1 && (int)get_post_meta( $post_id, 'dmeng_copyright_status', true )!==9 ) {
    $cc = get_post_meta( $post_id, 'dmeng_copyright_content', true );
    $cc = empty($cc) ? get_option('dmeng_copyright_content_default') : $cc;
    $cc = stripcslashes(htmlspecialchars_decode($cc));
    if($cc){
      
    ?><div class="entry-details" itemprop="copyrightHolder" itemtype="http://schema.org/Organization" itemscope>
      <details>
        <summary><?php 
          echo str_replace(array( '{name}', '{url}', '{title}', '{link}'), array(get_bloginfo('name'), home_url('/'), get_the_title($post_id), get_permalink($post_id)), $cc);
        ?></summary>
      </details>
  </div><?php
    }
  }
}

function dmeng_get_the_thumbnail( $post_id=0, $size='post-thumbnail', $match=true ) {
  $post_id = $post_id ? $post_id : get_the_ID();
  $post_thumbnail = (array)wp_unslash(json_decode(get_option('dmeng_post_thumbnail','{"on":"1","suffix":"?imageView2/1/w/220/h/146/q/100"}'), true));
  $post_thumbnail_on = intval($post_thumbnail['on']);
  $post_thumbnail_suffix = $post_thumbnail['suffix'];
  if(!in_array($post_thumbnail_on,array(1,2))) return;
  $image_url = '';
  if ( has_post_thumbnail() ) {
    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post_id) , $size);
    $image_url = !empty($image_url[0]) ? $image_url[0] : '';
  } else if ( $match ) {
    if ($post_thumbnail_on==2) {
      $post = get_post($post_id);
      $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
      if($output) $image_url = $matches[1][0];
    }
  }
  if ($image_url) {
    $image_url = $post_thumbnail_suffix ? $image_url.$post_thumbnail_suffix : $image_url;
    return apply_filters('dmeng_post_thumbnail', $image_url);
  }
}

function dmeng_thumbnail($default='', $link=true, $match=true) {
  $thumbnail = dmeng_get_the_thumbnail(0, 'post-thumbnail', $match);
  $thumbnail = $thumbnail ? $thumbnail : $default;
  if (empty($thumbnail))
    return;
  $img = '<img src="'.dmeng_grey_png().'" data-original="'.$thumbnail.'" alt="'.esc_attr(get_the_title()).'">';
  if ($link)
    $img = '<a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'">'.$img.'</a>';
  return '<div class="entry-thumbnail">'.$img.'</div>';
}

function dmeng_blank_thumbnail() {
  return apply_filters( 'dmeng_blank_thumbnail', get_template_directory_uri().'/images/blank-220x146.png' );
}

function dmeng_breadcrumb_html( $separator=' › ' ) {
  global $dmeng_Breadcrumb;
  $dmeng_Breadcrumb->separator = $separator;
  return $dmeng_Breadcrumb->output();
}

function dmeng_highlight_keyword($key, $content) {
  $keys = array_filter(explode(' ', addcslashes(trim($key),'\/')));
  if ($keys) {
    $content = preg_replace('/(' . join('|', $keys) .')/iu', '<em class="key">\0</em>', $content);
  }
  return $content;
}

add_filter( 'pre_comment_content', 'esc_html' );

function dmeng_grey_png() {
  return apply_filters( 'dmeng_grey_png', get_template_directory_uri().'/images/grey.png' );
}

function dmeng_get_look(){
  $text = array( '[呵呵]', '[嘻嘻]', '[哈哈]', '[可爱]', '[可怜]', '[挖鼻屎]', '[吃惊]', '[害羞]', '[挤眼]', '[闭嘴]', '[鄙视]', '[爱你]', '[泪]', '[偷笑]', '[亲亲]', '[生病]', '[太开心]', '[懒得理你]', '[右哼哼]', '[左哼哼]', '[嘘]', '[衰]', '[委屈]', '[吐]', '[哈欠]', '[抱抱]', '[怒]', '[疑问]', '[馋嘴]', '[拜拜]', '[思考]', '[汗]', '[困]', '[睡]', '[钱]', '[失望]', '[酷]', '[色]', '[哼]', '[鼓掌]', '[晕]', '[悲伤]', '[抓狂]', '[黑线]' );
  $file = array( 'hehe.gif', 'xixi.gif', 'haha.gif', 'keai.gif', 'kelian.gif', 'wabishi.gif', 'chijing.gif', 'haixiu.gif', 'jiyan.gif', 'bizui.gif', 'bishi.gif', 'aini.gif', 'lei.gif', 'touxiao.gif', 'qinqin.gif', 'shengbing.gif', 'taikaixin.gif', 'landelini.gif', 'youhengheng.gif', 'zuohengheng.gif', 'xu.gif', 'shuai.gif', 'weiqu.gif', 'tu.gif', 'haqian.gif', 'baobao.gif', 'nu.gif', 'yiwen.gif', 'chanzui.gif', 'baibai.gif', 'sikao.gif', 'han.gif', 'kun.gif', 'shui.gif', 'qian.gif', 'shiwang.gif', 'ku.gif', 'se.gif', 'heng.gif', 'guzhang.gif', 'yun.gif', 'beishang.gif', 'zhuakuang.gif', 'heixian.gif' );
  return array( 'text'=>$text, 'file'=>$file);
}

function dmeng_look_uri(){
  return apply_filters('dmeng_look_uri', get_template_directory_uri().'/images/look/');
}

function dmeng_look_replace($content) {

  $look = dmeng_get_look();
  
  $format = is_admin() ? '<img class="look" src="%2$s" width="22" height="22" />' : '<img class="look" src="%1$s" data-original="%2$s" width="22" height="22" />';

  foreach( $look['file'] as $file ){
    $html[] = sprintf($format, dmeng_grey_png(), dmeng_look_uri().$file);
  }

  $content = str_replace($look['text'], $html, $content);
  return $content;
}
add_filter('get_comment_text', 'dmeng_look_replace');

function dmeng_look_output(  $scripts_array ) {
  
  if ( is_singular() && comments_open( get_queried_object_id() ) ) {
    $look = array();
    $look['uri'] = dmeng_look_uri();
    $files = dmeng_get_look('file');
    foreach( $files['file'] as $key=>$file ){
      $title = str_replace(array('[',']'), '', $files['text'][$key]);
      $look['files'][$title] = $file;
    }
    $scripts_array['look'] = $look;
  }
  
  return $scripts_array;
}
add_filter( 'dmeng_scripts_args', 'dmeng_look_output' );

function dmeng_lazyload_avatar($avatar) {
  if ( !is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) )
    $avatar = preg_replace( "/src='(.+?)'/", "src='".dmeng_grey_png()."' data-original='\$1'", $avatar );
  return $avatar;
}
add_filter( 'get_avatar', 'dmeng_lazyload_avatar' );

function dmeng_auto_hide_comment_email( $content, $comment ) {
  if ( 
    (int)get_option('dmeng_hide_comment_email', 1) && 
    !current_user_can('moderate_comments') &&  
    !in_array(get_current_user_id(), array( get_post_field( 'post_author', $comment->comment_post_ID ), $comment->user_id ))
  ) {
    $content = preg_replace('#([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})#i', '<abbr title="'.__('只有评论/文章作者或更高权限的用户才可见','dmeng').'">'.__('邮箱已被自动隐藏','dmeng').'</abbr>', $content);
  }
  return $content;
}
add_filter( 'get_comment_text', 'dmeng_auto_hide_comment_email', 10, 2 );

function is_dmeng_ucenter() {
  global $dmeng_UCenter;
  return $dmeng_UCenter->is_ucenter_page();
}

function get_dmeng_ucenter_url( $path='' ) {
  global $dmeng_UCenter;
  return $dmeng_UCenter->get_ucenter_url( $path );
}

function dmeng_jquery_uri() {
  return apply_filters( 'dmeng_jquery_uri', get_template_directory_uri() . '/js/jquery-2.1.4.min.js' );
}

function dmeng_jsencrypt_uri() {
  return apply_filters( 'dmeng_jsencrypt_uri', get_template_directory_uri() . '/js/jsencrypt.min.js' );
}

function dmeng_number_format($number) {
  $prefixes = 'kMGTPEZY';
  if ($number < 100000)
    return number_format_i18n($number);
  
  for ($i=-1; $number>=1000; ++$i) {
      $number /= 1000;
  }
  return floor($number).$prefixes[$i].'+';
}

function dmeng_count_num($number) {
  return '<i itemprop="interactionCount" class="num">'.number_format_i18n($number).'</i>';
}

function dmeng_get_express_array(){
	return array( 
				1 => __('无须物流', 'dmeng'),
				2 => __('包邮', 'dmeng'),
				3 => __('邮费自付', 'dmeng')
			);
}

function dmeng_the_title( $post_id ) {
  if ( !is_numeric( $post_id ) )
    return '';
  global $post;
  $title = get_the_title( $post_id );
  return !empty($title) ? esc_html($title) : get_post_field( 'post_name', $post_id );
}

function dmeng_open_ctrl( $ctrl ) {
  return apply_filters( 'dmeng_open_'.$ctrl, true );
}

function dmeng_loading_tips( $wrapper=true ) {
  $text = __('加载中…', 'dmeng');
  return $wrapper ? '<p id="tips">'.$text.'</p>' : $text;
}

/**
 * 和自带的 get_term 不同的是，这个的 count 里包括了子分类的文章数
 * https://codex.wordpress.org/Function_Reference/get_term
 */
function dmeng_get_term($id, $taxonomy='category') {

  if ( !$id = (int) $id )
    return null;
    
  $_term = get_term_by('id', $id, $taxonomy);
  if ( is_wp_error( $_term ) )
    return false;

  $count = $_term->count;
  $terms = get_terms($taxonomy, array('child_of'=>$id));
  foreach ($terms as $term) {
    $count += $term->count;
  }

  $_term->count = $count;
  return $_term;
}

function dmeng_get_attachment_metadata($attachment_id) {

  $post = get_post($attachment_id);
	if ( empty( $post ) ) {
		return '';
	}

	$meta = wp_get_attachment_metadata( $post->ID );
  
  $fields = array();
  $labels = array(
    'file_title' => __('附件标题：', 'dmeng'),
    'file_url' => __('文件URL：', 'dmeng'),
    'file_name' => __('文件名：', 'dmeng'),
    'file_type' => __('文件类型：', 'dmeng'),
    'file_size' => __('文件大小：', 'dmeng'),
    'file_dims' => __('分辨率：', 'dmeng')
  );
  
  $fields['file_title'] = get_the_title( $post );
  
  $fields['file_url'] = '<a href="'.wp_get_attachment_url( $post->ID ).'" target="_blank"><span class="glyphicon glyphicon-link"></span> '.__('文件链接', 'dmeng').'</a>';
  $fields['file_name'] = esc_html( wp_basename( $post->guid ) );

  if ( preg_match( '/^.*?\.(\w+)$/', get_attached_file( $post->ID ), $matches ) ) {
    $fields['file_type'] = esc_html( strtoupper( $matches[1] ) );
    list( $mime_type ) = explode( '/', $post->post_mime_type );
    if ( $mime_type !== 'image' && ! empty( $meta['mime_type'] ) ) {
      if ( $meta['mime_type'] !== "$mime_type/" . strtolower( $matches[1] ) ) {
        $fields['file_type'] .= ' (' . $meta['mime_type'] . ')';
      }
    }
  } else {
    $fields['file_type'] = strtoupper( str_replace( 'image/', '', $post->post_mime_type ) );
  }

  $file  = get_attached_file( $post->ID );

  if ( isset( $meta['filesize'] ) )
    $fields['file_size'] = size_format($meta['filesize'], 2);
  elseif ( file_exists( $file ) )
    $fields['file_size'] = size_format(filesize( $file ), 2);

	if ( preg_match( '#^(audio|video)/#', $post->post_mime_type ) ) {

		$media_fields = apply_filters( 'media_submitbox_misc_sections', array(
			'length_formatted' => __( 'Length:' ),
			'bitrate'          => __( 'Bitrate:' ),
		) );

		foreach ( $media_fields as $key => $label ) {
			if ( empty( $meta[ $key ] ) ) {
				continue;
			}
      if ('bitrate'===$key) {
        $fields[$key] =  round( $meta['bitrate'] / 1000 ) . 'kb/s';
        if ( ! empty( $meta['bitrate_mode'] ) ) {
          $fields[$key] .=  ' ' . strtoupper( esc_html( $meta['bitrate_mode'] ) );
        }
      } else {
        $fields[$key] =  esc_html( $meta[ $key ] );
      }
      $labels[$key] = $label;
		}

		$audio_fields = apply_filters( 'audio_submitbox_misc_sections', array(
			'dataformat' => __( 'Audio Format:' ),
			'codec'      => __( 'Audio Codec:' )
		) );

		foreach ( $audio_fields as $key => $label ) {
			if ( empty( $meta['audio'][ $key ] ) ) {
				continue;
			}
      $fields[$key] = esc_html( $meta['audio'][$key] );
      $labels[$key] = $label;
		}

	}
  
	if ( isset( $meta['width'], $meta['height'] ) )
    $fields['file_dims'] = $meta['width'].'&times;'.$meta['height'];

  $fields = array_filter($fields);

  $output = '<ul class="attachment_metadata">';
  foreach ($fields as $key=>$field) {
    $output .= '<li class="misc-'.$key.'">'.$labels[$key].'<b>'.$field.'</b>'.'</li>';
  }
  $output .= '</ul>';

  return $output;
}