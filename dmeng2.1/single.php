<?php
get_header();
get_header( 'masthead' ); ?>
<div id="main" class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
  <div class="row">
      <?php 
        while ( have_posts() ) : the_post();
          get_template_part('content');
        endwhile;
      ?>
    <?php get_sidebar();?>
  </div>
 </div><!-- #main -->
<?php get_footer('colophon'); ?>
<?php get_footer(); ?>
