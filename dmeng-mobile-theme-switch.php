<?php

/*
Plugin Name: 多梦主题移动端切换
Description: 移动端访问自动切换主题及绑定移动端域名
Plugin URI: http://www.dmeng.net/
Author: 多梦
Author URI: http://duomeng.me/
Version: 1.0
Text Domain: dmeng
Domain Path: /lang
*/

if (false===function_exists('dmeng_is_mobile')) {
  function dmeng_is_mobile() {
    static $is_mobile;
    
    if ( isset($is_mobile) )
      return $is_mobile;

    if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
      $is_mobile = false;
    } elseif (
      strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
      || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
      || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
      || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
      || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false ) {
        $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') == false) {
      $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
      $is_mobile = false;
    } else {
      $is_mobile = false;
    }

    return $is_mobile;
  }
}

if (false===class_exists('dmeng_MobileTheme_Switch')) {
  
  class dmeng_MobileTheme_Switch {
    
    public $name = 'dmeng_MobileTheme_Switch';
    public $theme_field_name = 'dmeng_mobile_theme';
    public $home_field_name = 'dmeng_mobile_home';
    public $siteurl_field_name = 'dmeng_mobile_siteurl';

    public $home;
    public $siteurl;
    public $mobile_home;
    public $mobile_siteurl;
    public $mobile_theme;
    
    public $default_mobile_theme = 'dmeng2.1-mobile';
    
    public function __construct() {

      add_filter( 'dmeng_redirect_mobile', '__return_false' );
      add_action( 'template_redirect', array($this, 'home_redirect') );

      if (dmeng_is_mobile()) {
        
        $this->home = home_url();
        $this->siteurl = site_url();
        
        $this->mobile_home = get_option( $this->home_field_name );
        $this->mobile_siteurl = get_option( $this->siteurl_field_name );
        
        $this->mobile_theme = get_option( $this->theme_field_name, $this->default_mobile_theme );
        if ($this->mobile_theme) {
          add_filter( 'template', array($this, 'get_template') );
          add_filter( 'stylesheet', array($this, 'get_stylesheet') );
        }
        
        add_filter( 'pre_option_home', array($this, 'replace_url') );
        add_filter( 'pre_option_siteurl', array($this, 'replace_url') );
        
        if ( $this->mobile_siteurl && 'http://'!==$this->mobile_siteurl ) {
          add_filter('template_directory',       array($this, 'replace_uri'));
          add_filter('template_directory_uri',   array($this, 'replace_uri'));
          add_filter('stylesheet_directory',     array($this, 'replace_uri'));
          add_filter('stylesheet_directory_uri', array($this, 'replace_uri'));
        }
      }
      
      if (is_admin()) {
        add_filter('using_dmeng_theme_switch', '__return_true');
        add_action( 'admin_notices', array($this, 'admin_notice') );
      }
    }

    public function admin_notice() {
      if (defined('DMENG_VER'))
        return;

      deactivate_plugins( plugin_basename( __FILE__ ) );

      echo '<div class="error"><p>'.__('请注意：由于当前主题并非多梦主题，多梦主题移动端切换插件已被自动停用。', 'dmeng').'</p></div>';
    }

    public function get_theme() {
      return wp_get_theme($this->mobile_theme);
    }
    
    public function get_stylesheet($stylesheet) {
      $theme = $this->get_theme();
      $stylesheet = $theme['Stylesheet'];
      return $stylesheet;
    }

    public function get_template($template) {
      $theme = $this->get_theme();
      $template = $theme['Template'];
      return $template;
    }

    public function home_redirect() {
      $home = get_option('home');
      $parse = parse_url($home);
      if ($parse['host']==$_SERVER['HTTP_HOST'])
        return;

      header ('HTTP/1.1 301 Moved Permanently');
			header ('Location: '.rtrim($home, '/').'/'.ltrim((isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : ''), '/') );
      exit;
    }
    
    public function replace_url($url) {
      $filed_name = 'mobile_'.ltrim(current_filter(), 'pre_option_');
      $mobile_url = $this->{$filed_name};
      if ( $mobile_url && 'http://'!==$mobile_url )
        $url = $mobile_url;
      return $url;
    }
    
    public function replace_uri($path) {
      $path = str_replace($this->siteurl, $this->mobile_siteurl, $path);
      return $path;
    }

  }
  
  new dmeng_MobileTheme_Switch;
}