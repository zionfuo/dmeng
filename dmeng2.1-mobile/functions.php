<?php

/**
 * CSS/JS 代码版本号，和对应文件名中的版本号同步
 */

define( 'DMENG_MOBILE_CSS_VER', 150917 );
define( 'DMENG_MOBILE_SCRIPT_VER', 150917 );

/**
 * 当前主题代码版本
 * !注意! 不能改，修改会影响主题功能
 */

define( 'DMENG_MOBILE_VER', '2.1.d11' );

include_once get_stylesheet_directory() . '/inc/setup.class.php';

/**
 * 初始化
 */ 
$dmeng_Mobile = new dmeng_Mobile_Setup;
