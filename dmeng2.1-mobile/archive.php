<?php
get_header();
get_header( 'masthead' ); 

$classes = array();
if (false===wp_is_mobile())
  $classes[] = 'col-lg-8 col-md-8';

$classes[] = 'archive';
?>
<div id="main" class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
  <div class="row">
    <article id="content" class="<?php echo join(' ', $classes);?>" role="article" itemscope="" itemtype="http://schema.org/Article">
      <header class="entry-header">
        <div class="entry-location"><?php echo dmeng_breadcrumb_html();?></div>
        <h1 class="entry-title"><?php

          global  $wp_query;
          
          if ( $wp_query->is_author() ){
            $curauth = $wp_query->get_queried_object();
            printf( __( '用户：%s', 'dmeng' ), '<span class="vcard">' . $curauth->display_name . '</span>' );
          } else if ( $wp_query->is_search() ) {
            _e('搜索', 'dmeng');
            // printf( __( '搜索：%s', 'dmeng' ), get_search_query() );
          } else if ( $wp_query->is_home() ) {
            _e( '最新文章', 'dmeng' );
          } else {
            the_archive_title( '', '' );
          }

        ?></h1>
      <?php
        if ( $wp_query->is_search() ) {
      ?>
        <form class="input-group" role="search" method="get" id="archive-searchform" action="<?php echo home_url( '/' ); ?>">
          <input type="text" class="form-control" value="<?php echo esc_attr(get_search_query());?>" name="s" id="archive-s" required="">
          <span class="input-group-btn"><button type="submit" class="btn btn-default" id="archive-searchsubmit"><span class= "glyphicon glyphicon-search"></span></button></span>
        </form>
      <?php
        }
      ?>
        <div class="entry-meta">
          <?php
            the_archive_description( '', '' );
            printf( '共有 %1$s 个相关结果以及 %2$s 次浏览', dmeng_count_num($wp_query->found_posts), '<i data-num-views="true" class="num">'.number_format_i18n(get_dmeng_traffic()).'</i>' );
          ?>
        </div>
      </header>
      <div class="entry-content" itemprop="articleBody">
        <?php 
        if ( have_posts() ) {
          while ( have_posts() ) : the_post();
            get_template_part( 'content', 'archive' );
          endwhile;
        } else {

        }
        ?>
      </div>
      <footer class="entry-footer">
        <?php dmeng_paginate();?>
      </footer>
    </article>
    <?php get_sidebar();?>
  </div>
 </div><!-- #main -->
<?php get_footer('colophon'); ?>
<?php get_footer(); ?>
