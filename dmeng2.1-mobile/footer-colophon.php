<footer id="colophon" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
<?php

  $is_singular = is_singular() && false===is_front_page();

  if ($is_singular && has_nav_menu( 'mobile_menu' )) {
    wp_nav_menu( array(
      'menu'              => 'mobile_menu',
      'theme_location'    => 'mobile_menu',
      'depth'             => -1,
      'container'         => '',
      'container_class'   => '',
      'menu_id'        => 'mobile_menu',
      'menu_class'        => 'mobile_menu',
      'items_wrap'     => '<ul id="mobile_menu">%3$s</ul>',
      'walker'            => new dmeng_Bootstrap_Menu()
    )  );
  }
        
  $output = sprintf(
    '&copy; %s <a href="%s">%s</a> ',
    date( 'Y', current_time( 'timestamp', 0 ) ),
    home_url('/'),
    get_bloginfo('name')
  );
  
  $icp = get_option('zh_cn_l10n_icp_num', '');
  $output .=__('版权所有','dmeng').' '.( $icp ? '<br><a href="http://www.miitbeian.gov.cn/" rel="nofollow" target="_blank" class="text-muted">'.$icp.'</a>' : '' );

?>
  <div class="text-center"><?php echo $output;?></div>
<?php
if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
  ?><div id="debug-notice" class="num text-center small" style="color:#999;margin:0 15px">执行结果：共 <?php echo get_num_queries(); ?> 次查询，花费了 <?php timer_stop(3); ?> 秒 </div><?php
}
?>
  <div id="float-nav-wrapper">
    <div id="float-nav" class="clearfix" style="display:none">
      <ul id="quick-ucenter">
        <li id="ucenterAvatar">
          <a id="ucenterHome"></a>
        </li>
      </ul>
       <ul class="quick-btn-group">
       <?php if ($is_singular) { ?>
         <li id="goComments"><a href="javascript:;"><?php _e('评论', 'dmeng');?> <span class="num" data-quick-comments="true"></span></a></li>
         <li id="goVote"><a href="javascript:;" class="up"><?php _e('赞', 'dmeng');?> <span class="num" data-quick-like="true"></span></a></li>
         <li id="goTop"><a href="javascript:;"><?php _e('阅读', 'dmeng');?> <span class="num" data-quick-views="true"></span></a></li>
       </ul>
      <?php } else {
        if ( has_nav_menu( 'mobile_menu' ) ) {
          wp_nav_menu( array(
            'menu'              => 'mobile_menu',
            'theme_location'    => 'mobile_menu',
            'depth'             => -1,
            'container'         => '',
            'container_class'   => '',
            'menu_id'        => 'mobile_menu',
            'menu_class'        => 'mobile_menu',
            'items_wrap'     => '%3$s',
            'walker'            => new dmeng_Bootstrap_Menu()
          )  );
        }
      } ?>
         
    </div>
  </div>
</footer>