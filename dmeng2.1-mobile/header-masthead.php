<?php
/**
 *
 */
?>
<header id="masthead" itemscope itemtype="http://schema.org/WPHeader">
  <nav id="navbar" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
    <a href="javascript:;" class="prev-link" id="history-back" style="display:none"><span class="glyphicon glyphicon-menu-left"></span><?php _e('返回', 'dmeng');?></a>
    <a href="javascript:;" class="next-link" id="nav-tax-toggle"><span class="glyphicon glyphicon-equalizer"></span></a>
    <a href="<?php echo home_url('/');?>"><?php bloginfo('name');?></a>
  </nav><!-- #navbar -->
  <div id="nav-tax">
    <div class="cover">
<?php

$related_link = '';

if (is_singular()) {

  $post = get_post();
  $post_type = $post->post_type;

  if ($post_type && $post_type_link = get_post_type_archive_link($post_type)) {
    $post_type_object = get_post_type_object($post_type);
    if (!empty($post_type_object))
      $related_link .= '<a href="'.$post_type_link.'">'.$post_type_object->labels->name.'</a>';
  }

  $taxonomies = get_object_taxonomies($post_type, 'objects');
  foreach ($taxonomies as $tax_name=>$tax_object) {
    $terms = get_the_terms( $post->ID, $tax_name );
    if ( !empty( $terms ) ) {
      foreach ( $terms as $term ) {
        $term_link = get_term_link($term->term_id, $tax_name);
        if ($term_link)
          $related_link .= '<a href="'.$term_link.'">'.$term->name.'</a>';
      }
    }
  }

} else if (is_category() || is_tag() || is_tax()) {

  global $wp_query;
  $term = $wp_query->queried_object;
  $related_link = '<a href="'.get_term_link($term->term_id, $term->taxonomy).'">'.$term->name.'</a>';

}

if ($related_link)
  echo '<div class="tax-term"><h3>'.__('本页关联', 'dmeng').'</h3>'.$related_link.'</div>';

/**
 * 所有公开分类法都读取其中最多文章的20个分类
 */

$taxonomies = get_taxonomies(array(
  'public'   => true
), 'objects');

foreach ( $taxonomies as $tax_name=>$tax_object ) {

  $terms = get_terms( $tax_name, array(
    'orderby'    => 'count',
    'order'    => 'DESC',
    'fields' => 'id=>name',
    'hide_empty' => true,
    'number' => 20
  ) );

  if (!empty($terms)) {
    $terms_link = '';
    foreach ($terms as $term_id=>$term_name) {
      $term_link = get_term_link($term_id, $tax_name);
      if ($term_link)
        $terms_link .= '<a href="'.$term_link.'">'.$term_name.'</a>';
    }
    if (!empty($terms_link)) {
      echo '<div class="tax-term"><h3>'.sprintf(__('按%s浏览', 'dmeng'), $tax_object->labels->singular_name).'</h3>'.$terms_link.'</div>';
    }
  }

}

/**
 * 搜索框
 */

echo '<h3>'.__('搜索', 'dmeng').'</h3>';
get_search_form();

?>
    </div>
  </div>
</header>