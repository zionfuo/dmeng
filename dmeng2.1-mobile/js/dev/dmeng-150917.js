/*! 
 * dmeng mobile (http://www.dmeng.net) 
 * 9:49 2015/9/17
 * @author steven.chan.chihyu(http://duomeng.me)
 */
!function ($) {
  'use strict';

  var nav_tax_id = '#nav-tax',
  tax_toggle_id = '#nav-tax-toggle',
  nav_tax_toggle = function(){
    var $tax = $(nav_tax_id);
    if ($tax.is(':hidden')) {
      $(tax_toggle_id).addClass('active');
      $('#main, #colophon').hide();
      $tax.fadeIn();
    } else {
      $('#main, #colophon').show();
      $(tax_toggle_id).removeClass('active');
      $tax.hide();
    }
  };

  $(document).ready(function () {
    if (document.referrer) {
      $('#history-back').fadeIn();
    }
    dmeng.readyAjax.done(function (data) {

        $("[data-quick-views='true']").html(dmeng.numberFormat(data.tracker));

        if (data.post_vote) {
          if (data.post_vote.up) {
            $("[data-quick-like='true']").html(dmeng.numberFormat(data.post_vote.up));
            if (data.post_vote.active) {
              $('#quick-like').addClass('active');
            }
          }
        }

        if (data.comments) {
          $("[data-quick-comments='true']").html(dmeng.numberFormat(data.comments));
        }
        
      $(dmeng.ele.float_nav_id).fadeIn();
    });
  }).on('click', '#history-back', function(){
    if ($(nav_tax_id).is(':hidden')) {
      history.back();
    } else {
      nav_tax_toggle();
    }
  }).on('click', '#nav-tax-toggle', nav_tax_toggle);

  $(window).scroll(function () {
    dmeng.commentsLoad();
  });
}(jQuery);