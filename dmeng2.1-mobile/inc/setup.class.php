<?php

/**
 * 移动端主题初始化 
 */

class dmeng_Mobile_Setup {

  public function __construct() {
    add_filter( 'script_loader_src', array( $this, 'replace_dmeng_script_src' ), 10, 2 );
    add_filter( 'style_loader_src', array( $this, 'replace_dmeng_style_src' ), 10, 2 );
    
    add_filter( 'option_thread_comments_depth', array( $this, 'max_thread_comments_depth' ) );
    
    add_action( 'wp_print_styles', array( $this, 'ucenter_styles' ), 999 );
  }

  /**
   * 替换多梦主题脚本
   */
  public function replace_dmeng_script_src( $src, $handle ) {
    if ('dmeng'==$handle) {
      $src = get_stylesheet_directory_uri() . '/js/dev/dmeng-'.DMENG_SCRIPT_VER.'.js';
    } else if ('dmeng-min'==$handle) {
      $src = get_stylesheet_directory_uri() . '/js/dmeng-'.DMENG_SCRIPT_VER.'.min.js';
    }
    return $src;
  }

  public function replace_dmeng_style_src( $src, $handle ) {
    if ( 'dmeng'==$handle ) {
      $src = get_stylesheet_directory_uri() . '/css/dmeng-'.DMENG_CSS_VER.'.css';
    }
    return $src;
  }
  
  public function max_thread_comments_depth( $depth ) {
    if ( 3<(int)$depth ) {
      $depth = 3;
    }
    return $depth;
  }

  public function ucenter_styles() {
    if (false===is_dmeng_ucenter())
      return;
?>
<style>
.ucenter_page #content{margin:15px}
.ucenter_page #content .ucenter_avatar{float:none;display:block;margin:30px auto;}
#ucenter_menu{display:block}
.ucenter_page #content .ucenter_list li{padding:15px 5px}
</style>
<?php
  }

}