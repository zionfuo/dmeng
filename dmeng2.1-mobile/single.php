<?php 
get_header();
get_header('masthead');
?>
<div id="main" class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
      <?php 
        while ( have_posts() ) : the_post();
          ?>
  <div class="row">
<article id="content" <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/Article">
  <header class="entry-header">
    <div class="entry-location"><?php echo dmeng_breadcrumb_html();?></div>
    <h1 class="entry-title" itemprop="name"><?php echo apply_filters( 'dmeng_the_title', esc_html(get_the_title()) );?></h1>
    <?php dmeng_post_meta(); ?>
  </header>
  <div class="entry-content"  itemprop="articleBody" >
    <?php the_content();?>
    <?php dmeng_post_page_nav(); ?>
  </div>
  <footer class="entry-footer">
    <?php dmeng_post_copyright(get_the_ID());?>
    <?php dmeng_vote_html( get_the_ID(), 'post');?>
  </footer>
  <div id="comments"><?php comments_template(); ?></div>
</article>
          <?php
        endwhile;
      ?>
      </div>
 </div><!-- #main -->
<?php 
get_footer('colophon');
get_footer();