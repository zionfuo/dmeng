<?php 
get_header();
get_header('masthead');
?>
<div id="main" class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
  <div class="row">
    <?php 
      while ( have_posts() ) : the_post();
      
        $buyers = (array)wp_unslash(json_decode(get_post_meta( get_the_ID(), 'dmeng_gift_buyers', true )));
        $gift_info = (array)wp_unslash(json_decode(get_post_meta( get_the_ID(), 'dmeng_gift_info', true )));
        
        global $dmeng_Gift;
        $gift_info = $dmeng_Gift->info_base_filter($gift_info);

        $info_list = array(
          'credit' => array(
            'title' => __( '所需积分', 'dmeng' ),
            'value' => $gift_info['credit']
          ),
          'price' => array(
            'title' => __( '市场价格', 'dmeng' ),
            'value' => $gift_info['price']
          ),
          'stock' => array(
            'title' => __( '库存数量', 'dmeng' ),
            'value' => $gift_info['stock']
          ),
          'buyers' => array(
            'title' => __( '兑换人次', 'dmeng' ),
            'value' => count($buyers)
          ),
          'express' => array(
            'title' => __( '物流配送', 'dmeng' ),
            'value' => $gift_info['express']
          ),
          'tips' => array(
            'title' => __( '温馨提示', 'dmeng' ),
            'value' => get_option( 'dmeng_gift_tips' )
          )
        );
        
    ?>
    <article id="content" <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/Article">
      <header class="entry-header">
        <div class="entry-location"><?php echo dmeng_breadcrumb_html();?></div>
        <h1 class="entry-title" itemprop="name"><?php echo apply_filters( 'dmeng_the_title', esc_html(get_the_title()) );?></h1>
        <?php dmeng_post_meta(); ?>
      </header>
      <div class="entry-content"  itemprop="articleBody" >
        <?php the_content();?>
        <?php dmeng_post_page_nav(); ?>
      </div>
      <footer class="entry-footer">
        <?php dmeng_post_copyright(get_the_ID());?>
        <?php dmeng_vote_html( get_the_ID(), 'post');?>
      </footer>
      <div id="gift-content-box">
        <h2><?php _e('兑换须知', 'dmeng');?></h2>
        <div><?php echo get_option('dmeng_gift_notice');?></div>
        <div id="gift-content" style="display:none">
          <h2><?php _e('礼品内容', 'dmeng');?></h2>
          <div class="content"><!-- 礼品内容将显示在这里 --></div>
        </div>
        <div id="gift-attachment" style="display:none">
          <h2><?php _e('附件下载', 'dmeng');?></h2>
          <div class="content"><!-- 附件将显示在这里 --></div>
        </div>
      </div>
      
      
  <article class="single-post single-post-gift" role="article" itemscope itemtype="http://schema.org/Article">
    <h3 class="entry-title"><?php _e( '礼品信息', 'dmeng' ); ?></h3>
    <div class="single-gift">
      <a href="#gift-content" id="show-gift-action" class="show-gift" style="display:none" title="<?php _e( '查看礼品内容', 'dmeng' );?>"><?php _e( '可查看隐藏内容', 'dmeng' );?></a>
    <header class="entry-header">

      <div class="entry-meta">
        <div id="exchange-action">
          <p id="exchange-tips"><?php printf( __( '可兑换%1$s次，你已兑换%2$s次', 'dmeng' ), ( $gift_info['max'] ? '<i class="num">'.$gift_info['max'].'</i>' : __( '无数', 'dmeng' ) ), '<span id="exchange_num" class="num">0</span>' );?></p>
          <a href="javascript:;" class="btn btn-default btn-block disabled" id="exchange" data-loading-text="<?php _e('兑换中，请稍候…', 'dmeng'); ?>"><?php _e('加载中…', 'dmeng'); ?></a>
          <div class="collapse" id="exchange-confirm">
            <div class="well">
                <p class="text-danger"><?php _e( '将产生积分消费，请确认', 'dmeng' ); ?></p>
                <div class="btn-group btn-group-sm btn-group-justified" role="group" aria-label="Justified button group">
                  <a href="javascript:;" class="btn btn-default" id="exchange-cancel"><?php _e( '取消', 'dmeng' ); ?></a>
                  <a href="javascript:;" class="btn btn-danger" id="exchange-submit"><?php _e( '确认', 'dmeng' ); ?></a>
                </div>
            </div>
          </div>
        </div>
        <?php
          $info_output = '<ul id="gift-info">';
          foreach ( $info_list as $info_key=>$info ) {
            $info_output .= '<li class="'.$info_key.'"><span>'.$info['title'].'</span><em>'.$info['value'].'</em></li>';
          }
          $info_output .= '</ul>';
          echo $info_output;
          ?>

<script>
!function($){
  'use strict';
  var $action = $('#exchange-action'),
          $tips = $('#exchange-tips'),
          $confirm = $('#exchange-confirm'),
          $info = $('#gift-info'),
          $exchange_num = $('#exchange_num'),
          $exchange = $('#exchange'),
          $gift_content = $('#gift-content'),
          $gift_attachment = $('#gift-attachment');
  dmeng.giftinit = function (gift) {
    $.each(gift.info, function (key, value) {
      $info.find('.'+key+' em').html(value);
    });
    if ( gift.info.content ) {
      $('#show-gift-action').show();
      $gift_content.show().children('.content').html(gift.info.content);
    }
    if ( gift.info.attachment ) {
      $('#show-gift-action').show();
      $gift_attachment.show().children('.content').html(gift.info.attachment);
    }
    if (gift.exchange.new) {
      $exchange.removeClass('disabled btn-default').addClass('btn-success');
      if (parseInt(gift.info.credit)>0) {
        $exchange.attr({ 'data-toggle':'collapse', 'href':'#exchange-confirm', 'aria-expanded':'false', 'aria-controls':'exchange-confirm' });
      } else {
        $exchange.addClass('exchange-submit');
      }
    }
    $exchange.html(gift.exchange.title);
    $exchange_num.html(gift.exchange.num);
  };
  dmeng.exchange = function () {
    if ( !dmeng.user.ID ) {
      window.location.href = dmeng.loginurl();
      return false;
    }
    $exchange.button('loading');
    $.ajax({
      type: 'POST',
      url: dmeng.ajaxurl,
      data: { action: 'dmeng_exchange', post_id: dmeng.post_id, _wpnonce: dmeng._wpnonce },
      complete: function (e) {
        if ( !e.responseJSON ) {
          $tips.html(e.responseText).addClass('text-danger');
        } else {
          e = e.responseJSON;
          dmeng.giftinit(e.gift);
          if ( e.success ) {
            $exchange.html(e.success);
          } else if ( e.error ) {
            $exchange.html(e.error).removeClass('btn-success').addClass('btn-default disabled').prop('disabled', true);
            return;
          }
        }
        $exchange.button('reset');
      }
    });
    return false;
  }
  $(document).ready(function () {
    dmeng.readyAjax.complete(function (data) {
      data = data.responseJSON;
      if ( !data.gift ) {
        return;
      }
      dmeng.giftinit(data.gift);
    });
  }).on('click', '#exchange', function() {
    if ( $(this).hasClass('exchange-submit') ) {
      dmeng.exchange();
    }
  }).on('click', '#exchange-cancel', function() {
    $confirm.collapse('hide');
  }).on('click', '#exchange-submit', function() {
    $confirm.collapse('hide');
    dmeng.exchange();
  });
}(jQuery);
</script>

      </div>
    </header>
    </div>
  </article>
      
      
      <div id="comments"><?php comments_template(); ?></div>
    </article>
          <?php
        endwhile;
      ?>
  </div>
</div><!-- #main -->
<?php 
get_footer('colophon');
get_footer();