<?php

$title = esc_html(get_the_title());
$excerpt = apply_filters( 'the_excerpt', get_the_excerpt() );

if (is_search()) {
  $keyword = get_search_query();
  $title = dmeng_highlight_keyword($keyword, $title);
  $excerpt = dmeng_highlight_keyword($keyword, $excerpt);
}
?>
<article <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/Article">
  <header class="entry-header">
    <h3 class="entry-title" itemprop="name"><?php 

      echo apply_filters( 'dmeng_the_title', '<a href="'.get_permalink().'" rel="bookmark" itemprop="url"><span itemprop="name">'.$title.'</span></a>' );
      
    ?></h3>
    <?php dmeng_post_meta(); ?>
  </header>
  <?php echo dmeng_thumbnail(); ?>
  <div class="entry-excerpt" itemprop="articleBody"><a href="<?php the_permalink();?>"><?php echo strip_tags($excerpt, '<em>');?></a></div>
</article>
